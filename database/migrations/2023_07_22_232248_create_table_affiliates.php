<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('affiliates', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("username")->unique();
            $table->string("code",7)->unique();
            $table->string("picture");
            $table->string("cover");
            $table->string("otp_code")->nullable();
            $table->uuid("user_id");
            $table->float("default_commission")->default(5);
            $table->text("address");
            $table->enum("status", ["PENDING", "ACTIVE", "BLOCKED"])->default("PENDING");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('affiliates');
    }
};
