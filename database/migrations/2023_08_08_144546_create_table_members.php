<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('members', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("code",7)->unique();
            $table->string("otp_code")->nullable();
            $table->uuid("user_id");
            $table->uuid("member_type_id")->nullable();
            $table->enum("status", ["PENDING", "ACTIVE", "BLOCKED"])->default("PENDING");
            $table->boolean("have_purchased")->default(false);
            $table->timestamps();
            $table->uuid("approved_by")->nullable();
            $table->dateTime("approved_at")->nullable();
            $table->uuid("blocked_by")->nullable();
            $table->dateTime("blocked_at")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('members');
    }
};
