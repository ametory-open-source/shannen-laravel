<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->text("message");
            $table->json("options");
            $table->enum("type", ["CHAT", "PHOTO", "MEDIA", "URL", "PRODUCT", "ORDER", "TRACKING"])->default("CHAT");
            $table->string("attachment")->nullable()->default(null);
            $table->uuid('message_room_id');
            $table->uuid('user_id')->nullable();
            $table->uuid('admin_id')->nullable();
            $table->dateTime("readed_at")->nullable();
            $table->dateTime("readed_by_admin_at")->nullable();
            $table->uuid("readed_by_admin")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('messages');
    }
};
