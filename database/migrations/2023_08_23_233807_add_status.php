<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string("remarks");
            $table->string("tracking_number");
            $table->string("payment_receipt");
            $table->enum("status", ["UNPAID", "REJECTED_BY_ADMIN", "CANCELED_BY_USER", "PENDING", "PROCESSING", "REQUEST_PICKUP", "PICKING_UP","DROPPING_OFF", "SHIPPING", "SHIPPED", "FINISHED"])->default("UNPAID")->default("UNPAID");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                "remarks",
                "tracking_number",
                "payment_receipt",
                "status"
            ]);
        });
    }
};
