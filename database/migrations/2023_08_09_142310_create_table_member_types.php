<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('member_types', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("name")->unique();
            $table->text("description");
            $table->integer("first_minimum_order")->unsigned()->default(0);
            $table->integer("minimum_order")->unsigned()->default(0);
            $table->double("discount")->unsigned()->default(0);
            $table->double("below_minimum_order_discount")->unsigned()->default(0);
            $table->boolean("flag")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('member_types');
    }
};
