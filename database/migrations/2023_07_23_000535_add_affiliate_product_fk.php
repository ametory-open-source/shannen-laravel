<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('affiliate_product', function (Blueprint $table) {
            $table->foreign('affiliate_id')->references('id')->on('affiliates');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('created_by')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('affiliate_product', function (Blueprint $table) {
            $table->dropForeign(["affiliate_id"]);
            $table->dropForeign(["product_id"]);
            $table->dropForeign(["created_by"]);
        });
    }
};
