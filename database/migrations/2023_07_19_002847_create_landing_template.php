<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("top_sale_title")->nullable();
            $table->text("top_sale_subtitle");
            $table->string("blog_title")->nullable();
            $table->text("blog_subtitle");
            $table->string("join_title")->nullable();
            $table->text("join_subtitle");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "top_sale_title",
                "top_sale_subtitle",
                "blog_title",
                "blog_subtitle",
                "join_title",
                "join_subtitle",
            ]);
        });
    }
};
