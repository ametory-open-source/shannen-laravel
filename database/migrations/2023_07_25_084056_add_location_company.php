<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->integer("company_province_id")->unsigned()->nullable()->default(null);
            $table->integer("company_city_id")->unsigned()->nullable()->default(null);
            $table->integer("company_subdistrict_id")->unsigned()->nullable()->default(null);
            $table->boolean("affiliate_enabled")->default(false);
            $table->boolean("member_enabled")->default(false);
            $table->longText("member_term");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn("company_province_id");
            $table->dropColumn("company_city_id");
            $table->dropColumn("company_subdistrict_id");
            $table->dropColumn("affiliate_enabled");
            $table->dropColumn("member_enabled");
            $table->dropColumn("member_term");
        });
    }
};
