<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("blogs_banner")->nullable();
            $table->string("blogs_banner_title")->nullable();
            $table->text("blogs_banner_subtitle");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "blogs_banner",
                "blogs_banner_title",
                "blogs_banner_subtitle",
            ]);
        });
    }
};
