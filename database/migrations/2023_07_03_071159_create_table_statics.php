<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('statics', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("name");
            $table->string("description");
            $table->longText("content");
            $table->longText("scripts");
            $table->longText("styles");
            $table->boolean("flag")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('statics');
    }
};
