<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->enum("status", ["UNPAID", "REJECTED_BY_ADMIN", "CANCELED_BY_USER", "PENDING", "WAITING_FOR_ADMIN" , "PROCESSING", "REQUEST_PICKUP", "PICKING_UP","DROPPING_OFF", "SHIPPING",  "ORDER_ACCEPTED" ,"SHIPPED", "FINISHED"])->default("UNPAID")->default("UNPAID")->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
};
