<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("contact_picture")->nullable();
            $table->string("contact_title")->nullable();
            $table->text("contact_subtitle");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn("contact_picture");
            $table->dropColumn("contact_title");
            $table->dropColumn("contact_subtitle");
        });
    }
};
