<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->integer("province_id")->unsigned()->nullable()->default(null);
            $table->integer("city_id")->unsigned()->nullable()->default(null);
            $table->integer("subdistrict_id")->unsigned()->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn("province_id");
            $table->dropColumn("city_id");
            $table->dropColumn("subdistrict_id");
        });
    }
};
