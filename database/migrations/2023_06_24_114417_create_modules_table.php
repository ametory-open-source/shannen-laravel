<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("icon")->nullable();
            $table->string("title")->nullable();
            $table->string("subtitle")->nullable();
            $table->string("price")->nullable();
            $table->text("description");
            $table->string("picture")->nullable();
            $table->string("type")->nullable();
            $table->string("link")->nullable();
            $table->boolean("flag")->default(true);
            $table->integer("sort")->unsigned()->default(0);
            $table->string("category")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('modules');
    }
};
