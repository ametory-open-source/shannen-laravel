<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('affiliate_product', function (Blueprint $table) {
            $table->uuid("affiliate_id");
            $table->uuid("product_id");
            $table->float("commission")->default(5);
            $table->uuid("created_by");
            $table->dateTime("created_at");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('affiliate_product');
    }
};
