<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("faq_picture")->nullable();
            $table->string("faq_title")->nullable();
            $table->text("faq_subtitle");

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn("faq_picture");
            $table->dropColumn("faq_title");
            $table->dropColumn("faq_subtitle");
        });
    }
};
