<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("name");
            $table->string("slug")->unique();
            $table->uuid("product_category_id")->nullable();
            $table->float("price")->nullable()->default(null);
            $table->float("strike_price")->nullable()->default(null);
            $table->string("custom_promo_tag")->nullable()->default(null);
            $table->string("custom_promo_tag_color")->nullable()->default("#ff6565");
            $table->enum("promo_tag", ["new","sale", "discount"])->nullable()->default(null);
            $table->text("description");
            $table->string("keywords");
            $table->string("tags");
            $table->text("short_description");
            $table->float("weight")->nullable()->default(null);
            $table->enum("weight_unit", ["g","kg"])->nullable()->default("g");
            $table->float("dimension_width")->nullable()->default(null);
            $table->float("dimension_height")->nullable()->default(null);
            $table->float("dimension_length")->nullable()->default(null);
            $table->enum("dimension_unit", ["cm","m"])->nullable()->default("m");
            $table->text("other_info");
            $table->text("materials");
            $table->boolean("review_enabled")->default(true);
            $table->boolean("review_moderation")->default(true);
            $table->enum("status", ["DRAFT", "PUBLISHED", "UNPUBLISH"])->default("DRAFT");
            $table->uuid("created_by")->nullable();
            $table->uuid("updated_by")->nullable();
            $table->boolean("is_top_sale")->default(false);
            $table->boolean("is_feature_product")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
