<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("name");
            $table->string("avatar")->nullable();
            $table->string("email");
            $table->text("comment");
            $table->uuid("parent_id")->nullable();
            $table->uuid("blog_id");
            $table->enum("status", ["DRAFT", "PUBLISHED", "UNPUBLISH"])->default("DRAFT");
            $table->timestamps();
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('blog_id')->references('id')->on('blogs');
            $table->foreign('parent_id')->references('id')->on('comments');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
};
