<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->boolean("show_header")->default(true);
            $table->boolean("show_feature_banner")->default(true);
            $table->boolean("show_feature_category")->default(true);
            $table->boolean("show_top_sale")->default(true);
            $table->boolean("show_blog")->default(true);
            $table->boolean("show_join")->default(true);
            $table->boolean("show_cart")->default(true);
            $table->boolean("show_user")->default(true);
            $table->boolean("show_footer")->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "show_header",
                "show_feature_banner",
                "show_feature_category",
                "show_top_sale",
                "show_blog",
                "show_join",
                "show_cart",
                "show_user",
                "show_footer",
            ]);
        });
    }
};
