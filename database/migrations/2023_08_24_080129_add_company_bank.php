<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->string("company_bank")->nullable();
            $table->string("company_bank_account_number")->nullable();
            $table->string("company_bank_account_holder")->nullable();
            $table->string("company_bank_account_branch")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "company_bank",
                "company_bank_account_number",
                "company_bank_account_holder",
                "company_bank_account_branch",
            ]);
        });
    }
};
