<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('members', function (Blueprint $table) {
            $table->integer("province_id")->unsigned()->nullable()->default(null);
            $table->integer("city_id")->unsigned()->nullable()->default(null);
            $table->integer("subdistrict_id")->unsigned()->nullable()->default(null);
            $table->integer("village_id")->unsigned()->nullable()->default(null);
            $table->string("postal_code")->nullable();
            $table->string("id_card")->nullable();
            $table->text("address");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn("province_id");
            $table->dropColumn("city_id");
            $table->dropColumn("subdistrict_id");
            $table->dropColumn("village_id");
            $table->dropColumn("postal_code");
            $table->dropColumn("id_card");
            $table->dropColumn("address");
        });
    }
};
