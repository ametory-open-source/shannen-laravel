<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("title");
            $table->string("slug");
            $table->longtext("description");
            $table->string("feature_image")->nullable();
            $table->string("feature_image_caption")->nullable();
            $table->string("keywords");
            $table->string("tags");
            $table->text("short_description");
            $table->uuid("blog_category_id")->nullable();
            $table->enum("status", ["DRAFT", "PUBLISHED", "UNPUBLISH"])->default("DRAFT");
            $table->boolean("comment_enabled")->default(true);
            $table->boolean("comment_moderation")->default(true);
            $table->string("author")->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
};
