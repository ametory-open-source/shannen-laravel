<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('webs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("name")->default("");
            $table->string("short_description")->default("");
            $table->longtext("meta");
            $table->longtext("styles");
            $table->longtext("scripts");
            $table->string("keywords")->default("");
            $table->longText("header_menu");
            $table->string("logo")->default("");
            $table->string("loading")->default("");
            $table->string("hero_picture")->default("");
            $table->string("video_picture")->default("");
            $table->string("question_picture")->default("");
            $table->string("subscribe_picture")->default("");
            $table->string("copyright")->default("");
            $table->boolean("show_feature")->default(true);
            $table->string("company_name")->nullable()->default(null);
            $table->string("company_phone")->nullable()->default(null);
            $table->string("company_email")->nullable()->default(null);
            $table->string("company_web")->nullable()->default(null);
            $table->string("company_coordinate")->nullable()->default(null);
            $table->text("company_address")->nullable()->default(null);
            $table->text("company_map")->nullable()->default(null);
            $table->longText("description");
            $table->boolean("is_online")->default(true);
            $table->longText("offline_template");
            $table->longText("footer_left_link");
            $table->longText("footer_center_link");
            $table->longText("footer_right_link");
            $table->string("facebook_link");
            $table->string("instagram_link");
            $table->string("twitter_link");
            $table->string("youtube_link");
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('webs');
    }
};
