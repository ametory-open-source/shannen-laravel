<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->string("name");
            $table->string("slug")->unique('column');
            $table->text("description");
            $table->integer("sort")->default(0);
            $table->string("picture")->nullable()->default(null);
            $table->string("color")->default("#FFFFFF");
            $table->string("custom_promo_tag")->nullable()->default(null);
            $table->string("custom_promo_tag_color")->nullable()->default("#ff6565");
            $table->enum("promo_tag", ["new","sale", "discount"])->nullable()->default(null);
            $table->boolean("flag")->default(true);
            $table->boolean("is_featured")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_categories');
    }
};
