<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->boolean("show_main_banner")->default(true);
            $table->boolean("show_feature_product")->default(true);
            $table->text("side_menu");
            $table->string("logo_icon")->nullable()->default(null);
            $table->string("logo_alt")->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('webs', function (Blueprint $table) {
            $table->dropColumn([
                "show_main_banner",
                "show_feature_product",
                "side_menu",
                "logo_icon",
                "logo_alt",
            ]);
        });
    }
};
