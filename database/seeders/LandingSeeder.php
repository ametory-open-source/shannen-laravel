<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LandingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('webs')->insert([
            'id' => Str::orderedUuid(),
            'name' => env("APP_NAME"),
            'meta' => "",
            'styles' => "",
            'scripts' => "",
            'header_menu' => "",
            'description' => "",
            'offline_template' => "",
            'footer_left_link' => "",
            'footer_center_link' => "",
            'footer_right_link' => "",
            'facebook_link' => "",
            'instagram_link' => "",
            'twitter_link' => "",
            'youtube_link' => "",
            'top_sale_subtitle' => "",
            'blog_subtitle' => "",
            'join_subtitle' => "",
        ]);
    }
}
