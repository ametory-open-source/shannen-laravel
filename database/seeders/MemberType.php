<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\MemberType as MemberTypeModel;

class MemberType extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MemberTypeModel::create([
            "name" => "AFFILIATOR",
            "description" => "AFFILIATOR",
            "first_minimum_order" => 0,
            "minimum_order" => 0,
            "discount" => 10,
            "below_minimum_order_discount" => 10,
        ]);
        MemberTypeModel::create([
            "name" => "AGENT",
            "description" => "AGENT",
            "first_minimum_order" => 135000000,
            "minimum_order" => 10000000,
            "discount" => 25,
            "below_minimum_order_discount" => 0,
        ]);
        MemberTypeModel::create([
            "name" => "DISTRIBUTOR",
            "description" => "DISTRIBUTOR",
            "first_minimum_order" => 135000000,
            "minimum_order" => 25000000,
            "discount" => 25,
            "below_minimum_order_discount" => 0,
        ]);
    }
}
