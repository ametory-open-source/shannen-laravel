<?php

namespace App\Providers;
use App\Models\Web;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Order;
use App\Models\Member;
use App\Models\ContactUs;
use App\Models\Invitation;
use App\Observers\InvitationObserver;
use App\Observers\RegistrationObserver;
use App\Observers\OrderObserver;
use Illuminate\Support\Facades\Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $web = Web::first();
        View::composer('errors::404', function ($view) use($web) {

            $view->with('web', $web);
            $view->with('header_transparent', false);
            $view->with('top_sales', []);
            $view->with('feature_products', []);
        });
        View::share('web', $web);
        View::share('header_transparent', false);
        View::share('top_sales', []);
        View::share('feature_products', []);

        view()->composer('*', function ($view)
        {
            $view->with('frontendUser', false);
            $contact_us_unread = ContactUs::whereNull("readed_at")->count();

            if ($contact_us_unread > 99) {
                $contact_us_unread = "99+";
            }
            $count_pending = Member::where("status", "PENDING")->count();
            if (auth(env('APP_AFFILIATE_GUARD'))->check()) {
                $view->with('frontendUser', true);
            }
            if (auth()->check()) {
                if ($count_pending > 99) {
                    $count_pending = "99+";
                }
                $view->with('member_pending', $count_pending);
            }
            $order_count_pending = Order::where("status", "PENDING")->orWhere("status", "UNPAID")->count();
            if (auth()->check()) {
                if ($order_count_pending > 99) {
                    $order_count_pending = "99+";
                }
                $view->with('order_count_pending', $order_count_pending);
                $view->with('contact_us_unread', $contact_us_unread);
            }
        });




        // Invitation::observe(InvitationObserver::class);
        // User::observe(RegistrationObserver::class);
        Order::observe(OrderObserver::class);
        User::observe(RegistrationObserver::class);

    }
}
