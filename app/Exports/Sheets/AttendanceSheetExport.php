<?php
namespace App\Exports\Sheets;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;



class AttendanceSheetExport implements FromView, WithTitle
{
    private $employee;

    public function __construct($employee)
    {
        $this->employee = $employee;
    }

    public function view(): View
    {
        return view('exports.attendances', [
            'data' => $this->employee
        ]);
    }

    public function title(): string
    {
        return  $this->employee['name'];
    }
}
