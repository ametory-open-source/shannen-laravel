<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\AttendanceSheetExport;

class AttendanceReportExport implements WithMultipleSheets
{
    use Exportable;

    protected $data;
    
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->data as $key => $employee) {
            $sheets[] = new AttendanceSheetExport($employee);
        }
      
        return $sheets;
    }
}