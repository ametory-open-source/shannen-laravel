<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class RajaOngkirService implements Ongkir
{
    private $client;
    private $headers;

    public function __construct($headers = []) {
        $this->headers = [
            'key' => env('API_KEY_RAJA_ONGKIR'),
            'Content-Type' => 'application/json',
        ];

        $this->client = Http::withHeaders( $this->headers );
    }

    public function setHeaders($headers)
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }

    public function getProvince($params = [])
    {
        $response = $this->client->get(env('BASE_URL_RAJA_ONGKIR').'/province', $params);
        if ($response->status() == 200) {
            return $response->json()["rajaongkir"]["results"];
        }
        throw new \Exception($response->json()["rajaongkir"]["status"]["description"], 1);

    }
    public function getCity($params = [])
    {
        $response = $this->client->get(env('BASE_URL_RAJA_ONGKIR').'/city', $params);
        if ($response->status() == 200) {
            return $response->json()["rajaongkir"]["results"];
        }
        throw new \Exception($response->json()["rajaongkir"]["status"]["description"], 1);

    }
    public function getSubdistrict($params = [])
    {
        $response = $this->client->get(env('BASE_URL_RAJA_ONGKIR').'/subdistrict', $params);
        if ($response->status() == 200) {
            return $response->json()["rajaongkir"]["results"];
        }
        throw new \Exception($response->json()["rajaongkir"]["status"]["description"], 1);

    }

    public function getCost($params = [])
    {
        $response = $this->client->asForm()->post(env('BASE_URL_RAJA_ONGKIR').'/cost', $params);
        if ($response->status() == 200) {
            return $response->json()["rajaongkir"]["results"];
        }
        throw new \Exception($response->json()["rajaongkir"]["status"]["description"], 1);
    }

}
