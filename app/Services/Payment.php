<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class Payment {
    public $PAYMENT_LINK = '/api/payment-checkout/create-v2';
    public $listEnableBank = "002, 008, 009, 013, 022, 014";
    public $listEnabledEwallet = "shopeepay_ewallet";
    public $listDisabledPaymentMethods = "";
    public $expired;
    public $includeAdminFee = true;
    public $isOpen = false;

    public function __construct() {
        $this->expired = Carbon::now()->addDays(1);
    }

    public function createLink($senderName, $description, $paymentCode, $amount, $phoneNumber, $email, $note = "")
    {
        $headers = [
            'Content-Type' => 'application/json',
            'x-oy-username' => env("OY_USERNAME"),
            'x-api-key' => env("OY_API_KEY")
        ];
        $data = [
            "description" => $description,
            "partner_tx_id" => $paymentCode,
            "child_balance" => "",
            "notes" => $note,
            "sender_name" => $senderName,
            "amount" => $amount,
            "email" => $email,
            "phone_number" => $phoneNumber,
            "is_open" => $this->isOpen,
            "include_admin_fee" => $this->includeAdminFee,
            "list_disabled_payment_methods" => $this->listDisabledPaymentMethods,
            "list_enabled_banks" => $this->listEnableBank,
            "list_enabled_ewallet" => $this->listEnabledEwallet,
            "expiration" => $this->expired->format("Y-m-d H:i:s"),
        ];

        $response = Http::withHeaders($headers)->post(env("OY_BASE_URL").$this->PAYMENT_LINK, $data);

        return $response;

    }

}
