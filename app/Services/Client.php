<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
class Client {

    private $client;
    private $headers;
    private $baseUrl = "";
    private $prefix = "";
    public $response;

    public function __construct($baseUrl, $headers = null, $form = false)
    {
        $this->baseUrl = $baseUrl;
        $this->headers = $headers ?? [
            'Content-Type' => 'application/json',
        ];


        $this->client = Http::withHeaders( $this->headers );
        if ($form) {
            $this->client = $this->client->asForm();
        }
    }

    public function setHeaders($headers)
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function get($path, $params = [])
    {
        $response = $this->client->get($this->baseUrl . $this->prefix . $path, $params);
        $this->response = $response;
        return $response;
    }

    public function post($path, $data = [])
    {
        $response = $this->client->post($this->baseUrl . $this->prefix . $path, $data);
        $this->response = $response;
        return $response;
    }

    public function put($path, $data = [])
    {
        $response = $this->client->put($this->baseUrl . $this->prefix . $path, $data);
        $this->response = $response;
        return $response;
    }

    public function delete($path)
    {
        $response = $this->client->delete($this->baseUrl . $this->prefix . $path);
        $this->response = $response;
        return $response;
    }

}
