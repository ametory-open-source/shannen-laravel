<?php
namespace App\Services;

use App\Models\Order;
use App\Models\Web;
class Biteship implements Ongkir {
    private Client $client;

    public function __construct() {
        $headers =  [
            'Content-Type' => 'application/json',
            'Authorization' => env("BITESHIP_API_KEY"),
        ];

        $this->client = new Client(env("BITESHIP_URL"), $headers );
    }

    public function getProvince($params = []){

    }
    public function getCity($params = []){

    }

    public function getSubdistrict($params = []){

    }
    public function getCost($params = [])
    {
        return $this->client->post("/v1/rates/couriers", $params);
    }
    public function getCouriers()
    {
        return $this->client->get("/v1/couriers");
    }
    public function tracking(Order $order)
    {
        // dd("/v1/trackings/". $order->shipping_order["courier"]["tracking_id"]);
        // dd("/v1/trackings/".$order->shipping_order["courier"]["waybill_id"]."/couriers/".$order->shipping_order["courier"]["company"]);
        // return $this->client->get("/v1/trackings/".$order->shipping_order["courier"]["waybill_id"]."/couriers/".$order->shipping_order["courier"]["company"] );
        return $this->client->get("/v1/trackings/". $order->shipping_order["courier"]["tracking_id"] );
    }
    public function orderCourier(Order $order)
    {
        $web = Web::first();
        $dataCourier = [
            "shipper_contact_name" => $web->company_name,
            "shipper_contact_phone" => $web->company_phone,
            "shipper_contact_email" => $web->company_email,
            "shipper_organization" => $web->company_name,
            "origin_contact_name" => $web->company_name,
            "origin_contact_phone" => $web->company_phone,
            "origin_address" => $web->company_address,
            "origin_note" => "",
            "origin_postal_code" =>(int) $web->company_postal_code,
            "destination_contact_name" =>  $order->shipping_address->recipient_name,
            "destination_contact_phone" =>  $order->shipping_address->phone_number,
            "destination_contact_email" =>  $order->shipping_address->email,
            "destination_address" =>  $order->shipping_address->address,
            "destination_postal_code" => (int) $order->shipping_address->postal_code,
            "courier_company" => $order->shipping_cost->courier_code,
            "courier_type" =>  $order->shipping_cost->service_code,
            // "courier_insurance" => 500000,
            "delivery_type" => "now",
            // "delivery_date" => "2019-09-24",
            // "delivery_time" => "12:00",
            "order_note" => $order->notes,
            "metadata" => [],
            "items" => $order->items->map(function($q) {
                return [
                    "id" => $q->id,
                    "name" => $q->name,
                    "image" => $q->feature_image,
                    "description" => $q->name,
                    "value" => $q->price,
                    "quantity" => $q->quantity,
                    "height" => $q->height,
                    "length" => $q->length,
                    "weight" => $q->weight,
                    "width" => $q->width,
                ];
            })->toArray(),
        ];

        \Log::info("SEND COURIER ORDER", ["data" => json_encode($dataCourier, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)]);
        return $this->client->post("/v1/orders", $dataCourier);
    }
}
