<?php

namespace App\Services;

interface Ongkir {
    public function getProvince($params = []);
    public function getCity($params = []);
    public function getSubdistrict($params = []);
    public function getCost($params = []);
}
