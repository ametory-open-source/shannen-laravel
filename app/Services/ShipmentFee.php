<?php

namespace App\Services;

use KiriminAja\Base\Config\KiriminAjaConfig;
use KiriminAja\Base\Config\Cache\Mode;
use KiriminAja\Services\KiriminAja;

class ShipmentFee
{
	private static $instance ;

	private function __construct()
	{
        if (env('APP_ENV') == "production") {
            KiriminAjaConfig::setMode(Mode::Production)->setApiTokenKey(env(env('API_KIRIMIN_PROD')));
        } else {
            KiriminAjaConfig::setMode(Mode::Staging)->setApiTokenKey(env(env('API_KIRIMIN_STAGING')));
        }
	}

	private function __clone()
	{

	}

	private function __wakeup()
	{

	}

	public static function getInstance()
	{
		if(null === self::$instance){
			self::$instance = new static();
		}

		return self::$instance;
	}

}
