<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Session\SessionManager;

class CartService {
    const MINIMUM_QUANTITY = 1;
    const DEFAULT_INSTANCE = 'shopping-cart';
    const SHIPPING_COST = 'shipping-cost';
    const BILLING_ADDRESS = 'billing-address';
    const SHIPPING_ADDRESS = 'shipping-address';
    const DISCOUNT = 'discount';
    const LAST_PRODUCT = 'last_product';
    const COSTS = 'costs';

    protected $session;
    protected $instance;

    /**
     * Constructs a new cart object.
     *
     * @param Illuminate\Session\SessionManager $session
     */
    public function __construct(SessionManager $session)
    {
        $this->session = $session;
    }


    public function add($product, $quantity, $options = []): void
    {
        $this->setLastProduct($product);
        $cartItem = $this->createCartItem($product, $quantity, $options);

        $content = $this->getContent();

        if ($content->has($product->id)) {
            $cartItem->put('quantity', $content->get($product->id)->get('quantity') + $quantity);
        }
        \Log::info("cartItem", $cartItem->toArray());
        $content->put($product->id, $cartItem);

        $this->session->put(self::DEFAULT_INSTANCE, $content);


    }

    /**
     * Updates the quantity of a cart item.
     *
     * @param string $id
     * @param string $action
     * @return void
     */
    public function update(string $id, string $action): void
    {
        $content = $this->getContent();

        if ($content->has($id)) {
            $cartItem = $content->get($id);

            switch ($action) {
                case 'plus':
                    $cartItem->put('quantity', $content->get($id)->get('quantity') + 1);
                    break;
                case 'minus':
                    $updatedQuantity = $content->get($id)->get('quantity', 0) - 1;

                    if ($updatedQuantity < self::MINIMUM_QUANTITY) {
                        $updatedQuantity = self::MINIMUM_QUANTITY;
                    }

                    $cartItem->put('quantity', $updatedQuantity);
                    break;
            }

            $content->put($id, $cartItem);

            $this->session->put(self::DEFAULT_INSTANCE, $content);
        }
    }

    public function setAmount(string $id, $updatedQuantity): void
    {
        $content = $this->getContent();

        if ($content->has($id)) {
            $cartItem = $content->get($id);
            $cartItem->put('quantity', $updatedQuantity);

            $content->put($id, $cartItem);
            $this->session->put(self::DEFAULT_INSTANCE, $content);
        }
    }

    /**
     * Removes an item from the cart.
     *
     * @param string $id
     * @return void
     */
    public function remove(string $id): void
    {
        $content = $this->getContent();

        if ($content->has($id)) {
            $this->session->put(self::DEFAULT_INSTANCE, $content->except($id));
        }
    }

    /**
     * Clears the cart.
     *
     * @return void
     */
    public function clear(): void
    {
        $this->session->forget(self::DEFAULT_INSTANCE);
        $this->session->forget(self::SHIPPING_COST);
        $this->session->forget(self::BILLING_ADDRESS);
        $this->session->forget(self::SHIPPING_ADDRESS);
        $this->session->forget(self::DISCOUNT);
    }

    /**
     * Returns the content of the cart.
     *
     * @return Illuminate\Support\Collection
     */
    public function content(): Collection
    {
        return is_null($this->session->get(self::DEFAULT_INSTANCE)) ? collect([]) : $this->session->get(self::DEFAULT_INSTANCE);
    }


    public function total()
    {
        $content = $this->getContent();

        $total = $content->reduce(function ($total, $item) {
            return $total += cleanQty($item->get('price', 0)) * cleanQty($item->get('quantity', 0));
        });
        return $total;

    }

    public function grandTotal()
    {
        $content = $this->getContent();
        $shippingCost = $this->getShippingCost();
        $discount = $this->getDiscount();
        \Log::info("DISCOUNT SELECTED", [$discount]);
        $total = $content->reduce(function ($total, $item) {
            return $total += $item->get('price') * cleanQty($item->get('quantity'));
        });

        if (!$shippingCost) return $total - ($total * $discount / 100) ;
        return $total - ($total * $discount / 100) + ($shippingCost ? $shippingCost["value"] : 0);
    }

    public function weight(): int
    {
        $content = $this->getContent();

        $total = $content->reduce(function ($total, $item) {
            return $total += $item->get('weight') * cleanQty($item->get('quantity'));
        });

        return $total ?? 0;
    }

    public function count(): int
    {
        $content = $this->getContent();
        $total = 0;
        foreach ($content as $key => $item) {
            $total += cleanQty($item->get('quantity'));
        }

        return $total;
    }

    protected function getContent(): Collection
    {
        return $this->session->has(self::DEFAULT_INSTANCE) ? $this->session->get(self::DEFAULT_INSTANCE) : collect([]);
    }

    public function setShippingCost($cost) {
        if ($cost == null) {
            $this->session->forget(self::SHIPPING_COST);
            return;
        }
        $this->session->put(self::SHIPPING_COST, $cost);
    }
    public function getShippingCost() {
        return $this->session->get(self::SHIPPING_COST);
    }


    public function setLastProduct($product) {
        if ($product == null) {
            $this->session->forget(self::LAST_PRODUCT);
            return;
        }
        $this->session->put(self::LAST_PRODUCT, $product);
    }
    public function getLastProduct() {
        return $this->session->get(self::LAST_PRODUCT);
    }

    public function setDiscount($discount) {
        if ($discount == null) {
            $this->session->forget(self::DISCOUNT);
            return;
        }
        $this->session->put(self::DISCOUNT, $discount);
    }

    public function getDiscount() {
        return $this->session->get(self::DISCOUNT) ?? 0;
    }

    public function setCosts($costs) {
        if ($costs == null) {
            $this->session->forget(self::COSTS);
            return;
        }
        $this->session->put(self::COSTS, $costs);
    }

    public function getCosts() {
        return $this->session->get(self::COSTS) ?? 0;
    }


    protected function createCartItem( $product, string $quantity, array $options): Collection
    {
        $product->price = floatval($product->price);
        $product->quantity = intval($product->quantity);

        if ($product->quantity < self::MINIMUM_QUANTITY) {
            $product->quantity = self::MINIMUM_QUANTITY;
        }

        return collect([
            'id' => $product->id,
            'name' => $product->name,
            'url' => $product->url,
            'sku' => $product->sku,
            'price' => $product->price,
            'weight' => $product->weight ?? 0,
            'height' => $product->dimension_height ?? 0,
            'length' => $product->dimension_length ?? 0,
            'width' => $product->dimension_width ?? 0,
            'quantity' => (float) cleanQty($quantity),
            'feature_image' => $product->feature_image,
            'options' => $options,
        ]);
    }


    public function setBillingAddress($address) {
        if ($address == null) {
            $this->session->forget(self::BILLING_ADDRESS);
            return;
        }
        $this->session->put(self::BILLING_ADDRESS, $address);
    }
    public function getBillingAddress() {
        return $this->session->get(self::BILLING_ADDRESS);
    }

    public function setShippingAddress($address) {
        if ($address == null) {
            $this->session->forget(self::SHIPPING_ADDRESS);
            return;
        }
        $this->session->put(self::SHIPPING_ADDRESS, $address);
    }
    public function getShippingAddress() {
        return $this->session->get(self::SHIPPING_ADDRESS);
    }
}
