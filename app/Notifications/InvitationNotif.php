<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Invitation;

class InvitationNotif extends Notification implements ShouldQueue
{
    use Queueable;
    private $invitation;
    /**
     * Create a new notification instance.
     */
    public function __construct(Invitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->subject('SELAMAT DATANG DI APLIKASI SIRAJIN')
                    ->greeting('Halo!, '.$this->invitation->name)
                    ->line("Anda diundang untuk menggunakan aplikasi SIRAJIN")
                    ->line("Silakan gunakan kode validasi dibawah ini")
                    ->line(new \Illuminate\Support\HtmlString('<h1>'.$this->invitation->validation_code.'</h1>'))
                    ->action('Klik Disini', url('/invitations/'.$this->invitation->uuid))
                    ->line('Berlaku sampai :'. $this->invitation->valid_date->translatedFormat("d M Y"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
