<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class UserForgotPassword extends Notification implements ShouldQueue
{
    use Queueable;
    private $newPassword;
    private $user;

    /**
     * Create a new notification instance.
     */
    public function __construct($user, $newPassword)
    {
        $this->user = $user;
        $this->newPassword = $newPassword;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->subject("Permintaan Reset Password")
                    ->greeting('Hi, '. $this->user->name)
                    ->line('Silakan gunakan password sementara ini untuk masuk ke akun anda')
                    ->line(new HtmlString("<h1>".$this->newPassword."</h1>"))
                    ->line('Segera ganti password tersebut setelah masuk');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
