<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Member;
use Illuminate\Support\HtmlString;

class SendApprovalMember extends Notification
{
    use Queueable;
    private $member;
    /**
     * Create a new notification instance.
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        if ($this->member->status == "ACTIVE") {
            return (new MailMessage)
                ->subject("Permintaan Program Keanggotaan ".env("APP_NAME"))
                ->line('Hai '. $this->member->user->name)
                ->line('Selamat permintaan membership ')
                ->line(new HtmlString("<h1>".$this->member->type->name."</h1>"))
                ->line(new HtmlString("<h3 style='color: green'>telah diterima</h3>"))
                ;
        }
        return (new MailMessage)
            ->subject("Permintaan Program Keanggotaan ".env("APP_NAME"))
            ->line('Hai '. $this->member->name)
            ->line('Mohon maaf permintaan membership ')
            ->line(new HtmlString("<h1>".$this->member->type->name."</h1>"))
            ->line(new HtmlString("<h3 style='color: red'>telah ditolak</h3>"))
            ;

    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
