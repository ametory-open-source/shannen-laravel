<?php

namespace App\Observers;
use App\Models\User;
use App\Notifications\RegistrationNotif;

class RegistrationObserver
{
    public function created(User $user)
    {
        $user->notify(new RegistrationNotif($user));
    }
}
