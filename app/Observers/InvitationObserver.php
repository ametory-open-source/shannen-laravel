<?php

namespace App\Observers;

use App\Models\Invitation;
use App\Notifications\InvitationNotif;

class InvitationObserver
{
    public function created(Invitation $invitation)
    {
        $invitation->notify(new InvitationNotif($invitation));
    }
}
