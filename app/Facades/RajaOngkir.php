<?php

namespace App\Facades;

use App\Services\RajaOngkirService;
use Illuminate\Support\Facades\Facade;

class RajaOngkir extends Facade {
    protected static function getFacadeAccessor()
    {
        return RajaOngkirService::class;
    }
}
