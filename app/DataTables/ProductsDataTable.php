<?php

namespace App\DataTables;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'products.action')
            ->addColumn('picture', 'products.picture')
            ->editColumn('picture',function($d) {
                if ($d->pictures->count()) {
                    $images = collect([]);
                    foreach($d->pictures as $item) {
                        $images->push('<img class="img-product" src="'.$item->picture_url.'" title="'.$item->description.'" />');
                    }
                    return '<div class="d-flex">'.$images->join("\n").'</div>';
                }
                return "";
            })
            ->editColumn('price',function($d) {
                $prices = collect([]);
                if ($d->price) {
                    $prices->push('<span>'.$d->price_format.'</span>');
                }
                if ($d->strike_price) {
                    $prices->push('<span><s>'.$d->strike_price_format.'</s></span>');
                }
                return $prices->join("<br />");
            })
            ->editColumn('action',function($d) {
                return '<div class="btn-group">
                <a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/products/'.$d->id.'/edit" role="button"><i class="fa fa-eye"></i></a><a name="" id="" class=" btn bg-gradient-danger btn-sm btn-icon btn-delete" onclick="btnDelete(this)" data-message="anda yakin akan menghapus : '.$d->name.'?" data-href="/admin/products/'.$d->id.'/delete" role="button"><i class="fa fa-trash"></i></a></div>';
            })
            ->editColumn('status',function($d) {
                $top_sale = "-";
                $active = '<span class="badge bg-gradient-secondary">Draft</span>';
                $feature_product = "-";
                switch ($d->status) {
                    case "PUBLISHED":
                        $active = '<span class="badge bg-gradient-success">Published</span>';
                        break;
                        case "UNPUBLISH":
                        $active = '<span class="badge bg-gradient-danger">Unpublish</span>';
                        break;
                }
                if ($d->is_top_sale) {
                    $top_sale = '<i class="fas fa-check text-success"></i>';
                }
                if ($d->is_feature_product) {
                    $feature_product = '<i class="fas fa-check text-success"></i>';
                }
                return <<<EOD
                        <dl>
                            <dt>Status</dt>
                            <dd>{$active}</dd>
                            <dt>Featured</dt>
                            <dd>{$feature_product}</dd>
                            <dt>Top Sale</dt>
                            <dd>{$top_sale}</dd>
                        </dl>
                EOD;
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model): QueryBuilder
    {
        return $model
        ->leftJoin("product_categories", 'products.product_category_id', '=', 'product_categories.id')->selectRaw("products.*, product_categories.name as product_category_name")
        ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('products-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [

            Column::make('name'),
            Column::make([
                "name" => "product_categories.name AS product_category_name",
                "data" => "product_category_name",
                "title" => "Category",
                "orderable" => true,
                "searchable" => true
            ]),
            Column::make('price'),
            Column::computed('picture'),
            Column::make('status')->title("Active"),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Products_' . date('YmdHis');
    }
}
