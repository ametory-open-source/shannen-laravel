<?php

namespace App\DataTables;

use App\Models\Module;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ModulesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'modules.action')
            ->editColumn('icon',function($d) {
                if ($d->icon) {
                    return "<div class='text-center'><i  style='--fa-secondary-opacity: 0.3; --fa-primary-color: grey; --fa-secondary-color: red;' class='fa-5x fad ".$d->icon."'></div>";
                }
                return  "";
            })
            ->editColumn('picture',function($d) {
                if ($d->picture) {
                    return "<img style='width:100px' src='/storage/".$d->picture."' class='img-rounded' />";
                }
                return  "";
            })

            ->editColumn('flag',function($d) {
                if ($d->flag) {
                    return '<span class="badge bg-gradient-success">Active</span>';
                }
                return '<span class="badge bg-gradient-danger">Inactive</span>';
            })
            ->editColumn('type',function($d) {
                if ($d->type == "CASE") {

                    return splitModCat($d->type)."<br />".$d->category;
                }
                return splitModCat($d->type);
            })
            ->editColumn('action',function($d) {
                return '<div class="btn-group"><a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/modules/'.$d->id.'/edit" role="button"><i class="fa fa-eye"></i></a><a name="" id="" class="btn bg-gradient-danger btn-sm btn-icon btn-delete" onclick="btnDelete(this)" data-message="anda yakin akan menghapus '.$d->title.'" data-href="/admin/modules/'.$d->id.'/delete" role="button"><i class="fa fa-trash"></i></a></div>';
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Module $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Module $model): QueryBuilder
    {
        return $model->newQuery()->orderBy("created_at", "desc");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('modules-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [

            Column::make('id')->title("ID"),
            Column::make('title'),
            Column::make('icon'),
            Column::make('sort'),
            Column::make('type'),
            Column::make('picture'),
            Column::make('flag')->title("Status"),
            Column::computed('action')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Modules_' . date('YmdHis');
    }
}
