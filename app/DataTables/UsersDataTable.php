<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'users.action')
            ->editColumn('created_at',function($d) {
                return $d->created_at->format("d M Y");
            })
            ->editColumn('email_verification_time',function($d) {
                if ($d->email_verification_time)
                return $d->email_verification_time->format("d M Y");
            })
            ->editColumn('action',function($d) {
                return '<div class="btn-group"><a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/users/'.$d->id.'/edit" role="button"><i class="fa fa-eye"></i></a></div>';
            })
            ->addColumn('status', 'users.status')
            ->editColumn('status',function($d) {

                $status = '<span class="badge bg-gradient-success">Active</span>';
                if ($d->suspended_at) {
                    $status = '<span class="badge bg-gradient-danger">Suspended</span>';

                }


                return $status;
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model): QueryBuilder
    {
        return $model
        ->selectRaw("users.*, members.code as member_code, members.id as member_id")
        ->leftJoin("members", "members.user_id", "=", "users.id")
        ->whereRaw("members.id is null")
        ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [


            Column::make('name'),
            Column::make('email'),
            Column::make('phone_number'),

            Column::make('created_at')->title("Register"),
            Column::make('email_verification_time')->title("Verify"),
            Column::computed('status')->exportable(false)->printable(false)->searchable(false),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Users_' . date('YmdHis');
    }
}
