<?php

namespace App\DataTables;

use App\Models\Affiliate;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AffiliatesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'affiliates.action')
            ->editColumn('action',function($d) {
                return '<div class="btn-group"><a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/affiliates/'.$d->id.'/edit" role="button"><i class="fa fa-eye"></i></a><a name="" id="" class="btn bg-gradient-danger btn-sm btn-icon btn-delete" onclick="btnDelete(this)" data-href="/admin/affiliates/'.$d->id.'/delete" role="button"><i class="fa fa-trash"></i></a></div>';
            })
            ->editColumn('status',function($d) {

                $status = '<span class="badge bg-gradient-secondary">PENDING</span>';

                switch ($d->status) {
                    case "ACTIVE":
                        $status = '<span class="badge bg-gradient-success">Active</span>';
                        break;
                        case "BLOCKED":
                        $status = '<span class="badge bg-gradient-danger">Blocked</span>';
                        break;
                }

                return $status;
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Affiliate $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Affiliate $model): QueryBuilder
    {
        return $model
        ->selectRaw("affiliates.*, users.name as user_name, users.email as user_email, users.phone_number as user_phone_number")
        ->join("users", "affiliates.user_id", "=", "users.id")
        ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('affiliates-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make([
                "name" => "users.name AS user_name",
                "data" => "user_name",
                "title" => "Name",
                "orderable" => true,
                "searchable" => true
            ]),
            Column::make([
                "name" => "users.email AS user_email",
                "data" => "user_email",
                "title" => "Email",
                "orderable" => true,
                "searchable" => true
            ]),
            Column::make([
                "name" => "users.phone_number AS user_phone_number",
                "data" => "user_phone_number",
                "title" => "Name",
                "orderable" => true,
                "searchable" => true
            ]),
            Column::make('username'),
            Column::make('code'),
            Column::make('status'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Affiliates_' . date('YmdHis');
    }
}
