<?php

namespace App\DataTables;

use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductCategoriesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'productcategories.action')
            ->editColumn('promo_tag',function($d) {
                    return '<span class="badge badge-pill" style="background-color:'.$d->promo_tag_color.'">'.$d->promo_tag_label.'</span>';
            })
            ->editColumn('picture',function($d) {
                return '<div style="margin:auto;text-align:center;padding:10px; border-radius:5px;background-color:'.$d->color.'"><img src="'.$d->picture_url.'" /></div>';
            })
            ->editColumn('flag',function($d) {
                $featured = "-";
                $active = "-";
                if ($d->flag) {
                    $active = '<i class="fas fa-check text-success"></i>';
                }
                if ($d->is_featured) {
                    $featured = '<i class="fas fa-check text-success"></i>';
                }
                return <<<EOD
                        <dl>
                            <dt>Active</dt>
                            <dd>{$active}</dd>
                            <dt>Featured</dt>
                            <dd>{$featured}</dd>
                        </dl>
                EOD;
            })
            ->editColumn('action',function($d) {
                return '<a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/product_categories/'.$d->id.'/edit" role="button"><i class="fa fa-eye"></i></a><a name="" id="" class="mx-2 btn bg-gradient-danger btn-sm btn-icon btn-delete" onclick="btnDelete(this)" data-href="/admin/product_categories/'.$d->id.'/delete" role="button"><i class="fa fa-trash"></i></a>';
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ProductCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductCategory $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('productcategories-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('name'),
            Column::make('picture'),
            Column::make('promo_tag'),
            Column::make('flag')->title("Active"),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'ProductCategories_' . date('YmdHis');
    }
}
