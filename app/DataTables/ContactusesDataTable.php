<?php

namespace App\DataTables;

use App\Models\ContactUs;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ContactusesDataTable extends DataTable
{


    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'contactuses.action')
            ->editColumn('readed_at', function($d) {
                if (!$d->readed_at) return null;
                return $d->readed_at->format("d-m-Y H:i");
            })
            ->editColumn('created_at', function($d) {
                return $d->created_at->format("d-m-Y H:i");
            })
            ->editColumn('action',function($d) {
                return '<div class="btn-group">
                <a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/contact_us/'.$d->id.'/readed" title="Flag as readed" role="button"><i class="fa fa-book-open"></i></a><a name="" id="" class=" btn bg-gradient-danger btn-sm btn-icon btn-delete" onclick="btnDelete(this)" data-message="yakin ingin menghapus pesan ini?" data-href="/admin/contact_us/'.$d->id.'/delete" role="button"><i class="fa fa-trash"></i></a></div>';
            })
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ContactUs $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ContactUs $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('contactuses-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [

            Column::make('name'),
            Column::make('email'),
            Column::make('phone'),
            Column::make('description')->title("Message"),
            Column::make('created_at'),
            Column::make('readed_at'),

            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Contactuses_' . date('YmdHis');
    }
}
