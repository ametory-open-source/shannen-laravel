<?php

namespace App\DataTables;

use App\Models\Blog;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BlogsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'blogs.action')
            ->editColumn('action',function($d) {
                return '<div class="btn-group"><a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/blogs/'.$d->id.'/edit" role="button"><i class="fa fa-eye"></i></a><a name="" id="" class="btn bg-gradient-danger btn-sm btn-icon btn-delete" onclick="btnDelete(this)" data-message="Anda yakin akan menghapus '.$d->title.'" data-href="/admin/blogs/'.$d->id.'/delete" role="button"><i class="fa fa-trash"></i></a></div>';
            })
            ->editColumn('feature_image',function($d) {
                if ($d->feature_image) {
                    return "<img style='width:100px' src='/storage/".$d->feature_image."' class='img-rounded' title='".$d->feature_image_caption."' />";
                }
                return  "";
            })
            ->editColumn('blog_category_id',function($d) {
                return  $d->category;
            })
            ->editColumn('title',function($d) {

                return  $d->title."<br />";
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Blog $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Blog $model): QueryBuilder
    {
        return $model->leftJoin("blog_categories", 'blogs.blog_category_id', '=', 'blog_categories.id')->selectRaw("blogs.*, blog_categories.name as category")->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('blogs-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('title'),
            Column::make('blog_category_id')->title("Category"),
            Column::make('feature_image'),
            Column::make('status'),

            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Blogs_' . date('YmdHis');
    }
}
