<?php

namespace App\DataTables;

use App\Models\Order;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', 'orders.action')
            ->editColumn('created_at',function($d) {
                return  $d->created_at->format("d M Y, H:m");
            })
            ->editColumn('grand_total',function($d) {
                return  money($d->grand_total);
            })
            ->editColumn('action',function($d) {
                return '<div class="btn-group"><a name="" id="" class="btn bg-gradient-info btn-sm btn-icon" href="/admin/orders/'.$d->id.'" role="button"><i class="fa fa-eye"></i></a></div>';
            })
            ->editColumn('status',function($d) {

                $status = '<span class="badge bg-gradient-secondary">UNPAID</span>';

                switch ($d->status) {
                    case "REJECTED_BY_ADMIN":
                        $status = '<span class="badge bg-gradient-danger">Rejected By Admin</span>';
                        break;
                    case "CANCELED_BY_USER":
                        $status = '<span class="badge bg-gradient-danger">Canceled By User</span>';
                        break;
                    case "PENDING":
                        $status = '<span class="badge bg-gradient-warning">Pending</span>';
                        break;
                    case "WAITING_FOR_ADMIN":
                        $status = '<span class="badge bg-gradient-warning">Waiting For Admin</span>';
                        break;
                    case "PROCESSING":
                        $status = '<span class="badge bg-gradient-info">Processing</span>';
                        break;
                    case "SHIPPING":
                        $status = '<span class="badge bg-gradient-primary">Shipping</span>';
                        break;
                    case "SHIPPED":
                        $status = '<span class="badge bg-gradient-primary">Shipped</span>';
                        break;
                    case "ORDER_ACCEPTED":
                        $status = '<span class="badge bg-gradient-primary">Order Accepted</span>';
                        break;
                    case "FINISHED":
                        $status = '<span class="badge bg-gradient-success">Finished</span>';
                        break;
                }







                return $status;
            })
            ->escapeColumns([])
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model): QueryBuilder
    {
        return $model
        ->selectRaw("orders.*, users.name as user_name, users.email as user_email, users.phone_number as user_phone_number")
        ->join("users", "orders.user_id", "=", "users.id")
        ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('orders-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(3)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [

            Column::make('order_code'),
            Column::make([
                "name" => "users.name AS user_name",
                "data" => "user_name",
                "title" => "Name",
                "orderable" => true,
                "searchable" => true
            ]),
            Column::make([
                "name" => "grand_total",
                "data" => "grand_total",
                "title" => "Grand Total",
                "class" => "text-right",
                "orderable" => true,
                "searchable" => true
            ]),
            Column::make('created_at')->title("Order Created"),
            Column::make('status'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Orders_' . date('YmdHis');
    }
}
