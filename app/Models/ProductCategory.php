<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class ProductCategory extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    protected $fillable = [
        "name",
        "slug",
        "description",
        "picture",
        "sort",
        "color",
        "custom_promo_tag",
        "custom_promo_tag_color",
        "promo_tag",
        "flag",
        "is_featured",
    ];

    public function getPictureUrlAttribute()
    {
        if ($this->picture)
            return assetUrl($this->picture);

        return "/assets/images/product-placeholder.png";
    }
    public function getUrlAttribute()
    {


        return "/products?cat=". $this->slug;
    }

    public function getPromoTagColorAttribute()
    {
        switch ($this->promo_tag) {
            case 'new':
                return "#ff6565";
                break;
            case 'sale':
                return "rgb(131, 91, 244)";
                break;
            case 'discount':
                return "#457b9d";
                break;

            default:
                return $this->custom_promo_tag_color;
                break;
        }
    }
    public function setCustomPromoTagColorAttribute($value)
    {
        switch ($this->promo_tag) {
            case 'new':
                $this->attributes["custom_promo_tag_color"] = "#ff6565";
                break;
            case 'sale':
                $this->attributes["custom_promo_tag_color"] = "rgb(131, 91, 244)";
                break;

            default:
                $this->attributes["custom_promo_tag_color"] = $value;
                break;
        }
    }
    public function getPromoTagLabelAttribute()
    {
        switch ($this->promo_tag) {
            case 'new':
            case 'sale':
            case 'discount':
                return $this->promo_tag;
                break;
            default:
                return $this->custom_promo_tag;
                break;
        }
    }
}
