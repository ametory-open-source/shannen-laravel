<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Casts\Attribute;
class Employee extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    protected $fillable = [
        "id",
        "user_id",
        "employee_number",
        "company_id",
        "position_id",
        "division_group_id",
        "join_date",
        "confirm_date",
        "resign_date",
        "created_at",
        "updated_at",
        "is_default",
        "is_super_admin",
        "is_admin",
        "profile_picture_path",
        "profile_picture_path_type",
        "work_location_id",
        "is_suspended",
        "admin_is_suspended",
        "super_admin_is_suspended",
    ];
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }
    public function division_group()
    {
        return $this->belongsTo(DivisionGroup::class, 'division_group_id', 'id');
    }

    // public function avatar(): Attribute {
    //     if ($this->profile_picture_path) {
    //         return Attribute::make(
    //             get: fn (string $profile_picture_path) => "AWEU",
    //         );
    //     }
    //     return "";
    // }
    public function getAvatarAttribute() {
        if ($this->profile_picture_path) {
            return env("FIREBASE_STORAGE_BASE_URL") ."/o/".$this->profile_picture_path."?alt=media";
        }
        return "";
    }

}
