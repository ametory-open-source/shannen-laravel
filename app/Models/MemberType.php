<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class MemberType extends Model
{

    use HasFactory, HasUuids;
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        "name",
        "description",
        "first_minimum_order",
        "minimum_order",
        "discount",
        "below_minimum_order_discount",
    ];
}
