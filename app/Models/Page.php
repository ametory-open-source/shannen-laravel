<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
class Page extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    protected $fillable = [
        "title",
        "slug",
        "feature_image",
        "feature_image_caption",
        "keywords",
        "short_description",
        "description",
        "status",
    ];

    public function getPictureUrlAttribute()
    {
        if ($this->feature_image)
        return assetUrl($this->feature_image);
    }

}



