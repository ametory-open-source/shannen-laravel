<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;


class Division extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    public function groups()
    {
        return $this->hasMany(DivisionGroup::class);
    }
}
