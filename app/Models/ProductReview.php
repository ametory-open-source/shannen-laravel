<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    use HasFactory;

    protected $fillable = [
        "is_anonymous",
        "rate",
        "feedback",
        "user_id",
        "product_id",
        "order_id",
        "images",
    ];

    protected $casts = [
        "images" => "collection"
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getRatingStarsAttribute()
    {
        $stars  = collect([]);
        $starNumber = $this->rate;
        for( $x = 0; $x < 5; $x++ )
        {
            if( floor( $starNumber )-$x >= 1 )
            {
                $stars->push('<i class="fa-solid fa-star"></i>');
            }
            elseif( $starNumber-$x > 0 )
            {
                $stars->push('<i class="fa-regular fa-star-half"></i>');
            }
            else
            {
                $stars->push('<i class="fa-regular fa-star"></i>');
            }
        }

        return $stars->join("\n");
    }

    public function getProductReviewsAttribute()
    {
        if ($this->images) {
            return $this->images->map(function($d) {
                return assetUrl("/".$d);
            });
        }
    }

}
