<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;


class ProductImage extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';


    protected $fillable = [
        "picture",
        "description",
        "product_id",
        "is_cover",
    ];

    public function getPictureUrlAttribute()
    {
        if ($this->picture)
        return assetUrl($this->picture);
    }
}
