<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Notifications\Notifiable;

class Web extends Model
{
    use HasFactory, HasUuids, Notifiable;


    protected $fillable = [
        "name",
        "short_description",
        "meta",
        "styles",
        "scripts",
        "keywords",
        "header_menu",
        "side_menu",
        "logo",
        "logo_icon",
        "logo_alt",
        "loading",
        "hero_picture",
        "video_picture",
        "question_picture",
        "subscribe_picture",
        "blogs_banner",
        "copyright",
        "show_feature",
        "company_name",
        "company_phone",
        "company_email",
        "company_web",
        "company_coordinate",
        "company_address",
        "company_map",
        "description",
        "is_online",
        "offline_template",
        "footer_left_link",
        "footer_center_link",
        "footer_right_link",
        "facebook_link",
        "instagram_link",
        "twitter_link",
        "youtube_link",
        "tiktok_link",
        "top_sale_title",
        "top_sale_subtitle",
        "blog_title",
        "blog_subtitle",
        "blogs_banner_title",
        "blogs_banner_subtitle",
        "other_posts_title",
        "other_posts_subtitle",
        "join_title",
        "join_subtitle",
        "not_found",
        "contact_picture",
        "contact_title",
        "contact_subtitle",
        "faq_picture",
        "faq_title",
        "faq_subtitle",
        "show_header",
        "show_main_banner",
        "show_feature_banner",
        "show_feature_product",
        "show_feature_category",
        "show_top_sale",
        "show_blog",
        "show_join",
        "show_cart",
        "show_user",
        "show_footer",
        "banner_social_media",
        "affiliate_term",
        "company_province_id",
        "company_city_id",
        "company_subdistrict_id",
        "company_village_id",
        "company_postal_code",
        "affiliate_enabled",
        "member_enabled",
        "member_term",
        "couriers",
        "company_bank",
        "company_bank_account_number",
        "company_bank_account_holder",
        "company_bank_account_branch",


    ];

    protected $casts = [
        "couriers" => "json"
    ];

    public function routeNotificationForMail(): array|string
    {
        return [$this->company_email => $this->company_name];
    }


    public function getLogoUrlAttribute()
    {
        if ($this->logo)
        return assetUrl($this->logo);
    }
}
