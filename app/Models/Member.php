<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;



class Member extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        "code",
        "otp_code",
        "user_id",
        "member_type_id",
        "status",
        "have_purchased",
        "approved_by",
        "approved_at",
        "blocked_by",
        "blocked_at",
        "province_id",
        "city_id",
        "subdistrict_id",
        "village_id",
        "postal_code",
        "id_card",
        "address",
    ];

    protected $casts = [
        "approved_at" => "datetime",
        "blocked_at" => "datetime",
    ];

    public function setTypeAttribute($value)
    {
        $modelType = ModelType::where("name", $value)->first();
        if ($modelType)
        $this->attributes["member_type_id"] = $modelType->id;
    }


    public function type()
    {
        return $this->belongsTo(MemberType::class, 'member_type_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }
    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
