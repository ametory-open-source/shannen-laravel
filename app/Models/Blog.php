<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Laravel\Scout\Searchable;


class Blog extends Model
{
    use Searchable;
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    protected $fillable = [
        "title",
        "slug",
        "feature_image",
        "feature_image_caption",
        "keywords",
        "blog_category_id",
        "short_description",
        "description",
        "tags",
        "status",
        "comment_enabled",
        "comment_moderation",
        "author",
    ];

    public function searchableAs(): string
    {
        return 'blogs_index';
    }

    public function toSearchableArray()
    {
        return [
            'id' =>  $this->id,
            'title' => $this->title,
            'description' => $this->description,
        ];
    }


    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function category()
    {
        return $this->belongsTo(BlogCategory::class, "blog_category_id", "id");
    }
    public function comments_published()
    {
        return $this->hasMany(Comment::class)->where("status", "PUBLISHED")->whereNull("parent_id");
    }
    public function all_comments_published()
    {
        return $this->hasMany(Comment::class)->where("status", "PUBLISHED");
    }

    public function getPictureUrlAttribute()
    {
        if ($this->feature_image)
        return assetUrl($this->feature_image);
    }

    public function getUrlAttribute()
    {
        return url("/blog/".$this->slug);
    }

    public function getCategoryUrlAttribute()
    {
        return url("/blog?category=".$this->category->name);
    }

    public function getTagListAttribute()
    {
        if ($this->tags) {
            return collect(explode(",", $this->tags));
        }

        return collect([]);
    }
}
