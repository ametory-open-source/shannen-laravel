<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticModel extends Model
{
    use HasFactory;

    protected $table = "statics";
    protected $fillable = [
        "name",
        "description",
        "content",
        "scripts",
        "styles",
        "flag",
    ];
}
