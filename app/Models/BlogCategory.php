<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class BlogCategory extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    protected $fillable = [
        "name",
        "description",
        "picture",
    ];

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }

    public function published_blogs()
    {
        return $this->hasMany(Blog::class)->where("status", "PUBLISHED");
    }
}
