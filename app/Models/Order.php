<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;


class Order extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    protected $fillable = [
        "order_code",
        "items",
        "total",
        "grand_total",
        "shipping_cost",
        "billing_address",
        "shipping_address",
        "notes",
        "user_id",
        "shipping_order",
        "payment_link",
        "status",
        "images",
        "remarks",
        "discount",
        "discount",
        "tracking_order",
    ];
    protected $casts = [
        'items' => 'json',
        'shipping_cost' => 'json',
        'billing_address' => 'json',
        'shipping_order' => 'json',
        'shipping_address' => 'json',
        'tracking_order' => 'json',
        "images" => "collection"
    ];



    public function getItemsAttribute()
    {
        $items = collect([]);
        foreach(json_decode($this->attributes["items"], true) as $item) {
            $obj = new \App\Classes\OrderItem($item);
            $items->push($obj);
        }
        return $items;
    }
    public function getBillingAddressAttribute()
    {

        return new Address(json_decode($this->attributes["billing_address"], true));
    }
    public function getShippingAddressAttribute()
    {

        return new Address(json_decode($this->attributes["shipping_address"], true));
    }
    public function getShippingCostAttribute()
    {

        return new \App\Classes\ShippingCost(json_decode($this->attributes["shipping_cost"], true));
    }

    public function getExpiredDateAttribute() {
        if ($this->status == "UNPAID") {
            return $this->created_at->addDays(1);
        }
        return null;
    }
    public function getStatusBadgeAttribute() {
        switch ($this->status) {
            case "REJECTED_BY_ADMIN":
                $status = '<span class="badge bg-gradient-danger">Rejected By Admin</span>';
                break;
            case "CANCELED_BY_USER":
                $status = '<span class="badge bg-gradient-danger">Canceled By User</span>';
                break;
            case "PENDING":
                $status = '<span class="badge bg-gradient-warning">Pending</span>';
                break;
            case "WAITING_FOR_ADMIN":
                $status = '<span class="badge bg-gradient-warning">Waiting For Admin</span>';
                break;
            case "PROCESSING":
                $status = '<span class="badge bg-gradient-info">Processing</span>';
                break;
            case "REQUEST_PICKUP":
                $status = '<span class="badge bg-gradient-info">Request Pickup</span>';
                break;
            case "SHIPPING":
                $status = '<span class="badge bg-gradient-primary">Shipping</span>';
                break;
            case "PICKING_UP":
                $status = '<span class="badge bg-gradient-primary">Picking Up</span>';
                break;
            case "DROPPING_OFF":
                $status = '<span class="badge bg-gradient-primary">Dropping Off</span>';
                break;
            case "SHIPPED":
                $status = '<span class="badge bg-gradient-success">Shipped</span>';
                break;
            case "ORDER_ACCEPTED":
                $status = '<span class="badge bg-gradient-success">Order Accepted</span>';
                break;
            case "FINISHED":
                $status = '<span class="badge bg-gradient-success">Finished</span>';
                break;
            default :
                $status = '<span class="badge bg-gradient-secondary">UNPAID</span>';
        }

        return $status;

    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createPaymentLink()
    {
        if ($this->payment_link) {
            return $this->payment_link;
        }
        $payment = new Payment();
        $response = $payment->createLink($this->billing_address->recipient_name, "INVOICE PEMESANAN ". $this->order_code, $this->order_code, $this->grand_total, $this->billing_address->phone_number, $this->billing_address->email, $this->notes);
        \Log::info("PAYMENT LINKT ", $response->json());
        $url = $response->json()["url"];
        $this->update([
            "payment_link" => $url ,
        ]);

        return $url ;
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }


    public function getOrderImagesAttribute()
    {
        if ($this->images->count()) {
            return $this->images->map(function($d) {
                return assetUrl("/".$d);
            });
        }

        return null;
    }
}
