<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;


class Message extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    public $incrementing = false;


    protected $fillable = [
        "message",
        "options",
        "type",
        "attachment",
        "message_room_id",
        "user_id",
        "admin_id",
        "readed_at",
        "readed_by_admin_at",
        "readed_by_admin",
    ];

    protected $casts = [
        "options" => "json"
    ];


    public function Room()
    {
        return $this->belongsTo(MessageRoom::class, 'message_room_id', 'id');
    }
}
