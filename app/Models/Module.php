<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
class Module extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        "icon",
        "picture",
        "alt_picture",
        "type",
        "title",
        "subtitle",
        "price",
        "description",
        "link",
        "sort",
        "flag",
        "category",
    ];

    public function getPictureUrlAttribute()
    {
        if ($this->picture)
        return assetUrl($this->picture);
    }
    public function getAltPictureUrlAttribute()
    {
        if ($this->alt_picture)
        return assetUrl($this->alt_picture);
    }
}
