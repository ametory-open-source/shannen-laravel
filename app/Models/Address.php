<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Address extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        "longitude",
        "latitude",
        "label",
        "recipient_name",
        "phone_number",
        "email",
        "address",
        "user_id",
        "notes",
        "is_default",
        "province_id",
        "city_id",
        "subdistrict_id",
        "village_id",
        "postal_code",
    ];



    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }
    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
