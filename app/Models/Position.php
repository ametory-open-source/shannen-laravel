<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Position extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }
}
