<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Support\Str;

class Invitation extends Model
{
    use HasApiTokens, HasFactory, Notifiable, HasUuids;
    protected $keyType = 'string';
    protected $primaryKey = 'uuid';
    public $incrementing = false;

    protected $casts = [
        "join_date" => "datetime",
        "valid_date" => "datetime",
    ];

    protected $fillable = [
        "name",
        "email",
        "phone",
        "join_date",
        "valid_date",
        "position_code",
        "validation_code",
        "company_id",
        "employee_number",
    ];


    public function position()
    {
        return $this->belongsTo(Position::class, 'position_code', 'code');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

}
