<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class ContactUs extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    protected $table = 'contact_us';

    protected $fillable = [
        "name",
        "email",
        "phone",
        "description",
        "title",
        "readed_at",
    ];
    protected $casts = [
        "readed_at" => "datetime",
    ];
}
