<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Affiliate extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        "username",
        "code",
        "picture",
        "cover",
        "otp_code",
        "user_id",
        "default_commission",
        "address",
        "status",
        "display_name",
    ];



    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }


    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('commission', 'created_by');
    }

    public function getPictureUrlAttribute()
    {
        if ($this->picture)
        return assetUrl($this->picture);
    }

    public function getCoverUrlAttribute()
    {
        if ($this->cover)
        return assetUrl($this->cover);
    }

    public function toData()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'code' => $this->code,
        ];
    }
}
