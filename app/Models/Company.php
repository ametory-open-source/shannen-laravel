<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Company extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    
    protected $fillable = [
        "name",
        "logo",
        "logo_path_type",
        "created_by_admin",
        "updated_by_admin",
        "address",
        "email",
        "fax",
        "phone",
        "created_by_user",
        "updated_by_user",
        "use_face_match",
        "zoom_api_key",
        "zoom_api_secret",
        "zoom_api_url",
        "zoom_sdk_key",
        "zoom_sdk_secret",
        "zoom_enabled",
        "use_biometric",
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class, 'company_id', 'id');
    }

    public function getLogoUrlAttribute() 
    {
        if ($this->logo) {
            return env("FIREBASE_STORAGE_BASE_URL") . "/o/" . $this->logo . "?alt=media";
        }
        return null;
    }
}
