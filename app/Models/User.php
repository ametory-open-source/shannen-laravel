<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasUuids;
    protected $keyType = 'string';
    public $incrementing = false;
    public $temp_password;

    protected $fillable = [
        "name",
        "email",
        "phone_number",
        "email_verification_time",
        "phone_verification_time",
        "password",
        "address_id",
        "avatar",
    ];



    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        // 'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'id' => 'string',
        'email_verification_time' => 'datetime',
    ];

    /**
     * Always encrypt the password when it is updated.
     *
     * @param $value
    * @return string
    */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
        $this->temp_password = $value;
    }

    public function affiliate()
    {
        return $this->hasOne(Affiliate::class);
    }

    public function member()
    {
        return $this->hasOne(Member::class, 'user_id');
    }


    public function default_address()
    {
        return $this->hasOne(Address::class, 'id', 'address_id');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'user_id','id');
    }

    public function getMinimumOrderAttribute()
    {
        if (!$this->member) return 0;
        if ($this->member->status == "ACTIVE") {
            if(!$this->member->have_purchased && $this->member->type->first_minimum_order > 0 ) {
                return $this->member->type->first_minimum_order;
            }
            if($this->member->have_purchased)  {
                return $this->member->type->minimum_order;
            }
        }
        return 0;
    }
    public function getActiveDiscountAttribute()
    {
        if (!$this->member) return 0;
        if ($this->member->status == "ACTIVE") {
            return $this->member->type->discount;
        }
        return 0;
    }

    public function discount($subtotal)
    {

        if (!$this->member) return 0;
        if ($this->member->status == "ACTIVE") {
            if(!$this->member->have_purchased && $subtotal > $this->member->type->first_minimum_order ) {
                return $this->member->type->discount;
            }
            if($this->member->have_purchased && $subtotal > $this->member->type->minimum_order) {
                return $this->member->type->discount;
            }
        }
        return 0;
    }


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function wishlists()
    {
        return $this->belongsToMany(Product::class, 'wishlist_product', 'user_id', 'product_id');
    }
}
