<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Http\Request;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Searchable;
    use HasFactory, HasUuids;
    protected $keyType = 'string';

    public $affiliate_data;

    protected $fillable = [
        "slug",
        "name",
        "description",
        "materials",
        "other_info",
        "short_description",
        "keywords",
        "price",
        "strike_price",
        "weight",
        "weight_unit",
        "dimension_length",
        "dimension_width",
        "dimension_height",
        "dimension_unit",
        "product_category_id",
        "promo_tag",
        "custom_promo_tag",
        "custom_promo_tag_color",
        "tags",
        "status",
        "is_feature_product",
        "is_top_sale",
        "review_enabled",
        "review_moderation",
        "sku",
    ];

    public function searchableAs(): string
    {
        return 'products_index';
    }

    public function toSearchableArray()
    {
        return [
            'id' =>  $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => (float) $this->price,
        ];
    }



    public function affiliates()
    {
        return $this->belongsToMany(Affiliate::class);
    }

    public function setAffiliate($affiliate)
    {
        $this->affiliate_data = $affiliate;
    }

    public function getPromoTagColorAttribute()
    {
        switch ($this->promo_tag) {
            case 'new':
                return "#ff6565";
                break;
            case 'sale':
                return "rgb(131, 91, 244)";
                break;
            case 'discount':
                return "#457b9d";
                break;

            default:
                return $this->custom_promo_tag_color;
                break;
        }
    }
    public function setCustomPromoTagColorAttribute($value)
    {
        switch ($this->promo_tag) {
            case 'new':
                $this->attributes["custom_promo_tag_color"] = "#ff6565";
                break;
            case 'sale':
                $this->attributes["custom_promo_tag_color"] = "rgb(131, 91, 244)";
                break;

            default:
                $this->attributes["custom_promo_tag_color"] = $value;
                break;
        }
    }
    public function getPromoTagLabelAttribute()
    {
        switch ($this->promo_tag) {
            case 'new':
            case 'sale':
            case 'discount':
                return $this->promo_tag;
                break;
            default:
                return $this->custom_promo_tag;
                break;
        }
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }

    public function pictures()
    {
        return $this->hasMany(ProductImage::class);
    }
    public function getPriceFormatAttribute()
    {
        if ($this->price) {
            return money($this->price);
        }
    }

    public function getPriceCommissionFormatAttribute()
    {
        if ($this->pivot) {
            return money($this->pivot->commission / 100 * $this->price);
        }
    }

    public function getFeatureImageAttribute()
    {

        if ($this->pictures()->where("is_cover", true)->count()) {
            return $this->pictures()->where("is_cover", true)->first()->picture_url;
        }
        return "/assets/images/product-placeholder.png";
    }

    public function getFeatureImageCaptionAttribute()
    {

        if ($this->pictures()->where("is_cover", true)->count()) {
            return $this->pictures()->where("is_cover", true)->first()->description;
        }
        return null;
    }
    public function getStrikePriceFormatAttribute()
    {
        if ($this->strike_price) {
            return money($this->strike_price);
        }
    }

    public function getReviewCountAttribute()
    {
        return  \DB::table('product_reviews')->where("product_id", $this->id)->count();
    }

    public function getRatingAverageAttribute()
    {
        $avg = \DB::table('product_reviews')->where("product_id", $this->id)->selectRaw('avg(rate) as rating ')->get();
        if (count($avg)) {
            return (float) $avg[0]->rating;
        }
        return 0;
    }

    public function getRatingStarsAttribute()
    {
        $stars  = collect([]);
        $starNumber = $this->rating_average;
        for( $x = 0; $x < 5; $x++ )
        {
            if( floor( $starNumber )-$x >= 1 )
            {
                $stars->push('<i class="fa-solid fa-star"></i>');
            }
            elseif( $starNumber-$x > 0 )
            {
                $stars->push('<i class="fa-regular fa-star-half"></i>');
            }
            else
            {
                $stars->push('<i class="fa-regular fa-star"></i>');
            }
        }

        return $stars->join("\n");
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
    }

    public function getCleanDescriptionAttribute()
    {
        $output = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->description);
        return $output;
    }

    public function getAffiliateUrlAttribute()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if ($user && $user->affiliate) {
            return url('/'. $user->affiliate->username.'/'. $this->slug.'/'.$this->id);
        }
    }


    public function getUrlAttribute()
    {
        if ($this->affiliate_data) {
            return url('/'.$this->affiliate_data->username.'/'.$this->slug.'/'.$this->id);
        }
        return url('/products/'.$this->slug.'/'.$this->id);
    }

    private function checkEligible(): bool
    {
        if ($user = auth(env('APP_AFFILIATE_GUARD')->user())) {
            if (optional($user->member)->status == "ACTIVE") {
                if ($user->member->have_purchased) {

                }
            }
        }
        return false;
    }


    public function getFinalPriceAttribute()
    {
        return $this->price;
    }

    public function getIsFavoritedAttribute()
    {
        if ($user = auth(env('APP_AFFILIATE_GUARD'))->user()) {
            return \DB::table("wishlist_product")->where('product_id', $this->id)->where('user_id', $user->id)->count();
        }
        return false;
    }

    public function addReview(Request $request)
    {
        if ($user = auth(env('APP_AFFILIATE_GUARD'))->user()) {
            $input = $request->except("_token");
            $input["user_id"] = $user->id;
            $input["product_id"] = $this->id;
            $input["is_anonymous"] = isset($input["is_anonymous"]) ? $input["is_anonymous"] : false;
            if($this->reviews()->where("user_id", $user->id)->count() == 0) {
                // NEW REVIEW
                return ProductReview::create($input);
            } else {
                $review = $this->reviews()->where("user_id", $user->id)->first();
                return $review->update($input);
            }

        }
        return null;
    }

    public function getEligibleReviewAttribute()
    {
        if ($user = auth(env('APP_AFFILIATE_GUARD'))->user()) {
            // \DB::enableQueryLog();
            $orders = \DB::table('orders')
                ->whereRaw('JSON_EXTRACT(orders.items, "$[*].id")  like ?', ["%".$this->id."%"])
                ->where("orders.user_id", $user->id)
                ->where("orders.status", "FINISHED")
                ->count();
                return $orders;
        }

        return false;
    }
}
