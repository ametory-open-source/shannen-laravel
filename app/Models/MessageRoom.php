<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class MessageRoom extends Model
{
    use HasFactory, HasUuids;
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = [
        "user_id",
        "name",
        "last_message",
        "last_message_at",
    ];
    protected $casts = [
        'last_message_at' => 'datetime'
    ];
    public function messages()
    {
        return $this->hasMany(Message::class);
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getAdminReadedAttribute()
    {
       return $this->messages()->whereNull("readed_by_admin")->count() ? true : false;
    }
}
