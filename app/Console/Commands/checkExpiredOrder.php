<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
class checkExpiredOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:expired-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
       $orders = Order::where("status", "UNPAID")
       ->whereRaw("created_at + INTERVAL 1 DAY < now()")->get();
        foreach ($orders as $key => $order) {
            $order->status = "REJECTED_BY_ADMIN";
            $order->remarks = "UPDATE BY SYSTEM, NO PAYMENT EXISTS";
            $order->update();
            \Log::info("UPDATE ORDER ".$order->order_code. " TO  REJECTED_BY_ADMIN" );
        }

    }
}
