<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\PagesDataTable;
use App\Models\Page;

class PagesController extends Controller
{
    public function index(PagesDataTable $dataTable)
    {
        return $dataTable->render('pages.index');
    }

    public function create(Request $request)
    {
        return view("pages.create");
    }
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'title' => 'required',
                "slug" => 'required|unique:blogs',
            ]);

            $input = $request->except(["_token", "files"]);
            $input["keywords"] = "";
            Page::create(collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray());
            return redirect()->route("pages.index")->with('success', "Page ".$request->get("title"). " Saved");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }

    public function edit(Request $request, Page $page)
    {
        return view("pages.edit", compact("page"));
    }

    public function update(Request $request, Page $page)
    {
        try {
            $validated = $request->validate([
                'title' => 'required',
                "slug" => [
                    'required',
                    Rule::unique('pages')->ignore($page->id),
                ]
            ]);
            $input = $request->except(["_token", "files"]);
            $input["keywords"] = "";
            $page->update(collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray());
            return redirect()->route("pages.index")->with('success', "Page ".$request->get("title"). " Updated");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function delete(Page $page)
    {
        try {
            $page->delete();
            return redirect('admin/pages');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
}
