<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Biteship;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // $biteship = new Biteship();
        // $result = $biteship->getCost([
        //     "origin_postal_code" => 40221,
        //     "destination_postal_code" => 40232,
        //     "couriers" => "anteraja,jne,sicepat",
        //     "items" => [
        //           [
        //              "name" => "Shoes",
        //              "description" => "Black colored size 45",
        //              "value" => 199000,
        //              "length" => 30,
        //              "width" => 15,
        //              "height" => 20,
        //              "weight" => 200,
        //              "quantity" => 2
        //           ]
        //        ]
        //      ]
        // );

        // $result = $biteship->getCouriers();
        // return response()->json($result->json());
        return view('pages.dashboard');
    }

    public function updateProfile(Request $request) {
        $user = auth()->user();
        $user->name = $request->name;
        $user->update();
        return back()->withSuccess("Profile Updated");
    }
    public function changePassword(Request $request) {
        $rePass = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/m';
        preg_match($rePass, $request->password, $matches2 );

        if (count($matches2) == 0) {
            return back()->withError( "Password minimal 8 karakter,  mengandung angka dan huruf besar");
        }
        $user = auth()->user();
        if ($request->old_password == "") {
            return back()->withError("Password tidak boleh kosong");
        }
        if ($request->password == "" || $request->repeat_password == "") {
            return back()->withError("Password baru tidak boleh kosong");
        }
        if ( $request->password != $request->repeat_password) {
            return back()->withError("Password tidak sama");
        }

        if (!Hash::check($request->old_password,$user->password)) {
            return back()->withError("Password yang anda masukan salah");
        }

        $user->password = $request->password;
        $user->update();
        return back()->withSuccess("Password Changed");
    }
    public function profile() {
        return view("pages.profile");
    }
}
