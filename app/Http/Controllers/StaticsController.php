<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\StaticsDataTable;
use App\Models\StaticModel;
use App\Models\Web;
use App\Models\Affiliate;
use App\Models\Product;
use App\Facades\RajaOngkir;
use App\Models\Province;
use App\Models\City;
use App\Models\Subdistrict;
use App\Models\Village;
use Illuminate\Support\Facades\Http;

class StaticsController extends Controller
{
    public function index(StaticsDataTable $dataTable)
    {
        return $dataTable->render('statics.index');
    }

    public function create(Request $request)
    {
        return view('statics.create');
    }

    public function edit(Request $request, StaticModel $static)
    {
        return view('statics.edit', compact("static"));
    }

    public function store(Request $request)
    {
        try {
            $input = $request->except(["_token", "files"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $static = StaticModel::create($input);
            return redirect()->route("statics.edit", ["static" => $static])->with('success', "Static Feature ".$request->get("name"). " Created");
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage())->withInput();
        }
    }

    public function update(Request $request, StaticModel $static)
    {

        try {
            $input = $request->except(["_token", "files"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $static->update($input);
            return back()->with('success', "Static Feature ".$request->get("name"). " Updated");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }

    public function delete(StaticModel $static)
    {
        try {
            $static->delete();
            return redirect('statics');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }


    public function viewPage(string $page)
    {
        $web = Web::first();
        $static = StaticModel::where("name", $page)->first();
        if ($static) {
            return view('statics.view', compact("web","static"));
        }

        $affiliate = Affiliate::where("username", $page)->first();
        if ($affiliate) {
            return view("frontend.static.products", compact("affiliate"));
        }
        return abort(404);
    }

    public function viewProduct($prefix, $slug, $id)
    {
        if ($prefix == "products") {
            $product = Product::findOrFail($id);
            $feature_products = [$product];
            return view("frontend.static.product_detail", compact("product", "feature_products"));
        } else {
            $affiliate = Affiliate::where("username", $prefix)->first();
            if ($affiliate) {
                $product = Product::
                join('affiliate_product', 'affiliate_product.product_id', '=', 'products.id')
                ->join('affiliates', 'affiliates.id', '=', 'affiliate_product.affiliate_id')
                ->where('affiliates.id', $affiliate->id)
                ->selectRaw("products.*")
                ->findOrFail($id);
                $product->setAffiliate($affiliate);


                $feature_products = [$product];
                return view("frontend.static.product_detail", compact("product", "affiliate", "feature_products"));
            }
        }
        return abort(404);
    }

    public function locationProvince(Request $request)
    {
        $provinces =  Province::get();
        $provinces = collect($provinces)->pluck("name", "id")->prepend("", "")->toArray();


        return view('components.location.provinces', compact('provinces'));
    }

    public function locationCity(Request $request)
    {
        $city_data =  City::where("province_id", $request->province_id)->get();

        $cities = [
            "" => ""
        ];

        foreach($city_data as $value) {
            $cities[$value["id"]] = $value["name"];
        }

        $cities = collect($cities)->sort()->toArray();



        return view('components.location.cities', compact('cities'));
    }

    public function locationSubdistrict(Request $request)
    {
        $subdistrict_data =  Subdistrict::where("city_id", $request->city_id)->get();

        $subdistricts = [
            "" => ""
        ];

        foreach($subdistrict_data as $value) {
            $subdistricts[$value["id"]] = $value["name"];

        }

        $subdistricts = collect($subdistricts)->sort()->toArray();



        return view('components.location.subdistricts', compact('subdistricts'));
    }

    public function locationVillageDetail(Village $village)
    {
        return response()->json($village);
    }
    public function locationVillage(Request $request)
    {
        $village_data =  Village::where("subdistrict_id", $request->subdistrict_id)->get();

        $villages = [
            "" => ""
        ];

        foreach($village_data as $value) {
            $villages[$value["id"]] = $value["name"];

        }

        $villages = collect($villages)->sort()->toArray();



        return view('components.location.villages', compact('villages'));
    }

    public function place(Request $request)
    {
        $res = Http::get("https://maps.googleapis.com/maps/api/place/findplacefromtext/json", [
            "fields" => "formatted_address,name,rating,opening_hours,geometry",
            "input" => $request->search,
            "inputtype" => "textquery",
            "key" => env("APP_GOOGLE_MAP_KEY")
        ]);
        if ($res->successful()) {

            return response()->json($res->json()["candidates"]);
        }
        return response()->json([]);
    }

    public function reviewProduct(Request $request, $prefix, $slug, $id)
    {
        if ($prefix == "products") {
            $product = Product::findOrFail($id);
            $product->addReview($request);
            return back()->withSuccess("Produk berhasil di review");
        }
        return back();
    }

}
