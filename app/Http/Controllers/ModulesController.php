<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ModulesDataTable;
use App\Models\Module;
use App\Models\Web;

class ModulesController extends Controller
{
    public function index(ModulesDataTable $dataTable)
    {
        return $dataTable->render('modules.index');
    }

    public function create(Request $request)
    {
        $web = Web::first();
        if (!$web) $web = new Web();
        return view("modules.create", compact("web"));
    }

    public function store(Request $request)
    {
        try {
            // if ($request->type == "FEATURE" || $request->type == "STEP" || $request->type == "CLIENT") {
            //     $count = Module::where('type', $request->type)->where('flag', true)->count();
            //     if ($count >= 4) {
            //         throw new \Exception("Module Limited");
            //     }
            // }
            // if ($request->type == "STEP" || $request->type == "PRICING") {
            //     $count = Module::where('type', $request->type)->where('flag', true)->count();
            //     if ($count >= 3) {
            //         throw new \Exception("Module Limited");
            //     }
            // }
            // if ($request->type == "QUESTION") {
            //     $count = Module::where('type', $request->type)->where('flag', true)->count();
            //     if ($count >= 7) {
            //         throw new \Exception("Module Limited");
            //     }
            // }
            $input = $request->except(["_token"]);
            if (!$request->get("description")) {
                $input["description"] = ' ';
            }
            if ( $request->alt_picture) {
                $path = $request->file('alt_picture')->store('public/images', 'local');
                $input["alt_picture"] = str_replace("public", "", $path);
            }

            // dd($input);
            Module::create($input);
            return redirect('admin/modules');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
    public function edit(Module $module)
    {
        try {

            $web = Web::first();
            if (!$web) $web = new Web();
            return view("modules.edit", compact("module", "web"));
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];
            return back()->with("error", $message)->withInput();
        } catch (\Throwable $th) {
            return back()->with("error", $th->getMessage())->withInput();
        }

    }
    public function update(Request $request, Module $module)
    {
        try {
            $input = $request->except(["_token", "_method"]);
            if (!$request->get("description")) {
                $input["description"] = ' ';
            }

            if ( $request->alt_picture) {
                $path = $request->file('alt_picture')->store('public/images', 'local');
                $input["alt_picture"] = str_replace("public", "", $path);
            }
            $module->update($input);
            return redirect('admin/modules');
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];
            return back()->with("error", $message)->withInput();
        } catch (\Throwable $th) {
            return back()->with("error", $th->getMessage())->withInput();
        }

    }
    public function delete(Module $module)
    {
        try {
            $module->delete();
            return redirect('admin/modules')->with("success", "Modul ".$module->title. " Deleted");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
}
