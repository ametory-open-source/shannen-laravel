<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\InvitationsDataTable;

class InvitationsController extends Controller
{
    public function index(InvitationsDataTable $dataTable)
    {
        return $dataTable->render('invitations.index');
    }
}