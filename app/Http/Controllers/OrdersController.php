<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\OrdersDataTable;
use App\Models\Order;
use App\Services\Biteship;

class OrdersController extends Controller
{
    public function index(OrdersDataTable $dataTable)
    {
        return $dataTable->render('orders.index');
    }

    public function process(Order $order)
    {
        try {

            $order->status = "PROCESSING";
            $order->update();
            return back()->with('success', "Order ".$order->order_code. " set to Processing");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }


    }
    public function request_pickup(Order $order)
    {


        try {
            if ($order->shipping_cost->id != "SELF_PICKUP") {
                $biteship = new Biteship();
                $result = $biteship->orderCourier($order);
                $orderResult = $result->json();
                $order->shipping_order = $orderResult;
            }
            $order->status = "REQUEST_PICKUP";
            $order->update();
            return back()->with('success', "Order ".$order->order_code. " set to REQUEST_PICKUP");
        } catch (\Exception $e) {
            \Log::info("ERROR REQUEST PICKUP ", $e->getTrace());
            return back()->with('error', $e->getMessage())->withInput();
        }


    }
    public function shipping(Order $order)
    {
        try {

            $order->status = "SHIPPING";
            $order->update();
            return back()->with('success', "Order ".$order->order_code. " set to SHIPPING");
        } catch (\Exception $e) {
            \Log::info("ERROR REQUEST SHIPPING ", $e->getTrace());
            return back()->with('error', $e->getMessage())->withInput();
        }


    }
    public function updateReceipt(Request $request, Order $order) {
        $path = $request->file('file')->store('public/images', 'local');
        $order->payment_receipt = str_replace("public", "", $path);
        $order->update();
        return response()->json([
            "path" => str_replace("public", "", $path),
        ]) ;
    }
    public function shipped(Order $order)
    {
        try {


            $order->status = "SHIPPED";
            $order->update();
            return back()->with('success', "Order ".$order->order_code. " set to SHIPPED");
        } catch (\Exception $e) {
            \Log::info("ERROR REQUEST SHIPPED ", $e->getTrace());
            return back()->with('error', $e->getMessage())->withInput();
        }


    }
    public function finished(Order $order)
    {
        try {

            if ($order->user->member) {

                if (!$order->user->member->have_purchased) {

                    if ($order->total  > $order->user->member->type->first_minimum_order ) {
                        $order->user->member->update(["have_purchased" => true]);
                    }
                }
            }

            $order->status = "FINISHED";
            $order->update();
            return back()->with('success', "Order ".$order->order_code. " set to FINISHED");
        } catch (\Exception $e) {
            \Log::info("ERROR REQUEST FINISHED ", $e->getTrace());
            return back()->with('error', $e->getMessage())->withInput();
        }


    }

    public function trackingWebhook(Request $request)
    {
        // return response()->json(['message' => "order tracking updated"]);

        \Log::info("REQUEST FROM WEBHOOK", $request->all());
        \DB::enableQueryLog();
        try {
            if ($request->event == "order.waybill_id") {
                $orderData = \DB::table("orders")
                ->whereRaw('JSON_EXTRACT(orders.shipping_order, "$.id") = ?', [$request->order_id])
                // ->whereRaw('JSON_EXTRACT(orders.shipping_order, "$.courier.tracking_id") = ?', [$request->courier_tracking_id])
                // ->whereRaw('JSON_EXTRACT(orders.shipping_order, "$.courier.waybill_id") = ?', [$request->courier_waybill_id])

                // ->whereJsonContains('shipping_order->id', $request->order_id)
                // ->whereJsonContains('shipping_order->courier->tracking_id', $request->courier_tracking_id)
                // ->whereJsonContains('shipping_order->courier->waybill_id', $request->courier_waybill_id)
                ->first();

                $order = Order::find($orderData->id);
                switch ($request->status) {
                    case 'allocated':
                        $order->status = "PICKING_UP";
                        $history = [
                            "note" => "Courier is allocated and ready to pick up",
                            "status" => "allocated",
                            "updated_at" => now()->format("Y-m-d H:i:s"),
                        ];

                        break;
                    case 'picking_up':
                        $order->status = "PICKING_UP";
                        $history = [
                            "note" => "Courier is on the way to pick up location",
                            "status" => "picking_up",
                            "updated_at" => now()->format("Y-m-d H:i:s"),
                        ];

                        break;
                    case 'picked':
                        $order->status = "SHIPPING";
                        $history = [
                            "note" => "Item has been picked by courier",
                            "status" => "picked",
                            "updated_at" => now()->format("Y-m-d H:i:s"),
                        ];
                        break;
                    case 'dropping_off':
                        $order->status = "DROPPING_OFF";
                        $history = [
                            "note" => "Courier is dropping off item to destination",
                            "status" => "dropping_off",
                            "updated_at" => now()->format("Y-m-d H:i:s"),

                        ];
                        break;
                    case 'delivered':
                        $order->status = "SHIPPED";
                        $history = [
                            "note" => "Order has been delivered",
                            "status" => "delivered",
                            "updated_at" => now()->format("Y-m-d H:i:s"),
                        ];
                        break;
                }

                $tracking_order = $order->tracking_order;
                if (!isset($tracking_order["history"])) {
                    $tracking_order["history"] = [];
                }

                $histories = collect($tracking_order["history"]);
                $histories->push($history);

                $tracking_order["history"] = $histories->toArray();
                $tracking_order["waybill_id"] = $request->courier_waybill_id;

                $order->tracking_order = $tracking_order;
                \Log::info("ORDER ".$order->order_code. " UPDATED TO : ". $order->status);
                $order->update();
            }

            return response()->json(['message' => "order tracking updated"]);
        } catch (\Throwable $th) {
            \Log::info("WEBHOOK ERROR".$order->order_code. " : ".  $th->getMessage());
            return response()->json(
                ['message' => $th->getMessage()]
            , 400);
        }

    }

    public function update(Request $request, Order $order)
    {

        try {
                $order->update([
                "status" => $request->status,
                "remarks" => $request->remarks,
                 ]);
            return back()->with('success', "Order ".$order->order_code. " set to ".$request->status);
        } catch (\Exception $e) {
            \Log::info("ERROR REQUEST ".$request->status, $e->getTrace());
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
    public function tracking(Order $order)
    {
        try {


            $biteship = new Biteship();
            $result = $biteship->tracking($order);
            $orderResult = $result->json();
            dd($orderResult);
            $order->tracking_order = $orderResult;
            $order->update();
            if ($endStatus = end($orderResult["history"])) {
                if ($endStatus) {
                    if(isset($endStatus["status"])) {
                        if ($endStatus["status"] == "picked" && $order->status != "SHIPPING" ) {

                            return back()->with('picked', "Order ".$order->order_code. " Tracking Updated");
                        }
                    }
                }
            }
            return back()->with('success', "Order ".$order->order_code. " Tracking Updated");
        } catch (\Exception $e) {
            // dd($e);
            \Log::info("ERROR REQUEST SHIPPING ", $e->getTrace());
            return back()->with('error', $e->getMessage())->withInput();
        }


    }
    public function show(Order $order)
    {
        return view("orders.show", compact('order'));
    }
}
