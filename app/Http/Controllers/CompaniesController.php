<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\CompaniesDataTable;

class CompaniesController extends Controller
{
    public function index(CompaniesDataTable $dataTable)
    {
        return $dataTable->render('Companies.index');
    }
}