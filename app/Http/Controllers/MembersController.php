<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\MembersDataTable;
use App\Models\Member;
use App\Models\MemberType;
use App\Notifications\SendApprovalMember;
class MembersController extends Controller
{
    public function index(MembersDataTable $dataTable)
    {
        return $dataTable->render('members.index');
    }

    public function edit(Request $request, Member $member)
    {
        $member_types = MemberType::where("flag", true)->get()->pluck("name", "id");
        return view("members.edit", compact("member", "member_types"));
    }
    public function update(Request $request, Member $member)
    {
        try {

            $status = $member->status;

            $member->status = $request->status;

            $member->member_type_id = $request->member_type_id;


            if ($request->status == "ACTIVE") {
                $member->approved_by = auth()->user()->id;
                $member->approved_at = now();
            }
            if ($request->status == "BLOCKED") {
                $member->blocked_by = auth()->user()->id;
                $member->blocked_at = now();
            }





            $member->save();
            if ($request->status != "PENDING" && $status != $request->status)  {
                $member->user->notify(new SendApprovalMember($member));
            }
            return redirect("admin/members")->with("success", "Member Updated");
        } catch (\Throwable $th) {
            return back()->with("error", $th->getMessage());
        }

    }
}
