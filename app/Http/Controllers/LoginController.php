<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Password;
use App\Models\Admin;
use Illuminate\Support\Str;
use App\Notifications\UserForgotPassword;
class LoginController extends Controller
{
    /**
     * Display login page.
     *
     * @return Renderable
     */
    public function show()
    {
        return view('auth.login');
    }
    public function forgot()
    {
        return view('auth.forgot');
    }

    public function forgotAction(Request $request)
    {
        try {
            $input = $request->except(["_token", "_method"]);
            if (!Admin::where("email", $input["email"])->count()) {
                throw new \Exception( "Email tidak terdaftar");
            }

            $newPassword = Str::upper(Str::random(7));
            $user = Admin::where("email", $input["email"])->first();
            $user->password = $newPassword;
            $user->update();
            $user->notify(new UserForgotPassword($user, $newPassword));
            return back()->withSuccess("Reset password berhasil silakan cek email, untuk proses berikutnya");
        } catch (\Throwable $th) {
            dd($th);
            return back()->withErrors($th->getMessage())->withInput();
        }

    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        Auth::guard(env("APP_AFFILIATE_GUARD"))->logout();
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $request->session()->regenerate();
            // SET MODE ADMIN
            $request->session()->put('mode', 'admin');
            return redirect()->intended('admin/dashboard');
        }

        return back()->withErrors([
            'email' => 'Email admin tidak terdaftar atau password yang anda masukan salah',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('admin/login');
    }
}
