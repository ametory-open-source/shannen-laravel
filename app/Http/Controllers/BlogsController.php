<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\BlogsDataTable;
use App\Models\BlogCategory;
use App\Models\Blog;
use Illuminate\Validation\Rule;


class BlogsController extends Controller
{
    public function index(BlogsDataTable $dataTable)
    {
        return $dataTable->render('blogs.index');
    }
    public function create()
    {
        $categories = BlogCategory::get();
        return view("blogs.create", compact("categories"));
    }

    public function edit(Request $request, Blog $blog)
    {
        $categories = BlogCategory::get();
        return view("blogs.edit", compact("blog", "categories"));
    }

    public function update(Request $request, Blog $blog)
    {
        try {
            $validated = $request->validate([
                'title' => 'required',
                "slug" => [
                    'required',
                    Rule::unique('blogs')->ignore($blog->id),
                ]
            ]);
            $input = $request->except(["_token", "files"]);
            $input["keywords"] = "";
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $input["comment_enabled"] = isset($input["comment_enabled"]) ? true : false;
            $input["comment_moderation"] = isset($input["comment_moderation"]) ? true : false;

            $blog->update($input);
            return redirect()->route("blogs.index")->with('success', "Blog ".$request->get("title"). " Updated");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'title' => 'required',
                "slug" => 'required|unique:blogs',
            ]);
            $input = $request->except(["_token", "files"]);
            $input["keywords"] = "";
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();
            $input["comment_enabled"] = isset($input["comment_enabled"]) ? true : false;
            $input["comment_moderation"] = isset($input["comment_moderation"]) ? true : false;

            Blog::create($input);
            return redirect()->route("blogs.index")->with('success', "Blog ".$request->get("title"). " Saved");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }

    public function delete(Blog $blog)
    {
        try {
            $blog->delete();
            return redirect('admin/blogs');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
}
