<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ProductsDataTable;
use App\Models\ProductCategory;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Str;
use App\Http\Requests\ProductRequest;


class ProductsController extends Controller
{
    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('products.index');
    }

    public function create()
    {
        $product = new Product;
        $categories = ProductCategory::all();
        return view('products.form', compact("product", "categories"));
    }

    public function edit( Product $product)
    {
        $categories = ProductCategory::all();
        return view('products.form', compact("product", "categories"));
    }

    public function store(ProductRequest $request)
    {

        try {

            $input = $request->except(["_token", "feature_image", "picture", "caption"]);
            if ($input["promo_tag"] == "custom") {
                $input["promo_tag"] = null;
            }

            foreach([ "materials", "other_info", "short_description","tags"] as $item) {
                if (!$input[$item]) {
                    $input[$item] = "";
                }
            }

            foreach ([ "is_feature_product", "is_top_sale", "review_enabled", "review_moderation"] as $key => $item) {
                $input[$item] = isset($input[$item]) ? true : false;
            }

            $input["keywords"] = "";

            $product = Product::create($input);
            if ($request->has("picture") && is_array($request->get("picture"))) {
                foreach ($request->get("picture") as $key => $picture) {
                    $caption = $request->get("caption")[$key];
                    if (isset($request->get("item_id")[$key])) {
                        $galleryItem = ProductImage::find($request->get("item_id")[$key]);
                        $galleryItem->update([
                            "picture" => $picture,
                            "description" => $caption ?? "",
                            "product_id" => $product->id,
                        ]);
                    } else {
                        ProductImage::create([
                            "picture" => $picture,
                            "description" => $caption ?? "",
                            "is_cover" => $key == 0,
                            "product_id" => $product->id,
                        ]);
                    }

                }
            }

            return redirect("/admin/products")->with("success", $request->name . " SAVED");
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];
            if (str_contains($message, "A name is required")) {
                $message = "Nama Produk tidak boleh kosong";
            }
            if (str_contains($message, "Column 'slug' cannot be null")) {
                $message = "URL Slug [url] tidak boleh kosong";
            }
            if (str_contains($message, "products.products_slug_unique")) {
                $message = "Duplikat entri URL Slug [url]";
            }
            if (str_contains($message, "sku")) {
                $message = "SKU tidak boleh kosong";
            }
            return back()->with("error", $request->name . " ERROR (".$message.")")->withInput();
        } catch (\Throwable $th) {
            dd($th);
            return back()->with("error", $request->name . " ERROR")->withInput();
        }

    }

    public function update(ProductRequest $request,  Product $product)
    {
        try {

            $input = $request->except(["_token", "feature_image", "picture", "caption"]);
            if ($input["promo_tag"] == "custom") {
                $input["promo_tag"] = null;
            }

            foreach([ "materials", "other_info", "short_description", "tags"] as $item) {
                if (!$input[$item]) {
                    $input[$item] = "";
                }
            }

            foreach ([ "is_feature_product", "is_top_sale", "review_enabled", "review_moderation"] as $key => $item) {
                $input[$item] = isset($input[$item]) ? true : false;
            }

            $product->update($input);
            if ($request->has("picture") && is_array($request->get("picture"))) {
                foreach ($request->get("picture") as $key => $picture) {
                    $caption = $request->get("caption")[$key];
                    if (isset($request->get("item_id")[$key])) {
                        $galleryItem = ProductImage::find($request->get("item_id")[$key]);
                        $galleryItem->update([
                            "picture" => $picture,
                            "description" => $caption ?? "",
                            "product_id" => $product->id,
                        ]);
                    } else {
                        ProductImage::create([
                            "picture" => $picture,
                            "description" => $caption ?? "",
                            "is_cover" => $key == 0,
                            "product_id" => $product->id,
                        ]);
                    }

                }
            }

            return redirect("/admin/products")->with("success", $request->name . " SAVED");
        } catch (\Illuminate\Database\QueryException $e) {

            $message = $e->errorInfo[2];
            if (str_contains($message, "A name is required")) {
                $message = "Nama Produk tidak boleh kosong";
            }
            if (str_contains($message, "Column 'slug' cannot be null")) {
                $message = "URL Slug [url] tidak boleh kosong";
            }
            if (str_contains($message, "products.products_slug_unique")) {
                $message = "Duplikat entri URL Slug [url]";
            }
            if (str_contains($message, "sku")) {
                $message = "SKU tidak boleh kosong";
            }

            return back()->with("error", $request->name . " ERROR (".$message.")")->withInput();
        } catch (\Throwable $th) {

            return back()->with("error", $request->name . " ERROR ". $th->getMessage())->withInput();
        }

    }

    public function cover(Request $request, Product $product)
    {
        \DB::enableQueryLog();
        $productImage = ProductImage::find($request->item_id);
        ProductImage::query()->where('product_id', '=', $product->id)->update(['is_cover' => false]);
        $productImage->is_cover = true;
        $productImage->save();
        // dd(\DB::getQueryLog());
        return back()->with('success', "Gallery Cover Updated");
    }

    public function delete(Request $request, Product $product)
    {
        ProductImage::where('product_id', $product->id)->delete();
        \DB::table("wishlist_product")->where([
            "product_id" => $product->id,
        ])->delete();
        $product->delete();
        return back()->with('success', "Product Deleted");
    }

    public function deleteItem(Request $request, Product $product)
    {
        \DB::enableQueryLog();
        $productImage = ProductImage::find($request->item_id)->delete();
        if(ProductImage::where("is_cover", true)->count() == 0 && ProductImage::count()) {
            ProductImage::first()->update(["is_cover" => true]);
        }
        // dd(\DB::getQueryLog());
        return back()->with('success', "Gallery Item Deleted");
    }

}
