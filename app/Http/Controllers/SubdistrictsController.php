<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\SubdistrictsDataTable;

class SubdistrictsController extends Controller
{
    public function index(SubdistrictsDataTable $dataTable)
    {
        return $dataTable->render('subdistricts.index');
    }
}