<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\MemberTypesDataTable;

class MemberTypesController extends Controller
{
    public function index(MemberTypesDataTable $dataTable)
    {
        return $dataTable->render('member_types.index');
    }
}