<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\VillagesDataTable;

class VillagesController extends Controller
{
    public function index(VillagesDataTable $dataTable)
    {
        return $dataTable->render('villages.index');
    }
}