<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\AffiliatesDataTable;

class AffiliatesController extends Controller
{
    public function index(AffiliatesDataTable $dataTable)
    {
        return $dataTable->render('affiliates.index');
    }
}