<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ContactusesDataTable;
use App\Models\ContactUs;
class ContactusesController extends Controller
{
    public function index(ContactusesDataTable $dataTable)
    {
        return $dataTable->render('contact_uses.index');
    }

    public function readed(ContactUs $contact_us)
    {
        try {
            $contact_us->readed_at = now();
            $contact_us->update();
            return redirect('admin/contact_us');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
    public function delete(ContactUs $contact_us)
    {
        try {
            $contact_us->delete();
            return redirect('admin/contact_us');
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
