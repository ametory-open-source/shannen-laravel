<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Web;
use App\Models\Module;
use App\Models\Order;
use App\Models\Newsletter;
use App\Models\Page;
use App\Models\Blog;
use App\Models\Gallery;
use App\Models\BlogCategory;
use App\Models\ProductCategory;
use App\Models\Comment;
use App\Models\ContactUs;
use App\Models\Product;
use Vedmant\FeedReader\Facades\FeedReader;
use Illuminate\Support\Facades\Http;
use KiriminAja\Services\KiriminAja;
use App\Services\Payment;
use App\Facades\RajaOngkir;

class LandingController extends Controller
{
    public function main()
    {



        $web = Web::first();
        if (!$web) $web = new Web();



        // dd(RajaOngkir::setHeaders([
        //     "Content-Type" => "application/x-www-form-urlencoded"
        // ])->getCost([
        //     "origin" => $web->company_subdistrict_id,
        //     "originType" => "subdistrict",
        //     "destination" => auth(env('APP_AFFILIATE_GUARD'))->user()->default_address->subdistrict_id,
        //     "destinationType" => "subdistrict",
        //     "weight" => 1700,
        //     "courier" => "jne",
        //     ]
        // ));

        $main_banners = Module::where("type", "MAIN_BANNER")->limit(3)->where('flag', true)->orderBy("sort")->get();
        $feature_banners = Module::where("type", "FEATURE_BANNER")->limit(3)->where('flag', true)->orderBy("sort")->get();
        $categories = ProductCategory::where('flag', true)->where('is_featured', true)->orderBy("sort")->limit(6)->get();
        // $features = Module::where("type", "FEATURE")->where('flag', true)->limit(6)->orderBy("sort")->get();
        // $clients = Module::where("type", "CLIENT")->where('flag', true)->orderBy("sort")->get();
        // $showcases = Module::where("type", "SHOWCASE")->where('flag', true)->limit(3)->orderBy("sort")->get();
        // $platforms = Module::where("type", "PLATFORM")->where('flag', true)->limit(6)->orderBy("sort")->get();
        $blogs = Blog::where("status", "PUBLISHED")->orderBy("created_at", "desc")->limit(3)->get();

        $top_sales = Product::where("status", "PUBLISHED")->where("is_top_sale", 1)->limit(6)->get();
        $feature_products = Product::where("status", "PUBLISHED")->where("is_feature_product", 1)->limit(3)->get();
        // dd($top_sales);
        $header_transparent  = true;

        return view("frontend.home", compact("web","main_banners", "categories", "top_sales", "feature_banners", "feature_products", "blogs", "header_transparent"));
    }

    public function save_template(Request $request)
    {
        try {
            $input = $request->except(["_token"]);
            $input = collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray();


            foreach (config("landing.pictures") as $key => $pic) {
                if ($request->has($pic)) {
                    $path = $request->file($pic)->store('public/images', 'local');
                    $input[$pic] = str_replace("public", "", $path);
                }
            }


            $input["copyright"] = $input["copyright"] ?? "";

            foreach (config("landing.show_modules") as $key => $item) {
                $input[$item] = isset($input[$item]) ? true : false;
            }
            $input["is_online"] = isset($input["is_online"]) ? true : false;
            $input["affiliate_enabled"] = isset($input["affiliate_enabled"]) ? true : false;
            $input["member_enabled"] = isset($input["member_enabled"]) ? true : false;

            if (isset($input["province_id"])) {
                $input["company_province_id"] = $input["province_id"];
            }
            if (isset($input["city_id"])) {
                $input["company_city_id"] = $input["city_id"];
            }
            if (isset($input["subdistrict_id"])) {
                $input["company_subdistrict_id"] = $input["subdistrict_id"];
            }
            if (isset($input["village_id"])) {
                $input["company_village_id"] = $input["village_id"];
            }
            if (isset($input["postal_code"])) {
                $input["company_postal_code"] = $input["postal_code"];
            }

            $web = Web::first();
            if ($web) {
                $web->update($input);
            } else {
                Web::create($input);
            }
            return back()->with("success", "Template Updated");
        } catch (\Throwable $th) {
            dd($th);
        }


    }

    public function upload(Request $request)
    {
        $path = $request->file('file')->store('public/images', 'local');
        $data = [
            "file" => str_replace("public", "", $path),
            "url" => assetUrl(str_replace("public", "", $path)),
        ];
        return response()->json($data) ;
    }

    public function template()
    {
        $web = Web::first();
        if (!$web) $web = new Web();
        return view("template", compact("web"));
    }

    public function page(Request $request, $slug)
    {
        $top_sales = [];
        $feature_products = [];
        $header_transparent  = false;
        $web = Web::first();
        $page = Page::where("slug", $slug)->where("status", "PUBLISHED")->firstOrFail();

        return view('frontend.static.page', compact("page", "web",
        "top_sales",
        "feature_products",
        "header_transparent",
        ));
    }

    public function gallery(Request $request, Gallery $gallery)
    {
        $web = Web::first();
        return view('frontend.static.gallery', compact("gallery", "web"));
    }

    public function galleryList(Request $request)
    {

        $web = Web::first();
        $galleries = Gallery::where("status", "PUBLISHED")->orderBy("galleries.created_at", "desc")->paginate();

        return view("frontend.static.galleryList", compact("web", "galleries"));
    }

    public function contact(Request $request)
    {

        $web = Web::first();

        $top_sales = [];
        $feature_products = [];
        $header_transparent  = false;
        return view("frontend.static.contact", compact("web",
        "top_sales",
        "feature_products",
        "header_transparent",
    ));
    }
    public function contactSend(Request $request)
    {

        try {
            $input = $request->except(["_token"]);
            ContactUs::create($input);

            // $request->validate([
            //     'description' => ['required']
            // ]);
            return response()->json(["message" => "Terimakasih atas partisipasi anda"]);
        } catch (\Throwable $th) {

            // dd($th);
            return response()->json(["message" => $th->getMessage()], 400);
            // return back()->with("error", "Something error")->withInput();
        }
    }
    public function blogList(Request $request)
    {
        $web = Web::first();
        $blogs = Blog::where("status", "PUBLISHED")->when($request->has('category') && $request->get('category') != "", function($q) use($request) {
            $q->join('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')->where("blog_categories.name", $request->get("category"));
        })->when($request->has('search') && $request->get('search') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("title", "like", "%".$request->search."%");
                $q->orWhere("description", "like", "%".$request->search."%");
            });
        })->when($request->has('tag') && $request->get('tag') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("tags", "like", "%".$request->tag."%");
            });
        })->orderBy("blogs.updated_at", "desc")->limit(2)->get();


        $rights = Blog::where("status", "PUBLISHED")->when($request->has('category') && $request->get('category') != "", function($q) use($request) {
            $q->join('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')->where("blog_categories.name", $request->get("category"));
        })->when($request->has('search') && $request->get('search') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("title", "like", "%".$request->search."%");
                $q->orWhere("description", "like", "%".$request->search."%");
            });
        })->orderBy("blogs.updated_at", "desc")->offset(2)->limit(2)->get();

        $bottoms = Blog::where("status", "PUBLISHED")->when($request->has('category') && $request->get('category') != "", function($q) use($request) {
            $q->join('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')->where("blog_categories.name", $request->get("category"));
        })->when($request->has('search') && $request->get('search') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("title", "like", "%".$request->search."%");
                $q->orWhere("description", "like", "%".$request->search."%");
            });
        })->orderBy("blogs.updated_at", "desc")->offset(4)->limit(2)->get();

        $others = Blog::where("status", "PUBLISHED")->when($request->has('category') && $request->get('category') != "", function($q) use($request) {
            $q->join('blog_categories', 'blogs.blog_category_id', '=', 'blog_categories.id')->where("blog_categories.name", $request->get("category"));
        })->when($request->has('search') && $request->get('search') != "", function($q) use($request) {
            $q->where(function($q)  use($request)  {
                $q->where("title", "like", "%".$request->search."%");
                $q->orWhere("description", "like", "%".$request->search."%");
            });
        })->orderBy("blogs.updated_at", "desc")->offset(6)->limit(9)->get();

        $categories = BlogCategory::all();

        $recents = Blog::where("status", "PUBLISHED")->orderBy('created_at', "desc")->limit(5)->get();

        $categories = BlogCategory::get();

        $getTags = Blog::where("status", "PUBLISHED")->get(["tags"]);
        $tags = [];
        foreach($getTags as $tag) {
            if ($tag->tags)
            $tags = array_merge($tags, explode(",", $tag->tags));
        }

        $top_sales = [];
        $feature_products = [];
        $header_transparent  = false;
        $blog_banner = Module::where("type", "BLOG_BANNER")->limit(1)->where('flag', true)->inRandomOrder()->first();

        return view("frontend.static.blogList", compact("web", "blogs", "categories" ,'recents', "categories", "tags", "top_sales",
        "feature_products",
        "header_transparent", "rights", "bottoms", "others", "blog_banner"));
    }
    public function blog(Request $request, $slug)
    {
        $web = Web::first();
        $blog = Blog::where("slug", $slug)->where("status", "PUBLISHED")->firstOrFail();

        $prev = Blog::where("id", "<", $blog->id)->where("status", "PUBLISHED")->first();
        $next = Blog::where("id", ">", $blog->id)->where("status", "PUBLISHED")->first();

        if ($blog->short_description) {
            $web->description = $blog->short_description;
        }
        if ($blog->feature_image) {
            $web->image = assetUrl($blog->feature_image);
        }
        if ($blog->keywords) {
            $web->keywords = $blog->keywords;
        }

        $web->name .= " | ". $blog->title;

        $relateds = Blog::where("blog_category_id", $blog->blog_category_id)->where("status", "PUBLISHED")->where('id', "<>", $blog->id)->get();
        $recents = Blog::where("status", "PUBLISHED")->orderBy('created_at', "desc")->where('id', "<>", $blog->id)->limit(5)->get();

        $categories = BlogCategory::get();

        $getTags = Blog::where("status", "PUBLISHED")->get(["tags"]);
        $tags = [];
        foreach($getTags as $tag) {
            if ($tag->tags)
            $tags = array_merge($tags, explode(",", $tag->tags));
        }

        $top_sales = [];
        $feature_products = [];
        $header_transparent  = false;

        $blog_banner = Module::where("type", "BLOG_BANNER")->limit(1)->where('flag', true)->inRandomOrder()->first();

        return view('frontend.static.blog', compact("blog", "web", "prev", "next", "relateds", 'recents', "categories", "tags", "top_sales", "feature_products", "header_transparent", "blog_banner"));
    }

    public function postComment(Request $request, Blog $blog)
    {
        try {
            $input = $request->except(["_token"]);
            $input["status"] = $blog->comment_moderation ? "DRAFT" : "PUBLISHED";
            $msg = $blog->comment_moderation ? "Your comment will appear after being moderated by the admin" : "Thank you for your participation
            ";
            $input["blog_id"] = $blog->id;
            Comment::create($input);
            return back()->with("success", $msg);
        } catch (\Throwable $th) {

            // dd($th);
            return back()->with("error", "Something error")->withInput();
        }

    }

    public function newsletter(Request $request)
    {
        try {
            Newsletter::create($request->all());
            return response()->json(['status' => 'success', 'message' => "Terima  Kasih Telah Berlangganan"]);
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];

            if (str_contains($message, "Duplicate entry")) {
                $message = "EMAIL SUDAH TERDAFTAR";
            }
            return response()->json(['status' => 'failed', 'message' => $message]);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'failed', 'message' => "Berlanganan Gagal"]);
        }

    }

    public function pricing()
    {
        $web = Web::first();
        $pricings = Module::where("type", "PRICING")->where('flag', true)->get();

        return view("frontend.static.pricing", compact("web", "pricings"));
    }

    public function faq()
    {
        $web = Web::first();
        $faqs = Module::where("type", "FAQ")->where('flag', true)->get();

        return view("frontend.static.faq", compact("web", "faqs"));
    }

    public function post_auth_login(Request $request)
    {

        try {
            $response = Http::post(env("APP_BASE_URL")."/api/v2/Login", [
                'username' => $request->email,
                'password' => $request->password,
                'device' => "web"
            ]);

            if ($response->status() == 200) {
                $resp = $response->json();
                $token = $resp["token"];
                $default_company_id = $resp["default_company_id"];

                $link = env('APP_FRONTEND_URL')."/login?company_id=".$default_company_id."&token=".$token;
                return back()->with("redirect", $link)->withInput();
            }
        } catch (\Throwable $th) {
            return back()->with("error", "Something error")->withInput();
        }
    }

    public function auth_login()
    {
        $web = Web::first();


        return view("static.login", compact("web"));
    }
    public function post_join(Request $request)
    {
        $input = $request->except("_token");

        // dd($input);
        try {
            $response = Http::post(env("APP_BASE_URL")."/api/v2/Join", $input);
            if ($response->status() == 200) {
                return back()->with("success", "Your submission will be reviewed by our admin, please wait for the confirmation email")->withInput();
            } else {
                throw new Exception($response->body());
            }
        } catch (\Throwable $th) {
            return back()->with("error", "Something error")->withInput();
        }
    }
    public function join()
    {
        $web = Web::first();


        return view("frontend.static.join", compact("web"));
    }


    public function products(Request $request)
    {

        return view("frontend.static.products");
    }


    public function cart(Request $request)
    {


        return view("frontend.static.cart");
    }
    public function checkout(Request $request)
    {
        if ($user = auth(env('APP_AFFILIATE_GUARD'))->user()) {
            if (!$user->default_address) {
                return redirect("/profile?tab=address");
            }
        }

        return view("frontend.static.checkout");
    }
    public function order_success(Request $request, $order_id)
    {

        $order = Order::find($order_id);

        return view("frontend.static.order_success", compact("order"));
    }

    public function addWishlist(Request $request)
    {


        try {
            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            if ($user) {
                \DB::table("wishlist_product")->insert([
                    "product_id" => $request->product_id,
                    "user_id" => $user->id,
                ]);
            }
        } catch (\Throwable $th) {

        }
        return response()->json(["message" => "succeed"]);
    }

    public function deleteWishlist($id)
    {


        try {
            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            if ($user) {
                \DB::table("wishlist_product")->where([
                    "product_id" => $id,
                    "user_id" => $user->id,
                ])->delete();
            }
        } catch (\Throwable $th) {

        }
        return response()->json(["message" => "succeed"]);
    }


    public function search(Request $request)
    {

        $products = Product::search($request->get('query'))->paginate(6);
        $blogs = Blog::search($request->get('query'))->paginate(6);

        return view("frontend.search", compact("products", "blogs"));
    }

}
