<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\MessageRoomsDataTable;

class MessageRoomsController extends Controller
{
    public function index(MessageRoomsDataTable $dataTable)
    {
        return $dataTable->render('message_rooms.index');
    }
}