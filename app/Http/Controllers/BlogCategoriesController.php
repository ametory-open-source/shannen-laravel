<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\BlogCategoriesDataTable;
use App\Models\BlogCategory;

class BlogCategoriesController extends Controller
{
    public function index(BlogCategoriesDataTable $dataTable)
    {
        return $dataTable->render('blog_categories.index');
    }

    public function create()
    {
        return view("blog_categories.create");
    }

    public function edit(BlogCategory $blog_category)
    {
        return view("blog_categories.edit", compact("blog_category"));
    }

    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'name' => 'required',
            ]);

            $input = $request->except(["_token", "files"]);
            if (!isset($input["description"])) $input["description"] = "";
            BlogCategory::create(collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray());
            return redirect()->route("blog_categories.index")->with('success', "Category ".$request->get("name"). " Saved");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
    public function update(Request $request, BlogCategory $blog_category)
    {
        try {
            $validated = $request->validate([
                'name' => 'required',
            ]);
            $input = $request->except(["_token", "files"]);
            if (!isset($input["description"])) $input["description"] = "";
            $blog_category->update(collect($input)->map(function($d) {
                if (!$d) return "";
                return $d;
            })->toArray());
            return redirect("admin/blog_categories")->with('success', "Category ".$request->get("name"). " Updated");
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }

    public function delete(BlogCategory $blog_category)
    {
        try {
            $blog_category->delete();
            return redirect('admin/blog_categories')->with('success', "Category  Deleted");

        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
