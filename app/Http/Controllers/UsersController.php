<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\Models\User;
class UsersController extends Controller
{
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users.index');
    }

    public function edit(User $user)
    {
        return view("users.edit", compact("user"));
    }
    public function updateAvatar(Request $request, User $user)
    {
        if (!$request->has('file')) {
            return back()->withError("Please input file first");
        }


        $path = $request->file('file')->store('public/images', 'local');
        $path = str_replace("public", "", $path);
        $user->avatar = $path;
        $user->update();
        return back()->withSuccess("Avatar berhasil diupdate");
    }

    public function update(Request $request, User $user)
    {

        try {
            $phone = $user->phone_number;
            $input = $request->except(["_method", "_token"]);


            if ($input["phone_number"] != $phone) {
                $input["phone_verification_time"] = null;
            }


            $user->update($input);
            return back()->withSuccess("user berhasil diupdate");
        } catch(\Illuminate\Database\QueryException $e){
            return back()->withError($e->getMessage());
        } catch (\Throwable $e) {
            return back()->withError($e->getMessage());
        }

    }
    public function suspend(Request $request, User $user)
    {

        try {

            $user->suspended_at = now();
            $user->update();
            return back()->withSuccess("user berhasil di suspend");
        } catch(\Illuminate\Database\QueryException $e){
            return back()->withError($e->getMessage());
        } catch (\Throwable $e) {
            return back()->withError($e->getMessage());
        }

    }
    public function unsuspend(Request $request, User $user)
    {

        try {

            $user->suspended_at = null;
            $user->update();
            return back()->withSuccess("user berhasil di aktifkan kembali");
        } catch(\Illuminate\Database\QueryException $e){
            return back()->withError($e->getMessage());
        } catch (\Throwable $e) {
            return back()->withError($e->getMessage());
        }

    }
}
