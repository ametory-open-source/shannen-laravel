<?php

namespace App\Http\Controllers\Affiliator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Models\ProductReview;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    public function show(string $id)
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $order = Order::where('user_id', $user->id)->find($id);

        return view("frontend.affiliator.order", compact("order"));
    }
    public function review(string $id)
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $order = Order::where('user_id', $user->id)->where("status", "FINISHED")->findOrFail($id);

        return view("frontend.affiliator.review", compact("order"));
    }
    public function accepted(string $id)
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $order = Order::where('user_id', $user->id)->where("status", "SHIPPED")->findOrFail($id);

        return view("frontend.affiliator.accepted", compact("order"));
    }
    public function sendReview(Request $request, string $id)
    {
        try {
            $input = $request->all();
            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            $order = Order::where('user_id', $user->id)->where("status", "FINISHED")->findOrFail($id);


            foreach($input["reviews"] as $product_id => $val) {
                $data = [
                    "is_anonymous" => isset($val["anonym"]) ? true : false,
                    "rate" => $val["rate"],
                    "feedback" => $val["feedback"],
                    "user_id" => $user->id,
                    "product_id" => $product_id,
                    "order_id" => $order->id,
                ];
                ProductReview::create($data);
            }

            return redirect()->route("affOrder.show", ["id" => $order->id])->withSuccess("Terima kasih telah memberikan ulasan pemesanan");
        } catch (\Throwable $th) {
            \Log::error("ERROR REVIEW ". $th->getMessage());
            return redirect()->route("affOrder.show", ["id" => $order->id])->withError("Error dalam memberikan review, silakan hubungi admin");
        }

    }
    public function accepted_send(Request $request, string $id)
    {
        try {
            $input = $request->all();


            $files = collect([]);
            if ($request->has("images")) {
                foreach (json_decode ($request->get("images")) as $key => $img) {
                    $exp = explode("base64,",$img);
                    $filename = "images/". Str::random(40).".".str_replace(";","",explode("data:image/",$exp[0])[1]);
                    $data = base64_decode($exp[1]);
                    Storage::disk('public')->put($filename, $data);

                    $files->push($filename);

                }
            }


            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            $order = Order::where('user_id', $user->id)->where("status", "SHIPPED")->findOrFail($id);
            $order->images = json_encode($files);
            $order->status = "ORDER_ACCEPTED";
            $order->update();

            foreach($input["reviews"] as $product_id => $val) {
                $data = [
                    "is_anonymous" => isset($val["anonym"]) ? true : false,
                    "rate" => $val["rate"],
                    "feedback" => $val["feedback"],
                    "user_id" => $user->id,
                    "product_id" => $product_id,
                    "order_id" => $order->id,
                    "images" => json_encode($files),
                ];
                ProductReview::create($data);
            }

            return redirect()->route("affOrder.show", ["id" => $order->id])->withSuccess("Terima kasih telah berbelanja dan memberikan ulasan pemesanan");
        } catch (\Throwable $th) {
            \Log::error("ERROR REVIEW ". $th->getMessage());
            return redirect()->route("affOrder.show", ["id" => $order->id])->withError("Error dalam memberikan review, silakan hubungi admin");
        }

    }

    public function payment(string $id) {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $order = Order::where('user_id', $user->id)->find($id);
        $order->payment_method = "PAYMENT_GATEWAY";
        $order->update();
        $url = $order->createPaymentLink();
        return redirect($url);
    }
    public function update(Request $request, string $id) {
        try {
            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            $order = Order::where('user_id', $user->id)->find($id);

            if ($request->file) {
                $path = $request->file('file')->store('public/images', 'local');
                $path = str_replace("public", "", $path);
                $order->payment_receipt = $path;
                $order->status = "WAITING_FOR_ADMIN";
                $order->update();

                return back()->withSuccess("Upload bukti transfer berhasil");
            }
        } catch (\Throwable $th) {
            return  back()->withErrors($th->getMessage());
        }

    }
}
