<?php

namespace App\Http\Controllers\Affiliator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Member;
use App\Models\MemberType;
use App\Models\Affiliate;
use App\Models\Web;
use App\Models\User;
use App\Models\Address;
use App\Models\MessageRoom;
use App\Notifications\SendJoinRequest;
use App\Notifications\SendJoinMemberRequest;
class ProfileController extends Controller
{
    public function show()
    {


        return view('frontend.affiliator.profile');
    }

    public function change_password(Request $request)
    {
        $rePass = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/m';
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        try {
            if ($request->new_password != $request->confirm_password) {
                throw new \Exception("Password tidak sama");
            }

            if (!Hash::check($request->password, $user->password)) {
                throw new \Exception("Password yang anda masukan salah");
            }
            preg_match($rePass, $request->new_password, $matches2 );

            if (count($matches2) == 0) {
                throw new \Exception( "Password minimal 8 karakter,  mengandung angka dan huruf besar");
            }

            $user->password = $request->new_password;


            // dd($user->password);
            $user->update();
            return redirect("/profile?tab=account-info")->with("success", "Password baru berhasil di update");
        } catch (\Exception $e) {
            return redirect("/profile?tab=account-info")->withErrors($e->getMessage());
        }
    }

    public function profile_update(Request $request)
    {
        $re = '/^(^\+62|62|^0)(\d{3,4}-?){2}\d{3,4}$/m';
        try {
            $input = $request->except("_token");
            $validated = $request->validate([
                'name' => 'required',
                "phone_number" => 'required',
            ]);

            preg_match($re, $input["phone_number"], $matches );

            if (count($matches) == 0) {
                throw new \Exception( "Format nomor telepon tidak sesuai standar");
            }

            if (User::where("phone_number", $input["phone_number"])->count()) {
                throw new \Exception( "Nomor Telepon sudah dipakai");
            }


            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            if ($user->phone_number != $request->phone_number) {
                $input["phone_verification_time"] = null;
            }
            if ( $request->avatar) {
                $path = $request->file('avatar')->store('public/images', 'local');
                $input["avatar"] = str_replace("public", "", $path);
            }

            $user->update($input);
            return redirect("/profile?tab=account-info")->with("success", __("frontend.Data Updated"));
        } catch (\Throwable $e) {
            return redirect("/profile?tab=account-info")->withErrors($e->getMessage())->withInput();
        }

    }

    public function join_affiliate()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if ($user->affiliate) {
            return back()->withErrors(__("frontend.You have joined"));
        }
        return view('frontend.affiliator.join');
    }
    public function join_membership()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if ($user->member) {
            return back()->withErrors(__("frontend.You have joined"));
        }

        $memberships = MemberType::where('flag', true)->get();

        return view('frontend.member.join', compact('memberships'));
    }

    public function join_member_send(Request $request)
    {

        \DB::beginTransaction();
        $web = Web::first();
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();

        $input = $request->except(["_token"]);

        $validated = $request->validate([
            'member_type_id' => 'required',
            "province_id" => 'required',
            "city_id" => 'required',
            "subdistrict_id" => 'required',
            "village_id" => 'required',
            "postal_code" => 'required',
            "address" => 'required',
            "id_card" => 'required',
        ]);


        $path = $request->file('id_card')->store('public/images', 'local');
        $path = str_replace("public", "", $path);


        try {
            Member::create([
                "code" => Str::upper(Str::random(5)),
                "user_id" => $user->id,
                "member_type_id" => $request->member_type_id,
                "province_id" => $request->province_id,
                "city_id" => $request->city_id,
                "subdistrict_id" => $request->subdistrict_id,
                "village_id" => $request->village_id,
                "postal_code" => $request->postal_code,
                "address" => $request->address,
                "id_card" => $path,
            ]);
            $web->notify(new SendJoinMemberRequest($user));
            \DB::commit();
            return redirect("/profile")->with("success", __("frontend.member_request_message"));
        } catch (\Throwable $e) {
            \DB::rollback();
            return back()->withErrors($e->getMessage())->withInput();
        }
    }
    public function cancel_request(Request $request)
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if($user->member->status != "PENDING") {
            return back()->withErrors(__("frontend.cannot_cancel"))->withInput();
        }
        $user->member()->delete();
        return redirect("/profile")->with("success", __("frontend.cancel_request_succeed"));
    }
    public function join_affiliate_send(Request $request)
    {
        \DB::beginTransaction();
        $web = Web::first();
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        try {
            $data = [
                "username" => $request->username,
                "code" => Str::upper(Str::random(5)) ,
                "user_id" => $user->id,
                "address" => $request->address,
                "picture" => "",
                "cover" => "",
                "display_name" => $request->display_name,
            ];
            if (Affiliate::where('username', $request->username)->count()) {
                throw new \Exception("username already taken");
            }

            if ($request->username == "products") {
                throw new \Exception("username is forbidden");
            }

            Affiliate::create($data);
            $web->notify(new SendJoinRequest($user));
            \DB::commit();
            return redirect("/profile")->with("success", "Data Anda akan ditinjau oleh tim kami. Jika disetujui, kami akan menghubungi Anda melalui email atau telepon, Terima Kasih");
        } catch (\Throwable $e) {
            \DB::rollback();
            return back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function add_address()
    {
        $address = new Address;
        return view('frontend.affiliator.add_address', compact('address'));
    }

    public function update_affiliate(Request $request)
    {
        try {

            $input = $request->except("_token");
            if ( $request->picture) {
                $path = $request->file('picture')->store('public/images', 'local');
                $input["picture"] = str_replace("public", "", $path);
            }
            if ( $request->cover) {
                $path = $request->file('cover')->store('public/images', 'local');
                $input["cover"] = str_replace("public", "", $path);
            }
            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            $user->affiliate->update($input);
            return back()->with("success", __("frontend.Data Updated"));
        } catch (\Throwable $e) {
            return back()->withErrors($e->getMessage())->withInput();
        }

    }

    public function add_address_send(Request $request)
    {
        try {
            $validated = $request->validate([
                'label' => 'required',
                'recipient_name' => 'required',
                "phone_number" => 'required',
                "address" => 'required',
                "province_id" => 'required',
                "city_id" => 'required',
                "subdistrict_id" => 'required',
                "village_id" => 'required',
                "postal_code" => 'required',
            ]);

            $user = auth(env('APP_AFFILIATE_GUARD'))->user();
            $input = $request->except(["_token"]);
            $input["user_id"] = $user->id;
            $input["is_default"] = isset($input['is_default']) ? true : false;
            $input["notes"] = $input['notes'] ?? "";
            // dd($input);
            $address = Address::create($input);
            if ($request->is_default) {
                $user = auth(env('APP_AFFILIATE_GUARD'))->user();
                $user->update([
                    "address_id" => $address->id,
                ]);
            }
            return redirect('/profile?tab=address')->with("success", __("frontend.Address Added"));
        } catch (\Throwable $e) {
            dd($e);
            return back()->withErrors("Harap isi kolom dengan tanda merah")->withInput();
        }
    }

    public function delete_address($id)
    {
        try {
            $address = Address::find($id);
            $address->delete();
            return back()->with("success", "Alamat berhasil dihapus");
        } catch (\Throwable $e) {
            return back()->withErrors($e->getMessage())->withInput();
        }
    }
    public function show_address($id)
    {
        $address = Address::find($id);
        return view('frontend.affiliator.add_address', compact('address'));
    }
    public function update_address(Request $request, $id)
    {
        try {
            $validated = $request->validate([
                'label' => 'required',
                'recipient_name' => 'required',
                "phone_number" => 'required',
                "email" => 'required',
                "address" => 'required',
                "province_id" => 'required',
                "city_id" => 'required',
                "subdistrict_id" => 'required',
                "villlage_id" => 'required',
                "postal_code" => 'required',
            ]);

            $input = $request->except(['_token', '_method']);
            $address = Address::find($id);
            $input["is_default"] = isset($input['is_default']) ? true : false;
            $input["notes"] = $input['notes'] ?? "";
            $address->update($input);
            if ($request->is_default) {
                $user = auth(env('APP_AFFILIATE_GUARD'))->user();
                $user->update([
                    "address_id" => $id,
                ]);
            }
            return redirect('/profile?tab=address')->with("success", "Alamat berhasil di update");
        } catch (\Throwable $e) {
            return back()->withErrors($e->getMessage())->withInput();
        }

        // return view('frontend.affiliator.add_address', compact('address'));
    }
}
