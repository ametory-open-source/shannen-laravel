<?php

namespace App\Http\Controllers\Affiliator;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;
use App\Notifications\UserForgotPassword;

class AuthController extends Controller
{
    public function show()
    {

        if (auth(env("APP_AFFILIATE_GUARD"))->check()) {
            return redirect('/profile');
        } else if ($user = auth(env("APP_AFFILIATE_GUARD"))->viaRemember()) {
        if ($user) {
                return redirect('/profile');
            }
        }
        return view('frontend.affiliator.login');
    }
    public function forgot()
    {

        if (auth(env("APP_AFFILIATE_GUARD"))->check()) {
            return redirect('/profile');
        } else if ($user = auth(env("APP_AFFILIATE_GUARD"))->viaRemember()) {
        if ($user) {
                return redirect('/profile');
            }
        }
        return view('frontend.affiliator.forgot');
    }

    public function forgotAction(Request $request)
    {
        try {
            $input = $request->except(["_token", "_method"]);
            if (!$input["email"]) {
                throw new \Exception( "kolom email wajib diisi");
            }
            if (!User::where("email", $input["email"])->count()) {
                throw new \Exception( "Email tidak terdaftar");
            }

            $newPassword = Str::upper(Str::random(7));
            $user = User::where("email", $input["email"])->first();
            if (!$user) {
                throw new \Exception("Anda belum terdaftar");
            }
            if (!$user->email_verification_time) {
                throw new \Exception("User belum verifikasi email");
            }

            $user->password = $newPassword;
            $user->update();
            $user->notify(new UserForgotPassword($user, $newPassword));
            return back()->withSuccess("Reset password berhasil silakan cek email, untuk proses berikutnya");
        } catch (\Throwable $th) {
            return back()->withErrors($th->getMessage())->withInput();
        }


    }
    public function registerShow()
    {

        if (auth(env("APP_AFFILIATE_GUARD"))->check()) {
            return redirect('/profile');
        } else if ($user = auth(env("APP_AFFILIATE_GUARD"))->viaRemember()) {
        if ($user) {
                return redirect('/profile');
            }
        }
        return view('frontend.affiliator.register');
    }

    public function verification(User $user)
    {
        try {
            $user->email_verification_time = now();
            $user->save();

            auth(env("APP_AFFILIATE_GUARD"))->login($user);

            return redirect("/profile")->withSuccess("Verifikasi berhasil");
        } catch (\Throwable $th) {
            return redirect("login")->withErrors($th->getMessage());
        }

    }

    public function register(Request $request)
    {
        $re = '/^(^\+62|62|^0)(\d{3,4}-?){2}\d{3,4}$/m';
        $rePass = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/m';

        try {
            $input = $request->except(["_token", "_method"]);
            if (User::where("email", $input["email"])->count()) {
                throw new \Exception( "Email sudah dipakai");
            }
            if (User::where("phone_number", $input["phone_number"])->count()) {
                throw new \Exception( "Nomor Telepon sudah dipakai");
            }

            preg_match($re, $input["phone_number"], $matches );
            preg_match($rePass, $input["password"], $matches2 );

            if (count($matches) == 0) {
                throw new \Exception( "Format nomor telepon tidak sesuai standar");
            }

            if (count($matches2) == 0) {
                throw new \Exception( "Password minimal 8 karakter,  mengandung angka dan huruf besar");
            }
            User::create($input);
            return back()->withSuccess("Registrasi berhasil silakan cek email, untuk verifikasi");
        } catch (\Throwable $th) {
            return back()->withErrors($th->getMessage())->withInput();
        }




    }
    public function login(Request $request)
    {

        $request->session()->put('timezone', $request->timezone);
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);


        try {
            $user = User::where("email", $request->email)->first();

            if (!$user) {
                throw new \Exception("User tidak terdaftar");
            }

            if ($user->suspended_at) {
                throw new \Exception("Mohon maaf, akun anda telah di suspend");
            }
            if (!$user) {
                throw new \Exception("User belum terdaftar");
            }

            if (!$user->email_verification_time) {
                throw new \Exception("User belum verifikasi email");
            }

            if (Auth::guard(env("APP_AFFILIATE_GUARD"))->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                $request->session()->regenerate();

                if ($request->has("redirect_to")) {
                    return redirect()->intended($request->redirect_to);
                }
                return redirect()->intended('profile');
            }


            throw new \Exception("Login gagal, kemungkinan anda memasukan password yang  salah");

        } catch (\Exception $e) {
            // dd($e->getMessage());
            return back()->withErrors($e->getMessage())->withInput();
        }




    }

    public function logout(Request $request)
    {
        Auth::guard(env("APP_AFFILIATE_GUARD"))->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('login');
    }
}
