<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\DataTables\ProductCategoriesDataTable;
use App\Models\ProductCategory;
use Illuminate\Support\Str;

class ProductCategoriesController extends Controller
{
    public function index(ProductCategoriesDataTable $dataTable)
    {
        return $dataTable->render('product_categories.index');
    }

    public function create()
    {
        $product_category = new ProductCategory();
        return view("product_categories.form", compact("product_category"));
    }

    public function edit(ProductCategory $product_category)
    {
        return view("product_categories.form", compact("product_category"));
    }


    public function store(Request $request)
    {
        try {

            $input = $request->except(["_token"]);
            if (!$request->get("description")) {
                $input["description"] = ' ';
            }
            if ($input["promo_tag"] == "custom") {
                $input["promo_tag"] = null;
            }
            if (!$request->has("slug"))
            $input["slug"] = Str::slug($request->name, '-');;
            ProductCategory::create($input);
            return redirect('admin/product_categories')->with("success", $request->name . " Saved" );
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];
            if (str_contains($message, "A name is required")) {
                $message = "Nama Produk tidak boleh kosong";
            }
            if (str_contains($message, "Column 'slug' cannot be null")) {
                $message = "URL Slug [url] tidak boleh kosong";
            }
            if (str_contains($message, "product_categories.column")) {
                $message = "Duplikat entri URL Slug [url]";
            }
            if (str_contains($message, "sku")) {
                $message = "SKU tidak boleh kosong";
            }
            return back()->with("error", $request->name . " ERROR (".$message.")")->withInput();
        } catch (\Throwable $th) {

            return back()->with("error", $request->name . " ERROR")->withInput();
        }
    }

    public function update(Request $request, ProductCategory $product_category)
    {
        try {

            $input = $request->except(["_token"]);
            if (!$request->get("description")) {
                $input["description"] = ' ';
            }
            if ($input["promo_tag"] == "custom") {
                $input["promo_tag"] = null;
            }
            if (!$request->has("slug"))
            $input["slug"] = Str::slug($request->name, '-');
            $product_category->update($input);
            return redirect('admin/product_categories')->with("success", $product_category->name . " Updated" );
        } catch (\Illuminate\Database\QueryException $e) {
            $message = $e->errorInfo[2];
            if (str_contains($message, "A name is required")) {
                $message = "Nama Produk tidak boleh kosong";
            }
            if (str_contains($message, "Column 'slug' cannot be null")) {
                $message = "URL Slug [url] tidak boleh kosong";
            }
            if (str_contains($message, "product_categories.column")) {
                $message = "Duplikat entri URL Slug [url]";
            }
            if (str_contains($message, "sku")) {
                $message = "SKU tidak boleh kosong";
            }
            return back()->with("error", $request->name . " ERROR (".$message.")")->withInput();
        } catch (\Throwable $th) {

            return back()->with("error", $request->name . " ERROR")->withInput();
        }

    }
    public function delete(ProductCategory $product_category)
    {
        try {
            $product_category->delete();
            return redirect('admin/product_categories')->with("success", $product_category->name . " Deleted" );
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage())->withInput();
        }

    }
}
