<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Whitecube\LaravelTimezones\Facades\Timezone;

class Hr
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        Timezone::set(session('timezone') ?? 'Asia/Jakarta');
   
        if (!auth("hr")->check()) {
            return redirect('/login');
        }
        return $next($request);
    }
}
