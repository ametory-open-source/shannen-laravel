<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Whitecube\LaravelTimezones\Facades\Timezone;
class AffiliateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        Timezone::set(session('timezone') ?? 'Asia/Jakarta');

        if (!auth(env("APP_AFFILIATE_GUARD"))->check()) {
            return redirect('/login?redirect_to=/'.$request->path());
        }
        return $next($request);
    }
}
