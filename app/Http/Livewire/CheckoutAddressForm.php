<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Cache;
use App\Facades\RajaOngkir;

class CheckoutAddressForm extends Component
{
    public $address;
    public $addresses;

    protected $province_name;
    protected $city_name;
    protected $subdistrict_name;
    protected $listeners = ['addressChange' => 'addressChange'];



    public function mount() {
        $this->addresses = auth(env('APP_AFFILIATE_GUARD'))->user()->addresses;
        $this->getAddress();
    }


    public function addressChange($id)
    {
        $selected = $this->addresses->firstWhere("id", $id);
        // \Log::info("addressChange", $address);
        $this->address = $selected;
        $this->getAddress();
    }




    public function getAddress()
    {
        $this->emit("setLoading", true);
        $this->province_name  = optional($this->address->province)->name;
        $this->city_name  = optional($this->address->city)->name;
        $this->subdistrict_name  = optional($this->address->subdistrict)->name;
        $this->village_name  = optional($this->address->village)->name;
        $this->postal_code  = ($this->address->postal_code);
        // $this->isLoading = true;
        // if ($this->address->province_id) {
        //     $provinces = Cache::remember('province-'.$this->address->province_id, 3600, function () {
        //         return RajaOngkir::getProvince(["id" => $this->address->province_id]);
        //     });
        //     if (isset($provinces["province"])) {
        //         $this->province_name = $provinces["province"];
        //     }

        //     if ($this->address->city_id) {
        //         $cities = Cache::remember('city-'.$this->address->city_id, 3600, function () {
        //             return RajaOngkir::getCity(["province" => $this->address->province_id, "id" => $this->address->city_id]);
        //         });

        //         if (isset($cities["city_id"])) {
        //             $this->city_name = $cities["city_name"];
        //             if ($cities["type"] == "Kabupaten") {
        //                 $this->city_name = "Kabupaten ".$this->city_name;
        //             }
        //         }

        //         if ($this->address->subdistrict_id) {
        //             $subdistricts =  Cache::remember('subdistrict-'.$this->address->subdistrict_id, 3600, function () {
        //                 return RajaOngkir::getSubdistrict(["city" => $this->address->city_id, "id" => $this->address->subdistrict_id]);
        //             });
        //             if (isset($subdistricts["subdistrict_name"])) {
        //                 $this->subdistrict_name = $subdistricts["subdistrict_name"];
        //             }
        //         }

        //     }

        // }

        // $this->isLoading = false;
        $this->emit("setLoading", false);
    }


    public function render()
    {
        return view('livewire.checkout-address-form', [
            "address" => $this->address,
            "province_name" => $this->province_name,
            "city_name" => $this->city_name,
            "subdistrict_name" => $this->subdistrict_name,
        ]);
    }
}
