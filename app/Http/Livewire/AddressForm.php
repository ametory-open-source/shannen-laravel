<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AddressForm extends Component
{
    public $isForm;

    public function mount() {
        $this->isForm = false;
    }

    public function render()
    {
        return view('livewire.address-form');
    }
}
