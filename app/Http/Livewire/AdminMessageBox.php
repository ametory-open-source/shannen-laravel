<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MessageRoom;
use App\Models\Message;


class AdminMessageBox extends Component
{
    const LAST_ROOM_ID = 'last_room_id';
    protected $room;
    public $message = "";
    protected $session;


    protected $listeners = [
        'selectRoom' => 'getRoom',
        'refreshChat' => 'getRoom',
        'refreshBox' => 'refreshBox',
    ];

    public function mount()
    {

        $roomId = $this->getLastRoomId();
        if ($roomId) {
            $this->getRoom($roomId);
        }
    }
    public function render()
    {
        // \Log::info("RENDER BOX");
        return view('livewire.admin-message-box', [
            "room" => $this->room
        ]);
    }

    public function getRoom($id)
    {
        $this->room = MessageRoom::find($id);
        $this->setLastRoomId($id);
        Message::where("message_room_id", $id)->update([
            "readed_by_admin_at" => now(),
            "readed_by_admin" => auth()->user()->id,
        ]);
    }
    public function refreshBox()
    {
        $roomId = $this->getLastRoomId();
        if ($roomId) {
            $this->getRoom($roomId);
        }
        // $this->room = MessageRoom::find($this->room->id);
    }



    public function setLastRoomId($id)
    {
        if ($id == null) {
            session()->forget(self::LAST_ROOM_ID);
            return;
        }
        session()->put(self::LAST_ROOM_ID, $id);
    }
    public function getLastRoomId() {
        return session()->get(self::LAST_ROOM_ID);
    }
}
