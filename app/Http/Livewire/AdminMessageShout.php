<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Message;
use App\Models\MessageRoom;
class AdminMessageShout extends Component
{
    public $room;
    public $message;
    public function render()
    {
        return view('livewire.admin-message-shout');
    }


    public function sendMessage($id)
    {
        if (!$this->message) return;
        Message::create([
            "message" => $this->message,
            "options" => [],
            "type" => "CHAT",
            "message_room_id" => $id,
            "admin_id" => auth()->user()->id,
            "readed_by_admin_at" => now(),
            "readed_by_admin" => auth()->user()->id,
        ]);

        MessageRoom::find($id)->update([
            "last_message" => $this->message,
            "last_message_at" => now(),
        ]);
        $this->message = "";
        $this->emit("refreshChat", $id);
        $this->emit("refreshRooms");
        $this->emit("refreshBox");

        $this->dispatchBrowserEvent("scrollDown");
    }
}
