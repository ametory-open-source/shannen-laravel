<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductSearch extends Component
{
    public $search;

    function mount() {
        $this->search = "";
    }
    public function render()
    {

        $this->emit("setQuerySearch", $this->search);
        return view('livewire.product-search');
    }

    // public function onKeyDown()
    // {
    //
    // }
}
