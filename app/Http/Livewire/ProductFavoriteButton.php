<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductFavoriteButton extends Component
{
    public $product;
    public $target;
    public $isFavorited = false;

    public function mount()
    {
        $this->isFavorited = $this->product->is_favorited;
    }

    public function render()
    {

        return view('livewire.product-favorite-button', [
            "isFavorited" => $this->isFavorited
        ]);
    }

    function addToWishlist()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();

        $isFavorited = \DB::table("wishlist_product")->where([
            "product_id" => $this->product->id,
            "user_id" => $user->id,
        ])->count();

        if ($isFavorited) {
            $this->deleteWishlist();
            return;
        } else {
            \DB::table("wishlist_product")->insert([
                "product_id" => $this->product->id,
                "user_id" => $user->id,
            ]);
            $this->isFavorited = true;
            $this->emit("openModal", "modal-wishlist", ["product" => [
                "product" => $this->product->uuid,
                "name" => $this->product->name,
                "feature_image" => $this->product->feature_image,
            ]]);
        }
    }

    public function deleteWishlist()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if ($user) {
            \DB::table("wishlist_product")->where([
                "product_id" => $this->product->id,
                "user_id" => $user->id,
            ])->delete();
            $this->isFavorited = false;
        }
    }

}
