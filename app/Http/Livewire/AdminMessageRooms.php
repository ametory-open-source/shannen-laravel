<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MessageRoom;
use App\Models\Message;

class AdminMessageRooms extends Component
{
    protected $rooms = [];
    public $search = "";

    protected $listeners = [
        'refreshRooms' => 'refreshRooms',
        'searchRoom' => 'searchRoom',
    ];

    public function mount()
    {
        $this->getRooms();

    }
    public function render()
    {
        return view('livewire.admin-message-rooms', [
            "rooms" => $this->rooms
        ]);
    }

    public function updated($name, $value)
    {
        if ($name == "search") {

        }

        $this->getRooms();
    }



    private function getRooms()
    {
        \Log::info("SEARCH ". $this->search);
        $this->rooms = MessageRoom::with("user")->when($this->search, function($q) {
            $q->whereHas("user", function($q) {
                $q->where("name", "like", "%".$this->search."%");
            });
        })->orderBy("last_message_at", "desc")->get();
    }

    public function searchRoom()
    {

        \Log::info("SEARCH ". $this->search);
        $this->rooms = MessageRoom::with("user")->when($this->search, function($q) {
            $q->whereHas("user", function($q) {
                $q->where("name", "like", "%".$this->search."%");
            });
        })->orderBy("last_message_at", "desc")->get();


    }

    public function refreshRooms()
    {


        $this->getRooms();
    }
    public function selectRoom($id)
    {
        \Log::info("SEARCH ". $this->search);
        $this->emit("selectRoom", $id);
        Message::where("message_room_id", $id)->update([
            "readed_by_admin_at" => now(),
            "readed_by_admin" => auth()->user()->id,
        ]);
        $this->getRooms();
        $this->emit("refreshCount");
        $this->dispatchBrowserEvent("scrollDown");
    }
}
