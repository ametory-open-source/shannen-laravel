<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;
use App\Models\Web;
use App\Facades\RajaOngkir;

class Shipping extends Component
{
    public $address;
    public $selectedCostId;
    public $selectedCost;
    public $costs = [];
    protected $total;
    protected $weight;
    protected $content;

    protected $listeners = [
        'productAddedToCart' => 'updateCart',
    ];

    public function mount(): void
    {
        $this->selectCost = null;
        Cart::setShippingCost(null);
        $this->getUser();
        $this->updateCart();
        $this->getCost();
    }

    public function getUser()
    {
        if (auth(env('APP_AFFILIATE_GUARD'))->check()) {
            if (auth(env('APP_AFFILIATE_GUARD'))->user()->default_address) {
                $this->address = auth(env('APP_AFFILIATE_GUARD'))->user()->default_address;
            }
        }
    }

    public function getCost()
    {
        if ($this->address) {
            $costs = getCost($this->address, $this->weight);
            $selectedCost = Cart::getShippingCost();
            foreach ($costs as $key => $cost) {
                if ($key == 0 ) {
                    if (!$selectedCost) {
                        $this->selectedCost = $cost;
                        Cart::setShippingCost($this->selectedCost);
                        \Log::info("KESINI");
                        $this->refreshCart();
                    } else {
                        $this->selectedCost = $selectedCost;
                    }
                }
            }
            $this->costs = $costs;
        }
        if ($this->selectedCost) {
            \Log::info("SELECTED COST", [$this->selectedCost]);

        }
    }

    public function render()
    {
        return view('livewire.shipping', [
            "costs" => $this->costs,
            "selectedCost" => $this->selectedCost,
        ]);
    }

    public function updateCart()
    {
        $this->weight = Cart::weight();
    }

    public function refreshCart()
    {
        \Log::info("KESANA");

        $this->emit("refreshCartPage",  $this->selectedCost);
    }

    public function setShipping($id)
    {
        $this->selectedCost = collect($this->costs)->firstWhere("id", $id);
        Cart::setShippingCost($this->selectedCost);
        $this->emit("selectCost");
    }

    private function getId($item, $cost)
    {
        return $item["service"]."-".$cost["value"]."-".$cost["etd"];
    }
}
