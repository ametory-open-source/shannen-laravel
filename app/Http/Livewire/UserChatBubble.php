<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MessageRoom;
use App\Models\Message;

class UserChatBubble extends Component
{

    protected $listeners = [
        'refreshBubble' => 'refreshBubble',
    ];

    protected $count_unread = 0;

    public function mount() {
        $this->refreshBubble();
    }
    function refreshBubble() {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $count_unread =  Message::whereHas("room", function($q) use($user) {
            $q->where("user_id", $user->id);
        })->whereNull("readed_at")->count();
        $this->count_unread = $count_unread;
    }


    public function render()
    {

        return view('livewire.user-chat-bubble', [
            "count_unread" => $this->count_unread
        ]);
    }
}
