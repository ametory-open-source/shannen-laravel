<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Product;
use App\Facades\Cart;
use Livewire\WithPagination;
use App\Models\ProductCategory as Cat;
class ProductGrid extends Component
{
    use WithPagination;

    public $min;
    public $max;
    public $search;
    public $categoryId;
    public $affiliate;
    public $tag;


    protected $listeners = [
        'setPriceFilter' => 'setPriceFilter',
        'setCategoryId' => 'setCategoryId',
        'setQuerySearch' => 'setQuerySearch',
        'addToWishlist' => 'addToWishlist',
    ];

    public function mount()
    {
        if (request()->has('cat')) {
            $cat = Cat::where('slug', request()->get('cat'))->first(["id"]);
            \Log::info(request()->get('cat'));
            \Log::info($cat->id);
            $this->categoryId = $cat->id;
            $this->emit("setCategoryId", $cat->id);
        }

        if (request()->has('tag')) {
            $this->tag = request()->get('tag');
        }
    }

    public function render()
    {
        \DB::enableQueryLog();
        $products = Product::where("products.status", "PUBLISHED")
        ->when($this->categoryId, function($q) {
            $q->where('product_category_id', $this->categoryId);
        })
        ->when($this->tag, function($q) {
            $q->where('tags', "like", "%".$this->tag."%");
        })
        ->when($this->affiliate, function($q) {
            $q->join('affiliate_product', 'affiliate_product.product_id', '=', 'products.id');
            $q->join('affiliates', 'affiliates.id', '=', 'affiliate_product.affiliate_id');
            $q->where('affiliates.id', $this->affiliate->id);
        })
        ->when($this->search, function($q) {
            $q->where('products.name','like' , "%".$this->search. "%");
        })
        ->join("product_categories", "products.product_category_id", "=", "product_categories.id")
        ->whereRaw("product_categories.flag = 1")

        ->whereBetween("products.price", [$this->min, $this->max])
        ->selectRaw("products.*")
        ->paginate(15);


        \Log::info("query grid", \DB::getQueryLog());
        $total_record = $products->total();
        $this->emit("totalRecord", $total_record);
        return view('livewire.product-grid', [
            "products" => $products,
        ]);
    }

    public function setPriceFilter($values)
    {

        $this->min = $values[0];
        $this->max = $values[1];

    }

    public function addToCart($id)
    {
        // $option = [];
        // if ($this->affiliate) {
        //     $option["affiliate_data"] = $this->affiliate->toData();
        // }

        $product = Product::find($id);

        Cart::add($product, 1);
        $this->emit('productAddedToCart');
        $this->emit('updateCount');
        $this->emit("openModal", "add-to-cart", ["product" => [
            "uuid" => $product->uuid,
            "name" => $product->name,
            "feature_image" => $product->feature_image,
        ]]);
    }

    public function setCategoryId($id)
    {
        \Log::info("Category ID", [$id]);
        $this->categoryId = $id;
    }

    public function setQuerySearch($search)
    {
        $this->search = $search;
    }

    public function addToWishlist($id)
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();

        $isFavorited = \DB::table("wishlist_product")->where([
            "product_id" => $id,
            "user_id" => $user->id,
        ])->count();

        if ($isFavorited) {
            $this->deleteWishlist($id);
            return;
        } else {
            \DB::table("wishlist_product")->insert([
                "product_id" => $id,
                "user_id" => $user->id,
            ]);
        }
    }

    public function deleteWishlist($id)
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if ($user) {
            \DB::table("wishlist_product")->where([
                "product_id" => $id,
                "user_id" => $user->id,
            ])->delete();
        }
    }
}
