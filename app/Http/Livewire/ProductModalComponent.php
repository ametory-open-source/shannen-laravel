<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;

class ProductModalComponent extends Component
{
    public $product;
    public $quantity;

    public function mount(): void
    {
        $this->quantity = 1;
    }

    public function modalAddToCart(): void
    {
        if ($this->quantity == 0) {
            $this->dispatchBrowserEvent("quantityZero");
            return;
        }
        Cart::add($this->product, $this->quantity);
        $this->emit('productAddedToCart');
        $this->emit('updateCount');
    }

    public function dec(): void
    {
        if ($this->quantity > 1)
        $this->quantity -= 1;
    }

    public function inc(): void
    {
        $this->quantity += 1;
    }

    public function render()
    {
        return view('livewire.product-modal-component');
    }
}
