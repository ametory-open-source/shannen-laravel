<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShopSidebar extends Component
{
    public $min;
    public $max;


    protected $listeners = [
        'setPriceFilter' => 'setPriceFilter',
    ];


    public function mount()
    {
        $this->min = 10000;
        $this->max = 1000000;
    }


    public function render()
    {
        return view('livewire.shop-sidebar');
    }


}
