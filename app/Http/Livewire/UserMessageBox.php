<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MessageRoom;
use App\Models\Message;

class UserMessageBox extends Component
{
    protected $user;
    protected $room;

    protected $listeners = [
        'refreshBox' => 'mount',
    ];

    public function getRoom($id)
    {
        \Log::info("refreshChat");
        $this->room = MessageRoom::find($id);

    }

    public function  mount()
    {
        $this->user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $this->room = MessageRoom::where("user_id",$this->user->id)->first();
        if (!$this->room) {
            $this->room = MessageRoom::create([
                "user_id" => $this->user->id,
                "name" => $this->user->name,
            ]);
        }
        $this->dispatchBrowserEvent("scrollDown");
    }
    public function render()
    {


        // Message::where("message_room_id", $this->room->id)->update([
        //     "readed_at" => now(),
        // ]);



        return view('livewire.user-message-box', [
            "room" => $this->room
        ]);
    }
}
