<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\RajaOngkir;
use Illuminate\Support\Facades\Cache;
use App\Facades\Cart;

class CheckoutAddress extends Component
{
    public $shipping_is_different_address;
    public $address;
    public $shipping_address;
    public $selectCost;
    public $addresses;
    public $address_id;
    public $shipping_address_id;
    protected $province_name;
    protected $city_name;
    protected $subdistrict_name;
    protected $weight;
    public $isLoading = false;
    protected $listeners = [
        'setLoading' => 'setLoading',
    ];


    public function mount() {
        $this->shipping_is_different_address = false;
        $this->getUser();
        $this->updateCart();
    }

    public function updated($name, $value)
    {
        // \Log::info("updated", [$name]);
        // \Log::info("updated value", [$value]);
        // if ($name == "shipping_is_different_address") {
        //     if (!$value) {
        //         $this->shipping_address = null;
        //         $this->shipping_address_id = null;
        //     } else {
        //         \Log::info("kesini updated value", [$value]);
        //         $selected = $this->addresses->first();
        //         \Log::info("selected", $selected->toArray());
        //         $this->shipping_address_id = $selected->id;
        //         $this->shipping_address = $selected;
        //         Cart::setShippingAddress($this->shipping_address);
        //         if ($selected->id != $this->selectCost['id']) {
        //             $weight = Cart::weight();
        //             $costs = getCost($selected, $weight);
        //             \Log::info("costs updated", $costs);
        //             if (count($costs)) {
        //                 Cart::setShippingCost($costs[0]);
        //                 Cart::setCosts($costs);
        //                 $this->emit("costChanged", $costs);
        //             } else {
        //                 Cart::setShippingCost(null);
        //                 $this->emit("costChanged", []);
        //                 Cart::setCosts([]);
        //             }
        //         }
        //     }
        // }
        // if ($name == "address_id") {
        //     Cart::setShippingCost(null);

        // }
        // if ($name == "shipping_address_id" && $this->shipping_is_different_address) {
        //     Cart::setShippingCost(null);
        // }
    }

    public function getUser()
    {
        if ($user = auth(env('APP_AFFILIATE_GUARD'))->user()) {
            $this->addresses = $user->addresses;
            if ($user->default_address) {
                $this->address = $user->default_address;
                $this->address_id = $this->address->id;
                Cart::setBillingAddress($this->address);
                Cart::setShippingAddress($this->address);
                $selected = $this->addresses->firstWhere("id", $this->address_id);
                $weight = Cart::weight();
                $costs = getCost($selected, $weight);
                if (count($costs)) {
                    Cart::setShippingCost($costs[0]);
                    Cart::setCosts($costs);
                } else {
                    Cart::setShippingCost(null);
                    Cart::setCosts([]);
                }
            }
        }
    }

    public function render()
    {
        return view('livewire.checkout-address', [
            "address" => $this->address,
            "addresses" => $this->addresses,
        ]);
    }

    public function setLoading($val) {
        \Log::info("setLoading", [$val]);
        $this->isLoading = $val;
    }

    public function addressChange()
    {
        $selected = $this->addresses->firstWhere("id", $this->address_id);
        $this->address = $selected;
        \Log::info("addressChange", $selected->toArray());
        $this->emit("addressChange", $this->address_id);
        $weight = Cart::weight();
        $costs = getCost($selected, $weight);
        \Log::info("costs", $costs);
        if (count($costs)) {
            Cart::setShippingCost($costs[0]);
            Cart::setCosts($costs);
            $this->emit("costChanged", $costs);
        } else {
            Cart::setShippingCost(null);
            Cart::setCosts([]);
            $this->emit("costChanged", []);
        }
        Cart::setBillingAddress($this->address);
        if (!$this->shipping_is_different_address) {
            Cart::setShippingAddress($this->address);

        }
    }


    public function shippingAddressChange()
    {
        $selected = $this->addresses->firstWhere("id", $this->shipping_address_id);
        $this->shipping_address = $selected;
        $weight = Cart::weight();
        $costs = getCost($selected, $weight);
        \Log::info("costs", $costs);
        if (count($costs)) {
            Cart::setShippingCost($costs[0]);
            $this->emit("costChanged", $costs);
        } else {
            Cart::setShippingCost(null);
            $this->emit("costChanged", []);
        }
        Cart::setShippingAddress($this->shipping_address);
    }

    public function updateCart()
    {
        $this->weight = Cart::weight();
        $this->selectCost = Cart::getShippingCost();
    }
}
