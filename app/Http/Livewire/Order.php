<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class Order extends Component
{
    use WithPagination;
    public function render()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $orders = $user->orders()->orderBy("created_at", "desc")->paginate(10);
        return view('livewire.order', [
            "orders" => $orders,
        ]);
    }
}
