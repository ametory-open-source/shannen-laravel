<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;
use App\Models\Web;
use App\Facades\RajaOngkir;

class CartPage extends Component
{
    protected $total;
    protected $grandTotal;
    protected $weight;
    protected $content;
    public $address;
    public $selectedCostId;
    public $selectedCost;
    public $costs = [];
    public $quantities = [];
    public $discount = 0;
    public $minimum_order = 0;
    public $active_discount = 0;
    protected $listeners = [
        'productAddedToCart' => 'updateCart',
        'selectCost' => 'selectCost',
        'refreshCartPage' => 'selectCost',
    ];

    public function mount(): void
    {

        $this->updateCart();

    }

    public function selectCost()
    {
        \Log::info("update cart when select cost");
        $this->updateCart();
    }

    public function refreshCartPage($selectedCost)
    {
        \Log::info("refreshCartPage cart when select cost", [$selectedCost]);

        $this->updateCart();
    }

    public function updated($name, $value)
    {
        if (str_contains($name, "quantities")) {
            $id = explode(".", $name)[1];
            if (empty(trim($value))) {
                $value = 0;
            }
            Cart::setAmount($id, $value);
        }
        $this->updateCart();
    }

    public function render()
    {

        if ($this->content) {
            foreach($this->content as $id => $item) {
                $this->quantities[$id] = $item->get('quantity');
            }
        }

        return view('livewire.cart-page', [
            'grandTotal' => $this->grandTotal,
            'total' => $this->total,
            'content' => $this->content,
            'weight' => $this->weight,
            'costs' => $this->costs,
            'discount' => $this->discount,
            'minimum_order' => $this->minimum_order,
            'active_discount' => $this->active_discount,
        ]);
    }

    public function removeFromCart(string $id): void
    {
        Cart::remove($id);
        $this->updateCart();
    }


    public function clearCart(): void
    {
        Cart::clear();
        $this->updateCart();
    }

    public function updateCartItem(string $id, string $action): void
    {
        Cart::update($id, $action);
        $this->updateCart();
    }

    public function updateCart()
    {
        $this->total = Cart::total();
        $discount = 0;
        $minimum_order = 0;
        $active_discount = 0;
        if ($user = auth(env('APP_AFFILIATE_GUARD'))->user()) {
            $discount = $user->discount($this->total);

            Cart::setDiscount($discount);
            $minimum_order = $user->minimum_order;
            $active_discount = ($user->active_discount);
        }
        $this->discount = $discount;
        $this->minimum_order = $minimum_order;
        $this->active_discount = $active_discount;


        $this->weight = Cart::weight();
        $this->content = Cart::content();
        $this->grandTotal = Cart::grandTotal();
        $this->selectCost = Cart::getShippingCost();

        \Log::info("GRAND TOTAL", [$this->grandTotal]);
        $this->emit('updateCount');
    }



}
