<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MessageRoom;
use App\Models\Message;

class UserMessageShout extends Component
{
    public $room;
    public $user;
    public $message;
    public function render()
    {
        return view('livewire.user-message-shout');
    }




    public function  mount()
    {
        $this->user = auth(env('APP_AFFILIATE_GUARD'))->user();
    }

    public function sendMessage($id)
    {

        if (!$this->message) {
            return;
        }
        Message::create([
            "message" => $this->message,
            "options" => [],
            "type" => "CHAT",
            "message_room_id" => $id,
            "user_id" => $this->user->id,
            'readed_at' => now(),

        ]);

        MessageRoom::find($id)->update([
            "last_message" => $this->message,
            "last_message_at" => now(),
        ]);


        $this->dispatchBrowserEvent("scrollDown");

        Message::where("message_room_id", $id)->update([
                "readed_at" => now(),
            ]);

        \Log::info("SEND MESSAGE", [$this->message, $id]);
        $this->message = "";
        $this->emit("refreshChat", $id);
        $this->emit("refreshRooms");
        $this->emit("refreshBox");
        $this->emit("refreshBubble");
    }
}
