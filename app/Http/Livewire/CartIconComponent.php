<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;

class CartIconComponent extends Component
{
    protected $total;
    protected $listeners = [
        'updateCount' => 'updateCount',
    ];

    public function mount()
    {
        $this->updateCount();
    }

    public function render()
    {
        return view('livewire.cart-icon-component', [
            'total' => $this->total,
        ]);
    }

    public function updateCount()
    {
        \Log::info("UPDATE COUNT");

        $this->total = Cart::count();
    }
}
