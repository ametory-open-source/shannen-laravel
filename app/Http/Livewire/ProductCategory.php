<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\ProductCategory as Cat;
class ProductCategory extends Component
{

    public $min;
    public $max;
    public $affiliate;

    protected $listeners = [
        'setPriceFilter' => 'setPriceFilter',

    ];

    function mount()
    {

    }

    public function render()
    {

        $binding = [
            $this->min,
            $this->max,
            $this->min,
            $this->max,
            "PUBLISHED",
        ];
        if ($this->affiliate) {
            $binding = [
                $this->min,
                $this->max,
                $this->affiliate->id,
                $this->min,
                $this->max,
                "PUBLISHED",
            ];
        }
        \DB::enableQueryLog();
        $categories =  \DB::table('product_categories')
        ->join("products", "products.product_category_id", "=", "product_categories.id")
        // ->when($this->affiliate, function($q) {
        //     $q->join('affiliate_product', 'affiliate_product.product_id', '=', 'products.id');
        //     $q->join('affiliates', 'affiliates.id', '=', 'affiliate_product.affiliate_id');
        //     $q->whereRaw('affiliates.id = ?');
        // })
        ->whereRaw("products.price between ? and ?")
        ->whereRaw("products.status = ?")
        ->whereRaw("product_categories.flag = 1")
        ->select(\DB::raw("product_categories.id, product_categories.name, (select count(id) from products where product_category_id = product_categories.id and (products.price between ? and  ?)) as count"))
        ->groupBy(\DB::raw("product_categories.id, product_categories.name"))
        ->setBindings($binding)
        ->get();
        \Log::info("query", \DB::getQueryLog());
        return view('livewire.product-category', [
            "categories" => $categories
        ]);
    }

    public function goto($id)
    {

        $this->emit("setCategoryId", $id);
    }

    public function allProduct()
    {

        $this->emit("setCategoryId", null);
    }


    public function setPriceFilter($values)
    {

        $this->min = $values[0];
        $this->max = $values[1];

    }
}
