<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;
use App\Facades\RajaOngkir;
use App\Models\Order;
use Illuminate\Support\Str;
class CheckoutProduct extends Component
{
    public $total;
    public $grandTotal;
    public $weight;
    protected $content;
    protected $selectCost;
    public $notes;
    protected bool $isLoading = false;
    public $address;
    public $discount;
    public $shipping_address;
    public $shipping_is_different_address;

    public $costs = [];

    public function mount(): void
    {
        $this->updateCart();
    }


    protected $listeners = [
        'setCosts' => 'setCosts',
        'costChanged' => 'costChanged',
        'placeOrder' => 'placeOrder',
        'setAddress' => 'setAddress',
    ];


    public function setAddress($address, $shipping_address, $shipping_is_different_address)
    {
        \Log::info("setAddress");
        $this->address = $address;
        $this->shipping_address = $shipping_address;
        $this->shipping_is_different_address = $shipping_is_different_address;
    }

    public function setCosts($costs)
    {
        \Log::info("setCosts",count($costs));
        $this->costs = $costs;
        $this->updateCart();

        // $this->selectCost = Cart::getShippingCost();
    }

    public function costChanged($costs)
    {
        \Log::info("costChanged",[count($costs)]);
        $this->costs = $costs;
        $this->updateCart();
        // $this->selectCost = Cart::getShippingCost();
    }

    public function render()
    {
        return view('livewire.checkout-product', [
            'selectCost' => $this->selectCost,
            'content' => $this->content,
            'isLoading' => $this->isLoading,
        ]);
    }

    public function updated($name, $value)
    {
        $this->updateCart();
    }
    public function updateCart()
    {
        $this->total = Cart::total();
        $this->discount = Cart::getDiscount();
        $this->weight = Cart::weight();
        $this->content = Cart::content();
        $this->grandTotal = Cart::grandTotal();
        $this->selectCost = Cart::getShippingCost();
        $this->costs = Cart::getCosts();

        // \Log::info("content", $this->content->toArray());


    }

    public function setShipping($id)
    {
        $this->content = Cart::content();
        if ($id == "SELF_PICKUP") {
            $this->selectCost = [
                "id" => "SELF_PICKUP",
                "available_for_cash_on_delivery" => "",
                "available_for_proof_of_delivery" => "",
                "available_for_instant_waybill_id" => "",
                "courier_name" => "SELF_PICKUP",
                "service_type" => "SELF_PICKUP",
                "courier_code" => "SELF_PICKUP",
                "shipping_type" => "SELF_PICKUP",
                "service" => "SELF_PICKUP",
                "service_code" => "SELF_PICKUP",
                "value" => 0,
                "etd" => "0",
                "name" => "SELF_PICKUP",
                "address_id" => "",
            ];
        } else {
            $this->selectCost = collect($this->costs)->firstWhere("id", $id);

        }
        \Log::info("selectedCost", [$id]);
        Cart::setShippingCost($this->selectCost);
        $this->grandTotal = Cart::grandTotal();
        // $this->total = Cart::total();
        // $this->weight = Cart::weight();
        //
        // $this->emit("selectCost");
    }

    public function placeOrder()
    {
        $this->isLoading = true;
        $this->total = Cart::total();
        $this->weight = Cart::weight();
        $this->content = Cart::content();
        $this->grandTotal = Cart::grandTotal();
        $this->discount = Cart::getDiscount();
        $this->selectCost = Cart::getShippingCost();
        $billingAddress = Cart::getBillingAddress();
        $shippingAddress = Cart::getShippingAddress();
        $data = [
            "order_code" => Str::upper(Str::random(5)),
            "items" => $this->content->map(function($d) {
                return $d;
            })->values(),
            "total" => Cart::total(false),
            "grand_total" => Cart::grandTotal(false),
            "shipping_cost" => collect($this->selectCost) ,
            "billing_address" => $billingAddress,
            "shipping_address" => $shippingAddress,
            "shipping_order" => [],
            "tracking_order" => [],
            "notes" => $this->notes ?? "",
            "images" => [],
            "discount" => $this->discount,
            "user_id" => auth(env('APP_AFFILIATE_GUARD'))->check() ? auth(env('APP_AFFILIATE_GUARD'))->user()->id : null,

        ];



       $order =  Order::create($data);
       Cart::clear();
       $this->isLoading = false;
       return redirect()->to('/orders/'. $order->id);
    }
}
