<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class ProductAffiliate extends Component
{
    use WithPagination;
    protected $listeners = [
        'resetProductPage' => 'resetProductPage',
    ];

    public function render()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $products = $user->affiliate->products()->paginate(5);
        return view('livewire.product-affiliate', [
            "products" => $products,
        ]);
    }

    public function resetProductPage()
    {
        $this->resetPage();
    }
}
