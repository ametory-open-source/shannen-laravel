<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;

class ProductComponent extends Component
{
    public $product;
    public $quantity;

    public function mount(): void
    {
        $this->quantity = 1;
    }


    public function render()
    {
        return view('livewire.product-component');
    }

    public function addToCart(): void
    {
        Cart::add($this->product, $this->quantity);

        $this->emit('productAddedToCart');
        $this->emit('updateCount');
        $this->emit("openModal", "add-to-cart", ["product" => [
            "uuid" => $this->product->uuid,
            "name" => $this->product->name,
            "feature_image" => $this->product->feature_image,
        ]]);
    }




}
