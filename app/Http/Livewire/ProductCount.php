<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ProductCount extends Component
{
    public $total;
    protected $listeners = [
        'totalRecord' => 'totalRecord',

    ];

    public function totalRecord($total)
    {
        $this->total = $total;
    }

    public function render()
    {
        return view('livewire.product-count');
    }
}
