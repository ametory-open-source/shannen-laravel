<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SelectPayment extends Component
{
    protected $method = "BANK_TRANSFER";
    public $order;
    public function render()
    {
        return view('livewire.select-payment', [
            "method" => $this->method
        ]);
    }

    public function selectPayment($method) {
        $this->method = $method;
    }
}
