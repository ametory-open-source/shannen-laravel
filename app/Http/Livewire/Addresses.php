<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Address;

class Addresses extends Component
{
    use WithPagination;
    public $isForm;

    protected $listeners = [
        'resetAddressPage' => 'resetAddressPage',
    ];

    public function mount() {
        $this->isForm = false;
    }
    public function render()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $addresses = Address::where("user_id", $user->id)->paginate(10);
        return view('livewire.addresses', [
            "addresses" => $addresses,
            "user" => $user,
        ]);
    }

    public function toggle()
    {
        $this->isForm = !$this->isForm;
    }

    public function resetAddressPage()
    {
        $this->resetPage();
    }


    public function toForm()
    {
       return redirect("/profile/add-address");
    }
}
