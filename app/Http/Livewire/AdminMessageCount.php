<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\MessageRoom;

class AdminMessageCount extends Component
{

    protected $count;

    protected $listeners = [
        'notifyAdmin' => 'notifyAdmin',
        'refreshCount' => 'refreshCount',
    ];

    public function mount() {
        $this->count = $this->getCount();
    }
    public function render()
    {
        return view('livewire.admin-message-count', [
            "count" => $this->count,
        ]);
    }


    private function notifyAdmin()
    {
        $this->count = $this->getCount();
    }

    public function refreshCount()
    {
        // \Log::info("refreshCount");
        $this->count = $this->getCount();
    }

    private function getCount()
    {
        return MessageRoom::whereHas("messages", function($q) {
            $q->whereNull("readed_by_admin");
        })->count();
    }
}
