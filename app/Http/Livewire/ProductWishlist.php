<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
class ProductWishlist extends Component
{
    use WithPagination;
    public function render()
    {
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        $products = $user->wishlists()->paginate(10);
        return view('livewire.product-wishlist', [
            "products" => $products,
        ]);
    }
}
