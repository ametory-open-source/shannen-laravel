<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart;

class ProductDetail extends Component
{
    public $product;
    public $affiliate;
    public $quantity;

    public function mount() {
        $this->quantity = 1;
    }

    public function dec(): void
    {
        if ($this->quantity > 1)
        $this->quantity -= 1;
    }

    public function inc(): void
    {
        $this->quantity += 1;
    }

    public function render()
    {
        return view('livewire.product-detail', [
            'product' => $this->product
        ]);
    }

    public function addToCart()
    {
        $option = [];
        if ($this->affiliate) {
            $option["affiliate_data"] = $this->affiliate->toData();
        }

        Cart::add($this->product, $this->quantity, $option);
        $this->emit('productAddedToCart');
        $this->emit('updateCount');
        $this->emit("openModal", "add-to-cart", ["product" => [
            "product" => $this->product->uuid,
            "name" => $this->product->name,
            "feature_image" => $this->product->feature_image,
        ]]);
    }

    public function addToWishlist($id)
    {
        \Log::info("addToWishlist", [$id]);
        $user = auth(env('APP_AFFILIATE_GUARD'))->user();
        if ($user) {
            if ($this->product->is_favorited) {
                \DB::table("wishlist_product")->where([
                    "product_id" => $id,
                    "user_id" => $user->id,
                ])->delete();
            } else {
                \DB::table("wishlist_product")->insert([
                    "product_id" => $id,
                    "user_id" => $user->id,
                ]);

                $this->emit("openModal", "modal-wishlist", ["product" => [
                    "product" => $this->product->uuid,
                    "name" => $this->product->name,
                    "feature_image" => $this->product->feature_image,
                ]]);
            }

        }
    }
}
