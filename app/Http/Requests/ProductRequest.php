<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'price' => 'required|numeric',
        ];
    }


    public function messages(): array
    {
        return [
            'name.required' => 'Nama Produk Tidak Boleh Kosong',
            'price.required' => 'Harga Produk Tidak Boleh Kosong',
            'price.numeric' => 'Format Harga Produk salah',
        ];
    }


}
