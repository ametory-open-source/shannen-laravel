<?php
namespace App\Classes;

use App\Models\Address;

class ShippingCost
{
	public string $id;
	public string $etd;
	public string $name;
	public int $value;
    public string $service;
	public string $address_id;
	public string $courier_code;
	public string $courier_name;
	public string $service_code;
	public string $service_type;
	public string $shipping_type;
	public bool $available_for_cash_on_delivery;
	public bool $available_for_proof_of_delivery;
	public bool $available_for_instant_waybill_id;


	public function __construct(Array $properties=array()){
        $this->address = new Address;
        foreach($properties as $key => $value){
            $this->{$key} = $value;
            if ($key == "address_id") {
                $this->address  = Address::find($value);
            }
        }
    }


}
