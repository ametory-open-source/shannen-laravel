<?php

use Kreait\Firebase\Factory;
use App\Models\Web;
use App\Facades\RajaOngkir;
use App\Facades\Cart;
use App\Services\Biteship;

function youtubeID($url)  {
    preg_match('/(http(s|):|)\/\/(www\.|)yout(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $results);
    return end($results);
}

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function assetUrl($text) {

    return asset("/storage". $text);
}
function parsePricing($text) {
    $exp = explode("\n", $text);
    $list = [];
    foreach ($exp as $key => $item) {
        if (str_contains($item, "[-]")) {
            array_push($list, sprintf("<li class=\"not-included\">%s</li>", str_replace("[-]", "",$item)));
        } else {
            array_push($list, sprintf("<li>%s</li>", $item));
        }
    }
    return implode("\n",$list);
}



function convertKebabCase(string $string)
{
    // Replace repeated spaces to underscore
    $string = preg_replace('/[\s.]+/', '_', $string);
    // Replace un-willing chars to hyphen.
    $string = preg_replace('/[^0-9a-zA-Z_\-]/', '-', $string);
    // Skewer the capital letters
    $string = strtolower(preg_replace('/[A-Z]+/', '-\0', $string));
    $string = trim($string, '-_');

    return preg_replace('/[_\-][_\-]+/', '-', $string);
}

function firebase() {
    $factory = (new Factory)->withServiceAccount(app_path().'/../suksesirajin-firebase-adminsdk.json');

    return $factory;
}


function arrayDate($star_date, $end_date) {
    return new DatePeriod(
        new DateTime($star_date),
        new DateInterval('P1D'),
        new DateTime($end_date)
   );
}

function colorWheel($alpha, $opacity = 1) {
    list($r, $g, $b) = sscanf($alpha, "#%02x%02x%02x");
    return "rgba($r, $g, $b, $opacity)";
}

function splitModCat($word) {
    return collect(explode("_",$word))->map(function($d) {
        return ucfirst(strtolower($d) );
    })->join(" ");
}


function randomPastelColor() {
    $colors = [
        "#93DA7F",
        "#F4D97D",
        "#9DABFD",
        "#F6CF98",
        "#F9B3BE",
        "#F3D9A2",
        "#f9e6df",
        "#A876FA",
        "#C9ACF8",
        "#F89898"
    ];

    return $colors[array_rand($colors)];
}


function money($value) {
   return env("APP_CURRENCY"). number_format($value ,0,",",".");
}
function number($value) {
   return number_format($value ,0,",",".");
}



function getCost($address, $weight = 1000, $shipping_provider = "jne") {
    $web = Web::first();
    $costs = [];
    $mapping_cost = [];
    try {

        $content = Cart::content();

        $items = collect([]);

        foreach($content as $id => $item) {
            $items->push([
                "name" => $item->get('name'),
                "description" => $item->get('name'),
                "value" => $item->get('price'),
                "length" => $item->get('length'),
                "width" => $item->get('width'),
                "height" => $item->get('height'),
                "weight" => $item->get('weight'),
                "quantity" => $item->get('quantity')
            ]);
        }
        $dataCost = [
                "origin_postal_code" => (int) $web->company_postal_code,
                "destination_postal_code" => (int) $address->postal_code,
                "couriers" => implode(",", array_keys($web->couriers)) ,
                "items" => $items->toArray()
            ];

        $biteship = new Biteship();
        $result = $biteship->getCost($dataCost);
        // \Log::info("LOAD FROM BITESHIP");
        \Log::info("LOAD FROM BITESHIP", $dataCost);
        \Log::info("");
        \Log::info("");
        $costResult = $result->json();
        // $costResult = json_decode(config("landing.sample_response"), true);
        \Log::info("RESULT FROM BITESHIP", $result->json());

        foreach ($costResult["pricing"] as $k =>  $item) {
            $cost =  [
                "id" => getCostId($item),
                "available_for_cash_on_delivery" => $item["available_for_cash_on_delivery"],
                "available_for_proof_of_delivery" => $item["available_for_proof_of_delivery"],
                "available_for_instant_waybill_id" => $item["available_for_instant_waybill_id"],
                "courier_name" => $item["courier_name"],
                "service_type" => $item["service_type"],
                "courier_code" => $item["courier_code"],
                "shipping_type" => $item["shipping_type"],
                "service" => $item["courier_service_name"],
                "service_code" => $item["courier_service_code"],
                "value" => $item["price"],
                "etd" => $item["shipment_duration_range"] . " " . $item["shipment_duration_unit"],
                "name" => $item["courier_name"],
                "address_id" => $address->id,
            ];
            $mapping_cost[] = $cost;
        }

        return collect($mapping_cost)->sortBy("value")->toArray();
    } catch (\Throwable $th) {
        \Log::info("ERROR", [$th->getMessage()]);
    }
    return [];
}


// function getCostId($item, $cost)
// {
//     return $item["service"]."-".$cost["value"]."-".$cost["etd"];
// }
function getCostId($item)
{
    return preg_replace('/\s+/', '', $item["courier_code"]."-".$item["price"]."-".$item["courier_service_name"]);
}


function countWeight ($value) {
    $unit = "g";
    if ($value >= 1000) {
        $value = ($value / 1000 );
        $unit = "Kg";
    }
    return number_format( ($value ) ,2,",",".") . " ".$unit;
}


function cleanQty($val) {
    if (empty(trim($val))) return 0;
    return $val;
}
