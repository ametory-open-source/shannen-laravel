<?php
namespace App\Classes;


class OrderItem
{

	public string $id = "";
	public string $sku = "";
	public string $name = "";
	public float $price = 0;
	public float $width = 0;
	public float $height = 0;
	public float $length = 0;
	public float $weight = 0;
	public array $options = [];
	public float $quantity = 0;
	public string $featureImage = "";



	public function __construct(Array $properties=array()){
        foreach($properties as $key => $value){
            $this->{$key} = $value;
        }
    }


}
