<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserProfileController;
use App\Http\Controllers\ResetPassword;
use App\Http\Controllers\ChangePassword;
use App\Http\Controllers\LandingController;

// Route::get('/', function () {
// 	return view('home');
// });

Route::get('/tool/command', function () {
	switch (request()->get("q")) {
		case 'migrate':
			Artisan::call('migrate', [
				'--path' => 'database/migrations',
				'--database' => 'mysql',
				'--force' => true
			]);
			break;

		case 'seed':
			Artisan::call('db:seed', [
				'--database' => 'mysql',
				'--force' => true
			]);
			break;

		case 'link':
			Artisan::call('storage:link', []);
			break;

		default:
			# code...
			break;
	}

	$output = Artisan::output();
	\Log::info(json_encode($output));

	return view('tool', compact("output"));

});

Route::get('/', [LandingController::class, 'main'])->name('main');
Route::get('/join', [LandingController::class, 'join'])->name('join');
Route::post('/join', [LandingController::class, 'post_join'])->name('join.post');
Route::get('/contact', [LandingController::class, 'contact'])->name('contact');
Route::get('/pricing', [LandingController::class, 'pricing'])->name('pricing');

Route::get('/p/{slug}', [LandingController::class, 'page'])->name('page');
Route::get('/blog', [LandingController::class, 'blogList'])->name('blogList');
Route::get('/products', [LandingController::class, 'products'])->name('products');
Route::get('/search', [LandingController::class, 'search'])->name('search');


// Route::get('/auth/login', [LandingController::class, 'auth_login'])->name('auth.login');
// Route::post('/auth/login', [LandingController::class, 'post_auth_login'])->name('auth.postLogin');
Route::get('/faq', [LandingController::class, 'faq'])->name('faq');
Route::post('/contact', [LandingController::class, 'contactSend'])->name('contactSend');
Route::get('/gallery', [LandingController::class, 'galleryList'])->name('galleryList');
Route::get('/gallery/{gallery}', [LandingController::class, 'gallery'])->name('gallery');
Route::get('/blog/{slug}', [LandingController::class, 'blog'])->name('blog');
Route::get('/galleries', [LandingController::class, 'galleries'])->name('galleries');
Route::post('/comments/{blog}', [LandingController::class, 'postComment'])->name('postComment');

Route::get('/cart', [LandingController::class, 'cart'])->name('cart');



Route::group(['prefix'=>'admin'],function(){
	// Route::post('/newsletter', [LandingController::class, 'newsletter'])->name('newsletter');
	// Route::get('/register', [RegisterController::class, 'create'])->middleware('guest')->name('register');
	// Route::post('/register', [RegisterController::class, 'store'])->middleware('guest')->name('register.perform');
	Route::get('/login', [LoginController::class, 'show'])->middleware('guest')->name('login');
	Route::get('/forgot', [LoginController::class, 'forgot'])->middleware('guest')->name('forgot');
	Route::post('/forgot', [LoginController::class, 'forgotAction'])->middleware('guest')->name('forgotAction');
	Route::post('/login', [LoginController::class, 'login'])->middleware('guest')->name('login.perform');

	// Route::get('/reset-password', [ResetPassword::class, 'show'])->middleware('guest')->name('reset-password');
	// Route::post('/reset-password', [ResetPassword::class, 'send'])->middleware('guest')->name('reset.perform');
	// Route::get('/change-password', [ChangePassword::class, 'show'])->middleware('guest')->name('change-password');
	// Route::post('/change-password', [ChangePassword::class, 'update'])->middleware('guest')->name('change.perform');


	Route::group(['middleware' => 'auth'], function () {
        Route::get('/profile', [HomeController::class, 'profile'])->name('admin.profile');
        Route::post('/profile', [HomeController::class, 'updateProfile'])->name('admin.updateProfile');
        Route::post('/change-password', [HomeController::class, 'changePassword'])->name('admin.changePassword');
		Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
		Route::post('/file/upload', [LandingController::class, 'upload'])->name('upload');
		Route::get('/template', [LandingController::class, 'template'])->name('template');
		Route::post('/template', [LandingController::class, 'save_template'])->name('template.post');
		// Route::get('/virtual-reality', [PageController::class, 'vr'])->name('virtual-reality');
		// Route::get('/rtl', [PageController::class, 'rtl'])->name('rtl');
		// Route::get('/profile', [UserProfileController::class, 'show'])->name('profile');
		// Route::post('/profile', [UserProfileController::class, 'update'])->name('profile.update');
		// Route::get('/profile-static', [PageController::class, 'profile'])->name('profile-static');
		// Route::get('/sign-in-static', [PageController::class, 'signin'])->name('sign-in-static');
		// Route::get('/sign-up-static', [PageController::class, 'signup'])->name('sign-up-static');

		Route::get('logout', [LoginController::class, 'logout'])->name('admin.logout');

		Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
		Route::get('/users/{user}/edit', [App\Http\Controllers\UsersController::class, 'edit'])->name('users.edit');
		Route::get('/users/{user}/suspend', [App\Http\Controllers\UsersController::class, 'suspend'])->name('users.suspend');
		Route::get('/users/{user}/unsuspend', [App\Http\Controllers\UsersController::class, 'unsuspend'])->name('users.unsuspend');
		Route::post('/users/{user}/update-avatar', [App\Http\Controllers\UsersController::class, 'updateAvatar'])->name('users.updateAvatar');
		Route::put('/users/{user}/update', [App\Http\Controllers\UsersController::class, 'update'])->name('users.update');




		Route::get('/pages', [App\Http\Controllers\PagesController::class, 'index'])->name('pages.index');
		Route::get('/pages/create', [App\Http\Controllers\PagesController::class, 'create'])->name('pages.create');
		Route::post('/pages', [App\Http\Controllers\PagesController::class, 'store'])->name('pages.store');
		Route::get('/pages/{page}/edit', [App\Http\Controllers\PagesController::class, 'edit'])->name('pages.edit');
		Route::put('/pages/{page}/update', [App\Http\Controllers\PagesController::class, 'update'])->name('pages.update');
		Route::get('/pages/{page}/delete', [App\Http\Controllers\PagesController::class, 'delete'])->name('modules.delete');


		Route::get('/modules', [App\Http\Controllers\ModulesController::class, 'index'])->name('modules.index');
		Route::get('/modules/create', [App\Http\Controllers\ModulesController::class, 'create'])->name('modules.create');
		Route::post('/modules', [App\Http\Controllers\ModulesController::class, 'store'])->name('modules.store');
		Route::get('/modules/{module}/delete', [App\Http\Controllers\ModulesController::class, 'delete'])->name('modules.delete');
		Route::get('/modules/{module}/edit', [App\Http\Controllers\ModulesController::class, 'edit'])->name('modules.edit');
		Route::put('/modules/{module}/update', [App\Http\Controllers\ModulesController::class, 'update'])->name('modules.update');


		Route::get('/newsletters', [App\Http\Controllers\NewslettersController::class, 'index'])->name('newsletters.index');


		Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');
		Route::get('/blogs/create', [App\Http\Controllers\BlogsController::class, 'create'])->name('blogs.create');
		Route::get('/blogs/{blog}/edit', [App\Http\Controllers\BlogsController::class, 'edit'])->name('blogs.edit');
		Route::put('/blogs/{blog}/update', [App\Http\Controllers\BlogsController::class, 'update'])->name('blogs.update');
		Route::get('/blogs/{blog}/delete', [App\Http\Controllers\BlogsController::class, 'delete'])->name('blogs.delete');


		Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');


		Route::get('/blogs', [App\Http\Controllers\BlogsController::class, 'index'])->name('blogs.index');
		Route::post('/blogs', [App\Http\Controllers\BlogsController::class, 'store'])->name('pages.store');


		Route::get('/blog_categories', [App\Http\Controllers\BlogCategoriesController::class, 'index'])->name('blog_categories.index');
		Route::get('/blog_categories/create', [App\Http\Controllers\BlogCategoriesController::class, 'create'])->name('blog_categories.create');
		Route::post('/blog_categories', [App\Http\Controllers\BlogCategoriesController::class, 'store'])->name('blog_categories.store');
		Route::get('/blog_categories/{blog_category}/edit', [App\Http\Controllers\BlogCategoriesController::class, 'edit'])->name('blog_categories.edit');
		Route::put('/blog_categories/{blog_category}/update', [App\Http\Controllers\BlogCategoriesController::class, 'update'])->name('blog_categories.update');
		Route::get('/blog_categories/{blog_category}/delete', [App\Http\Controllers\BlogCategoriesController::class, 'delete'])->name('blog_categories.delete');


		Route::get('/comments', [App\Http\Controllers\CommentsController::class, 'index'])->name('comments.index');
		Route::get('/comments/{comment}/edit', [App\Http\Controllers\CommentsController::class, 'edit'])->name('comments.edit');
		Route::post('/comments/{comment}/approval', [App\Http\Controllers\CommentsController::class, 'approval'])->name('comments.approval');
		Route::get('/comments/{comment}/delete', [App\Http\Controllers\CommentsController::class, 'delete'])->name('comments.delete');


		Route::get('/galleries', [App\Http\Controllers\GalleriesController::class, 'index'])->name('galleries.index');
		Route::get('/galleries/create', [App\Http\Controllers\GalleriesController::class, 'create'])->name('galleries.create');
		Route::post('/galleries', [App\Http\Controllers\GalleriesController::class, 'store'])->name('galleries.store');
		Route::get('/galleries/{gallery}/edit', [App\Http\Controllers\GalleriesController::class, 'edit'])->name('galleries.edit');
		Route::put('/galleries/{gallery}/update', [App\Http\Controllers\GalleriesController::class, 'update'])->name('galleries.update');
		Route::get('/galleries/{gallery}/cover', [App\Http\Controllers\GalleriesController::class, 'cover'])->name('galleries.cover');
		Route::get('/galleries/{gallery}/delete-item', [App\Http\Controllers\GalleriesController::class, 'deleteItem'])->name('galleries.deleteItem');



		Route::get('/contact_us', [App\Http\Controllers\ContactusesController::class, 'index'])->name('contact_us.index');
		Route::get('/contact_us/{contact_us}/delete', [App\Http\Controllers\ContactusesController::class, 'delete'])->name('contact_us.delete');
		Route::get('/contact_us/{contact_us}/readed', [App\Http\Controllers\ContactusesController::class, 'readed'])->name('contact_us.readed');


		Route::get('/statics', [App\Http\Controllers\StaticsController::class, 'index'])->name('statics.index');
		Route::get('/statics/create', [App\Http\Controllers\StaticsController::class, 'create'])->name('statics.create');
		Route::get('/statics/{static}/edit', [App\Http\Controllers\StaticsController::class, 'edit'])->name('statics.edit');
		Route::put('/statics/{static}/update', [App\Http\Controllers\StaticsController::class, 'update'])->name('statics.update');
		Route::get('/statics/{static}/delete', [App\Http\Controllers\StaticsController::class, 'delete'])->name('statics.delete');
		Route::post('/statics', [App\Http\Controllers\StaticsController::class, 'store'])->name('statics.store');


		// Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
		// Route::get('/users/{user}/edit', [App\Http\Controllers\UsersController::class, 'edit'])->name('users.edit');



	Route::get('/product_categories', [App\Http\Controllers\ProductCategoriesController::class, 'index'])->name('product_categories.index');
	Route::get('/product_categories/create', [App\Http\Controllers\ProductCategoriesController::class, 'create'])->name('product_categories.create');
	Route::post('/product_categories', [App\Http\Controllers\ProductCategoriesController::class, 'store'])->name('product_categories.store');
	Route::get('/product_categories/{product_category}/edit', [App\Http\Controllers\ProductCategoriesController::class, 'edit'])->name('product_categories.edit');
	Route::put('/product_categories/{product_category}/update', [App\Http\Controllers\ProductCategoriesController::class, 'update'])->name('product_categories.update');
	Route::get('/product_categories/{product_category}/delete', [App\Http\Controllers\ProductCategoriesController::class, 'delete'])->name('product_categories.delete');


	Route::get('/products', [App\Http\Controllers\ProductsController::class, 'index'])->name('products.index');
	Route::get('/products/create', [App\Http\Controllers\ProductsController::class, 'create'])->name('products.create');
	Route::get('/products/{product}/edit', [App\Http\Controllers\ProductsController::class, 'edit'])->name('products.edit');
	Route::get('/products/{product}/cover', [App\Http\Controllers\ProductsController::class, 'cover'])->name('products.cover');
	Route::post('/products', [App\Http\Controllers\ProductsController::class, 'store'])->name('products.store');
	Route::put('/products/{product}/update', [App\Http\Controllers\ProductsController::class, 'update'])->name('products.update');

    Route::get('/products/{product}/delete-item', [App\Http\Controllers\ProductsController::class, 'deleteItem'])->name('products.deleteItem');
    Route::get('/products/{product}/delete', [App\Http\Controllers\ProductsController::class, 'delete'])->name('products.delete');


	Route::get('/affiliates', [App\Http\Controllers\AffiliatesController::class, 'index'])->name('affiliates.index');
	Route::get('/members', [App\Http\Controllers\MembersController::class, 'index'])->name('members.index');
	Route::get('/members/{member}/edit', [App\Http\Controllers\MembersController::class, 'edit'])->name('members.edit');
	Route::put('/members/{member}/update', [App\Http\Controllers\MembersController::class, 'update'])->name('members.update');





	Route::get('/member_types', [App\Http\Controllers\MemberTypesController::class, 'index'])->name('member_types.index');


	Route::get('/provinces', [App\Http\Controllers\ProvincesController::class, 'index'])->name('provinces.index');


	Route::get('/cities', [App\Http\Controllers\CitiesController::class, 'index'])->name('cities.index');


	Route::get('/subdistricts', [App\Http\Controllers\SubdistrictsController::class, 'index'])->name('subdistricts.index');


	Route::get('/villages', [App\Http\Controllers\VillagesController::class, 'index'])->name('villages.index');


	Route::get('/orders', [App\Http\Controllers\OrdersController::class, 'index'])->name('orders.index');
	Route::get('/orders/{order}', [App\Http\Controllers\OrdersController::class, 'show'])->name('orders.show');
	Route::post('/orders/{order}/process', [App\Http\Controllers\OrdersController::class, 'process'])->name('orders.process');
	Route::post('/orders/{order}/request_pickup', [App\Http\Controllers\OrdersController::class, 'request_pickup'])->name('orders.request_pickup');
	Route::post('/orders/{order}/shipping', [App\Http\Controllers\OrdersController::class, 'shipping'])->name('orders.shipping');
	Route::post('/orders/{order}/tracking', [App\Http\Controllers\OrdersController::class, 'tracking'])->name('orders.tracking');
	Route::post('/orders/{order}/shipped', [App\Http\Controllers\OrdersController::class, 'shipped'])->name('orders.shipped');
	Route::post('/orders/{order}/finished', [App\Http\Controllers\OrdersController::class, 'finished'])->name('orders.finished');
	Route::post('/orders/{order}/update-receipt', [App\Http\Controllers\OrdersController::class, 'updateReceipt'])->name('orders.updateReceipt');
	Route::post('/orders/{order}/update', [App\Http\Controllers\OrdersController::class, 'update'])->name('orders.update');


	Route::get('/message_rooms', [App\Http\Controllers\MessageRoomsController::class, 'index'])->name('message_rooms.index');


	// DON'T REMOVE THIS LINE


		//
	});
});





Route::get('/login', [App\Http\Controllers\Affiliator\AuthController::class, 'show'])->middleware('guest')->name('login');
Route::get('/forgot', [App\Http\Controllers\Affiliator\AuthController::class, 'forgot'])->middleware('guest')->name('forgot');
Route::post('/forgot', [App\Http\Controllers\Affiliator\AuthController::class, 'forgotAction'])->middleware('guest')->name('forgotAction');
Route::get('/register', [App\Http\Controllers\Affiliator\AuthController::class, 'registerShow'])->middleware('guest')->name('registerShow');
Route::post('/register', [App\Http\Controllers\Affiliator\AuthController::class, 'register'])->middleware('guest')->name('register');
Route::get('/users/verification/{user}', [App\Http\Controllers\Affiliator\AuthController::class, 'verification'])->name('verification');
Route::get('/logout', [App\Http\Controllers\Affiliator\AuthController::class, 'logout'])->middleware('guest')->name('logout');
Route::post('/login', [App\Http\Controllers\Affiliator\AuthController::class, 'login'])->middleware('guest')->name('hr.post.login');

Route::group(['middleware' => env('APP_AFFILIATE_GUARD')], function () {
    Route::get('/profile', [App\Http\Controllers\Affiliator\ProfileController::class, 'show'])->name('affiliator.profile');
    Route::get('/profile/add-address', [App\Http\Controllers\Affiliator\ProfileController::class, 'add_address'])->name('affiliator.add_address');
    Route::post('/profile/add-address', [App\Http\Controllers\Affiliator\ProfileController::class, 'add_address_send'])->name('affiliator.add_address_send');
    Route::get('/profile/address/{id}/delete', [App\Http\Controllers\Affiliator\ProfileController::class, 'delete_address'])->name('affiliator.delete_address');
    Route::get('/profile/address/{id}', [App\Http\Controllers\Affiliator\ProfileController::class, 'show_address'])->name('affiliator.show_address');
    Route::put('/profile/address/{id}', [App\Http\Controllers\Affiliator\ProfileController::class, 'update_address'])->name('affiliator.update_address');
    Route::post('/profile', [App\Http\Controllers\Affiliator\ProfileController::class, 'profile_update'])->name('affiliator.profile_update');
    Route::post('/change-password', [App\Http\Controllers\Affiliator\ProfileController::class, 'change_password'])->name('affiliator.change_password');
    Route::post('/update-affiliate', [App\Http\Controllers\Affiliator\ProfileController::class, 'update_affiliate'])->name('affiliator.update_affiliate');
    Route::get('/join-affiliate', [App\Http\Controllers\Affiliator\ProfileController::class, 'join_affiliate'])->name('affiliator.join_affiliate');
    Route::get('/join-membership', [App\Http\Controllers\Affiliator\ProfileController::class, 'join_membership'])->name('affiliator.join_membership');
    Route::post('/profile/membership-cancel', [App\Http\Controllers\Affiliator\ProfileController::class, 'cancel_request'])->name('affiliator.cancel_request');
    Route::post('/join-membership', [App\Http\Controllers\Affiliator\ProfileController::class, 'join_member_send'])->name('affiliator.join_member_send');
    Route::post('/join-affiliate', [App\Http\Controllers\Affiliator\ProfileController::class, 'join_affiliate_send'])->name('affiliator.join_affiliate_send');

    Route::get('/checkout', [LandingController::class, 'checkout'])->name('checkout');
    Route::get('/order-success/{order_id}', [LandingController::class, 'order_success'])->name('order_success');
    Route::get('/orders/{id}', [App\Http\Controllers\Affiliator\OrderController::class, 'show'])->name('affOrder.show');
    Route::get('/orders/{id}/review', [App\Http\Controllers\Affiliator\OrderController::class, 'review'])->name('affOrder.review');
    Route::get('/orders/{id}/accepted', [App\Http\Controllers\Affiliator\OrderController::class, 'accepted'])->name('affOrder.accepted');
    Route::post('/orders/{id}/accepted', [App\Http\Controllers\Affiliator\OrderController::class, 'accepted_send'])->name('affOrder.accepted_send');
    Route::post('/orders/{id}/review', [App\Http\Controllers\Affiliator\OrderController::class, 'sendReview'])->name('affOrder.sendReview');
    Route::post('/orders/{id}', [App\Http\Controllers\Affiliator\OrderController::class, 'update'])->name('affOrder.update');
    Route::get('/orders/{id}/payment', [App\Http\Controllers\Affiliator\OrderController::class, 'payment'])->name('affOrder.payment');

    Route::get('/location/place', [App\Http\Controllers\StaticsController::class, 'place'])->name('location.place');
});

Route::post('/add-wishlist', [LandingController::class, 'addWishlist'])->name('addWishlist');
Route::delete('/delete-wishlist/{id}', [LandingController::class, 'deleteWishlist'])->name('deleteWishlist');

Route::get('/{page}', [App\Http\Controllers\StaticsController::class, 'viewPage'])->name('static');
Route::get('/{prefix}/{slug}/{id}', [App\Http\Controllers\StaticsController::class, 'viewProduct'])->name('viewProduct');
Route::post('/{prefix}/{slug}/{id}/review', [App\Http\Controllers\StaticsController::class, 'reviewProduct'])->name('reviewProduct');


Route::get('/location/province', [App\Http\Controllers\StaticsController::class, 'locationProvince'])->name('location.province');
Route::get('/location/city', [App\Http\Controllers\StaticsController::class, 'locationCity'])->name('location.city');
Route::get('/location/subdistrict', [App\Http\Controllers\StaticsController::class, 'locationSubdistrict'])->name('location.subdistrict');
Route::get('/village/{village}', [App\Http\Controllers\StaticsController::class, 'locationVillageDetail'])->name('location.village_detail');
Route::get('/location/village', [App\Http\Controllers\StaticsController::class, 'locationVillage'])->name('location.villages');
