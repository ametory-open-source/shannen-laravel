@extends("frontend.layout")

@php
$user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
@section('content')
@include('frontend.components.modules.flash-message')
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home') !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.My Account') !!}</li>
                    </ol>
                    <h2 class="page-header-title">{!! __('frontend.My Account') !!}</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->

<!--== Start My Account Area Wrapper ==-->
<section class="my-account-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                <div class="my-account-tab-menu nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link active" id="dashboad-tab" data-bs-toggle="tab" data-bs-target="#dashboad"
                        type="button" role="tab" aria-controls="dashboad" aria-selected="true">{!! __('frontend.Dashboard') !!}</button>
                    <button class="nav-link" id="orders-tab" data-bs-toggle="tab" data-bs-target="#orders" type="button"
                        role="tab" aria-controls="orders" aria-selected="false"> {!! __('frontend.Orders') !!}</button>
                    <button class="nav-link" id="chat-tab" data-bs-toggle="tab" data-bs-target="#chat" type="button"
                        role="tab" aria-controls="chat" aria-selected="false" style="display: flex; justify-content:space-between">
                        <span>{!! __('frontend.Chat') !!}</span>
                        <livewire:user-chat-bubble>

                    </button>
                    <button class="nav-link" id="address-tab" data-bs-toggle="tab" data-bs-target="#address"
                        type="button" role="tab" aria-controls="address" aria-selected="false"> {!! __('frontend.Address') !!}</button>
                    <button class="nav-link" id="member-tab" data-bs-toggle="tab" data-bs-target="#member" type="button"
                        role="tab" aria-controls="member" aria-selected="false"> {!! __('frontend.Membership Program') !!}</button>
                    <button class="nav-link" id="wishlist-tab" data-bs-toggle="tab" data-bs-target="#wishlist" type="button"
                        role="tab" aria-controls="wishlist" aria-selected="false"> {!! __('frontend.Wishlist') !!}</button>
                    @if (false)
                    <button class="nav-link" id="affiliate-tab" data-bs-toggle="tab" data-bs-target="#affiliate"
                        type="button" role="tab" aria-controls="affiliate" aria-selected="false"> Affiliate
                        Program</button>
                    @endif
                    @if ($user->affiliate && false)
                    <button class="nav-link" id="product-tab" data-bs-toggle="tab" data-bs-target="#product"
                        type="button" role="tab" aria-controls="product" aria-selected="false"> Product</button>
                    @endif


                    <button class="nav-link" id="account-info-tab" data-bs-toggle="tab" data-bs-target="#account-info"
                        type="button" role="tab" aria-controls="account-info" aria-selected="false">{!! __('frontend.Account Details') !!}</button>
                    <button class="nav-link" onclick="window.location.href='/logout'" type="button">{!! __('frontend.Logout') !!}</button>
                </div>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="dashboad" role="tabpanel" aria-labelledby="dashboad-tab">
                        @include('frontend.components.profile.dashboard')
                    </div>
                    <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                        @include('frontend.components.profile.order')
                    </div>
                    <div class="tab-pane fade" id="chat" role="tabpanel" aria-labelledby="chat-tab">
                        @include('frontend.components.profile.chat')
                    </div>
                    <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab">
                        @include('frontend.components.profile.address')
                    </div>
                    <div class="tab-pane fade" id="account-info" role="tabpanel" aria-labelledby="account-info-tab">
                        @include('frontend.components.profile.account')
                    </div>
                    <div class="tab-pane fade" id="member" role="tabpanel" aria-labelledby="member-tab">
                        @include('frontend.components.profile.member')
                    </div>
                    <div class="tab-pane fade" id="wishlist" role="tabpanel" aria-labelledby="wishlist-tab">
                        @include('frontend.components.profile.wishlist')
                    </div>
                    @if(false)
                    <div class="tab-pane fade" id="affiliate" role="tabpanel" aria-labelledby="affiliate-tab">
                        @include('frontend.components.profile.affiliate')
                    </div>
                    @endif
                    @if ($user->affiliate)
                    <div class="tab-pane fade" id="product" role="tabpanel" aria-labelledby="product-tab">
                        @include('frontend.components.profile.product')
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End My Account Area Wrapper ==-->
@endsection

@push('css')
<style>

</style>
@endpush
@push('scripts')

<script>
    //

    $('.nav-link').on('shown.bs.tab', function (e) {
        var target = $(e.target).data("bs-target") // activated tab
        if (target == "#product") {
            Livewire.emitTo('product-affiliate', 'resetProductPage')
        }
        if (target == "#address") {
            Livewire.emitTo('addresses', 'resetAddressPage')
        }
    });

    $(function() {
        let searchParams = new URLSearchParams(window.location.search)
        if (searchParams.has('tab')) {
            // $()
            $('#'+searchParams.get('tab')+'-tab').click()
        }
        // address-tab
    })
</script>
@endpush
