@extends("frontend.layout")

@section('content')
@include('frontend.components.modules.flash-message')
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home') !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.Profile') !!}</li>
                    </ol>
                    @if ($address->id)
                    <h2 class="page-header-title">{!! __('frontend.Edit Address') !!}</h2>
                    @else
                    <h2 class="page-header-title">{!! __('frontend.Add Address') !!}</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->

<section class="section-space">
    <div class="container">
        <div class="row mb-n8">
            <div class="myaccount-content">
                @if ($address->id)
                <h3>{!! __('frontend.Edit Address') !!}</h3>
                @else
                <h3>{!! __('frontend.Add Address') !!}</h3>
                @endif
                <form method="post" {{$address->id ? 'action=/profile/address/'.$address->id : null}}>
                    @csrf
                    @if ($address->id)
                    @method("PUT")
                    @endif
                    @form_hidden("longitude",$address->longitude)
                    @form_hidden("latitude",$address->latitude)
                    <div class="account-details-form">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="label" class="required">Label</label>
                                    <input type="text" name="label" id="label" value="{{ old('label') ?? $address->label }}" placeholder="ex: Office" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="recipient_name" class="required">{!! __('frontend.Recipient Name') !!}</label>
                                    <input required type="text" id="recipient_name" name="recipient_name"
                                        value="{{ old('recipient_name')  ?? $address->recipient_name  }}" placeholder="Recipient Name" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="phone_number" class="required">{!! __('frontend.Phone') !!}</label>
                                    <input required type="text" name="phone_number" id="phone_number" value="{{ old('phone_number')  ?? $address->phone_number }}"
                                        placeholder="ex: +62-812-xxx" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" id="email" value="{{ old('email')  ?? $address->email }}"
                                        placeholder="ex: amet@ametory.id" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="province_id" class="required">{!! __('frontend.Province') !!}</label>
                                    <span id="province_wrapper"><span>Loading ...</span></span>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="city_id" class="required">{!! __('frontend.City') !!}</label>
                                    <span id="city_wrapper"></span>
                                </div>
                            </div>


                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="subdistrict_id" class="required">{!! __('frontend.Subdistrict') !!}</label>
                                    <span id="subdistrict_wrapper"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="village_id" class="required">{!! __('frontend.Village') !!}</label>
                                    <span id="village_wrapper"></span>
                                </div>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="postal_code" class="required">{!! __('frontend.Postal Code') !!}</label>
                                    <input name="postal_code" type="text"
                                     id="postal_code" value="{{ old('postal_code') ?? $address->postal_code }}" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="notes">{!! __('frontend.Notes') !!}</label>
                                    <input name="notes" type="text"
                                     id="notes" value="{{ old('notes') ?? $address->notes }}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="single-input-item">
                                    <label for="address" class="required">{!! __('frontend.Address') !!}</label>
                                    <textarea onchange="changeAddress(this)" name="address" type="text" style="
                                        border: 1px solid #e8e8e8;
                                        width: 100%;
                                        background-color: transparent;
                                        padding: 20px 20px;
                                        color: #1f2226;
                                        font-size: 13px;
                                    " id="address" required="">{{ old('address')  ?? $address->address }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <div class="row">
                            <div class="col-lg-12 mb-2">
                                <div class="form-group">
                                    <input type="checkbox" name="is_default" style="width: 20px" checked value="1">
                                    <label for="address"  {{ $address->is_default ? 'checked' : null }} >{!! __('frontend.Default Address') !!}</label>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12 mb-2">
                                <label for="address" class="required">Drop Point</label>
                                <div id="googleMap" style="width:100%;height:400px;"></div>
                            </div>

                        </div>
                        <div class="d-flex">

                            <div class="single-input-item">
                                <button class="check-btn sqr-btn">{{ $address->id ? __('frontend.Edit Address')  : __('frontend.Add New Address') }}</button>
                            </div>
                            <div class="aside-cart-wrapper mr-2" style="margin-left:20px">
                                <button type="button" onclick="location.href='/profile?tab=address'" class="check-btn btn-total ">{!! __('frontend.Cancel') !!}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@push('css')
    <style>
        /* select {
            border: 1px solid #e8e8e8 !important;
            width: 100%;
            height: 50px;
            background-color: transparent;
            padding: 2px 20px;
            color: #1f2226;
            font-size: 13px;
            border-radius: 0px !important;
        } */
        .form-control.wide {
            border-radius: 0;
            box-shadow: none;
            border: 1px solid #e8e8e8;
            color: #626262;
            font-size: 14px;
            height: 50px;
            line-height: 50px;
            padding: 0 20px;
        }
        .nice-select:after {
            right: 10px !important;
        }
    </style>
@endpush

@push('scripts')

<script>
    var province_id = {{$address->province_id ?? 'null'}}
    var city_id = {{$address->city_id ?? 'null'}}
    var subdistrict_id = {{$address->subdistrict_id ?? 'null'}}
    var village_id = {{$address->village_id ?? 'null'}}
    var latitude = {{ $address->latitude ?? 'null' }}
    var longitude = {{ $address->longitude ?? 'null' }}
    var myLatlng;
    var marker;
    var map
    const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
    };

    function success(pos) {
        const crd = pos.coords;
        myLatlng = new google.maps.LatLng(latitude ?? crd.latitude , longitude ?? crd.longitude);
        $("[name=latitude]").val(crd.latitude)
        $("[name=longitude]").val(crd.longitude)
        var mapProp= {
            center: myLatlng,
            zoom: 17,
        };
        map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable: true
        });


        marker.setPosition(myLatlng);
        marker.setVisible(true);


        google.maps.event.addListener(marker, 'dragend', function() {

            $("[name=latitude]").val(marker.getPosition().lat())
            $("[name=longitude]").val(marker.getPosition().lng())
        })
    }

    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }



    function myMap() {
        navigator.geolocation.getCurrentPosition(success, error, options);
    }

    function getProvince() {
        $.get("/location/province", function(data, err) {
            $('#province_wrapper').html(data)
            if (province_id) {
                $('[name=province_id]').val(province_id)
                getCity(province_id)
            }

            $('select').niceSelect();
        })
    }
    function getCity(province_id) {

        $('#city_wrapper').html('<select class="form-control"></select>')
        $('#subdistrict_wrapper').html('<select class="form-control"></select>')
        $('#village_wrapper').html('<select class="form-control"></select>')

        $.get("/location/city?province_id="+province_id, function(data, err) {
            $('#city_wrapper').html(data)
            if (city_id) {
                $('[name=city_id]').val(city_id)
                getSubdistrict(city_id)
            }

            $('select').niceSelect();
        })
    }
    function getSubdistrict(city_id) {
        $('#subdistrict_wrapper').html('<select class="form-control"></select>')
        $('#village_wrapper').html('<select class="form-control"></select>')

        $.get("/location/subdistrict?city_id="+city_id, function(data, err) {
            $('#subdistrict_wrapper').html(data)
            if (subdistrict_id) {
                $('[name=subdistrict_id]').val(subdistrict_id)
                getVillage(subdistrict_id)
            } else {
                selectSubdistrict()
            }

            $('select').niceSelect();
        })
    }
    function getVillage(subdistrict_id) {
        $('#village_wrapper').html('<select class="form-control"></select>')

        $.get("/location/village?subdistrict_id="+subdistrict_id, function(data, err) {
            $('#village_wrapper').html(data)
            if (village_id) {
                $('[name=village_id]').val(village_id)
            } else {
                selectVillage()
            }
            // alert({{$address->village_id}})
            $('select').niceSelect();
        })
    }
    function changeAddress(el)
    {
        $.get("/location/place?search="+$(el).val(), function(d) {
            if (d.length > 0) {

                var newPos = new google.maps.LatLng(d[0].geometry.location.lat, d[0].geometry.location.lng)
                marker.setPosition(newPos);
                var latLng = marker.getPosition();
                map.setCenter(latLng);
                marker.setVisible(true);
                var conf = confirm("Change address to\n\""+d[0].formatted_address +"\"")
                if (conf) {
                    $("[name=address]").val(d[0].formatted_address)
                }
            }
        });
    }
    function selectSubdistrict(id)
    {

        $.get("/location/place?search="+$('[name=subdistrict_id] option:selected').text(), function(d) {
            // if (d.length > 0) {
            //     var newPos = new google.maps.LatLng(d[0].geometry.location.lat, d[0].geometry.location.lng)
            //     marker.setPosition(newPos);
            //     var latLng = marker.getPosition();
            //     map.setCenter(latLng);
            //     marker.setVisible(true);
            // }
        });
    }
    function selectVillage(id)
    {
        // alert()
        if ($('[name=village_id] option:selected').text()) {
            $.get("/location/place?search="+$('[name=village_id] option:selected').text(), function(d) {
                if (d.length > 0) {
                    var newPos = new google.maps.LatLng(d[0].geometry.location.lat, d[0].geometry.location.lng)
                    marker.setPosition(newPos);
                    var latLng = marker.getPosition();
                    map.setCenter(latLng);
                    marker.setVisible(true);
                }
            });
        }

    }

    function getPostalCode(village_id)
    {
        $.get("/village/"+village_id, function(d) {
            $('[name=postal_code]').val(d.postal_code)
        });
    }

    $(function() {
        getProvince()

        $(document).on("change", "[name=province_id]", function() {
            city_id = null
            subdistrict_id = null
            village_id = null
            getCity($(this).val())
        })
        $(document).on("change", "[name=city_id]", function() {
            subdistrict_id = null
            village_id = null
            getSubdistrict($(this).val())
        })
        $(document).on("change", "[name=subdistrict_id]", function() {
            village_id = null
            // alert($(this).val())
            getVillage($(this).val())
        })
        $(document).on("change", "[name=village_id]", function() {
            selectVillage($(this).val())
            getPostalCode($(this).val())
        })

    })
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('APP_GOOGLE_MAP_KEY')}}&callback=myMap"></script>

@endpush
