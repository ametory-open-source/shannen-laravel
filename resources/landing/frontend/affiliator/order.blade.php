@extends("frontend.layout")

@php
$user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
@section('content')
@include('frontend.components.modules.flash-message')
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home')
                                !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.My Account')
                            !!}</li>
                    </ol>
                    <h2 class="page-header-title">{!! __('frontend.Order Detail') !!}</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="shopping-checkout-wrap section-space">
    <div class="container">
        <div class="checkout-page-coupon-wrap">
        </div>
        <div class="row">
            <div class="col-lg-8">
                {{-- <div class="checkout-order-details-wrap">



                </div> --}}
                <div class="checkout-order-details-wrap">
                    <div class="order-details-table-wrap table-responsive">

                        <h2 class="title mb-25">{!! __('frontend.Your order') !!}</h2>
                        <h5>Kode Pesanan: {{ $order->order_code }}</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="product-name">{!! __('frontend.Product') !!}</th>
                                    <th class="product-total">{!! __('frontend.Total') !!}</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($order->items as $id => $item)
                                <tr class="cart-item">
                                    <td class="product-name">{{ $item->name }} <p class="product-quantity"><i>{{
                                                money($item->price) }} × {{number($item->quantity)}}</i></p>
                                    </td>
                                    <td class="product-total">{{ money($item->price * $item->quantity) }}</td>
                                </tr>
                                @endforeach

                            </tbody>
                            <tfoot class="table-foot">
                                <tr class="cart-subtotal">
                                    <th>{!! __('frontend.Subtotal') !!}</th>
                                    <td>{{ money($order->total) }}</td>
                                </tr>
                                <tr class="shipping">
                                    <th>{!! __('frontend.Shipping') !!}</th>
                                    <td>{{ money($order->shipping_cost->value) }}</td>

                                </tr>
                                @if($order->discount)
                                <tr class="order-total">
                                    <th>Discount </th>
                                    <td>{{ $order->discount }}%</td>
                                </tr>
                                @endif
                                <tr class="order-total">
                                    <th>Total </th>
                                    <td>{{ money($order->grand_total) }}</td>
                                </tr>
                                <tr class="order-total">
                                    <th>Status </th>
                                    <td>{{ __('frontend.'.$order->status) }}</td>
                                </tr>
                                @if($order->status == "SHIPPED")
                                <tr class="order-total">
                                    <th>Selesaikan </th>
                                    <td>
                                        <a style="text-align: center" href="/orders/{{$order->id}}/accepted"
                                            class="red-button mt-20">Pesanan Telah Diterima</a>
                                    </td>
                                </tr>
                                @endif
                                <tr class="order-total">
                                    <th>{{ __('frontend.Notes') }} </th>
                                    <td>{{ $order->notes }}</td>
                                </tr>

                            </tfoot>
                        </table>
                        @if($order->status == "UNPAID")
                        <livewire:select-payment :order='$order' />
                        @endif
                        @if($order->payment_receipt)
                        <h6>Bukti Transfer</h6>
                        <img src="{{ assetUrl($order->payment_receipt) }}" alt="" srcset="">
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="checkout-billing-details-wrap">
                    <h2 class="title">{!! __('frontend.Shipping details') !!}</h2>
                    <div class="billing-form-wrap">
                        <h5>{{ $order->shipping_address->recipient_name}}</h5>
                        <p style="margin-bottom:2px">{{ $order->shipping_address->address}}</p>
                        <p style="margin-bottom:2px"><strong>{!! __('frontend.Phone') !!}</strong> : {{
                            $order->shipping_address->phone_number}}</p>
                        <p style="margin-bottom:2px"><strong>{!! __('frontend.Email') !!} </strong>: {{
                            $order->shipping_address->email}}</p>
                    </div>
                    <h2 class="title" style="margin-top: 80px">{!! __('frontend.Billing details') !!}</h2>
                    <div class="billing-form-wrap">
                        <h5>{{ $order->billing_address->recipient_name}}</h5>
                        <p style="margin-bottom:2px">{{ $order->billing_address->address}}</p>
                        <p style="margin-bottom:2px"><strong>{!! __('frontend.Phone') !!} </strong>: {{
                            $order->billing_address->phone_number}}</p>
                        <p style="margin-bottom:2px"><strong>{!! __('frontend.Email') !!}</strong> : {{
                            $order->billing_address->email}}</p>
                    </div>

                    @if(isset($order->tracking_order["history"]))
                    <h2 class="title" style="margin-top: 80px">Tracking History</h2>
                    @foreach ($order->tracking_order["history"] as $item)
                    <div class="order-history">
                        <div style="text-align: left"> <strong> <small> {{ \Carbon\Carbon::parse(
                                    $item["updated_at"])->format("d M Y H:i:s") }} </small> </strong></div>
                        <div><i> {{$item["note"]}}</i></div>
                    </div>
                    @endforeach
                    @endif

                    @if (!$order->reviews->count() && $order->status == 'FINISHED')

                    <a href="/orders/{{$order->id}}/review" class="red-button mt-20">Buat
                        Ulasan Pesanan</a>
                    @endif

                    @if($order->order_images)
                    <h2 class="title" style="margin-top: 80px">Foto</h2>
                    <div style="display:flex">
                        @foreach ($order->order_images as $img)
                        <a target="_blank" href="{{ $img }}">
                            <img style="width: 100px;height:100px;object-fit: cover; cursor: pointer;"
                                src="{{ $img }}" alt="" srcset="">
                        </a>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</section>

<!--== End My Account Area Wrapper ==-->
@endsection

@push('css')
<style>
    th {
        font-weight: bold !important;
    }

    .bank_account_wrapper {
        padding: 20px !important;
        border: 1px solid rgba(52, 53, 56, .1);
    }

    .bank_account.bank_account_header {
        font-weight: bold
    }

    .bank_account {
        margin-bottom: 2px;
        font-size: 12pt;
    }

    .order-history {
        margin-bottom: 10px;
        padding-bottom: 10px;
        border-bottom: 1px solid rgba(52, 53, 56, .1);
    }

    .order-history:last-child {
        border-bottom: none;
    }
</style>
@endpush
@push('scripts')

<script>
    $('#check_payments').on('hidden.bs.collapse', function () {
        console.log($(this))
    })


</script>
@endpush
