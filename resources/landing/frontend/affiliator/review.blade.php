@extends("frontend.layout")

@php
$user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
@section('content')
@include('frontend.components.modules.flash-message')
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home')
                                !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.My Account')
                            !!}</li>
                    </ol>
                    <h2 class="page-header-title">{!! __('frontend.Order Detail') !!}</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="shopping-checkout-wrap section-space">
    <div class="container">
        <div class="checkout-page-coupon-wrap">
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form action="" method="post">
                    @csrf

                <div class="checkout-order-details-wrap">
                    <div class="order-details-table-wrap table-responsive">

                        <h2 class="title mb-25">{!! __('frontend.Your order') !!}</h2>
                        <h5>Kode Pesanan: {{ $order->order_code }}</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="product-name">{!! __('frontend.Product') !!}</th>
                                    <th class="product-total">{!! __('frontend.Total') !!}</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                                @foreach ($order->items as $id => $item)
                                <tr class="cart-item">
                                    <td class="product-name">{{ $item->name }} <p class="product-quantity"><i>{{
                                                money($item->price) }} × {{number($item->quantity)}}</i></p>
                                        <div class="form-input-item" wire:ignore="">
                                            <div class="form-ratings-item">
                                                <span class="">Berikan Rating</span>
                                                <div class="product-ratingsform-form-wrap">

                                                    <ul class="list-inline rating-list">
                                                        <li><i data-id="{{$item->id}}" data-rate="1" class="fa fa-star yellow"></i></li>
                                                        <li><i data-id="{{$item->id}}" data-rate="2" class="fa fa-star yellow"></i></li>
                                                        <li><i data-id="{{$item->id}}" data-rate="3" class="fa fa-star yellow"></i></li>
                                                        <li><i data-id="{{$item->id}}" data-rate="4" class="fa fa-star yellow"></i></li>
                                                        <li><i data-id="{{$item->id}}" data-rate="5" class="fa fa-star gray"></i></li>
                                                        @form_hidden("reviews[".$item->id."][rate]", 4, ["id" => "rate-".$item->id])
                                                    </ul>
                                                </div>
                                            </div>
                                            <textarea style="margin-top: 10px" id="review-{{$item->id}}" name="reviews[{{$item->id}}][feedback]" class="form-control" placeholder="{!! __('frontend.Enter your feedback') !!}"></textarea>
                                            <input style="margin-top: 10px"  type="checkbox" name="reviews[{{$item->id}}][anonym]" id="anonym-{{$item->id}}" id=""> {!! __('frontend.Provide ratings anonymously') !!}.
                                        </div>
                                    </td>
                                    <td class="product-total">{{ money($item->price * $item->quantity) }}</td>
                                </tr>
                                @endforeach

                            </tbody>

                        </table>

                        <button type="submit" class="red-button mt-20">PESANAN</button>
                    </div>
                </div>


                </form>
            </div>

        </div>
    </div>

</section>

<!--== End My Account Area Wrapper ==-->
@endsection

@push('css')
<style>
    .fa-star:before {
        content: "\f005";
    }

    .rating-list li i.yellow {
        color: #FFD700;
    }

    .rating-list li i.gray {
        color: #bbb;
    }

    .list-inline>li {
        display: inline-block;
        padding-right: 5px;
        padding-left: 5px;
    }

    .rating-list li {
        padding: 0px;
    }

    .fa {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        transform: translate(0, 0);
    }
</style>
@endpush
@push('scripts')

<script>
    var current_star_statusses = [];

    star_elements = $('.fa-star').parent();

    star_elements.find(".fa-star").each(function(i, elem) {
    current_star_statusses.push($(elem).hasClass('yellow'));
    });

    star_elements.find(".fa-star").mouseenter(changeRatingStars);
    star_elements.find(".fa-star").mouseleave(resetRatingStars);


function changeRatingStars()
{
    var star = $(this);
    var rate = star.data("rate")
    var id = star.data("id")
    console.log(id, rate)
    $('#rate-'+id).val(rate)
    $('.fa-star').removeClass('gray').removeClass('yellow');
    star.addClass('yellow');
    star.parent().prevAll().children('.fa-star').addClass('yellow');
    star.parent().nextAll().children('.fa-star').addClass('gray');
}


function resetRatingStars() {
    star_elements.each(function(i, elem) {
        $(elem).removeClass('yellow').removeClass('gray').addClass(current_star_statusses[i] ? 'yellow' : 'gray');
    });
}

</script>
@endpush
