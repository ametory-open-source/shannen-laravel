@extends("frontend.layout")

@section('content')
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home') !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.Account') !!}
                        </li>
                    </ol>
                    <h2 class="page-header-title">{!! __('frontend.Account') !!}</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->


<!--== Start Account Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row mb-n8">
            @if (!request()->get('register_only'))
            <div class="col-lg-6 mb-8">
                <!--== Start Login Area Wrapper ==-->
                <div class="my-account-item-wrap">
                    <h3 class="title">{!! __('frontend.Login') !!}</h3>
                    @if(session('success'))
                    <div id="attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>{{ session('success') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @push('scripts')
                    <script>
                        $(function() {
                                    $("#attention").modal("show")
                                })
                    </script>
                    @endpush
                    @push('css')
                    <style>
                        #attention .modal-body {
                            background-color: #01830A;
                            color: white;
                        }
                    </style>
                    @endpush
                    @endif
                    @if ($errors->any())
                    @foreach ($errors->all() as $item)
                    <div id="attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>{{$item}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @push('scripts')
                    <script>
                        $(function() {
                                    $("#attention").modal("show")
                                })
                    </script>
                    @endpush
                    @push('css')
                    <style>
                        #attention .modal-body {
                            background-color: #ff6565;
                            color: white;
                        }
                    </style>
                    @endpush
                    @endif
                    <div class="my-account-form">
                        <form method="post">
                            @csrf
                            <div class="form-group mb-6">
                                <label for="login_username"> {!! __('frontend.Email Address') !!} <sup>*</sup></label>
                                <input name="email" value="{{ old('email') }}" type="email" id="login_username"
                                    placeholder="{!! __('frontend.Email Address') !!}">
                            </div>

                            <div class="form-group mb-6">
                                <label for="login_pwsd">Password <sup>*</sup></label>
                                <input name="password" type="password" id="login_pwsd" placeholder="Password">
                            </div>

                            <div class="form-group d-flex align-items-center mb-14">
                                <button class="btn" type="submit">{!! __('frontend.Login') !!}</button>

                                <div class="form-check ms-3">
                                    <input type="checkbox" class="form-check-input" name="remember" id="remember_pwsd">
                                    <label class="form-check-label" for="remember_pwsd">{!! __('frontend.Remember Me')
                                        !!}</label>
                                </div>
                            </div>
                            <a style="color: #333; font-weight:600" class="lost-password" href="/register">{!!
                                __('frontend.register_words') !!}?</a>
                            <a class="lost-password" href="/forgot">{!! __('frontend.Lost your Password') !!}?</a>
                        </form>
                    </div>
                </div>
                <!--== End Login Area Wrapper ==-->
            </div>
            @endif


        </div>
    </div>
</section>
<!--== End Account Area Wrapper ==-->


@endsection
