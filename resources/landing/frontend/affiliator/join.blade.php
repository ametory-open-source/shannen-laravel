@extends("frontend.layout")
@php
    $user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
@section("content")
@include('frontend.components.modules.flash-message')
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">Home</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">Join Affiliate</li>
                    </ol>
                    <h2 class="page-header-title">Join Affiliate</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="my-account-area section-space">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-8">
                <div class="my-account-item-wrap">
                    <h3 class="title">Join Affliate Program</h3>
                    <div class="my-account-form">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mb-6">
                                <label for="register_username"> Email Address <sup>*</sup></label>
                                <input name="email" type="email" id="register_username" value="{{$user->email}}" disabled>
                            </div>

                            <div class="form-group mb-6">
                                <label for="username">Username <sup>*</sup></label>
                                <input name="username" type="text" id="username" value="{{ old('username') }}" required>
                            </div>
                            <div class="form-group mb-6">
                                <label for="display_name">Display Name <sup>*</sup></label>
                                <input name="display_name" type="text" id="display_name" value="{{ old('display_name') }}" required>
                            </div>
                            <div class="form-group mb-6">
                                <label for="address">Address <sup>*</sup></label>
                                <textarea name="address" type="text" style="
                                border: 1px solid #e8e8e8;
                                width: 100%;
                                background-color: transparent;
                                padding: 20px 20px;
                                color: #1f2226;
                                font-size: 13px;
                            " id="address" required>{{ old('address') }}</textarea>
                            </div>

                            <div class="form-group">
                                <p class="desc mb-4">By filling out this form I have read and agree to all terms and conditions.
                                </p>
                                <button class="btn" href="my-account.html">Register</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 mb-8">
                <div class="my-account-item-wrap">
                    <h3 class="title">Term & Condition</h3>
                    <div class="term">
                        {!! $web->affiliate_term !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('css')
<style>
    .term {
        height: 500px;
        overflow-y: scroll;
        font-size: 80%;
        padding: 20px;
        border: 1px solid #f5f5f5;
    }
</style>
@endpush
