@extends("frontend.layout")

@section('content')
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home') !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.Account') !!}</li>
                    </ol>
                    <h2 class="page-header-title">{!! __('frontend.Account') !!}</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->


<!--== Start Account Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row mb-n8">

            <div class="col-lg-6 mb-8">
                <!--== Start Register Area Wrapper ==-->
                <div class="my-account-item-wrap">
                    <h3 class="title">{!! __('frontend.Register') !!}</h3>
                    @if(session('success'))
                    <div id="attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>{{ session('success') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @push('scripts')
                            <script>
                                $(function() {
                                    $("#attention").modal("show")
                                })
                            </script>
                        @endpush
                        @push('css')
                            <style>
                                #attention .modal-body{
                                    background-color: #01830A;
                                    color: white;
                                }
                            </style>
                        @endpush
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $item)
                        <div id="attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <p>{{$item}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @push('scripts')
                            <script>
                                $(function() {
                                    $("#attention").modal("show")
                                })
                            </script>
                        @endpush
                        @push('css')
                            <style>
                                #attention .modal-body{
                                    background-color: #ff6565;
                                    color: white;
                                }
                            </style>
                        @endpush
                    @endif
                    <div class="my-account-form">
                        <form action="/register" method="post" >
                            @csrf
                            <div class="form-group mb-6">
                                <label for="register_fullname"> {!! __('frontend.Full Name') !!} <sup>*</sup></label>
                                <input placeholder="{!! __('frontend.Full Name') !!}" name="name" type="text" id="register_fullname" value="{{old('name')}}" required >
                            </div>
                            <div class="form-group mb-6">
                                <label for="register_phone_number"> {!! __('frontend.Phone Number') !!} <sup>*</sup></label>
                                <input required name="phone_number" type="text" id="register_phone_number" value="{{old('phone_number')}}" placeholder="{!! __('frontend.Phone Number') !!}">
                            </div>
                            <div class="form-group mb-6">
                                <label for="register_username"> {!! __('frontend.Email Address') !!} <sup>*</sup></label>
                                <input required name="email" type="email" id="register_username" value="{{ request('email') ?? old('email')}}" placeholder="{!! __('frontend.Email Address') !!}">
                            </div>

                            <div class="form-group mb-6">
                                <label for="register_pwsd">{!! __('frontend.Password') !!} <sup>*</sup></label>
                                <input required name="password" type="password" id="register_pwsd" placeholder="{!! __('frontend.Password') !!}">
                            </div>

                            <div class="form-group">
                                <p class="desc mb-4">{!! __('frontend.register_message') !!}</p>
                                <button class="btn" type="submit">{!! __('frontend.Register') !!}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--== End Register Area Wrapper ==-->
            </div>
        </div>
    </div>
</section>
<!--== End Account Area Wrapper ==-->


@endsection

