@extends("frontend.layout")
@php
$user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
@section("content")
@include('frontend.components.modules.flash-message')
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">Home</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">Join Membership</li>
                    </ol>
                    <h2 class="page-header-title">Join Membership</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="my-account-area section-space">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-8 myaccount-content">
                <div class="my-account-item-wrap  account-details-form">
                    <h3 class="title">{{ __('frontend.Join Membership Program') }}</h3>
                    <div class="my-account-item-wrap">
                        <h3 class="title">{{ __('frontend.Term & Condition') }}</h3>
                        <div class="term-membership">
                            {!! $web->member_term !!}
                        </div>
                    </div>
                    <h5 class="mb-5">{{ __('frontend.Membership Type') }} </h5>
                    <form action="" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="myaccount-table table-responsive text-center">
                            <table class="table table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th>{!! __('frontend.membership_type') !!}</th>
                                        <th>{!! __('frontend.first_minimum_order') !!}</th>
                                        <th>{!! __('frontend.minimum_order') !!}</th>
                                        <th>{!! __('frontend.discount') !!}</th>
                                        <th>{!! __('frontend.below_minimum_order_discount') !!}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($memberships as $item)
                                    <tr>
                                        <td>{!! $item->name !!}</td>
                                        <td>{!! money($item->first_minimum_order) !!}</td>
                                        <td>{!! money($item->minimum_order) !!}</td>
                                        <td>{!! $item->discount !!}%</td>
                                        <td>{!! $item->below_minimum_order_discount !!}%</td>
                                        <td>@form_radio("member_type_id", $item->id, ["required"])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <h5 class="mb-5 text-left mt-10">{{ __('frontend.Personal Data') }} </h5>
                        <div class="row">
                            @form_file("id_card", ["style" => "visibility:hidden", "accept" => "image/*"])
                            <div class="col-lg-6">
                                <h5 class="mb-0">Upload KTP</h5>
                                <img width="400" src="/assets/images/id-card.png" alt="" srcset="" id="id-card"
                                    class="pointer">
                            </div>
                            <div class="col-lg-6">

                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="province_id" class="required text-left">{!! __('frontend.Province')
                                        !!}</label>
                                    <span id="province_wrapper"><span>Loading ...</span></span>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="city_id" class="required text-left">{!! __('frontend.City') !!}</label>
                                    <span id="city_wrapper"></span>
                                </div>
                            </div>


                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="subdistrict_id" class="required text-left">{!!
                                        __('frontend.Subdistrict')
                                        !!}</label>
                                    <span id="subdistrict_wrapper"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="village_id" class="required text-left">{!! __('frontend.Village')
                                        !!}</label>
                                    <span id="village_wrapper"></span>
                                </div>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-6">
                                <div class="single-input-item">
                                    <label for="postal_code" class="required text-left">{!! __('frontend.Postal Code')
                                        !!}</label>
                                    <input name="postal_code" type="text" id="postal_code"
                                        value="{{ old('postal_code') }}" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="single-input-item">
                                    <label for="address" class="required text-left">{!! __('frontend.Address')
                                        !!}</label>
                                    <textarea onchange="changeAddress(this)" name="address" type="text" style="
                                            border: 1px solid #e8e8e8;
                                            width: 100%;
                                            background-color: transparent;
                                            padding: 20px 20px;
                                            color: #1f2226;
                                            font-size: 13px;
                                        " id="address" required="">{{ old('address') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                        <div class="row">


                        </div>
                        <div class="row">
                            <div class="col-lg-12 mb-2">
                                <label for="address" class="required text-left">Drop Point</label>
                                <div id="googleMap" style="width:100%;height:400px;"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <p class="desc mb-4">{!! __('frontend.agree_message') !!}
                            </p>
                            <button class="btn">{!! __('frontend.I Agree') !!}</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>

    </div>
</section>
@endsection

@push('css')
<style>
    .term {
        height: 500px;
        overflow-y: scroll;
        font-size: 80%;
        padding: 20px;
        border: 1px solid #f5f5f5;
    }

    .form-control.wide {
        border-radius: 0;
        box-shadow: none;
        border: 1px solid #e8e8e8;
        color: #626262;
        font-size: 14px;
        height: 50px;
        line-height: 50px;
        padding: 0 20px;
    }

    .nice-select:after {
        right: 10px !important;
    }
</style>
@endpush


@push('scripts')

<script>
    var province_id = null;
    var city_id = null;
    var subdistrict_id = null;
    var village_id = null;
    var latitude = null;
    var longitude = null;
    var myLatlng;
    var marker;
    var map
    const options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0,
    };

    function success(pos) {
        const crd = pos.coords;
        myLatlng = new google.maps.LatLng(latitude ?? crd.latitude , longitude ?? crd.longitude);
        $("[name=latitude]").val(crd.latitude)
        $("[name=longitude]").val(crd.longitude)
        var mapProp= {
            center: myLatlng,
            zoom: 17,
        };
        map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
        marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable: true
        });


        marker.setPosition(myLatlng);
        marker.setVisible(true);


        google.maps.event.addListener(marker, 'dragend', function() {

            $("[name=latitude]").val(marker.getPosition().lat())
            $("[name=longitude]").val(marker.getPosition().lng())
        })
    }

    function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }



    function myMap() {
        navigator.geolocation.getCurrentPosition(success, error, options);
    }

    function getProvince() {
        $.get("/location/province", function(data, err) {
            $('#province_wrapper').html(data)
            if (province_id) {
                $('[name=province_id]').val(province_id)
                getCity(province_id)
            }

            $('select').niceSelect();
        })
    }
    function getCity(province_id) {

        $('#city_wrapper').html('<select class="form-control"></select>')
        $('#subdistrict_wrapper').html('<select class="form-control"></select>')
        $('#village_wrapper').html('<select class="form-control"></select>')

        $.get("/location/city?province_id="+province_id, function(data, err) {
            $('#city_wrapper').html(data)
            if (city_id) {
                $('[name=city_id]').val(city_id)
                getSubdistrict(city_id)
            }

            $('select').niceSelect();
        })
    }
    function getSubdistrict(city_id) {
        $('#subdistrict_wrapper').html('<select class="form-control"></select>')
        $('#village_wrapper').html('<select class="form-control"></select>')

        $.get("/location/subdistrict?city_id="+city_id, function(data, err) {
            $('#subdistrict_wrapper').html(data)
            if (subdistrict_id) {
                $('[name=subdistrict_id]').val(subdistrict_id)
                getVillage(subdistrict_id)
            } else {
                selectSubdistrict()
            }

            $('select').niceSelect();
        })
    }
    function getVillage(subdistrict_id) {
        $('#village_wrapper').html('<select class="form-control"></select>')

        $.get("/location/village?subdistrict_id="+subdistrict_id, function(data, err) {
            $('#village_wrapper').html(data)
            if (village_id) {
                $('[name=village_id]').val(village_id)
            } else {
                selectVillage()
            }

            $('select').niceSelect();
        })
    }
    function changeAddress(el)
    {
        $.get("/location/place?search="+$(el).val(), function(d) {
            if (d.length > 0) {

                var newPos = new google.maps.LatLng(d[0].geometry.location.lat, d[0].geometry.location.lng)
                marker.setPosition(newPos);
                var latLng = marker.getPosition();
                map.setCenter(latLng);
                marker.setVisible(true);
                var conf = confirm("Change address to\n\""+d[0].formatted_address +"\"")
                if (conf) {
                    $("[name=address]").val(d[0].formatted_address)
                }
            }
        });
    }
    function selectSubdistrict(id)
    {

        $.get("/location/place?search="+$('[name=subdistrict_id] option:selected').text(), function(d) {
            // if (d.length > 0) {
            //     var newPos = new google.maps.LatLng(d[0].geometry.location.lat, d[0].geometry.location.lng)
            //     marker.setPosition(newPos);
            //     var latLng = marker.getPosition();
            //     map.setCenter(latLng);
            //     marker.setVisible(true);
            // }
        });
    }
    function selectVillage(id)
    {
        // alert()
        if ($('[name=village_id] option:selected').text()) {
            $.get("/location/place?search="+$('[name=village_id] option:selected').text(), function(d) {
                if (d.length > 0) {
                    var newPos = new google.maps.LatLng(d[0].geometry.location.lat, d[0].geometry.location.lng)
                    marker.setPosition(newPos);
                    var latLng = marker.getPosition();
                    map.setCenter(latLng);
                    marker.setVisible(true);
                }
            });
        }

    }

    function getPostalCode(village_id)
    {
        $.get("/village/"+village_id, function(d) {
            $('[name=postal_code]').val(d.postal_code)
        });
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $("#id-card").attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function() {
        $("#id-card").click(function(){
            $('[name=id_card]').trigger("click")
        })
        $('[name=id_card]').change(function() {
            readURL(this);

        })
        getProvince()

        $(document).on("change", "[name=province_id]", function() {
            city_id = null
            subdistrict_id = null
            village_id = null
            getCity($(this).val())
        })
        $(document).on("change", "[name=city_id]", function() {
            subdistrict_id = null
            village_id = null
            getSubdistrict($(this).val())
        })
        $(document).on("change", "[name=subdistrict_id]", function() {
            village_id = null
            // alert($(this).val())
            getVillage($(this).val())
        })
        $(document).on("change", "[name=village_id]", function() {
            selectVillage($(this).val())
            getPostalCode($(this).val())
        })

    })
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('APP_GOOGLE_MAP_KEY')}}&callback=myMap"></script>

@endpush
