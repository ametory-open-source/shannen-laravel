@if($web->is_online || auth()->user())
<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{!! $web->name !!}</title>
    {{--
    <meta name="robots" content="noindex, follow" /> --}}
    <meta name="description" content="{!! $web->description !!}">
    <meta name="title" content="{!! $web->name !!}">
    {{--
    <meta name="keywords" content="{!! $web->keywords !!}"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="{!! $web->description !!}" />
    <meta property="og:site_name" content="{!! $web->name !!}">
    <meta property="og:url" content="{{ url("/") }}">
    @if($web->image)
    <meta property="og:image" content="{!! $web->image !!}" />
    @endif
    <meta property="og:image" content="@yield('og_image')" />
    {!! $web->meta !!}

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <!-- CSS (Font, Vendor, Icon, Plugins & Style CSS files) -->

    <!-- Font CSS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

    <!-- Vendor CSS (Bootstrap & Icon Font) -->
    <link rel="stylesheet" href="/web/assets/css/vendor/bootstrap.min.css">

    <!-- Plugins CSS (All Plugins Files) -->
    <link rel="stylesheet" href="/web/assets/css/plugins/swiper-bundle.min.css">
    {{--
    <link rel="stylesheet" href="/web/assets/css/plugins/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="/web/assets/css/plugins/fancybox.min.css">
    <link rel="stylesheet" href="/web/assets/css/plugins/nice-select.css">


    <link rel="stylesheet" href="/web/assets/css/style.min.css">
    <style>
        @media only screen and (max-width: 767px),
        only screen and (min-width:768px) and (max-width:991px),
        only screen and (min-width:992px) and (max-width:1199px),
        only screen and (min-width:1200px) and (max-width:1549px) {
            .header-logo img {
                width: 160px
            }
        }
    </style>
    {!! $web->styles !!}
    <style>
        .custom-file-input::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-file-input::before {
            content: 'Select some files';
            display: inline-block;
            border-radius: 3px;
            padding: 10px 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
            font-weight: 700;
            font-size: 10pt;
        }

        .custom-file-input:hover::before {
            border-color: black;
        }

        .custom-file-input:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }

        .avatar-big {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            object-fit: cover;
        }

        .avatar-small {
            margin-top: 3px;
            width: 24px;
            height: 24px;
            border-radius: 50%;
            object-fit: cover;
        }

        .pointer {
            cursor: pointer;
        }

        .cart-totals-wrap table tbody .amount {
            text-transform: initial
        }
        .product-ratingsform-form-wrap .product-ratingsform-form-icon-fill {
            position: initial !important;
        }
        .aside-cart-product-list {
            max-height: 60vh;
            overflow-y: auto;
        }
        .red-button {
            border: none;
            background-color: #ff6565;
            text-transform: uppercase;
            font-weight: 600;
            padding: 20px 40px;
            line-height: 1;
            color: #fff;
            font-size: 13px;
            display: inline-block;
        }
        .red-button:hover {
            background-color: #1f2226;
        }
        .mt-20 {
            margin-top:20px;
        }
        img.icon {
            margin: auto;
        }
        .align-bottom {
            vertical-align: inherit !important;
        }

        @media only screen and (max-width: 767px),
        only screen and (min-width:768px) and (max-width:991px),
        only screen and (min-width:992px) and (max-width:1199px),
        only screen and (min-width:1200px) and (max-width:1549px) {
            .product-item .product-action-bottom .product-action-btn {
                width: 100%;
            }
        }
        label.required:after {
            content:" *";
            color: #ff6565
        }
        .count_chat {
            font-size: 10pt;
            color: white;
            width: 24px;
            height: 24px;
            border-radius: 50%;
            background-color: #ff6565;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>
    @stack("css")
    @livewireStyles
</head>

<body>

    <!--== Wrapper Start ==-->
    <div class="wrapper">

        @includeWhen($web->show_header, "frontend.components.landing.header")


        <main class="main-content">
            @yield("content")
        </main>


        @includeWhen($web->show_footer, "frontend.components.landing.footer")

        <!--== Scroll Top Button ==-->
        <div id="scroll-to-top" class="scroll-to-top"><span class="fa fa-angle-up"></span></div>




        @include("frontend.components.landing.wishlist_modal")
        @include("frontend.components.landing.cart_modal")

        @include("frontend.components.landing.search")
        @include("frontend.components.landing.product_modal")

        @includeWhen($web->show_cart ,"frontend.components.landing.cart")
        @include("frontend.components.landing.sidemenu")

    </div>
    <!--== Wrapper End ==-->

    <!-- JS Vendor, Plugins & Activation Script Files -->

    <!-- Vendors JS -->
    <script src="/web/assets/js/vendor/modernizr-3.11.7.min.js"></script>
    <script src="/web/assets/js/vendor/jquery-3.6.0.min.js"></script>
    <script src="/web/assets/js/vendor/jquery-migrate-3.3.2.min.js"></script>
    <script src="/web/assets/js/vendor/bootstrap.bundle.min.js"></script>

    <!-- Plugins JS -->
    <script src="/web/assets/js/plugins/swiper-bundle.min.js"></script>
    <script src="/web/assets/js/plugins/fancybox.min.js"></script>
    <script src="/web/assets/js/plugins/jquery.nice-select.min.js"></script>

    @livewireStyles

    <!-- Scripts -->
    <!-- Alpine Plugins -->
    <script defer src="https://cdn.jsdelivr.net/npm/@alpinejs/focus@3.13.0/dist/cdn.min.js"></script>

<!-- Alpine Core -->
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.13.0/dist/cdn.min.js"></script>

    {!! $web->scripts !!}
    @livewire('livewire-ui-modal')
    @livewireScripts
    <script>
        setInterval(() => {
            console.log("refresh chat")
            Livewire.emitTo('user-message-box', 'refreshBox')
            Livewire.emitTo('user-chat-bubble', 'refreshBubble')
        }, 10000);
    </script>
    @stack("scripts")
    @stack("script_modules")

    <!-- Custom Main JS -->
    <script src="/web/assets/js/main.js"></script>
    <script>


        window.addEventListener('quantityZero', () => {
           alert("Qty tidak boleh kosong")

        })
        function btnDelete(event) {
            var msg = $(event).data("message")
            var conf = confirm(msg || "Are you sure?")
                if (!conf) {
                    return false;
                }
            window.location.href = $(event).data("href")
        }
        function addToWishlist(id) {
            $.ajax({
                url: "/add-wishlist",
                data: { product_id: id },
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function() {
                }
            });
        }
        function deleteWhislist(id) {
            var conf = confirm("Anda yakin akan menghapus produk ini dari daftar wishlist?")
            if (conf) {
                $.ajax({
                    url: "/delete-wishlist/"+id,
                    type: "DELETE",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function() {
                        location.href = "/profile?tab=wishlist"
                    }
                });
            }

        }
    </script>

</body>

</html>
@else
{!! $web->offline_template !!}
@endif
