<!--== Start Hero Area Wrapper ==-->
<section class="hero-slider-area position-relative">
    <div class="swiper hero-slider-container">
        <div class="swiper-wrapper">
            @foreach ($main_banners as $item)
            <div class="swiper-slide hero-slide-item">
                <div class="container">
                    <div class="row align-items-center position-relative">
                        <div class="col-12 col-md-6">
                            <div class="hero-slide-content">
                                <div class="hero-slide-text-img"><img src="/web/assets/images/slider/text-theme.webp" width="427" height="232" alt="Image"></div>
                                <h2 class="hero-slide-title">{{ $item->title }}</h2>
                                <p class="hero-slide-desc">{{ $item->subtitle }}</p>
                                <a class="btn btn-border-dark" href="{{ $item->link }}">BUY NOW</a>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="hero-slide-thumb">
                                <img src="{{ $item->picture_url }}" width="841" height="832" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hero-slide-text-shape"><img src="{{ $item->alt_picture_url }}" width="70" height="955" alt="Image"></div>
                <div class="hero-slide-social-shape"></div>
            </div>
            @endforeach
        </div>
        <!--== Add Pagination ==-->
        <div class="hero-slider-pagination"></div>
    </div>
    @if($web->banner_social_media)
    <div class="hero-slide-social-media">
        <a href="{{ $web->instagram_link }}" target="_blank" rel="noopener"><i class="fa-brands fa-instagram"></i></a>
        <a href="{{ $web->twitter_link }}" target="_blank" rel="noopener"><i class="fa-brands fa-twitter"></i></a>
        <a href="{{ $web->facebook_link }}" target="_blank" rel="noopener"><i class="fa-brands fa-facebook"></i></a>
        <a href="{{ $web->tiktok_link }}" target="_blank" rel="noopener"><i class="fa-brands fa-tiktok"></i></a>
    </div>
    @endif
</section>
<!--== End Hero Area Wrapper ==-->
