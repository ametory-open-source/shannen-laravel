@foreach ($top_sales as $item)

<!--== Start Product Quick View Modal ==-->
<aside class="product-cart-view-modal modal fade" id="action-{{ $item->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="product-quick-view-content">
                    <button type="button" class="btn-close" data-bs-dismiss="modal">
                        <span class="fa fa-close"></span>
                    </button>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <!--== Start Product Thumbnail Area ==-->
                                <div class="product-single-thumb">
                                    <img src="{{ $item->feature_image }}" width="544" height="560" alt="Image-HasTech">
                                </div>
                                <!--== End Product Thumbnail Area ==-->
                            </div>
                            <livewire:product-modal-component :product='$item' />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>
<!--== End Product Quick View Modal ==-->
@endforeach
@foreach ($feature_products as $item)
<!--== Start Product Quick View Modal ==-->
<aside class="product-cart-view-modal modal fade" id="action-feature-{{ $item->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="product-quick-view-content">
                    <button type="button" class="btn-close" data-bs-dismiss="modal">
                        <span class="fa fa-close"></span>
                    </button>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <!--== Start Product Thumbnail Area ==-->
                                <div class="product-single-thumb">
                                    <img src="{{ $item->feature_image }}" width="544" height="560" alt="Image-HasTech">
                                </div>
                                <!--== End Product Thumbnail Area ==-->
                            </div>
                            <livewire:product-modal-component :product='$item' />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>
<!--== End Product Quick View Modal ==-->
@endforeach
