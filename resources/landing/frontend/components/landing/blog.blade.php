<!--== Start Blog Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2 class="title">{{ $web->blog_title }}</h2>
                    <p>{{ $web->blog_subtitle }}</p>
                </div>
            </div>
        </div>
        <div class="row mb-n9">
            @foreach ($blogs as $item)
            <div class="col-sm-6 col-lg-4 mb-8">
                @include("frontend.components.modules.blog_card", ["item" => $item])
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--== End Blog Area Wrapper ==-->
