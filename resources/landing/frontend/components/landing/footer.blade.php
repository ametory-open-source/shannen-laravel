 <!--== Start Footer Area Wrapper ==-->
 <footer class="footer-area">
    <!--== Start Footer Main ==-->
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="widget-item">
                        <div class="widget-about">
                            <a class="widget-logo" href="/">
                                <img src="{{ $web->logo_url ?? '/web/assets/images/logo.webp'}}" width="200" height="68" alt="Logo">
                            </a>
                            <p class="desc">{{ $web->short_description }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 mt-md-0 mt-9">
                    <div class="widget-item">
                        <h4 class="widget-title">{!! __('frontend.Information') !!}</h4>
                        <ul class="widget-nav">
                            {!! $web->footer_center_link !!}
                        </ul>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 mt-lg-0 mt-6">
                    <div class="widget-item">
                        <h4 class="widget-title">{!! __('frontend.Social Info') !!}</h4>
                        <div class="widget-social">
                            <a target="_blank" href="{{ $web->twitter_link }}" target="_blank" rel="noopener"><i class="fa fa-brands fa-twitter"></i></a>
                            <a target="_blank" href="{{ $web->facebook_link }}" target="_blank" rel="noopener"><i class="fa fa-brands fa-facebook"></i></a>
                            <a target="_blank" href="{{ $web->instagram_link }}" target="_blank" rel="noopener"><i class="fa fa-brands fa-instagram"></i></a>
                            <a target="_blank" href="{{ $web->tiktok_link }}" target="_blank" rel="noopener"><i class="fa fa-brands fa-tiktok"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--== End Footer Main ==-->

    <!--== Start Footer Bottom ==-->
    <div class="footer-bottom">
        <div class="container pt-0 pb-0">
            <div class="footer-bottom-content">
                <p class="copyright">© 2022 {{ env('APP_NAME' )}}.
                    {{-- Made with <i class="fa fa-heart"></i> by <a target="_blank" href="https://biskod.com">Biskod Dev</a> --}}
            </p>
            </div>
        </div>
    </div>
    <!--== End Footer Bottom ==-->
</footer>
<!--== End Footer Area Wrapper ==-->
