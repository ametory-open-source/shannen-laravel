<!--== Start Aside Cart ==-->
<aside class="aside-cart-wrapper offcanvas offcanvas-end" tabindex="-1" id="AsideOffcanvasCart" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <h1 class="d-none" id="offcanvasRightLabel">{{ __('frontend.Shopping Cart') }}</h1>
        <button class="btn-aside-cart-close" data-bs-dismiss="offcanvas" aria-label="Close">{{ __('frontend.Shopping Cart') }} <i class="fa fa-chevron-right"></i></button>
    </div>
    <livewire:cart-component />
</aside>
