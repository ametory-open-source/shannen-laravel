<!--== Start News Letter Area Wrapper ==-->
<section class="section-space pt-0">
    <div class="container">
        <div class="newsletter-content-wrap" data-bg-img="/web/assets/images/photos/bg1.webp">
            <div class="newsletter-content">
                <div class="section-title mb-0">
                    <h2 class="title">{{ $web->join_title }}</h2>
                    <p>{{ $web->join_subtitle }}</p>
                </div>
            </div>
            <div class="newsletter-form">
                <form action="/register">
                    @form_hidden("register_only", true)
                    <input name="email" type="email" class="form-control" placeholder="enter your email">
                    <button class="btn-submit" type="submit"><i class="fa fa-paper-plane"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
<!--== End News Letter Area Wrapper ==-->
