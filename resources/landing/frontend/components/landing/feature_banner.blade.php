 <!--== Start Product Banner Area Wrapper ==-->
 <section>
    <div class="container">
        <div class="row">
            @foreach ($feature_banners as $item)
            <div class="col-sm-6 col-lg-4">
                <!--== Start Product Category Item ==-->
                <a href="{{ $item->link }}" class="product-banner-item">
                    <img src="{{ $item->picture_url }}" width="370" height="370" alt="">
                </a>
                <!--== End Product Category Item ==-->
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--== End Product Banner Area Wrapper ==-->
