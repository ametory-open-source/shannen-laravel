<!--== Start Product Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2 class="title">{{ $web->top_sale_title }}</h2>
                    <p>{{ $web->top_sale_subtitle }}</p>
                </div>
            </div>
        </div>
        <div class="row mb-n4 mb-sm-n10 g-3 g-sm-6">
            @foreach ($top_sales as $item)
            <div class="col-6 col-lg-4 mb-4 mb-sm-9">
                <!--== Start Product Item ==-->
                <div class="product-item">
                    <div class="product-thumb">
                        <a class="d-block" href="{{ $item->url }}">
                            <img src="{{ $item->feature_image }}" width="370" height="450" alt="Image-HasTech">
                        </a>
                        @if ($item->promo_tag_label)
                        <span class="flag-new" style="background-color: {{$item->promo_tag_color}}">{{ $item->promo_tag_label }}</span>
                        @endif
                        <div class="product-action">
                            {{-- <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" >
                                <i class="fa fa-expand"></i>
                            </button> --}}
                            <livewire:product-component :product='$item' />
                            @if($frontendUser)
                            <livewire:product-favorite-button :product="$item"  />
                            @endif
                        </div>
                    </div>
                    <div class="product-info">
                        <div class="product-rating">
                            <div class="rating">
                                {!! $item->rating_stars !!}
                            </div>
                            @if($item->review_count)
                            <div class="reviews">{{ $item->review_count }} {!! __('frontend.reviews') !!}</div>
                            @endif
                        </div>
                        <h4 class="title"><a href="{{ $item->url }}">{{ $item->name }}</a></h4>
                        <div class="prices">
                            <span class="price">{{ $item->price_format }}</span>
                            @if($item->strike_price_format)
                            <span class="price-old">{{ $item->strike_price_format }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="product-action-bottom">
                        {{-- <button style="{{!$frontendUser ? 'width:100%' : null}}" type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" >
                            <i class="fa fa-expand"></i>
                        </button> --}}
                        @if($frontendUser)
                        <livewire:product-favorite-button :product="$item" :target="'#action-Wishlist-'.$item->id" />

                        @endif

                        <livewire:product-component :product='$item' />
                    </div>
                </div>
                <!--== End prPduct Item ==-->
            </div>
            @endforeach
        </div>
    </div>
</section>

<!--== End Product Area Wrapper ==-->


