<!--== Start Product Category Area Wrapper ==-->
<section class="section-space pb-0">
    <div class="container">
        <div class="row g-3 g-sm-6">

            @foreach ($categories as $item)
            <div class="col-6 col-lg-4 col-lg-2 col-xl-2 mb-4">
                <!--== Start Product Category Item ==-->
                <a href="/products?cat={{$item->slug}}" class="product-category-item" data-bg-color="{{ $item->color }}">
                    <img class="icon" src="{{$item->picture_url}}" width="80" height="80"
                        alt="{{$item->slug}}">
                    <h3 class="title">{{$item->name}}</h3>
                    @if($item->promo_tag_label)
                    <span data-bg-color="{{$item->promo_tag_color}}" class="flag-new">{{$item->promo_tag_label}}</span>
                    @endif
                </a>
                <!--== End Product Category Item ==-->
            </div>
            @endforeach

        </div>
    </div>
</section>
<!--== End Product Category Area Wrapper ==-->
