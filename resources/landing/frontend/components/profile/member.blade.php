<div class="myaccount-content">
    <h3>{!! __('frontend.Membership Program') !!}</h3>
    @if (!$user->member)
    <div class="welcome">

        <div class="myaccount-content">
            <p>{!! __('frontend.membership_not_join') !!}</p>
            <div class="account-details-form">

                <div class="single-input-item" style="">
                    <button onclick="window.location.href='/join-membership'" class="check-btn sqr-btn">{!!
                        __('frontend.Join Now') !!}</button>
                </div>
            </div>
        </div>
    </div>


    @else
    <div class="myaccount-content">
        @if ($user->member->status == "PENDING")
        <p>{!! __('frontend.membership_pending')!!}</p>
        <div class="account-details-form">
            <div class="single-input-item">
                <form action="/profile/membership-cancel" method="post">
                    @csrf
                    <button class="check-btn sqr-btn">{!! __('frontend.Cancel Request') !!}</button>
                </form>
            </div>
        </div>
        @endif

        @if ($user->member->status == "ACTIVE")
        <h3 class="text-success">{{$user->member->status}}</h3>
        <small>{!! __('frontend.approved_at') !!} <span
                class="text-info">{{$user->member->approved_at->format("d-m-Y")}}</span></small>

        <h6 style="margin-bottom: 0px; margin-top:10px">{{ __('frontend.membership_type') }}</h6>
        <p>{{ $user->member->type->name }}</p>
        <h6 style="margin-bottom: 0px; margin-top:10px">{{ __('frontend.member_code') }}</h6>
        <p>{{ $user->member->code }}</p>
            @if($user->member->type->first_minimum_order > 0 && !$user->member->have_purchased)
            <p>Minimal pembelian untuk mendapatkan diskon adalah <strong> {{ money($user->member->type->first_minimum_order) }}</strong> dan pembelian berikutnya <strong> {{ money($user->member->type->minimum_order) }} </strong></p>
            @endif

            @if ($user->member->have_purchased)
            <p>Minimal pembelian untuk mendapatkan diskon adalah {{ money($user->member->type->minimum_order) }} </p>
            @endif
        @endif
        @if ($user->member->status == "BLOCKED")
        <p>{!! __('frontend.membership_blocked') !!}</p>
        @endif

    </div>
    @endif

</div>
