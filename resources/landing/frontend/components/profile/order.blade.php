<div class="myaccount-content">
    <h3>{!! __('frontend.Orders') !!}</h3>
    <div class="myaccount-table table-responsive text-center">
        <livewire:order />
    </div>
</div>
