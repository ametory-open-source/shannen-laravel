<div class="myaccount-content">
    <h3>Program Membership</h3>
    @if (!$user->affiliate)
    <div class="welcome">

        <div class="myaccount-content">
            <p>You have not joined our affiliate program, to join you must fill out the affiliate program submission
                form and agree to the terms and conditions of this program</p>
            <div class="account-details-form">

                <div class="single-input-item" style="">
                    <button onclick="window.location.href='/join-affiliate'" class="check-btn sqr-btn">Join Now</button>
                </div>
            </div>
        </div>
    </div>


    @else
    <div class="account-details-form">
        <form method="post" action="/update-affiliate" enctype="multipart/form-data">
            @csrf
        <fieldset>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="display_name" class="required">Display Name</label>
                        <input type="text" name="display_name" id="display_name" value="{{ $user->affiliate->display_name }}"
                             />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="username" class="required">Username</label>
                        <input type="text" name="username" id="username" value="{{ $user->affiliate->username }}"
                            disabled />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="picture" class="required">Picture</label>
                        @if ($user->affiliate->picture_url)
                            <img src="{{$user->affiliate->picture_url}}" alt="" srcset="" style="
                                width: 100%;
                                height: 160px;
                                object-fit: cover;
                                margin-bottom: 10px;

                            ">
                        @endif
                        <input style="padding-top:10px" type="file" name="picture" id="picture" />
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="cover" class="required">Cover</label>
                        @if ($user->affiliate->cover_url)
                            <img src="{{$user->affiliate->cover_url}}" alt="" srcset="" style="
                                width: 100%;
                                height: 160px;
                                object-fit: cover;
                                margin-bottom: 10px;

                            ">
                        @endif
                        <input style="padding-top:10px" type="file" name="cover" id="cover" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="code" class="required">Code</label>
                        <input type="text" id="code" name="phone_number" value="{{ $user->affiliate->code }}"
                            disabled />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="status" class="required">Status</label>
                        <input type="text" id="status" name="status" value="{{ $user->affiliate->status }}"
                            disabled />
                    </div>
                </div>
            </div>
            <div class="single-input-item">
                <label for="address" class="required">Address</label>
                <textarea style="
                            border: 1px solid #e8e8e8;
                            width: 100%;
                            background-color: transparent;
                            padding: 20px 20px;
                            color: #1f2226;
                            font-size: 13px;
                        " id="address" name="address">{{ $user->affiliate->address }}</textarea>
            </div>
        </fieldset>
        <div class="single-input-item">
            <button class="check-btn sqr-btn">Save Change</button>
        </div>
    </form>
    </div>
    @endif

</div>
