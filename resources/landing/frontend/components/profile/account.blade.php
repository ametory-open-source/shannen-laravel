@php
$user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
<div class="myaccount-content">
    <h3>{!! __('frontend.Account Details') !!}</h3>
    <div class="account-details-form">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    @if($user->avatar)
                    <img class="avatar-big mb-3 pointer" src="{{ assetUrl($user->avatar) }}" alt="" srcset=""
                        onclick="clickAvatar()">
                    @else
                    <img class="avatar-big mb-3 pointer" src="/img/no-picture.jpeg" alt="" srcset=""
                        onclick="clickAvatar()">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="fullname" class="required">{!! __('frontend.Full Name') !!}</label>
                        <input type="text" name="name" id="fullname" value="{{ old('name') ??  $user->name }}" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="phone" class="required">{!! __('frontend.Phone') !!}</label>
                        <input type="text" id="phone" name="phone_number"
                            value="{{ old('phone_number') ?? $user->phone_number }}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-input-item">
                        <label for="email" class="required">{!! __('frontend.Email') !!}</label>
                        <input type="email" id="email" name="email" value="{{ old('email') ?? $user->email }}"
                            disabled />
                    </div>
                </div>
                <div class="col-lg-6" style="visibility: hidden">
                    <div class="single-input-item">
                        <label for="avatar" class="required">{!! __('frontend.Profile Picture') !!}</label>
                        <input accept="image/*" class="custom-file-input" type="file" id="avatar" name="avatar"
                            value="{{ old('avatar') ?? $user->avatar }}" />
                    </div>
                </div>
            </div>
            <div class="single-input-item">

            </div>
            <div class="single-input-item">
                <button class="check-btn sqr-btn">{!! __('frontend.Save Changes') !!}</button>
            </div>
        </form>
        <br>


        <form action="/change-password" method="post">
            @csrf
            <fieldset>
                <legend>{!! __('frontend.Password change') !!}</legend>
                <div class="single-input-item">
                    <label for="current-pwd" class="required">{!! __('frontend.Current Password') !!}</label>
                    <input type="password" id="current-pwd" name="password" required />
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="single-input-item">
                            <label for="new-pwd" class="required">{!! __('New Password') !!}</label>
                            <input type="password" id="new-pwd" name="new_password" required />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-input-item">
                            <label for="confirm-pwd" class="required">{!! __('frontend.Confirm Password') !!}</label>
                            <input type="password" id="confirm-pwd" name="confirm_password" required />
                        </div>
                    </div>
                </div>

            </fieldset>
            <div class="single-input-item">
                <button class="check-btn sqr-btn">{!! __('frontend.Save New Password') !!}</button>
            </div>
        </form>

    </div>
</div>

@push('scripts')
<script>
    function clickAvatar() {
        $("[name=avatar]").trigger("click")
    }

    $(function(){
    $('[name=avatar]').change(function(){
        var input = this;
        var url = $(this).val();
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
        {
            var reader = new FileReader();

            reader.onload = function (e) {
            $('.avatar-big').attr('src', e.target.result);
            }
        reader.readAsDataURL(input.files[0]);
        }
        else
        {
        $('#img').attr('src', '/assets/no_preview.png');
        }
    });

    });
</script>
@endpush
