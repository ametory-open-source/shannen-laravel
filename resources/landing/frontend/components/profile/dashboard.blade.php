@php
    $user = auth(env('APP_AFFILIATE_GUARD'))->user();
@endphp
<div class="myaccount-content">
    <h3>{!! __('frontend.Dashboard') !!}</h3>
    <div class="welcome">
        <p>{!! __('frontend.dashboard_hello', ["name" => $user->name ]) !!}</p>
    </div>
    <p>{!! __('frontend.dashboard_welcome') !!}</p>
</div>
