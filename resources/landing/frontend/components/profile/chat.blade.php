<div class="myaccount-content">
    <h3>{!! __('frontend.Chat') !!}</h3>
    <livewire:user-message-box>
</div>


@push('css')
    <style>
        .chat-room {
            height: 60vh;
            overflow-y: auto;
            overflow-x: none;
            margin-bottom: 20px;
        }
        .chat-shout input {
            border-radius: 0;
        }
        .chat-shout {
            display: flex;
        }
        .chat-bubble {
            border: 2px solid  #e9e9e9;
            border-radius: 10px;
            padding: 10px 20px;
        }
        .chat-admin {
            margin-left: 20px;
        }
        .chat-user {
            margin-right: 20px;
            background: #ededed;
        }
    </style>
@endpush
