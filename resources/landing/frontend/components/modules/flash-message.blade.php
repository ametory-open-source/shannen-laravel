@if ($message = Session::get('success'))
    <div id="attention-success" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>{{$message}}</p>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(function() {
                $("#attention-success").modal("show")
            })
        </script>
    @endpush
@endif
@if ($message = Session::get('error'))
    <div id="attention-success" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>{{$message}}</p>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(function() {
                $("#attention-error").modal("show")
            })
        </script>
    @endpush
@endif
@if ($errors->any())
    @foreach ($errors->all() as $item)
    <div id="attention-error" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>{{$item}}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @push('scripts')
        <script>
            $(function() {
                $("#attention-error").modal("show")
            })
        </script>
    @endpush

@endif


@push('css')
    <style>
        #attention-error .modal-body{
            background-color: #ff6565;
            color: white;
        }
        #attention-success .modal-body{
            background-color: #137444;
            color: white;
        }
    </style>
@endpush
