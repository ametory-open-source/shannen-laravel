@if ($paginator->hasPages())
<div class="col-12">
    <ul class="pagination justify-content-center me-auto ms-auto mt-5 mb-10">

        <li class="page-item">
            @if ($paginator->onFirstPage())
            <a class="page-link previous disabled">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
            </a>
            @else
            <a class="page-link previous" href="{{$paginator->previousPageUrl()}}"  aria-label="Previous">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
            </a>
            @endif
        </li>


        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item"><a style="color: #ff6565" class="page-link">{{ $page }}</a></li>
                    @else
                        <li class="page-item "><a class="page-link" href="{{$paginator->url($page)}}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach


        {{-- <li class="page-item"><a class="page-link" href="product.html">01</a></li>
        <li class="page-item"><a class="page-link" href="product.html">02</a></li>
        <li class="page-item"><a class="page-link" href="product.html">03</a></li>
        <li class="page-item"><a class="page-link" href="product.html">....</a></li> --}}

        <li class="page-item">
            @if ($paginator->hasMorePages())
            <a class="page-link next" href="{{$paginator->nextPageUrl()}}" aria-label="Next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
            </a>
            @else
            <a class="page-link next disabled" aria-label="Next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
            </a>
            @endif
        </li>

    </ul>
</div>
@endif
