<!--== Start Blog Item ==-->
<div class="post-item">
    <a href="{{ $item->url }}" class="thumb">
        <img src="{{ $item->picture_url }}" width="370" height="320" alt="{{ $item->title }}">
    </a>
    <div class="content">
        <a class="post-category" style="background-color: {{ randomPastelColor() }}" href="{{ $item->category_url }}">{{ $item->category->name }}</a>
        <h4 class="title"><a href="{{ $item->url }}">{{ $item->title }}</a></h4>
        <ul class="meta">
            <li class="author-info"><span>By:</span> <a href="{{ $item->author_url }}">{{ $item->author }}</a></li>
            <li class="post-date">{{ $item->created_at->format("d M Y")}}</li>
        </ul>
    </div>
</div>
<!--== End Blog Item ==-->
