@extends("frontend.layout")

@section('content')

    @includeWhen($web->show_main_banner, "frontend.components.landing.main_banner")
    @includeWhen($web->show_feature_category, "frontend.components.landing.feature_category")
    @includeWhen($web->show_top_sale, "frontend.components.landing.top_sale")
    @includeWhen($web->show_feature_banner, "frontend.components.landing.feature_banner")
    @includeWhen($web->show_feature_product, "frontend.components.landing.feature_product")
    @includeWhen($web->show_blog, "frontend.components.landing.blog")
    @includeWhen($web->show_join, "frontend.components.landing.join")

@endsection
