@extends("frontend.layout")

@section('content')
<!--== Start Page Header Area Wrapper ==-->
<nav aria-label="breadcrumb" class="breadcrumb-style1">
    <div class="container">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Faq</li>
        </ol>
    </div>
</nav>

<!--== Start Faq Area Wrapper ==-->
<section class="faq-area">
    <div class="container">
        <div class="row flex-xl-row-reverse">
            <div class="col-lg-6 col-xl-7">
                <div class="faq-thumb">
                    <img src="{{ assetUrl($web->faq_picture) }}" width="601" height="368" alt="Image">
                </div>
            </div>
            <div class="col-lg-6 col-xl-5">
                <div class="faq-content">
                    <div class="faq-text-img"><img src="/web/assets/images/photos/faq.webp" width="199" height="169" alt="Image"></div>
                    <h2 class="faq-title">{{ $web->faq_title }}</h2>
                    <div class="faq-line"></div>
                    <p class="faq-desc">{{ $web->faq_subtitle }}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="accordion" id="FaqAccordion">
                    @foreach ($faqs as $i => $item)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading{{ $i + 1}}">
                            <button class="accordion-button {{ $i ? 'collapsed' : null}} " type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $i + 1}}" aria-expanded="true" aria-controls="collapse{{ $i + 1}}">
                                {{ $item->title }}
                            </button>
                        </h2>
                        <div id="collapse{{ $i + 1}}" class="accordion-collapse collapse {{ !$i ? 'show' : null}}" aria-labelledby="heading{{ $i + 1}}" data-bs-parent="#FaqAccordion">
                            <div class="accordion-body">
                                <p>{{ $item->description }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Faq Area Wrapper ==-->
@endsection
