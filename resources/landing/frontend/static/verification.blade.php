@extends("layouts.landing")

@section('content')
@includeWhen($web->show_header, "components.landing.header")

<div class="blog-list-page">
    <div class="container">
        @if ($message)
        <div style="padding:20px; cursor: pointer;" class="alert-wrapper" data-bs-dismiss="alert" aria-label="Close">
            <div class="alert alert-{{ $class }} alert-dismissible fade show" role="alert">
                {{ $message }}
            </div>
        </div>
        @endif
       

    </div>

</div>


@includeWhen($web->show_footer, "components.landing.footer")
@push("scripts")
<script>
    function reply(comment_id) {
        $("#parent_id").val(comment_id)
    }
    $(".alert-wrapper").click(function() {
        $(this).remove()
    })


</script>
@endpush
<!--blog single end-->
@endsection