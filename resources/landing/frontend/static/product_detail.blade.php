@extends("frontend.layout")
@section('title', $product->name)
@section('og_image', $product->feature_image)
@section('content')
@if(isset($affiliate))
<section class="section-space" style="
    background: url({{$affiliate->cover_url}}) no-repeat center center;
    height: 300px;
    background-size: cover;
    position:relative;
    padding:initial;
">
    <div class="container" style="height:300px;position: relative; ">
        <div style="bottom: 10px;
    left: 10px;
    position: absolute;
    display: flex;
    ">
            @if ($affiliate->picture_url)
            <img src="{{$affiliate->picture_url}}" alt="" style="
            width: 100px;
            height: 100px;
            object-fit: cover;
            padding: 5px;
            background-color: white;
            border: 1px solid #dedede;
            border-radius: 5px;
            margin-right:20px;
        ">
            @endif
            <div style="    padding: 15px;
        width: auto;

        display: block;
        border-radius: 10px;">

                <h1 style="font-weight: 700; color: #111;text-shadow: 0 0 20px #fff"  > <a href="/{{ $affiliate->username }}"> {{ $affiliate->display_name }}</a>
                </h1>
            </div>
        </div>

    </div>
</section>
@else
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">Home</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{{ $product->category->name }}</li>
                    </ol>
                    <h2 class="page-header-title">{{ $product->name }}</h2>
                </div>
            </div>
            <div class="col-md-7">
                {{-- <h5 class="showing-pagination-results mt-5 mt-md-9 text-center text-md-end">Showing Single Product</h5> --}}
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->
@endif



<!--== Start Product Details Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        @if(session('success'))
                    <div id="attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>{{ session('success') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @push('scripts')
                            <script>
                                $(function() {
                                    $("#attention").modal("show")
                                })
                            </script>
                        @endpush
                        @push('css')
                            <style>
                                #attention .modal-body{
                                    background-color: #01830A;
                                    color: white;
                                }
                            </style>
                        @endpush
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $item)
                        <div id="attention" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <p>{{$item}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @push('scripts')
                            <script>
                                $(function() {
                                    $("#attention").modal("show")
                                })
                            </script>
                        @endpush
                        @push('css')
                            <style>
                                #attention .modal-body{
                                    background-color: #ff6565;
                                    color: white;
                                }
                            </style>
                        @endpush
                    @endif

        <livewire:product-detail :product="$product" :affiliate="$affiliate ?? null" >
        <div class="row">
            <div class="col-lg-7">
                <div class="nav product-details-nav" id="product-details-nav-tab" role="tablist">
                    <button class="nav-link active" id="spec-tab" data-bs-toggle="tab" data-bs-target="#spec" type="button" role="tab" aria-controls="spec" aria-selected="true">{!! __('frontend.Specification') !!}</button>
                    <button class="nav-link " id="review-tab" data-bs-toggle="tab" data-bs-target="#review" type="button" role="tab" aria-controls="review" aria-selected="true">{!! __('frontend.Review') !!}</button>

                </div>
                <div class="tab-content" id="product-details-nav-tabContent" wire:ignore>
                    <div class="tab-pane fade " id="specification" role="tabpanel" aria-labelledby="specification-tab">

                    </div>
                    <div class="tab-pane fade show active" id="spec" role="tabpanel" aria-labelledby="spec-tab">
                        <ul class="product-details-info-wrap">
                            <li><span>{!! __('frontend.Weight') !!}</span>
                                <p>{{ $product->weight }} {{ $product->weight_unit }}</p>
                            </li>
                            <li><span>{!! __('frontend.Dimensions') !!}</span>
                                <p>{{ $product->dimension_length }} x {{ $product->dimension_width }} x {{ $product->dimension_height }} {{ $product->dimension_unit }}</p>
                            </li>
                            <li><span>{!! __('frontend.Materials') !!}</span>
                                <p>{{ $product->materials }}</p>
                            </li>
                            <li><span>{!! __('frontend.Other Info') !!}</span>
                                <p>{{ $product->other_info }}</p>
                            </li>
                        </ul>
                        <p></p>
                        {!! $product->description !!}
                    </div>
                    <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                        @if(!$product->reviews->count())
                            <blockquote>Produk ini belum memiliki review</blockquote>
                        @endif
                        @foreach ($product->reviews as $item)
                        <!--== Start Reviews Content Item ==-->
                        <div class="product-review-item">
                            <div class="product-review-top">
                                <div class="product-review-thumb">
                                    @if($item->is_anonymous)
                                    <img src="/web/assets/images/shop/product-details/comment1.webp" alt="Images">
                                    @else
                                    <img style="width: 64px; height:64px;object-fit: cover;" src="{{ assetUrl($item->user->avatar)}}" alt="" srcset="">
                                    @endif
                                </div>

                                <div class="product-review-content">
                                    @if($item->is_anonymous)
                                    <span class="product-review-name">Anonymous</span>
                                    @else
                                    <span class="product-review-name">{{($item->user->name)}}</span>
                                    @endif

                                    <div class="product-review-icon">
                                       {!! $item->rating_stars !!}
                                    </div>
                                </div>
                            </div>
                            <p class="desc">
                                {{ $item->feedback }}
                                <div style="display:flex">
                                @foreach ($item->product_reviews as $review)
                                    <a target="_blank" href="{{ $review }}">
                                        <img style="width: 100px;height:100px;object-fit: cover; cursor: pointer;" src="{{ $review }}" alt="" srcset="">
                                    </a>
                                    @endforeach
                                </div>
                            </p>
                            {{-- <button type="button" class="review-reply"><i class="fa fa fa-undo"></i></button> --}}
                        </div>
                        <!--== End Reviews Content Item ==-->
                        @endforeach


                    </div>
                </div>
            </div>
            {{-- @if($product->review_enabled && $frontendUser && $product->eligible_review)
            <div class="col-lg-5">
                <div class="product-reviews-form-wrap">
                    <h4 class="product-form-title">{!! __('frontend.Review') !!}</h4>
                    <div class="product-reviews-form">
                        <form action="{{$product->url}}/review" method="post" >
                            @csrf
                            <div class="form-input-item">
                                <textarea name="feedback" class="form-control" placeholder="{!! __('frontend.Enter your feedback') !!}"></textarea>
                            </div>

                            <div class="form-input-item" wire:ignore>
                                <div class="form-ratings-item">
                                    <select  name="rate" id="product-review-form-rating-select" class="select-ratings">
                                        <option value="1">01</option>
                                        <option value="2">02</option>
                                        <option value="3">03</option>
                                        <option value="4">04</option>
                                        <option value="5" selected>05</option>
                                    </select>
                                    <span class="title">{!! __('frontend.Provide Your Ratings') !!}</span>
                                    <div class="product-ratingsform-form-wrap">
                                        <div class="product-ratingsform-form-icon">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div id="product-review-form-rating" class="product-ratingsform-form-icon-fill" style="width: 100%;">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews-form-checkbox">
                                    <input name="is_anonymous" class="form-check-input" type="checkbox" value="1" id="ReviewsFormCheckbox" checked>
                                    <label class="form-check-label" for="ReviewsFormCheckbox">{!! __('frontend.Provide ratings anonymously') !!}.</label>
                                </div>
                            </div>
                            <div class="form-input-item mb-0">
                                <button type="submit" class="btn">{!! __('frontend.SUBMIT') !!}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif --}}
        </div>
    </div>
</section>
<!--== End Product Details Area Wrapper ==-->

<!--== Start Product Banner Area Wrapper ==-->
{{-- <div class="container">
    <!--== Start Product Category Item ==-->
    <a href="product.html" class="product-banner-item">
        <img src="/web/assets/images/shop/banner/7.webp" width="1170" height="240" alt="Image-HasTech">
    </a>
    <!--== End Product Category Item ==-->
</div> --}}
<!--== End Product Banner Area Wrapper ==-->

<!--== Start Product Area Wrapper ==-->
{{-- <section class="section-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">Related Products</h2>
                    <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis</p>
                </div>
            </div>
        </div>
        <div class="row mb-n10">
            <div class="col-12">
                <div class="swiper related-product-slide-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide mb-10">
                            <!--== Start Product Item ==-->
                            <div class="product-item product-st2-item">
                                <div class="product-thumb">
                                    <a class="d-block" href="product-details.html">
                                        <img src="/web/assets/images/shop/8.webp" width="370" height="450" alt="Image-HasTech">
                                    </a>
                                    <span class="flag-new">new</span>
                                </div>
                                <div class="product-info">
                                    <div class="product-rating">
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="reviews">150 reviews</div>
                                    </div>
                                    <h4 class="title"><a href="product-details.html">Readable content DX22</a></h4>
                                    <div class="prices">
                                        <span class="price">$210.00</span>
                                        <span class="price-old">300.00</span>
                                    </div>
                                    <div class="product-action">
                                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">
                                            <span>Add to cart</span>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" data-bs-target="#action-QuickViewModal">
                                            <i class="fa fa-expand"></i>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal" data-bs-target="#action-WishlistModal">
                                            <i class="fa-regular fa-heart"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--== End prPduct Item ==-->
                        </div>
                        <div class="swiper-slide mb-10">
                            <!--== Start Product Item ==-->
                            <div class="product-item product-st2-item">
                                <div class="product-thumb">
                                    <a class="d-block" href="product-details.html">
                                        <img src="/web/assets/images/shop/7.webp" width="370" height="450" alt="Image-HasTech">
                                    </a>
                                    <span class="flag-new">new</span>
                                </div>
                                <div class="product-info">
                                    <div class="product-rating">
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="reviews">150 reviews</div>
                                    </div>
                                    <h4 class="title"><a href="product-details.html">Readable content DX22</a></h4>
                                    <div class="prices">
                                        <span class="price">$210.00</span>
                                        <span class="price-old">300.00</span>
                                    </div>
                                    <div class="product-action">
                                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">
                                            <span>Add to cart</span>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" data-bs-target="#action-QuickViewModal">
                                            <i class="fa fa-expand"></i>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal" data-bs-target="#action-WishlistModal">
                                            <i class="fa-regular fa-heart"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--== End prPduct Item ==-->
                        </div>
                        <div class="swiper-slide mb-10">
                            <!--== Start Product Item ==-->
                            <div class="product-item product-st2-item">
                                <div class="product-thumb">
                                    <a class="d-block" href="product-details.html">
                                        <img src="/web/assets/images/shop/5.webp" width="370" height="450" alt="Image-HasTech">
                                    </a>
                                    <span class="flag-new">new</span>
                                </div>
                                <div class="product-info">
                                    <div class="product-rating">
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="reviews">150 reviews</div>
                                    </div>
                                    <h4 class="title"><a href="product-details.html">Readable content DX22</a></h4>
                                    <div class="prices">
                                        <span class="price">$210.00</span>
                                        <span class="price-old">300.00</span>
                                    </div>
                                    <div class="product-action">
                                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">
                                            <span>Add to cart</span>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" data-bs-target="#action-QuickViewModal">
                                            <i class="fa fa-expand"></i>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal" data-bs-target="#action-WishlistModal">
                                            <i class="fa-regular fa-heart"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--== End prPduct Item ==-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!--== End Product Area Wrapper ==-->
@endsection
