@extends("frontend.layout")

@section('title', $page->title)
@section('content')

<nav aria-label="breadcrumb" class="breadcrumb-style1">
    <div class="container">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $page->title }}</li>
        </ol>
    </div>
</nav>

<section class="section-space pb-0">
    <div class="container">
        <div class="blog-detail">
            <h3 class="blog-detail-title">{{ $page->title }}</h3>

            @if($page->feature_image)
                <img class="blog-detail-img mb-7 mb-lg-10" src="{{ $page->picture_url }}" width="1170" height="1012" alt="Image">
            @endif
            <div class="mt-5">
                {!! $page->description !!}
            </div>
            <div class="" style="margin-top:200px">
            </div>
        </div>


    </div>
</section>
@endsection
