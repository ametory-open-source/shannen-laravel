@extends("layouts.landing")

@section('content')
<section class="hero blog_hero">
    <div class="hero__wrapper blog_hero__wrapper">
        <div class="container">
            <div class="row">
                <div>
                    <h1>Our Gallery.</h1>
                </div>
                <div>
                    <ul>
                        <li><a href="/">Beranda</a></li>
                        <li><a href="/galleries"><i class="fad fa-long-arrow-right"></i>Gallery</a></li>
                    </ul>
                    <div class="icon">
                        <i class="fad fa-bullhorn"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog">
    <div class="blog__wrapper">

        <div class="blog__content">
            <div class="container">
                <div class="row">
                    @foreach ($galleries as $key => $item)
                    @php
                        $class = "col-lg-4";
                    @endphp
                    @switch(fmod($key + 1, 4))
                        @case(2)
                        @php
                            $class = "col-lg-8";
                        @endphp
                            @break
                        @case(3)
                        @php
                            $class = "col-lg-8 left";
                        @endphp

                            @break
                        @default
                    @endswitch
                    <div class="{{ $class }}">
                        <a href="/gallery/{{ $item->id}}">
                            <div class="blog__single blog__single--{{ $key+1 }}">
                                <div class="blog__single-image">
                                    <img src="{{ assetUrl($item->cover) }}" alt="image">
                                </div>
                                <div class="blog__single-info">
                                    <h3>{{ $item->title }}</h3>
                                    <h4>{{
                                        $item->created_at->format("d-m-Y") }}</h4>
                                    <p class="paragraph dark">{{ $item->short_description}} </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        <a href="#" class="button">
            <span>LOAD MORE <i class="fad fa-long-arrow-right"></i></span>
        </a>
    </div>
</section>
<!--blog single end-->
@endsection
@push("scripts")
<script>
    function(ev) {

    }
</script>
@endpush
