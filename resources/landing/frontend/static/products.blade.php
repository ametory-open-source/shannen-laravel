@extends("frontend.layout")

@section('content')
@if(isset($affiliate))
<section class="section-space" style="
    background: url({{$affiliate->cover_url}}) no-repeat center center;
    height: 300px;
    background-size: cover;
    position:relative;
    padding:initial;
" >
<div class="container" style="height:300px;position: relative; ">
    <div style="bottom: 10px;
    left: 10px;
    position: absolute;
    display: flex;
    ">
    @if ($affiliate->picture_url)
        <img src="{{$affiliate->picture_url}}" alt="" style="
            width: 100px;
            height: 100px;
            object-fit: cover;
            padding: 5px;
            background-color: white;
            border: 1px solid #dedede;
            border-radius: 5px;
            margin-right:20px;
        ">
    @endif
        <div style="
        padding: 15px;
        width: auto;
        display: block;
        border-radius: 10px;">
           <h1 style="font-weight: 700; color: #111;text-shadow: 0 0 20px #fff"> {{ $affiliate->display_name }}</h1>
        </div>
    </div>

</div>
</section>
@else
<!--== Start Page Header Area Wrapper ==-->
<section class="page-header-area pt-10 pb-9" data-bg-color="#FFF3DA">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="page-header-st3-content text-center text-md-start">
                    <ol class="breadcrumb justify-content-center justify-content-md-start">
                        <li class="breadcrumb-item"><a class="text-dark" href="/">{!! __('frontend.Home') !!}</a></li>
                        <li class="breadcrumb-item active text-dark" aria-current="page">{!! __('frontend.Products') !!}</li>
                    </ol>
                    <h2 class="page-header-title">{!! __('frontend.All Products') !!}</h2>
                </div>
            </div>
            <div class="col-md-7">
                <livewire:product-count />
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->
@endif



<!--== Start Product Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row justify-content-between flex-xl-row-reverse">
            <div class="col-xl-9">
                <livewire:product-grid :affiliate="$affiliate ?? null" />
            </div>
            <div class="col-xl-3">
                <div class="product-sidebar-widget">
                    <div class="product-widget-search">
                        <livewire:product-search >
                    </div>
                    <div class="product-widget">
                        <h4 class="product-widget-title">{!! __('frontend.Price Filter') !!}</h4>
                        <div class="product-widget-range-slider">
                            <div class="slider-range" id="slider-range"></div>
                            <div class="slider-labels">
                                <span id="slider-range-value1"></span>
                                <span>—</span>
                                <span id="slider-range-value2"></span>
                            </div>
                        </div>
                    </div>
                    <livewire:product-category :affiliate="$affiliate ?? null"  />
                    {{-- <div class="product-widget mb-0">
                        <h4 class="product-widget-title">Popular Tags</h4>
                        <ul class="product-widget-tags">
                            <li><a href="blog.html">Beauty</a></li>
                            <li><a href="blog.html">MakeupArtist</a></li>
                            <li><a href="blog.html">Makeup</a></li>
                            <li><a href="blog.html">Hair</a></li>
                            <li><a href="blog.html">Nails</a></li>
                            <li><a href="blog.html">Hairstyle</a></li>
                            <li><a href="blog.html">Skincare</a></li>
                        </ul>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Product Area Wrapper ==-->

<!--== Start Product Banner Area Wrapper ==-->
{{-- <section>
    <div class="container">
        <!--== Start Product Category Item ==-->
        <a href="product.html" class="product-banner-item">
            <img src="/web/assets/images/shop/banner/7.webp" width="1170" height="240" alt="Image-HasTech">
        </a>
        <!--== End Product Category Item ==-->
    </div> --}}
</section>
<!--== End Product Banner Area Wrapper ==-->

<!--== Start Product Area Wrapper ==-->
{{-- <section class="section-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2 class="title">Related Products</h2>
                    <p class="m-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis</p>
                </div>
            </div>
        </div>
        <div class="row mb-n10">
            <div class="col-12">
                <div class="swiper related-product-slide-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide mb-8">
                            <!--== Start Product Item ==-->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a class="d-block" href="product-details.html">
                                        <img src="/web/assets/images/shop/4.webp" width="370" height="450" alt="Image-HasTech">
                                    </a>
                                    <span class="flag-new">new</span>
                                    <div class="product-action">
                                        <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" data-bs-target="#action-QuickViewModal">
                                            <i class="fa fa-expand"></i>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">
                                            <span>Add to cart</span>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal" data-bs-target="#action-WishlistModal">
                                            <i class="fa-regular fa-heart"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <div class="product-rating">
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="reviews">150 reviews</div>
                                    </div>
                                    <h4 class="title"><a href="product-details.html">Readable content DX22</a></h4>
                                    <div class="prices">
                                        <span class="price">$210.00</span>
                                        <span class="price-old">300.00</span>
                                    </div>
                                </div>
                            </div>
                            <!--== End prPduct Item ==-->
                        </div>
                        <div class="swiper-slide mb-8">
                            <!--== Start Product Item ==-->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a class="d-block" href="product-details.html">
                                        <img src="/web/assets/images/shop/5.webp" width="370" height="450" alt="Image-HasTech">
                                    </a>
                                    <span class="flag-new">new</span>
                                    <div class="product-action">
                                        <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" data-bs-target="#action-QuickViewModal">
                                            <i class="fa fa-expand"></i>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">
                                            <span>Add to cart</span>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal" data-bs-target="#action-WishlistModal">
                                            <i class="fa-regular fa-heart"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <div class="product-rating">
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="reviews">150 reviews</div>
                                    </div>
                                    <h4 class="title"><a href="product-details.html">Readable content DX22</a></h4>
                                    <div class="prices">
                                        <span class="price">$210.00</span>
                                        <span class="price-old">300.00</span>
                                    </div>
                                </div>
                            </div>
                            <!--== End prPduct Item ==-->
                        </div>
                        <div class="swiper-slide mb-8">
                            <!--== Start Product Item ==-->
                            <div class="product-item">
                                <div class="product-thumb">
                                    <a class="d-block" href="product-details.html">
                                        <img src="/web/assets/images/shop/6.webp" width="370" height="450" alt="Image-HasTech">
                                    </a>
                                    <span class="flag-new">new</span>
                                    <div class="product-action">
                                        <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal" data-bs-target="#action-QuickViewModal">
                                            <i class="fa fa-expand"></i>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">
                                            <span>Add to cart</span>
                                        </button>
                                        <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal" data-bs-target="#action-WishlistModal">
                                            <i class="fa-regular fa-heart"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="product-info">
                                    <div class="product-rating">
                                        <div class="rating">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="reviews">150 reviews</div>
                                    </div>
                                    <h4 class="title"><a href="product-details.html">Readable content DX22</a></h4>
                                    <div class="prices">
                                        <span class="price">$210.00</span>
                                        <span class="price-old">300.00</span>
                                    </div>
                                </div>
                            </div>
                            <!--== End prPduct Item ==-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!--== End Product Area Wrapper ==-->
@endsection
@push('css')
<link rel="stylesheet" href="/web/assets/css/plugins/range-slider.css">

@endpush
@push('scripts')

    <script src="/web/assets/js/plugins/range-slider.js"></script>

    <script>
        var rangeSlider = document.getElementById('slider-range');

        function refresh() {
            $('.noUi-handle').on('click', function() {
                $(this).width(50);
            });

            var moneyFormat = wNumb({
                decimals: 0,
                thousand: '.',
                prefix: 'Rp'
            });
            noUiSlider.create(rangeSlider, {
                start: [12000, 500000],
                step: 50,
                range: {
                'min': [10000],
                'max': [2500000]
                },
                format: moneyFormat,
                connect: true
            });

            // Set visual min and max values and also update value hidden form inputs
            rangeSlider.noUiSlider.on('update', function(values, handle) {
                Livewire.emitTo('product-grid', 'setPriceFilter', [moneyFormat.from(values[0]), moneyFormat.from(values[1])]);
                Livewire.emitTo('product-category', 'setPriceFilter', [moneyFormat.from(values[0]), moneyFormat.from(values[1])]);
                document.getElementById('slider-range-value1').innerHTML = values[0];
                document.getElementById('slider-range-value2').innerHTML = values[1];
                document.getElementsByName('min-value').value = moneyFormat.from(values[0]);
                document.getElementsByName('max-value').value = moneyFormat.from(values[1]);
            });
        }
            // Livewire.emitTo('shop-sidebar', 'setPriceFilter', [100, 200]);

            // Initialize slider:
            $(document).ready(function() {
                refresh()
            });



    </script>
@endpush
