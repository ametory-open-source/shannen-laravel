@extends("frontend.layout")

@section('content')
<!--== Start Page Header Area Wrapper ==-->
<nav aria-label="breadcrumb" class="breadcrumb-style1">
    <div class="container">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Order Success</li>
        </ol>
    </div>
</nav>
<!--== End Page Header Area Wrapper ==-->

<!--== Start Shopping Checkout Area Wrapper ==-->
<section class="shopping-checkout-wrap section-space">
    <div class="container">

        <div class="row">
            <div class="offset-md-3 col-lg-6">
                <h1 class="text-center">Thank You for purchasing</h1>
                <h4 class="text-center">Please follow next link to complete your order</h4>
                <div class="form-group d-flex align-items-center mb-14 mt-10 d-flex justify-content-center">
                    <a href="{{ $order->payment_link }}" target="_blank" class="btn" type="submit">Payment Link</a>
                </div>
            </div>

        </div>
    </div>
</section>
<!--== End Shopping Checkout Area Wrapper ==-->
@endsection
