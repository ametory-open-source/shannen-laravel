@extends("frontend.layout")
@section('title', $blog->title)
@section('og_image', $blog->picture_url)
@section('content')
<!--== Start Page Header Area Wrapper ==-->
<nav aria-label="breadcrumb" class="breadcrumb-style1">
    <div class="container">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item"><a href="/blog">Blog</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $blog->title }}</li>
        </ol>
    </div>
</nav>
<!--== End Page Header Area Wrapper ==-->
 <!--== Start Blog Detail Area Wrapper ==-->
 <section class="section-space pb-0">
    <div class="container">
        <div class="blog-detail">
            <h3 class="blog-detail-title">{{ $blog->title }}</h3>
            <div class="blog-detail-category">
                <a class="category" data-bg-color="{{ randomPastelColor() }}" href="{{ $blog->category_url }}">{{ $blog->category->name }}</a>
            </div>
            <img class="blog-detail-img mb-7 mb-lg-10" src="{{ $blog->picture_url }}" width="1170" height="1012" alt="{{ $blog->feature_image_caption }}">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-md-7">
                            <ul class="blog-detail-meta">
                                <li class="meta-admin">
                                    {{-- <img src="assets/images/blog/admin.webp" alt="Image"> --}}
                                {{ $blog->author }}</li>
                                <li>{{ $blog->created_at->format("d M Y") }}</li>
                            </ul>
                        </div>
                        <div class="col-md-5">
                            {{-- <div class="blog-detail-social">
                                <span>Share:</span>
                                <a href="https://www.pinterest.com/" target="_blank" rel="noopener"><i class="fa fa-pinterest-p"></i></a>
                                <a href="https://twitter.com/" target="_blank" rel="noopener"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.facebook.com/" target="_blank" rel="noopener"><i class="fa fa-facebook"></i></a>
                            </div> --}}
                        </div>
                    </div>

                </div>
            </div>
            <div class="mt-5">
                {!! $blog->description !!}
            </div>

            <div class="blog-detail-category mt-5">
                @foreach ($blog->tag_list as $item)
                <a class="category" data-bg-color="{{ randomPastelColor() }}" href="/blog?tag={{ $item }}">{{ $item }}</a>
                @endforeach

            </div>
        </div>
        @if ($blog_banner)
        <div class="section-space pb-0">

            <!--== Start Product Category Item ==-->
            <a href="{{ $blog_banner->link }}" class="product-banner-item">
                <img src="{{ $blog_banner->picture_url }}" width="1170" height="200" alt="{{ $blog_banner->title }}">
            </a>
            <!--== End Product Category Item ==-->
        </div>
        @endif

        <div class="row justify-content-between align-items-center pt-10 mt-4 section-space">
            <div class="col-sm-6">
                @if ($prev)
                <a href="{{ $prev->url }}" class="blog-next-previous">
                    <div class="thumb">
                        <span class="arrow">PREV</span>
                        <img src="{{ $prev->picture_url }}" width="93" height="80" alt="Image">
                    </div>
                    <div class="content">
                        <h4 class="title">{{ $prev->title }}</h4>
                        <h5 class="post-date">{{ $prev->created_at->format("d M Y") }}</h5>
                    </div>
                </a>
                @endif
            </div>
            <div class="col-sm-6">
                @if ($next)
                <a href="{{ $next->url }}" class="blog-next-previous blog-next">
                    <div class="thumb">
                        <span class="arrow">NEXT</span>
                        <img src="{{ $next->picture_url }}" width="93" height="80" alt="Image">
                    </div>
                    <div class="content">
                        <h4 class="title">{{ $next->title }}</h4>
                        <h5 class="post-date">{{ $next->created_at->format("d M Y") }}</h5>
                    </div>
                </a>
                @endif
            </div>
        </div>
        {{-- @if ($blog->comment_enabled) --}}
        @if (false)
        <div class="blog-comment-form-wrap">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <h4 class="blog-comment-form-title">Comment</h4>
                    {{-- <div class="blog-comment-form-info">
                        <div class="blog-comment-form-social">
                            <span>Share:</span>
                            <a href="https://www.pinterest.com/" target="_blank" rel="noopener"><i class="fa fa-pinterest-p"></i></a>
                            <a href="https://twitter.com/" target="_blank" rel="noopener"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.facebook.com/" target="_blank" rel="noopener"><i class="fa fa-facebook"></i></a>
                        </div>
                        <select class="blog-comment-form-select">
                            <option selected>Short By Newest</option>
                            <option value="1">Best</option>
                            <option value="2">Newest</option>
                            <option value="3">Oldest</option>
                        </select>
                    </div> --}}
                    <div class="blog-comment-form">
                        @if (auth("affiliator")->check() || auth("user")->check())
                        <img class="blog-comment-img" src="assets/images/blog/form1.webp" width="110" height="110" alt="Image">
                        @endif
                        <textarea class="blog-comment-control" placeholder="type your comment"></textarea>
                        <button type="button" class="comment-button comment-button-action" style="">
                            <span>Send</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
<!--== End Blog Detail Area Wrapper ==-->

<!--== Start Blog Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2 class="title">{{ $web->blog_title}} </h2>
                    <p>{{ $web->blog_subtitle}}</p>
                </div>
            </div>
        </div>
        <div class="row mb-n9">
            @foreach ($relateds as $item)
            <div class="col-sm-6 col-lg-4 mb-8">
                @include("frontend.components.modules.blog_card", ["item" => $item])
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--== End Blog Area Wrapper ==-->

@includeWhen($web->show_join, "frontend.components.landing.join")



@endsection
@push('css')
    <style>
        .comment-button {
            border-radius: 50px;
            height: 50px;
            letter-spacing: .2em;
            font-size: 13px;
            color: #231942;
            font-weight: 500;
            margin: 0 20px;
            padding: 5px 28px 5px 32px;
            text-align: center;
            text-transform: uppercase;
            transition-delay: .15s;
        }

        .comment-button-action {
            background-color: #fff;
            border: 2px solid #e63946;
            color: #231942;
            transition: all .5s ease 0s;
            transition-delay: .1s;
        }
    </style>
@endpush

@push("scripts")
<script>
    // function reply(comment_id) {
    //     $("#parent_id").val(comment_id)
    // }
    // $(".alert-wrapper").click(function() {
    //     $(this).remove()
    // })


</script>
@endpush
