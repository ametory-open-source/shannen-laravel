@extends("frontend.layout")

@section('content')
<!--== Start Page Header Area Wrapper ==-->
<nav aria-label="breadcrumb" class="breadcrumb-style1 mb-10">
    <div class="container">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Beranda</a></li>
            <li class="breadcrumb-item active" aria-current="page">Blog</li>
        </ol>
    </div>
</nav>

<section class="page-header-area page-header-style2-area" data-bg-img="{{ $web->blogs_banner ? assetUrl($web->blogs_banner) : null }}">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-7">
                <div class="page-header-content page-header-st2-content">
                    <div class="title-img"><img src="/web/assets/images/photos/page-header-text1.webp" alt="Image"></div>
                    <h2 class="page-header-title">{{ $web->blogs_banner_title }}</h2>
                    <p class="page-header-desc">{{ $web->blogs_banner_subtitle }}</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Page Header Area Wrapper ==-->

<!--== Start Blog Area Wrapper ==-->
<section class="section-space pb-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2 class="title">{{ $web->blog_title }}</h2>
                    <p>{{ $web->blog_subtitle }}</p>
                </div>
            </div>
        </div>
        <div class="row mb-n9">
            @foreach ($blogs as $item)
            <div class="col-sm-6 col-lg-4 mb-8">
                @include("frontend.components.modules.blog_card", ["item" => $item])
            </div>
            @endforeach
            <div class="col-sm-12 col-lg-4 mb-8">
                <div class="row mb-n10">
                    @foreach ($rights as $item)
                    <div class="col-md-6  col-lg-12 mb-10">
                        <!--== Start Blog Item ==-->
                        <div class="post-item">
                            <div class="content">
                                <h4 class="title mt-0"><a href="{{ $item->url }}">{{ $item->title }}</a></h4>
                                <p class="desc">{{ $item->short_desc }}</p>
                                <a class="btn-link" href="{{ $item->url }}">Learn more</a>
                            </div>
                        </div>
                        <!--== End Blog Item ==-->
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!--== End Blog Area Wrapper ==-->

<!--== Start Blog Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="row mb-n9">
            @foreach ($bottoms as $item)
            <div class="col-sm-6 mb-8">
                <div class="post-item">
                    <a href="{{ $item->url }}" class="thumb">
                        <img src="{{ $item->picture_url }}" width="570" height="340" alt="{{ $item->title }}">
                    </a>
                    <div class="content">
                        <a class="post-category" style="background-color: {{ randomPastelColor() }}" href="{{ $item->category_url }}">{{ $item->category->name }}</a>
                        <h4 class="title"><a href="{{ $item->url }}">{{ $item->title }}</a></h4>
                        <ul class="meta">
                            <li class="author-info"><span>By:</span> <a href="{{ $item->author_url }}">{{ $item->author }}</a></li>
                            <li class="post-date">{{ $item->created_at->format("d M Y")}}</li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>
<!--== End Blog Area Wrapper ==-->

@if ($others->count())
<!--== Start Blog Area Wrapper ==-->
<section class="section-space pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h2 class="title">{{ $web->other_posts_title }}</h2>
                    <p>{{ $web->other_posts_subtitle }}</p>
                </div>
            </div>
        </div>
        <div class="row mb-n9">
            @foreach ($others as $item)
            <div class="col-sm-6 col-lg-4 mb-8">
                @include("frontend.components.modules.blog_card", ["item" => $item])
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--== End Blog Area Wrapper ==-->
@endif


@if($blog_banner)
<!--== Start Product Banner Area Wrapper ==-->
<section class="section-space pt-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-lg-8">
                <!--== Start Product Category Item ==-->
                <a href="{{ $blog_banner->link }}" class="product-banner-item">
                    <img src="{{ $blog_banner->picture_url }}" width="770" height="250" alt="{{ $blog_banner->title }}">
                </a>
                <!--== End Product Category Item ==-->
            </div>
        </div>
    </div>
</section>
<!--== End Product Banner Area Wrapper ==-->
@endif

<div class="mt-5"></div>
<!--== End Blog Area Wrapper ==-->
@includeWhen($web->show_join, "frontend.components.landing.join")
@endsection
