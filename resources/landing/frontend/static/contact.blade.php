@extends("frontend.layout")

@section('content')
 <!--== Start Contact Area Wrapper ==-->
 <section class="contact-area">
    <div class="container">
        <div class="row">
            <div class="offset-lg-6 col-lg-6">
                <div class="section-title position-relative">
                    <h2 class="title">{{ $web->contact_title }}</h2>
                    <p class="m-0">{{ $web->contact_subtitle }}</p>
                    <div class="line-left-style mt-4 mb-1"></div>
                </div>
                <!--== Start Contact Form ==-->
                <div class="contact-form">
                    <form id="contact-form" action="/contact" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="required"></label>
                                    <input required class="form-control" type="text" name="name" placeholder="{!! __('frontend.Full Name') !!}" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class=""></label>
                                    <input  name="phone" class="form-control" type="text" placeholder="{!! __('frontend.Phone') !!}">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="" class="required"></label>
                                    <input class="form-control" required type="email" name="email" placeholder="{!! __('frontend.Email Address') !!}">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="" class="required"></label>
                                    <textarea class="form-control" name="description" placeholder="{!! __('frontend.Message') !!}"></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mb-0">
                                    <button class="btn btn-sm" type="submit">{!! __('frontend.SUBMIT') !!}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--== End Contact Form ==-->

                <!--== Message Notification ==-->
                <div class="form-message mt-5"></div>
            </div>
        </div>
    </div>
    <div class="contact-left-img" data-bg-img="{{ assetUrl($web->contact_picture) }}"></div>
</section>
<!--== End Contact Area Wrapper ==-->

<!--== Start Contact Area Wrapper ==-->
<section class="section-space">
    <div class="container">
        <div class="contact-info">
            <div class="contact-info-item">
                <img class="icon" src="/web/assets/images/icons/1.webp" width="30" height="30" alt="Icon">
                <a href="tel://{{ $web->company_phone }}">{{ $web->company_phone }}</a>
            </div>
            <div class="contact-info-item">
                <img class="icon" src="/web/assets/images/icons/2.webp" width="30" height="30" alt="Icon">
                <a href="mailto:{{ $web->company_email }}">{{ $web->company_email }}</a>

            </div>
            <div class="contact-info-item mb-0">
                <img class="icon" src="/web/assets/images/icons/3.webp" width="30" height="30" alt="Icon">
                <p>{{ $web->company_address }}</p>
            </div>
        </div>
    </div>
</section>
<!--== End Contact Area Wrapper ==-->

<div class="map-area">
    {!! $web->company_map !!}
</div>


@push("scripts")
<script>


</script>
@endpush
<!--blog single end-->
@endsection
