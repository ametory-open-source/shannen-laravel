@extends("layouts.landing")

@section('content')
<section class="hero blog_hero">
    <div class="hero__wrapper blog_hero__wrapper">
        <div class="container">
            <div class="row">
                <div>
                    <h1>{{ $gallery->title}}</h1>
                </div>
                <div>
                    <ul>
                        <li><a href="/">Beranda</a></li>
                        <li><a href="/galleries"><i class="fad fa-long-arrow-right"></i>Gallery</a></li>
                    </ul>
                    <div class="icon">
                        <i class="fad fa-photo-video"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog">
    <div class="blog__wrapper">

        <div class="blog__content">
            <div class="container">
                <div class="row">
                    @foreach ($gallery->items as $key => $item)
                    @php
                        $class = "col-lg-4";
                    @endphp
                    @switch(fmod($key + 1, 4))
                        @case(2)
                        @php
                            $class = "col-lg-8";
                        @endphp
                            @break
                        @case(3)
                        @php
                            $class = "col-lg-8 left";
                        @endphp

                            @break
                        @default
                    @endswitch
                    <div class="{{ $class }}">
                        <a href="javascript:void(0)" onclick="show('{{ assetUrl($item->picture) }}', '{{ $item->description }}')">
                            <div class="blog__single blog__single--{{ $key+1 }}">
                                <div class="blog__single-image">
                                    <img src="{{ assetUrl($item->picture) }}" alt="image">
                                </div>
                                <div class="blog__single-info">
                                    <h3>{{ $item->description }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>
</section>
<section id="full-screen">
    <img  alt="" srcset="">
    <div class="img-caption"></div>
    <i class="fas fa-window-close fa-5x  close"></i>
</section>
<!--blog single end-->
@endsection
@push("scripts")
<script>
    function show(url, caption) {
        $("#full-screen img").attr("src", url)
        $("#full-screen .img-caption").html(caption)
        $("#full-screen").addClass("show")
    }

    $(".close").click(function() {
        $("#full-screen").removeClass("show")
    })
</script>
@endpush
@push('css')
<link rel="stylesheet" href="/assets/css/all.min.css">
<style>
    #full-screen {
        width: 100%;
        height: 100vh;
        background: rgba(0,0,0,0.8);
        position: fixed;
        bottom: 0;
        right: 0;
        left: 0;
        top: 0;
        display: none;
        z-index: 9999;
    }
    #full-screen.show{
        display: flex;
        justify-content: center;
        padding: 60px;
    }
    #full-screen img{

    }
    #full-screen .img-caption{
        position: absolute;
        bottom: 9px;
        font-size: 12pt;
        background-color: rgba(0,0,0,0.8);
        padding: 10px;
        border-radius: 10px;
        color: white;
        min-width: 160px;
        max-width: 80vw;
        text-align: center;
    }
    #full-screen .close{
        position: absolute;
        right: 20px;
        top: 20px;
    }
</style>
@endpush
