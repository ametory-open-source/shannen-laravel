@extends("layouts.landing")

@section('content')
@includeWhen($web->show_header, "components.landing.header")


<!--==========================-->
<!--=         Banner         =-->
<!--==========================-->
<section class="page-banner-two">

    <div class="container pr">
        <ul class="banner-pertical-three">
            <li>
                <img src="/web/media/banner/main/rabar.png" data-parallax='{"y": -50, "x": -50}' alt="astriol pertical">
            </li>
            <li>
                <img src="/web/media/banner/main/flash.png" data-parallax='{"y": -50, "x": 50}' alt="astriol pertical">
            </li>
        </ul>
        <!-- /.banner-pertical -->

        <div class="page-title-wrapper text-left">
            <h1 class="page-title">Welcome</h1>

            <ul class="breadcrumbs">
                <li>
                    <a href="/">{{ $invitation->name }}</a>
                </li>
                
            </ul>
        </div>
        <!-- /.page-title-wrapper -->
    </div>
    <!-- /.container -->
</section>
<!-- /.page-banner -->

<!--================================-->
<!--=         Contact Form         =-->
<!--================================-->
<section class="contact-form-page">
    <div class="container">
        <div class="row">
            <div class="col-3"></div>
            
            <div class="col-6">
                <div class="contact-form-wrapper">
                    
                    @if(now()->diffInSeconds($invitation->valid_date, false) > 0)
                    <form action="/invitations/{{ $invitation->uuid }}/validation" method="post" class=" d-flex justify-content-center flex-column" >
                        @csrf
                            
                            <div class="form-group">
                                <h3 class="text-center mb-5">{{ $invitation->company->name }}, <br> has invited you to join as a/an {{ $invitation->position->name }}</h3>
                                <h5 class="text-center mb-5">Please Enter The Validation Code That We Have Sent Via Email</h3>
                                @if ($message = Session::get('error'))
                                    <div style="padding:20px; cursor: pointer;" class="alert-wrapper" data-bs-dismiss="alert"
                                        aria-label="Close">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            {{ $message }}
                                        </div>
                                    </div>
                                @endif
                                @if ($message = Session::get('success'))
                                    <div style="padding:20px; cursor: pointer;" class="alert-wrapper" data-bs-dismiss="alert"
                                        aria-label="Close">
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            {{ $message }}
                                        </div>
                                    </div>
                                @else
                                    <input type="text" name="validation_code" placeholder="Enter Validation Code" class="gp-input" value="{{ old('validation_code') }}">
                                @endif
                            </div>
                            @if (!Session::get('success'))
                                <button type="submit" class="btn-validation" style="margin: auto; text-align:center">Send Validation</button>
                            @endif
                    </form>
                    @else
                    <div class="alert alert-danger" role="alert">
                        Your invitation is Expired, Please ask your administrator to re-invite
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
</section>

@includeWhen($web->show_footer, "components.landing.footer")
@push("css")
<style>
    .btn-validation {
        padding: 10px 32px;
        color: #fff;
        cursor: pointer;
        display: inline-block;
        position: relative;
        border: 2px solid #1abf68;
        -webkit-transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
        -o-transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
        transition: all 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) 0s;
        z-index: 2;
        border-radius: 3px;
        font-size: 15px;
        font-weight: 500;
        background-color: #1abf68 !important;
        box-shadow: 0 10px 20px 0 rgba(6, 148, 73, 0.3);
}
</style>
@endpush
@push("scripts")

@endpush
<!--blog single end-->
@endsection