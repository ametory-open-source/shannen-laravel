@extends("layouts.landing")

@section('content')


<section class="contact">
    <div class="container mt-5">
        <div class="row">
            @includeWhen($web->show_pricing, 'components.landing.pricing')
        </div>
        
    </div>
</section>

<!--blog single end-->
@endsection
@push("scripts")
<script>
    @if ($message = Session::get('success'))
        alert("Thank you for your participation")
    @endif
    @if ($message = Session::get('error'))
        alert("Something error, we cannot receive your message")
    @endif
   
</script>
@endpush
@push('css')
    <style>
        .map_wrapper {
            width: calc(100% - 10rem);
            margin: 0 auto;
        }
        .map_wrapper iframe {
            border-radius: 3rem;
        }
        address .fas {
            color: #999;
        }
    </style>
@endpush