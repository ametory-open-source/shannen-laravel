@extends("layouts.landing")

@section('content')

<section id="login" class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="/join" method="post">
                @csrf

                <div class="input-wrapper">
                    <h1 class="section-heading color-black" style="margin-bottom: 10px">Bergabung Sekarang</h1>
                </div>
                <div class="input-wrapper" style="margin-bottom: 40px">
                    <h3>Dapatkan Akses Premium Gratis Selama 3 Bulan</h3>
                </div>
                <div class="input-wrapper">
                    <input value="{{ old('name') }}" type="text" name="name" required placeholder="Nama Lengkap *"
                        class="input-field">
                </div>
                <div class="input-wrapper">
                    <input value="{{ old('company_name') }}" type="text" name="company_name" required placeholder="Nama Usaha *"
                        class="input-field">
                </div>
                <div class="input-wrapper">
                    <input value="{{ old('email') }}" type="email" name="email" required placeholder="Email *"
                        class="input-field">
                </div>
                <div class="input-wrapper">
                    <input value="{{ old('phone') }}" type="phone" name="phone" required placeholder="Phone *"
                        class="input-field">
                </div>
                <div class="input-wrapper">
                    <input type="text" value="{{ old('city') }}" type="text" name="city" required
                    placeholder="Kota *" class="input-field">
                </div>
                <div class="input-wrapper">
                    <input type="text" value="{{ old('province') }}" type="text" name="province" required
                    placeholder="Provinsi *" class="input-field">
                </div>
                <div class="input-wrapper">
                    <button class="button" type="submit"><span>Gabung Sekarang <i
                        class="fad fa-long-arrow-right"></i></span></button>
                </div>
            </form>
        </div>
    </div>
</section>

<!--blog single end-->
@endsection
@push("scripts")
<script>
    @if ($message = Session::get('success'))
        alert('{{$message}}')
    @endif
    @if ($message = Session::get('error'))
        alert('{{$message}}')
    @endif
</script>
@endpush
@push('css')
<style>
    .map_wrapper {
        width: calc(100% - 10rem);
        margin: 0 auto;
    }

    .map_wrapper iframe {
        border-radius: 3rem;
    }

    address .fas {
        color: #999;
    }

    #login {
        margin-top: 300px
    }

    .input-field {
        width: 50%;
        margin-bottom: 20px;
    }

    .input-wrapper button {
        width: 50%;
        margin-bottom: 20px;
    }

    .input-wrapper {
        display: flex;
        justify-content: center;
    }
    .section-heading {
        text-align: center;
        margin-bottom: 40px;
    }
</style>
@endpush