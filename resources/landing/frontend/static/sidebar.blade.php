<div class="col-md-4 sidebar-container sidebar-widget-area">
    <aside class="sidebar">
        <div id="search-2" class="widget astriol_widget widget_search">
            <form role="search" method="get" class="search-form" action="/blog">
                <label>
                    <input type="search" class="search-field" placeholder="Search..." name="search"
                        title="Search for:" >
                </label>
                <button type="submit" class="search-submit">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
        <div id="gp-posts-widget-2" class="widget gp-posts-widget">
            <h2 class="widget-title">Recent Posts</h2>
            <div class="gp-posts-widget-wrapper">
                @foreach ($recents as $item)
                <div class="post-item">
                    <div class="post-widget-thumbnail">
                        <a href="/blog/{{ $item->slug }}">
                            <img src="{{ assetUrl($item->feature_image) }}" alt="thumb">
                        </a>
                    </div>
                    <div class="post-widget-info">
                        <h5 class="post-widget-title">
                            <a href="/blog/{{ $item->slug }}" title="This Is Test Post">{{ $item->title }}</a>
                        </h5>
                        <ul class="post-meta">
                            <li><a href="/blog?category={{ optional($item->category)->name }}">{{ optional($item->category)->name }}</a></li>
                            <li><a href="#">{{ $item->created_at->format("d M Y") }}</a></li>
                        </ul>
                    </div>
                </div>    
                @endforeach
            </div>
        </div>
        <div id="categories" class="widget widget_categories">
            <h2 class="widget-title">Categories</h2>
            <ul>
                @foreach ($categories as $item)
                @if ($item->published_blogs->count())
                <li>
                    <a href="/blog?category={{ $item->name }}">{{ $item->name }} <span class="count">({{$item->published_blogs->count()}})</span></a>
                </li>        
                @endif
                @endforeach
            </ul>
        </div>
        {{-- <div id="widget_recent_comments" class="widget widget_recent_comments">
            <h2 class="widget-title">Recent Comments</h2>
            <div class="recent-comments">
                <div class="comment-list">
                    <div class="avatar">
                        <img src="media/blog/ca1.jpg" alt="author">
                    </div>

                    <div class="comment-content">
                        <a href="#" class="comments-text">My good sir at public school the BBC some
                            dodgy
                            chav boot ruddy gosh.</a>
                        <a href="#" class="comment-author-link">Hanson Deck</a>
                    </div>
                </div><!-- /.comment-list -->

                <div class="comment-list">
                    <div class="avatar">
                        <img src="media/blog/ca2.jpg" alt="author">
                    </div>

                    <div class="comment-content">
                        <a href="#" class="comments-text">Nancy boy get stuffed mate cup of char loo up
                            the
                            duff naff cheeky.</a>
                        <a href="#" class="comment-author-link">Guy Mann</a>
                    </div>
                </div><!-- /.comment-list -->

                <div class="comment-list">
                    <div class="avatar">
                        <img src="media/blog/ca3.jpg" alt="author">
                    </div>

                    <div class="comment-content">
                        <a href="#" class="comments-text">My good sir at public school the BBC some
                            dodgy
                            chav boot ruddy gosh.</a>
                        <a href="#" class="comment-author-link">Hanson Deck</a>
                    </div>
                </div><!-- /.comment-list -->

            </div><!-- /.recent-comments -->
        </div>
        <aside id="tags" class="widget widget_tag">
            <h3 class="widget-title">Popular Tags</h3>
            <div class="tagcloud">
                <a href="#">Astriol</a>
                <a href="#">Web Design</a>
                <a href="#">App & Saas</a>
                <a href="#">Saas Lading</a>
                <a href="#">Project</a>
                <a href="#">Article</a>
                <a href="#">Software</a>
                <a href="#">Portfolio</a>
                <a href="#">Creativity</a>
            </div>
        </aside><!-- /.widget --> --}}
    </aside>
    <!-- /.sidebar -->
</div>