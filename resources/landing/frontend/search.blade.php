@extends("frontend.layout")

@section('content')

<nav aria-label="breadcrumb" class="breadcrumb-style1 mb-10">
    <div class="container">
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Search</li>
        </ol>
    </div>
</nav>


<section class="section-space pb-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title text-center">
                    <h4>Hasil Pencarian Produk : {{ request()->get("query")}}</h4>
                </div>
            </div>
            <div class="col-12">
                <div class="row g-3 g-sm-6">
                    @if (!$products->count())
                        <p>Pencarian tidak ditemukan</p>
                    @endif
                    @foreach ($products as $item)
                    <div class="col-6 col-lg-4 col-xl-4 mb-4 mb-sm-8" :wire:key="item-grid-{{$item->id}}">
                        <!--== Start Product Item ==-->
                        <div class="product-item product-st3-item">
                            <div class="product-thumb">
                                <a class="d-block" href="{{ $item->url }}">
                                    <img src="{{ $item->feature_image }}" width="370" height="450" alt="Image-HasTech">
                                </a>
                                @if ($item->promo_tag_label)
                                <span class="flag-new" style="background-color: {{$item->promo_tag_color}}">{{
                                    $item->promo_tag_label }}</span>
                                @endif
                                <div class="product-action">

                                    @if($frontendUser)
                                    <button type="button" wire:click="addToWishlist('{{$item->id}}')"
                                        class="product-action-btn action-btn-wishlist" data-bs-toggle="modal"
                                        data-bs-target="#action-grid-Wishlist-{{$item->id}}">
                                        @if ($item->is_favorited)
                                        <i class="fa-solid fa-heart" style="color: #ff6565"></i>
                                        @else
                                        <i class="fa-regular fa-heart"></i>
                                        @endif
                                    </button>
                                    @endif
                                </div>
                            </div>
                            <div class="product-info">
                                <div class="product-rating">
                                    <div class="rating">
                                        {!! $item->rating_stars !!}
                                    </div>
                                    <div class="reviews">{{ $item->review_count }} {!! __('frontend.reviews') !!}</div>
                                </div>
                                <h4 class="title"><a href="{{ $item->url }}">{{ $item->name }}</a></h4>
                                <div class="prices">
                                    <span class="price">{{ $item->price_format }}</span>
                                    @if($item->strike_price_format)
                                    <span class="price-old">{{ $item->strike_price_format }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <!--== End prPduct Item ==-->
                    </div>
                    @endforeach

                    {{ $products->links('frontend.components.modules.paginator2') }}
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="section-title text-center">
                    <h4>Hasil Pencarian Artikel : {{ request()->get("query")}}</h4>
                </div>
            </div>
            <div class="row mb-5">
                @if (!$blogs->count())
                        <p>Pencarian tidak ditemukan</p>
                @endif
                @foreach ($blogs as $item)
                <div class="col-sm-6 col-lg-4 mb-8">
                    @include("frontend.components.modules.blog_card", ["item" => $item])
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
