@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])
@php
    $form_title = "Add Product Category";
    if ($product_category->id) {
        $form_title = "Edit Product Category";
    }
@endphp
@section('content')
@include('layouts.navbars.auth.topnav', ['title' => $form_title])
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">{{$form_title}}</div>
        <div class="card-body table-responsive">
            <form action="/admin/product_categories{{ $product_category->id ?  '/'.$product_category->id.'/update' : null }}" method="post" class="form" enctype='multipart/form-data'>
                @csrf
                @if( $product_category->id)
                @method("PUT")
                @endif
                <input type="hidden" name="picture" value="{{ $product_category->picture }}" class="picture">
                <input type="hidden" name="icon" value="{{ $product_category->icon }}" class="icon">
                <div class="row mb-3">
                    <div id="dropzone"
                        class="dropzone-wrapper dropzone {{ $product_category->picture ? 'col-md-6' : 'col-md-8'}} offset-md-2">

                        <div class="dz-message needsclick">
                            <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                    class="path2"></span></i>
                            <!--begin::Info-->
                            <div class="ms-4">
                                <h3 class="fs-5 fw-bold text-gray-900 mb-1">Drop files here or click to upload.</h3>
                            </div>
                            <!--end::Info-->
                        </div>

                    </div>
                    @if ($product_category->picture)
                    <div class="col-md-2 img-preview-wrapper">
                        <div class="img-preview" style="background: url('/storage{{ $product_category->picture }}')"></div>
                    </div>
                    @endif
                </div>


                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="name">Name</label></div>
                    <div class="col-md-6">
                        <input type="text" name="name" id="" value="{{ old("name") ??  $product_category->name }}" class="form-control"
                            placeholder="Name">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="slug">Slug</label></div>
                    <div class="col-md-6">
                        <input type="text" name="slug" id="" class="form-control" placeholder="Slug"
                            value="{{ old("slug") ??  $product_category->slug }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="link">Color</label></div>
                    <div class="col-md-6">
                        <input type="color" name="color" id="" class="form-control" placeholder="Color"
                            value="{{ old("color") ??  $product_category->color }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="sort">Sort</label></div>
                    <div class="col-md-6">
                        <input type="number" name="sort" id="" class="form-control" placeholder="Sort"
                            value="{{ old("sort") ??  $product_category->sort }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="promo_tag">Promo Tag</label></div>
                    <div class="col-md-6">
                        @form_select("promo_tag", ["custom" => "Custom", "new" => "New", "sale" => "Sale", "discount" => "Discount", ], old("promo_tag") ??  $product_category->promo_tag, ["class" => "form-control"])
                    </div>
                </div>

                <div class="custom_promo_tag_wrapper">
                    <div class="form-group row">
                        <div class="col-md-2 offset-md-2"><label for="custom_promo_tag">Custom Promo Tag</label></div>
                        <div class="col-md-6">
                            <input type="text" name="custom_promo_tag" id="" class="form-control" placeholder="Custom Promo Tag"
                                value="{{ old("custom_promo_tag") ??  $product_category->custom_promo_tag }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2 offset-md-2"><label for="custom_promo_tag_color">Custom Promo Tag Color</label></div>
                        <div class="col-md-6">
                            <input type="color" name="custom_promo_tag_color" id="" class="form-control" placeholder="Color"
                                value="{{  old("custom_promo_tag_color") ??  $product_category->custom_promo_tag_color }}">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="description">Description</label></div>
                    <div class="col-md-6">
                        <textarea rows="9" name="description" id="description" class="form-control"
                            placeholder="Description">{{ old("description") ?? $product_category->description }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2">
                        <label for="flag">Active</label>

                    </div>
                    <div class="col-md-6">
                        @form_select("flag", [true => "Active", false => "Inactive"], old("flag") ??  $product_category->flag, [
                            "class" => "form-control"
                        ])
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2 offset-md-2">
                        <label for="flag">Featured</label>
                    </div>
                    <div class="col-md-6">
                        @form_select("is_featured", [ false => "No", true => "Yes"], old("is_featured") ??  $product_category->is_featured, [
                            "class" => "form-control"
                        ])
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2 offset-md-4">
                        <button type="submit" class="btn bg-gradient-primary mt-5"
                            style="min-width: 160px">Save</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>
@push("scripts")
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.js"
    integrity="sha512-sSWQXoxIkE0G4/xqLngx5C53oOZCgFRxWE79CvMX2X0IKx14W3j9Dpz/2MpRh58xb2W/h+Y4WAHJQA0qMMuxJg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/keymap/sublime.min.js"
    integrity="sha512-SV3qeFFtzcmGtUQPLM7HLy/7GKJ/x3c2PdiF5GZQnbHzIlI2q7r77y0IgLLbBDeHiNfCSBYDQt898Xp0tcZOeA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/htmlmixed/htmlmixed.min.js"
    integrity="sha512-HN6cn6mIWeFJFwRN9yetDAMSh+AK9myHF1X9GlSlKmThaat65342Yw8wL7ITuaJnPioG0SYG09gy0qd5+s777w=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/xml/xml.min.js"
    integrity="sha512-LarNmzVokUmcA7aUDtqZ6oTS+YXmUKzpGdm8DxC46A6AHu+PQiYCUlwEGWidjVYMo/QXZMFMIadZtrkfApYp/g=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/css/css.min.js"
    integrity="sha512-rQImvJlBa8MV1Tl1SXR5zD2bWfmgCEIzTieFegGg89AAt7j/NBEe50M5CqYQJnRwtkjKMmuYgHBqtD1Ubbk5ww=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/javascript/javascript.min.js"
    integrity="sha512-Cbz+kvn+l5pi5HfXsEB/FYgZVKjGIhOgYNBwj4W2IHP2y8r3AdyDCQRnEUqIQ+6aJjygKPTyaNT2eIihaykJlw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js"
    integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/css-hint.min.js"
    integrity="sha512-1BD3lo262s+DtDFoeD8+ssEL1zVJU8SHWWMtUyxfLqMV/jQDEnyQlS/CL3mD1kkLSv2hhYq1sdcA/zAPDz4JVA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js"
    integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
    // The dropzone method is added to jQuery elements and can
    // be invoked with an (optional) configuration object.
    $(".dropzone").dropzone({
        url: "/admin/file/upload" ,
        method: 'post',
	    headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        accept: function(file, done) {
            done()
        },
        success: function(file, response) {

            $(".picture").val(response.file)
            $('.img-preview-wrapper').remove()
            $('.dropzone-wrapper').removeClass("col-md-6").addClass("col-md-8")
        },

    });

    $('.icon-wrapper').click(function(el) {
        $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
        $(".icon").val($(this).data('icon'))
    })

    $(function() {

        if ($("[name=promo_tag]").val() == "custom") {
            $(".custom_promo_tag_wrapper").show();
        } else {
            $(".custom_promo_tag_wrapper").hide();
            $("[name=custom_promo_tag]").val("")
            $("[name=custom_promo_tag_color]").val("")
        }
    })

    $("[name=promo_tag]").change(function(d) {
        if (d.target.value == "custom") {
            $(".custom_promo_tag_wrapper").show();
        } else {
            $(".custom_promo_tag_wrapper").hide();
            $("[name=custom_promo_tag]").val("")
            $("[name=custom_promo_tag_color]").val("")
        }
    })

    // var conf = {
    //     theme:  "monokai",
    //     mode:  "htmlmixed",
    //     htmlMode: true,
    //     keyMap: "sublime",
    //     indentUnit: 4,
    //     tabSize: 4,
    //     indentWithTabs: false,
    //     lineNumbers: true,
    //     autoCloseTags: true,
    //     lineWrapping: true,
    //     autofocus: true,
    //     extraKeys: {"Ctrl-Space": "autocomplete"},
    // }
    // CodeMirror.fromTextArea(document.getElementById(`description`),conf);
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<style>

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.css"
    integrity="sha512-uf06llspW44/LZpHzHT6qBOIVODjWtv4MxCricRxkzvopAlSWnTf6hpZTFxuuZcuNE9CBQhqE0Seu1CoRk84nQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/theme/monokai.min.css"
    integrity="sha512-R6PH4vSzF2Yxjdvb2p2FA06yWul+U0PDDav4b/od/oXf9Iw37zl10plvwOXelrjV2Ai7Eo3vyHeyFUjhXdBCVQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@endsection
