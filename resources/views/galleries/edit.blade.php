@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Galleries'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h5>
                Update Gallery
            </h5>
            <button type="button" class="btn bg-gradient-primary btn-save" style="min-width: 160px">Save</button>
        </div>
        <div class="card-body">
            <form action="/admin/galleries/{{$gallery->id}}/update" method="post" class="form">
                @csrf
                @method("PUT")
                <input type="hidden" class="feature_image" name="feature_image" value="{{ old('feature_image') }}">
                <input type="hidden" name="slug" value="{{ old('slug') }}">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" aria-describedby="title" name="title"
                                placeholder="Gallery title ..." value="{{ old('title') ?? $gallery->title }}">
                            <small id="slug" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="title">Content</label>
                            <textarea name="description" class="form-control summernote" id="description"
                                aria-describedby="description"
                                placeholder="Article content ...">{{ old('description') ?? $gallery->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="keywords">Keywords</label>
                            <textarea rows="3" class="form-control" id="keywords" aria-describedby="keywords"
                                name="keywords"
                                placeholder="Keywords ...">{{ old('keywords') ?? $gallery->keywords }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input data-role="tagsinput" name="tags" value="{{ old('tags') ?? $gallery->tags }}" />
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select type="text" class="form-control" id="status" aria-describedby="status"
                                name="status">
                                <option value="DRAFT" {{ $gallery->status == 'DRAFT' ? 'SELECTED' : null }}>Draft</option>
                                <option value="PUBLISHED" {{ $gallery->status == 'PUBLISHED' ? 'SELECTED' : null }}>Published</option>
                                <option value="UNPUBLISH" {{ $gallery->status == 'UNPUBLISH' ? 'SELECTED' : null }}>Unpublish</option>
                            </select>

                        </div>

                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="">Feature Image</label>
                            <div id="dropzone" class="dropzone-wrapper dropzone">
                                <div class="dz-message needsclick">
                                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                            class="path2"></span></i>
                                    <!--begin::Info-->
                                    <div class="ms-4">
                                        <h3 class="text-center fs-5 fw-bold text-gray-900 mb-1">Drop files here or click
                                            to upload.
                                        </h3>
                                    </div>
                                    <!--end::Info-->
                                </div>
                            </div>
                        </div>

                        <div id="uploaded">
                            @foreach ($gallery->items as $item)
                            <div class="form-group img-wrapper" data-index="">
                                <img src="{{ assetUrl($item->picture) }}" alt="" srcset="" class="img-responsive">
                                <input type="hidden" name="picture[]" value="{{ $item->picture }}" />
                                <input type="hidden" name="item_id[]" value="{{ $item->id }}" />
                                <label for="">Caption</label>
                                <input class="form-control" name="caption[]" value="{{ $item->description }}" />
                                <div class="d-flex justify-content-between">
                                    @if(!$item->is_cover)
                                    <a href="/galleries/{{$gallery->id}}/cover?item_id={{$item->id}}"
                                        class="btn bg-gradient-success btn-sm mt-2">Make Cover</a>
                                    @else
                                    <span style="height: 25px" class="badge bg-gradient-primary mt-2">Cover</span>
                                    @endif
                                    <a data-href="/galleries/{{$gallery->id}}/delete-item?item_id={{$item->id}}"
                                        onclick="btnDelete(this)"
                                        class="btn btn-delete bg-gradient-danger btn-sm mt-2">Delete</a>
                                </div>

                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</main>
<template id="template">
    <div class="form-group img-wrapper" data-index="">
        <img src="" alt="" srcset="" class="img-responsive">
        <input type="hidden" name="picture[]" value="" />
        <label for="">Caption</label>
        <input class="form-control" name="caption[]" value="" />
    </div>
</template>

@push('scripts')
<script>
    var date = new Date() / 1;
        var baseURL ="{{ url('/') }}"
            
            $(function() {
                $("#title").keyup(function() {
                    var title = $(this).val();
                    var slug = title.replaceAll(" ", "-").toLowerCase()+"-"+date;
                    $("[name=slug]").val(slug)
                    $("#slug").text(baseURL + "/gallery/" +slug)
                })
            })
</script>
<script src="/assets/js/bootstrap-tagsinput.min.js"></script>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
    Dropzone.options.fileupload = {
        acceptedFiles: "image/jpeg, image/png, image/jpg"
    }

        // The dropzone method is added to jQuery elements and can
        // be invoked with an (optional) configuration object.
        var myDropzone = $(".dropzone").dropzone({ 
            url: "/admin/file/upload" ,
            method: 'post',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            maxFiles: 20,
            maxFilesize: 2, // MB
            addRemoveLinks: true,
            // uploadMultiple: true,
            accept: function(file, done) {
                done()
            },
            // successmultiple: function(files, response) {
            //     console.log(files)
            //     // console.log(response)
            // },
            success: function(file, response) {
                // console.log(file)
                // console.log(response)
                var $template = $($("#template").html())
                $template.find("[name='picture[]']").val(response.file)
                $template.find("img").attr("src", response.url)

                $("#uploaded").prepend($template)
                
            },
        });
        
        // myDropzone.on("successmultiple", function(multiple) {
        //         alert("DONE")
        // });

        $(".btn-save").click(function() {
            $(".form").submit()
        })
    
        // $('.icon-wrapper').click(function(el) {
        //     $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
        //     $(".icon").val($(this).data('icon'))
        // })
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<style>
    .img-responsive {
        width: 100%;
        border-radius: 10px;
        margin-bottom: 10px;
    }

    .img-wrapper {
        padding: 10px;
        background-color: #fefefe;
        border: 1px solid #eee;
    }
</style>
@endpush
@endsection