@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Galleries'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h5>
                Create Gallery
            </h5>
            <button type="button" class="btn bg-gradient-primary" style="min-width: 160px">Save</button>
        </div>
        <div class="card-body">
            <form action="/admin/galleries" method="post" class="form">
                @csrf
                <input type="hidden" class="feature_image" name="feature_image" value="{{ old('feature_image') }}">
                <input type="hidden" name="slug" value="{{ old('slug') }}">
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" aria-describedby="title" name="title"
                                placeholder="Gallery title ..." value="{{ old('title') }}">
                            <small id="slug" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="title">Content</label>
                            <textarea name="description" class="form-control summernote" id="description"
                                aria-describedby="description"
                                placeholder="Article content ...">{{ old('description') }}</textarea>
                        </div>


                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="keywords">Keywords</label>
                            <textarea rows="3" class="form-control" id="keywords" aria-describedby="keywords"
                                name="keywords" placeholder="Keywords ...">{{ old('keywords') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input data-role="tagsinput" name="tags" value="{{ old('tags') }}" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</main>
@push('scripts')
<script>
    var date = new Date() / 1;
        var baseURL ="{{ url('/') }}"

            $(function() {
                $("#title").keyup(function() {
                    var title = $(this).val();
                    var slug = title.replaceAll(" ", "-").toLowerCase()+"-"+date;
                    $("[name=slug]").val(slug)
                    $("#slug").text(baseURL + "/gallery/" +slug)
                })
            })
</script>
<script src="/assets/js/bootstrap-tagsinput.min.js"></script>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
        // The dropzone method is added to jQuery elements and can
        // be invoked with an (optional) configuration object.
        $(".dropzone").dropzone({
            url: "/admin/file/upload" ,
            method: 'post',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            accept: function(file, done) {
                done()
            },
            success: function(file, response) {
                $(".feature_image").val(response.file)
            },

        });

        $("button").click(function() {
            $(".form").submit()
        })

        // $('.icon-wrapper').click(function(el) {
        //     $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
        //     $(".icon").val($(this).data('icon'))
        // })
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />

@endpush
@endsection
