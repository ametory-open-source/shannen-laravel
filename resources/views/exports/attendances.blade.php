<table >
    <tbody>
        <tr class="row0">
            <td class="column0" colspan="2">Perhitungan Jam Kerja</td>
            <td class="column2"></td>
            <td class="column3" colspan="2">Ringkasan</td>
            <td class="column5"></td>
            <td class="column6" colspan="2">Keterlambatan</td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row1">
            <td class="column0">Nama Karyawan</td>
            <td class="column1">{{ $data["name"] }}</td>
            <td class="column2"></td>
            <td class="column3">Hadir</td>
            <td class="column4">{{ $data["presences"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row2">
            <td class="column0">Bulan</td>
            <td class="column1">{{ $data["month"] }}</td>
            <td class="column2"></td>
            <td class="column3">Ijin</td>
            <td class="column4">{{ $data["permitts"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row3">
            <td class="column0">Periode</td>
            <td class="column1">{{ $data["periode"] }}</td>
            <td class="column2"></td>
            <td class="column3">Sakit</td>
            <td class="column4">{{ $data["sicks"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row4">
            <td class="column0">Jumlah Hari Kerja</td>
            <td class="column1">{{ $data["work_days_amount"] }}</td>
            <td class="column2"></td>
            <td class="column3">Dinas</td>
            <td class="column4">{{ $data["official_travel"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row5">
            <td class="column0">Jumlah Jam Kerja</td>
            <td class="column1">{{ $data["work_hours_amount"] }}</td>
            <td class="column2"></td>
            <td class="column3">Cuti</td>
            <td class="column4">{{ $data["leaves"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row6">
            <td class="column0"></td>
            <td class="column1"></td>
            <td class="column2"></td>
            <td class="column3">Remote</td>
            <td class="column4">{{ $data["remotes"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row7">
            <td class="column0"></td>
            <td class="column1"></td>
            <td class="column2"></td>
            <td class="column3">Alpha</td>
            <td class="column4">{{ $data["unauthorised_absence"] }}</td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row8">
            <td class="column0 style2"></td>
            <td class="column1"></td>
            <td class="column2"></td>
            <td class="column3"></td>
            <td class="column4"></td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row9">
            <td class="column0">Daily Activity</td>
            <td class="column1"></td>
            <td class="column2"></td>
            <td class="column3"></td>
            <td class="column4"></td>
            <td class="column5"></td>
            <td class="column6"></td>
            <td class="column7"></td>
            <td class="column8"></td>
            <td class="column9"></td>
        </tr>
        <tr class="row10">
            <td class="column0">No</td>
            <td class="column1">Tgl</td>
            <td class="column2">Hari</td>
            <td class="column3">Jam Masuk</td>
            <td class="column4">Jam Keluar</td>
            <td class="column5">Jumlah Jam Istirahat</td>
            <td class="column6">Jumlah Jam Kerja</td>
            <td class="column7">Kehadiran</td>
            <td class="column8">Keterlambatan</td>
            <td class="column9">Aktifitas</td>
        </tr>
        @foreach ($data["attendances"] as $key => $attendance)
            <tr class="row{{ $key+11 }}">
                <td class="column0">{{ $key + 1}}</td>
                <td class="column1">{{ $attendance->created_at->translatedFormat("d/M/Y") }}</td>
                <td class="column2">{{ $attendance->created_at->translatedFormat("l") }}</td>
                <td class="column3">{{ optional($attendance->clockin_time)->translatedFormat("H:i") }}</td>
                <td class="column4">{{ optional($attendance->clockout_time)->translatedFormat("H:i") }}</td>
                <td class="column5">{{ $attendance->work_hour }}</td>
                <td class="column6"></td>
                <td class="column7"></td>
                <td class="column8"></td>
                <td class="column9">
                    @foreach ($attendance->reports as $item)
                        <p>Judul : {{ $item->title}}</p>
                        <p>Ket : {{ $item->description}}</p>
                        <p>Mulai : {{ $item->start_time->format("H:i")}} </p>
                        @if($item->start_time != $item->end_time)
                        <p>Selesai : {{ $item->end_time->format("H:i")}} </p>
                        @endif
                        <p></p>
                        <p></p>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </tbody>
</table>