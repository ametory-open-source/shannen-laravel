@extends("layouts.app", ['class' => 'g-sidenav-show bg-gray-100'])

@php
$pictures = config("landing.pictures");
@endphp
@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Template'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Manage Template</h3>
        </div>
        <div class="card-body table-responsive" style="padding: 40px">
            @form_open([
            "class" => 'form',
            'enctype' => 'multipart/form-data'
            ])
            <div class="nav-wrapper position-relative end-0">
                <ul class="nav nav-pills nav-fill p-1" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1 active" data-bs-toggle="tab" href="#app-tabs-simple"
                            role="tab" aria-controls="app" aria-selected="true">
                            <i class="fas fa-rocket"></i> App
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#company-tabs-simple" role="tab"
                            aria-controls="company" aria-selected="false">
                            <i class="fas fa-building"></i> Company
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#template-tabs-simple" role="tab"
                            aria-controls="template" aria-selected="false">
                            <i class="fas fa-draw-polygon"></i> Template
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#picture-tabs-simple" role="tab"
                            aria-controls="picture" aria-selected="false">
                            <i class="fas fa-images"></i> Picture
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#module-tabs-simple" role="tab"
                            aria-controls="module" aria-selected="false">
                            <i class="fas fa-box-open"></i> Module
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="app-tabs-simple" role="tabpanel"
                        aria-labelledby="app-tabs-simple-tab">
                        {{-- input name --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="name">Name</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="" aria-describedby="name"
                                    placeholder="Name" value="{!! $web->name  ?? '' !!}">
                            </div>
                        </div>
                        {{-- end input name --}}


                        {{-- input short_description --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="short_description">Short Description</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="short_description" id=""
                                    aria-describedby="short_description" placeholder="Short Description"
                                    rows="2"
                                    maxlength="150"
                                    >{!! $web->short_description  ?? '' !!}</textarea>
                                <small>max: 150 characters</small>
                            </div>
                        </div>
                        {{-- end input short_description --}}
                        {{-- input description --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="description"> Description</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="description" rows="5" id=""
                                    aria-describedby="description"
                                    placeholder=" Description">{!! $web->description ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input description --}}
                        {{-- input meta --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="meta">Meta</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="meta" rows="5" id="meta"
                                    aria-describedby="meta" placeholder="Meta">{!! $web->meta ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input meta --}}
                        {{-- input styles --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="styles">Styles</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="styles" rows="5" id="styles"
                                    aria-describedby="styles"
                                    placeholder="Styles">{!! $web->styles ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input styles --}}
                        {{-- input scripts --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="scripts">Scripts</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="scripts" rows="5" id="scripts"
                                    aria-describedby="scripts"
                                    placeholder="Scripts">{!! $web->scripts  ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input scripts --}}
                    </div>
                    <div class="tab-pane fade" id="company-tabs-simple" role="tabpanel"
                        aria-labelledby="company-tabs-simple-tab">
                        <div class="form-group row d-flex">
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_name">Company Name</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_name" id=""
                                        aria-describedby="company_name" placeholder="Company Name"
                                        value="{!! $web->company_name  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_phone">Company Phone</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_phone" id=""
                                        aria-describedby="company_phone" placeholder="Company Phone"
                                        value="{!! $web->company_phone  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_email">Company Email</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_email" id=""
                                        aria-describedby="company_email" placeholder="Company Email"
                                        value="{!! $web->company_email  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_web">Company Web</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_web" id=""
                                        aria-describedby="company_web" placeholder="Company Web"
                                        value="{!! $web->company_web  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_coordinate">Company Location Coordinate</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_coordinate" id=""
                                        aria-describedby="company_coordinate"
                                        placeholder="Company Location Coordinate"
                                        value="{!! $web->company_coordinate  ?? '' !!}">

                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_map">Company Location Map</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea rows="9" type="text" class="form-control" name="company_map" id=""
                                        aria-describedby="company_map"
                                        placeholder="Company Location Map">{!! $web->company_map  ?? '' !!}</textarea>
                                    @if ($web->company_map)
                                    {!! $web->company_map !!}
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_address">Company Address</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control" name="company_address" rows="9" id=""
                                        aria-describedby="company_address"
                                        placeholder="Company Address">{!! $web->company_address  ?? '' !!}</textarea>
                                </div>
                            </div>




                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_bank_account_number">Company Bank</label>
                                </div>
                                <div class="col-sm-10">
                                    <input class="form-control" name="company_bank" rows="9" id=""
                                        aria-describedby="company_bank"
                                        placeholder="Company Bank" value="{!! $web->company_bank  ?? '' !!}"/>
                                </div>
                            </div>

                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_bank_account_number">Company Bank Account</label>
                                </div>
                                <div class="col-sm-10">
                                    <input class="form-control" name="company_bank_account_number" rows="9" id=""
                                        aria-describedby="company_bank_account_number"
                                        placeholder="Company Bank Account" value="{!! $web->company_bank_account_number  ?? '' !!}"/>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_bank_account_holder">Company Bank Account Holder</label>
                                </div>
                                <div class="col-sm-10">
                                    <input class="form-control" name="company_bank_account_holder" rows="9" id=""
                                        aria-describedby="company_bank_account_holder"
                                        placeholder="Company Bank Account Holder" value="{!! $web->company_bank_account_holder  ?? '' !!}"/>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="company_bank_account_branch">Company Bank Account Branch</label>
                                </div>
                                <div class="col-sm-10">
                                    <input class="form-control" name="company_bank_account_branch" rows="9" id=""
                                        aria-describedby="company_bank_account_branch"
                                        placeholder="Company Bank Account Branch" value="{!! $web->company_bank_account_branch  ?? '' !!}"/>
                                </div>
                            </div>
                            {{-- <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="affiliate_term">Affiliate Term & Condition</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control summernote" name="affiliate_term" rows="9"
                                        id="" aria-describedby="affiliate_term"
                                        placeholder="Affiliate Term & Condition">{!! $web->affiliate_term  ?? '' !!}</textarea>
                                </div>

                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="affiliate_enabled">Affiliate Enabled</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="col-md-3">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" name="affiliate_enabled" type="checkbox"
                                                role="switch" id="affiliate_enabled" {{ $web->affiliate_enabled ?
                                            'CHECKED' : null}}>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="member_enabled">Member Enabled</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="col-md-3">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" name="member_enabled" type="checkbox"
                                                role="switch" id="member_enabled" {{ $web->member_enabled ? 'CHECKED' :
                                            null}}>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="province_id">Province</label>
                                </div>
                                <div class="col-sm-10 province_id_wrapper">
                                    <span>Loading ...</span>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="city_id">City</label>
                                </div>
                                <div class="col-sm-10 city_id_wrapper">
                                    <span>Loading ...</span>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="subdistrict_id">Subdistrict</label>
                                </div>
                                <div class="col-sm-10 subdistrict_id_wrapper">
                                    <span>Loading ...</span>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="village_id">Village</label>
                                </div>
                                <div class="col-sm-10 village_id_wrapper">
                                    <span>Loading ...</span>
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="postal_code">Postal Code</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="company_postal_code" id=""
                                        aria-describedby="company_postal_code" placeholder="Postal Code"
                                        value="{!! $web->company_postal_code  ?? '' !!}">
                                </div>
                            </div>
                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="affiliate_term">Member Term & Condition</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control summernote" name="member_term" rows="9"
                                        id="" aria-describedby="member_term"
                                        placeholder="Member Term & Condition">{!! $web->member_term  ?? '' !!}</textarea>
                                </div>

                            </div>

                            <div class="form-group row d-flex">
                                <div class="col-sm-2">
                                    <label for="postal_code">Couriers
                                    </label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="row">
                                        @foreach (config('landing.couriers') as $item)
                                        <div class="col-md-3">
                                            @form_checkbox('couriers['.$item.']',true,
                                            isset($web->couriers[$item]) )
                                            {{strtoupper($item)}}
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="template-tabs-simple" role="tabpanel"
                        aria-labelledby="template-tabs-simple-tab">

                        @foreach (config("landing.template_inputs") as $item => $type)

                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="{{$item}}">{{ collect(explode("_", $item ))->map(function($d) { return
                                    ucfirst($d) ;})->join(" ") }}</label>
                            </div>
                            <div class="col-sm-10">
                                @if ($type == "textarea")
                                <textarea rows="5" class="form-control" name="{{$item}}" id="{{$item}}"
                                    aria-describedby="{{$item}}" placeholder="{{ collect(explode(" _", $item ))->map(function($d) { return ucfirst($d) ;})->join(" ") }}"
                                    >{!! $web->{$item}  ?? '' !!}</textarea>
                                @else
                                <input type="text" class="form-control" name="{{$item}}" id="{{$item}}"
                                    aria-describedby="{{$item}}" placeholder="{{ collect(explode(" _", $item
                                    ))->map(function($d) { return ucfirst($d) ;})->join(" ") }}"
                                value="{!! $web->{$item} ?? '' !!}">
                                @endif
                            </div>
                        </div>

                        @endforeach

                        {{-- end input offline_template --}}
                        {{-- input offline_template --}}
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="offline_template"> Offline Template</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control" name="offline_template" rows="5"
                                    id="offline_template" aria-describedby="offline_template"
                                    placeholder=" Offline Template">{!! $web->offline_template ?? '' !!}</textarea>
                            </div>
                        </div>
                        {{-- end input offline_template --}}
                        <div class="col-md-3 mt-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" name="is_online" type="checkbox" role="switch"
                                    id="is_online" {{ $web->is_online ? 'CHECKED' : null}}>
                                <label class="form-check-label" for="is_online">Online</label>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="picture-tabs-simple" role="tabpanel"
                        aria-labelledby="picture-tabs-simple-tab">
                        @foreach ($pictures as $pic)
                        <div class="form-group row d-flex">
                            <div class="col-sm-2">
                                <label for="{!! $pic !!}">{{ str_replace("_", " ", Str::ucfirst($pic)) }}</label>


                            </div>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" name="{!! $pic !!}" id=""
                                    aria-describedby="{!! $pic !!}" placeholder="{{ Str::ucfirst($pic) }}">
                                @if ($web[$pic])
                                <div class="img-preview-wrapper mt-2">
                                    <img src="/storage{{ $web[$pic] }}" alt="">
                                </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="tab-pane fade" id="module-tabs-simple" role="tabpanel"
                        aria-labelledby="module-tabs-simple-tab">
                        <div class="row">
                            @foreach (config("landing.show_modules") as $item)
                            <div class="col-md-3 mt-3">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" name="{{$item}}" type="checkbox" role="switch"
                                        id="{{$item}}" {{ $web->{$item} ? 'CHECKED' : null}}>
                                    <label class="form-check-label" for="$item">{{ collect(explode("_", $item
                                        ))->map(function($d) { return ucfirst($d) ;})->join(" ") }}</label>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>

                </div>
            </div>





            <button type="submit" class="btn bg-gradient-primary mt-5" style="min-width: 160px">Save</button>

            @form_close()
        </div>
    </div>
</div>
</main>
@push('css')
<style>
    .img-preview-wrapper {
        background-color: #f5f5f5;
        padding: 10px;
        margin: auto;
        text-align: center;
        border-radius: 10px;
    }

    .img-preview-wrapper img {
        max-height: 300px;
        min-height: 60px;
        max-width: 800px;
    }

    .tab-pane {
        padding: 40px 20px;
    }

    .nav-item .fas {
        color: #999;
        font-size: 10pt;
        margin-right: 10px;
    }
</style>
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.js"
    integrity="sha512-sSWQXoxIkE0G4/xqLngx5C53oOZCgFRxWE79CvMX2X0IKx14W3j9Dpz/2MpRh58xb2W/h+Y4WAHJQA0qMMuxJg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/keymap/sublime.min.js"
    integrity="sha512-SV3qeFFtzcmGtUQPLM7HLy/7GKJ/x3c2PdiF5GZQnbHzIlI2q7r77y0IgLLbBDeHiNfCSBYDQt898Xp0tcZOeA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/htmlmixed/htmlmixed.min.js"
    integrity="sha512-HN6cn6mIWeFJFwRN9yetDAMSh+AK9myHF1X9GlSlKmThaat65342Yw8wL7ITuaJnPioG0SYG09gy0qd5+s777w=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/xml/xml.min.js"
    integrity="sha512-LarNmzVokUmcA7aUDtqZ6oTS+YXmUKzpGdm8DxC46A6AHu+PQiYCUlwEGWidjVYMo/QXZMFMIadZtrkfApYp/g=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/css/css.min.js"
    integrity="sha512-rQImvJlBa8MV1Tl1SXR5zD2bWfmgCEIzTieFegGg89AAt7j/NBEe50M5CqYQJnRwtkjKMmuYgHBqtD1Ubbk5ww=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/javascript/javascript.min.js"
    integrity="sha512-Cbz+kvn+l5pi5HfXsEB/FYgZVKjGIhOgYNBwj4W2IHP2y8r3AdyDCQRnEUqIQ+6aJjygKPTyaNT2eIihaykJlw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js"
    integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/css-hint.min.js"
    integrity="sha512-1BD3lo262s+DtDFoeD8+ssEL1zVJU8SHWWMtUyxfLqMV/jQDEnyQlS/CL3mD1kkLSv2hhYq1sdcA/zAPDz4JVA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js"
    integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    var company_province_id = {{$web->company_province_id ?? 'null'}}
    var company_city_id = {{$web->company_city_id  ?? 'null'}}
    var company_subdistrict_id = {{$web->company_subdistrict_id  ?? 'null'}}
    var company_village_id = {{$web->company_village_id  ?? 'null'}}
    function getProvince() {
        $.get("/location/province", function(data, err) {
            $(".province_id_wrapper").html(data)
            if (company_province_id) {
                $('[name=province_id]').val(company_province_id)
                getCity(company_province_id)
            }
        })
    }
    function getCity(province_id) {

            $.get("/location/city?province_id="+province_id, function(data, err) {
                $(".city_id_wrapper").html(data)
                if (company_city_id) {
                    $('[name=city_id]').val(company_city_id)
                    getSubdistrict(company_city_id)
                }
            })

    }
    function getSubdistrict(city_id) {

        $.get("/location/subdistrict?city_id="+city_id, function(data, err) {
            $(".subdistrict_id_wrapper").html(data)
            if (company_subdistrict_id) {
                $('[name=subdistrict_id]').val(company_subdistrict_id)
                getVillage(company_subdistrict_id)
            }
        })

    }

    function getVillage(subdistrict_id) {
        $.get("/location/village?subdistrict_id="+subdistrict_id, function(data, err) {
            $(".village_id_wrapper").html(data)
            if (company_village_id) {
                $('[name=village_id]').val(company_village_id)
            }
        })
    }


    function getPostalCode(village_id)
    {
        $.get("/village/"+village_id, function(d) {
            $('[name=postal_code]').val(d.postal_code)
        });
    }

    window.onload = function() {
        var conf = {
            theme:  "monokai",
            mode:  "htmlmixed",
            htmlMode: true,
            keyMap: "sublime",
            indentUnit: 4,
            tabSize: 4,
            indentWithTabs: false,
            lineNumbers: true,
            autoCloseTags: true,
            lineWrapping: true,
            autofocus: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
        }
        CodeMirror.fromTextArea(document.getElementById(`meta`),conf);
        CodeMirror.fromTextArea(document.getElementById(`scripts`),conf);
        CodeMirror.fromTextArea(document.getElementById(`styles`),conf);
        $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");
            if (target === "#company-tabs-simple") {

                getProvince()
                $(document).on("change", "[name=province_id]", function() {
                    $('[name=city_id]').html('<span>Loading ...</span>')
                    $('[name=subdistrict_id]').html('<span>Loading ...</span>')
                    getCity($(this).val())
                })

                $(document).on("change", "[name=city_id]", function() {
                    $('[name=subdistrict_id]').html('<span>Loading ...</span>')
                    getSubdistrict($(this).val())
                });

                $(document).on("change", "[name=subdistrict_id]", function() {
                    $('[name=village_id]').html('<span>Loading ...</span>')
                    getVillage($(this).val())
                });

                $(document).on("change", "[name=village_id]", function() {
                    getPostalCode($(this).val())
                });


            }
            if (target === "#template-tabs-simple") {
                    CodeMirror.fromTextArea(document.getElementById(`header_menu`),conf);
                    CodeMirror.fromTextArea(document.getElementById(`side_menu`),conf);
                    // CodeMirror.fromTextArea(document.getElementById(`footer_left_link`),conf);
                    CodeMirror.fromTextArea(document.getElementById(`footer_center_link`),conf);
                    // CodeMirror.fromTextArea(document.getElementById(`footer_right_link`),conf);
                    CodeMirror.fromTextArea(document.getElementById(`offline_template`),conf);

            }
        });




    }



</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.css"
    integrity="sha512-uf06llspW44/LZpHzHT6qBOIVODjWtv4MxCricRxkzvopAlSWnTf6hpZTFxuuZcuNE9CBQhqE0Seu1CoRk84nQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/theme/monokai.min.css"
    integrity="sha512-R6PH4vSzF2Yxjdvb2p2FA06yWul+U0PDDav4b/od/oXf9Iw37zl10plvwOXelrjV2Ai7Eo3vyHeyFUjhXdBCVQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@endsection
