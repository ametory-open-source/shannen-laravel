@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Members'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">Edit Members</div>
        <div class="card-body  row">
            <div class="col-6">
                <form action="/admin/members/{!! $member->id !!}/update" method="post">
                    @csrf
                    @method("PUT")
                    <div class="form-group row d-flex">
                        <div class="col-sm-2">
                            <label for="name">Name</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" rows="5" id="name"
                                aria-describedby="name" placeholder="Name" value="{!! $member->user->name ?? '' !!}"
                                readonly />
                        </div>
                    </div>
                    <div class="form-group row d-flex">
                        <div class="col-sm-2">
                            <label for="email">Email</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="email" rows="5" id="name"
                                aria-describedby="email" placeholder="Name" value="{!! $member->user->email ?? '' !!}"
                                readonly />
                        </div>
                    </div>
                    <div class="form-group row d-flex">
                        <div class="col-sm-2">
                            <label for="email">Type</label>
                        </div>
                        <div class="col-sm-10">
                            @form_select("member_type_id", $member_types, $member->member_type_id, ["class" =>
                            "form-control"])
                        </div>
                    </div>
                    <div class="form-group row d-flex">
                        <div class="col-sm-2">
                            <label for="email">Status</label>
                        </div>
                        <div class="col-sm-10">
                            @form_select("status", [
                            'PENDING' => 'PENDING',
                            'ACTIVE' => 'ACTIVE',
                            'BLOCKED' => 'BLOCKED',
                            ], $member->status, ["class" => "form-control"])
                        </div>
                    </div>

                    <button type="submit" class="btn bg-gradient-primary mt-5" style="min-width: 160px">Save</button>
                </form>
            </div>
            <div class="col-6">
                <h5>Personal data</h5>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>Photo</label>
                    </div>
                    <div class="col-sm-10">
                        @if($member->user->avatar)
                        <img class="img-fluid"  src="{{ assetUrl($member->user->avatar) }}" alt="" srcset="">
                        @else
                        <img class="avatar-big mb-3 pointer img-fluid" src="/img/no-picture.jpeg" alt="" srcset="" onclick="clickAvatar()">

                        @endif
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>Address</label>
                    </div>
                    <div class="col-sm-10">
                        {{ $member->address }}
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>Province</label>
                    </div>
                    <div class="col-sm-10">
                        {{ optional($member->province)->name }}
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>City</label>
                    </div>
                    <div class="col-sm-10">
                        {{ optional($member->city)->name }}
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>Subdistrict</label>
                    </div>
                    <div class="col-sm-10">
                        {{ optional($member->subdistrict)->name }}
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>Village</label>
                    </div>
                    <div class="col-sm-10">
                        {{ optional($member->village)->name }}
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>Postal Code</label>
                    </div>
                    <div class="col-sm-10">
                        {{ $member->postal_code }}
                    </div>
                </div>
                <div class="form-group row d-flex">
                    <div class="col-sm-2">
                        <label>ID CARD</label>
                    </div>
                    <div class="col-sm-10">
                        <img src="{{ assetUrl($member->id_card) }}" alt="" srcset="">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</main>
@push('scripts')

@endpush
@endsection
