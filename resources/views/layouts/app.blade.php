<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <title>
        SHANNEN BEAUTY DASHBOARD
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="/assets/css/argon-dashboard.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/assets/css/all.min.css">
    @stack("css")
    @livewireStyles
    <style>
        .dataTables_info {
            font-size: 10px;
        }

        .dataTable,
        .dataTables_paginate {
            font-size: 12px;
            width: 100%;
            table-layout: fixed;
        }

        .dataTable .scroll {
            border: 0;
            border-collapse: collapse;
        }

        .dataTable .scroll tr {
            display: flex;
        }

        .dataTable td {
            flex: 1;
            border-bottom-width: 1px;
            border-bottom-color: #f5f5f5;
        }

        .dataTable tr:nth-child(even) {
            background-color: #f5f5f5;
        }

        .dataTable .scroll td {
            flex: 1;
        }

        .dataTable th {
            color: #8392ab !important;
        }
        .dataTable th.text-right {
            text-align: right !important;
        }

        .dataTable .scroll thead tr:after {
            overflow-y: scroll;
            visibility: hidden;
            height: 0;
        }

        .dataTable .scroll thead th {
            flex: 1;
            display: block;
        }

        .dataTable .scroll tbody {
            display: block;

            overflow-y: auto;
            height: calc(100vh - 300px);
        }


        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background: #f5f5f5;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            background: #f2f2f2;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background: #333;
        }

        table.dataTable tbody td {
            padding: 8px 10px;
            text-wrap: initial;
        }

        div.dt-buttons a {
            font-size: 10px;
            background: #f5f5f5;
            color: #8392ab !important;
            border-color: #8392ab !important;
        }

        div.dt-buttons a:hover:not(.disabled) {
            background: #aaa;
        }

        .dataTables_length {
            margin-right: 10px;
        }

        .dataTables_processing {
            background: rgba(0, 0, 0, 0.8);
            color: white !important;
            border-radius: 10px;
            padding: 10px;
        }

        .text-right {
            text-align: right;
        }

        .btn-icon {
            padding: 5px 10px;
        }

        .dropzone-wrapper {
            padding: 20px;
            background-color: #f5f5f5;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: 10px;
        }

        .icon-wrapper {
            cursor: pointer;
            border-radius: 10px;
            background-color: #f5f5f5;
            margin: 5px;
        }

        .icon-list {
            height: 300px;
            overflow-y: auto;
        }

        .icon-selected {
            display: block;
        }

        .img-preview {
            margin: 20px auto;
            border-radius: 20px;
            overflow: hidden;
            width: 120px;
            height: 120px;
            position: relative;
            display: block;
            z-index: 10;
            background-color: grey !important;
            background-size: cover !important;
            background-repeat: no-repeat !important;
        }

        .dropzone-wrapper {
            padding: 20px;
            background-color: #f5f5f5;
            border: 1px solid rgba(0, 0, 0, .1);
            border-radius: 10px;
        }

        .icon-wrapper {
            cursor: pointer;
            border-radius: 10px;
            background-color: #f5f5f5;
            margin: 5px;
        }

        .icon-list {
            height: 300px;
            overflow-y: auto;
        }

        .icon-selected {
            display: block;
        }
        .CodeMirror {
            font-size: 12px;
        }

        .navbar-vertical .navbar-nav .nav-item .collapse .nav .nav-item .nav-link, .navbar-vertical .navbar-nav .nav-item .collapsing .nav .nav-item .nav-link {
            position: relative;
            background-color: transparent;
            box-shadow: none;
            color: rgba(33, 37, 41, 0.5);
            margin-left: initial;
        }
        .count-notif {
            font-size: 7pt;
            color: white;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            background-color: #f54451;
            text-align: center;
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .address_label {
            margin-bottom: 5px;
            text-indent: -16px;
            padding-left: 0px;
            margin-left: 32px;
        }
        .address_name {
            font-size: 16pt;
            text-indent: -32px;
            color: #333;
        }
        .address_label .fas {
            margin-right: 10px;
        }

        .group-slug span, .group-slug input{
            border: none;
            padding-right: 0;
        }
        .group-slug input:focus {
            box-shadow: none;
        }
        img.rounded-circle {
            width: 180px;
            height: 180px;
            border-radius: 50%;
            object-fit: cover;
        }
        .pointer {
            cursor: pointer;
        }
        .min-height-vh-60 {
            height: 80vh !important;
        }
        label.required:after {
            content:" *";
            color: #ff6565
        }

    </style>

</head>

<body class="{{ $class ?? '' }}">

    @guest
    @yield('content')
    @endguest

    @auth
    @if (in_array(request()->route()->getName(), ['sign-in-static', 'sign-up-static', 'login', 'register',
    'recover-password', 'rtl', 'virtual-reality']))
    @yield('content')
    @else
    @if (!in_array(request()->route()->getName(), ['profile', 'profile-static']))
    <div class="min-height-300 bg-primary position-absolute w-100"></div>
    @elseif (in_array(request()->route()->getName(), ['profile-static', 'profile']))
    <div class="position-absolute w-100 min-height-300 top-0"
        style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/profile-layout-header.jpg'); background-position-y: 50%;">
        <span class="mask bg-primary opacity-6"></span>
    </div>
    @endif
    @include('layouts.navbars.auth.sidenav')
    <main class="main-content border-radius-lg">
        @include('flash-message')
        @yield('content')
    </main>
    {{-- @include('components.fixed-plugin') --}}
    @endif
    @endauth

    <!--   Core JS Files   -->
    <script src="/assets/js/core/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"
        integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>
    <script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="/assets/js/argon-dashboard.js"></script>
    @stack('js');
    @stack('scripts');
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <script>
        $('.summernote').summernote({
          placeholder: '....',
          tabsize: 2,
          height: 400,
          toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['codeview', 'help']]
          ]
        });
        function btnDelete(event) {
            var msg = $(event).data("message")
            var conf = confirm(msg || "Are you sure?")
                if (!conf) {
                    return false;
                }
            window.location.href = $(event).data("href")
        }


        $('.close-alert').click(function() {
            $('.alert-wrapper').remove()
        })


    </script>
    @livewireScripts
    <script>
        setInterval(() => {
            Livewire.emitTo('admin-message-count', 'refreshCount')
            Livewire.emitTo('admin-message-rooms', 'refreshRooms')
            Livewire.emitTo('admin-message-box', 'refreshBox')
        }, 10000);
    </script>
</body>

</html>
