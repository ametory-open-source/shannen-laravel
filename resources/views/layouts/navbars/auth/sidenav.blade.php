<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 "
    id="sidenav-main">

    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0" href="#" target="_blank">
            <img src="/web/assets/images/logo_shannen.png" class="navbar-brand-img h-100" alt="main_logo">
            <span class="ms-1 font-weight-bold">{{ env("APP_NAME" )}} Dashboard</span>
        </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
        <ul class="navbar-nav">

            <li class="nav-item">
                <a class="nav-link" target="_blank"
                    href="/">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-world-2 text-primary text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Lihat Website</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'home' ? 'active' : '' }}"
                    href="{{ route('home') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-tv-2 text-primary text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Dashboard</span>
                </a>
            </li>
            @php
            $isWebExpanded = false;
            if (
            Route::currentRouteName() == 'template' ||
            Route::currentRouteName() == 'modules.index' || Route::currentRouteName() == 'modules.create' ||
            Route::currentRouteName() == 'modules.edit' ||
            Route::currentRouteName() == 'blogs.index' || Route::currentRouteName() == 'blogs.create' ||
            Route::currentRouteName() == 'blogs.edit' ||
            Route::currentRouteName() == 'blog_categories.index' || Route::currentRouteName() ==
            'blog_categories.create' || Route::currentRouteName() == 'blog_categories.edit' ||
            Route::currentRouteName() == 'pages.index' || Route::currentRouteName() == 'pages.create' ||
            Route::currentRouteName() == 'pages.edit' ||
            Route::currentRouteName() == 'comments.index' || Route::currentRouteName() == 'comments.create' ||
            Route::currentRouteName() == 'comments.edit' ||
            Route::currentRouteName() == 'galleries.index' || Route::currentRouteName() == 'galleries.create' ||
            Route::currentRouteName() == 'galleries.edit' ||
            Route::currentRouteName() == 'statics.index' || Route::currentRouteName() == 'statics.create' ||
            Route::currentRouteName() == 'statics.edit' ||
            Route::currentRouteName() == 'contact_us.index'

            ) {
            $isWebExpanded = true;
            }
            @endphp
            <li class="nav-item">
                <a data-bs-toggle="collapse" href="#webWrapper"
                    class="nav-link {{ !$isWebExpanded ? null : 'collapsed'}}" aria-controls="webWrapper" role="button"
                    aria-expanded="false">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="ni ni-ungroup text-warning text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Web</span>
                </a>
                <div class="collapse {{ $isWebExpanded ? 'show' : null}}" id="webWrapper" style="">
                    <ul class="nav ms-4">
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'template' ? 'active' : '' }}"
                                href="{{ route('template') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="ni ni-ruler-pencil text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Template</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'modules.index' || Route::currentRouteName() == 'modules.create' || Route::currentRouteName() == 'modules.edit'  ? 'active' : '' }}"
                                href="{{ route('modules.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="ni ni-folder-17 text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Module</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'blogs.index' || Route::currentRouteName() == 'blogs.create' || Route::currentRouteName() == 'blogs.edit' ? 'active' : '' }}"
                                href="{{ route('blogs.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="ni ni-books text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Blog</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'blog_categories.index' || Route::currentRouteName() == 'blog_categories.create' || Route::currentRouteName() == 'blog_categories.edit' ? 'active' : '' }}"
                                href="{{ route('blog_categories.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="ni ni-bullet-list-67 text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Category</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'pages.index' || Route::currentRouteName() == 'pages.create' || Route::currentRouteName() == 'pages.edit' ? 'active' : '' }}"
                                href="{{ route('pages.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="ni ni-collection text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Page</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'comments.index' || Route::currentRouteName() == 'comments.create' || Route::currentRouteName() == 'comments.edit' ? 'active' : '' }}"
                                href="{{ route('comments.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-comments text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Comment</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'galleries.index' || Route::currentRouteName() == 'galleries.create' || Route::currentRouteName() == 'galleries.edit' ? 'active' : '' }}"
                                href="{{ route('galleries.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-images text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Gallery</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'statics.index' || Route::currentRouteName() == 'statics.create' || Route::currentRouteName() == 'statics.edit' ? 'active' : '' }}"
                                href="{{ route('statics.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fab fa-angular text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Static Feature</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'contact_us.index' ? 'active' : '' }}"
                                href="{{ route('contact_us.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-address-book text-primary text-sm opacity-10"></i>
                                </div>
                                <div class="d-flex justify-content-between" style="width:100%">
                                    <span class="nav-link-text ms-1">Contact Us</span>
                                    @if($contact_us_unread)
                                    <span class="count-notif">{{ $contact_us_unread }}</span>
                                    @endif
                                </div>

                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @php
            $isProductExpanded = false;
            if (
            Route::currentRouteName() == 'product_categories.index' || Route::currentRouteName() ==
            'product_categories.create' || Route::currentRouteName() == 'product_categories.edit' ||
            Route::currentRouteName() == 'products.index' || Route::currentRouteName() == 'products.create' ||
            Route::currentRouteName() == 'products.edit'
            ) {
            $isProductExpanded = true;
            }
            @endphp
            <li class="nav-item">
                <a data-bs-toggle="collapse" href="#productWrapper"
                    class="nav-link {{ !$isProductExpanded ? null : 'collapsed'}}" aria-controls="productWrapper"
                    role="button" aria-expanded="false">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-shopping-cart text-warning text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Product</span>
                </a>
                <div class="collapse {{ $isProductExpanded ? 'show' : null}}" id="productWrapper" style="">
                    <ul class="nav ms-4">
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'product_categories.index' || Route::currentRouteName() == 'product_categories.create' || Route::currentRouteName() == 'product_categories.edit' ? 'active' : '' }}"
                                href="{{ route('product_categories.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-list text-primary text-sm opacity-10"></i>

                                </div>
                                <span class="nav-link-text ms-1">Product Category</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'products.index' || Route::currentRouteName() == 'products.create' || Route::currentRouteName() == 'products.edit' ? 'active' : '' }}"
                                href="{{ route('products.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-box-open text-primary text-sm opacity-10"></i>

                                </div>
                                <span class="nav-link-text ms-1">Product</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @php
            $isMemberExpanded = false;
            if (
            Route::currentRouteName() == 'members.index' || Route::currentRouteName() == 'members.create' ||
            Route::currentRouteName() == 'members.edit' ||
            $member_pending

            ) {
            $isMemberExpanded = true;
            }
            @endphp
            <li class="nav-item">
                <a data-bs-toggle="collapse" href="#memberWrapper"
                    class="nav-link {{ !$isMemberExpanded  ? null : 'collapsed'}}" aria-controls="memberWrapper"
                    role="button" aria-expanded="false">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-link text-warning text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Member</span>
                </a>
                <div class="collapse {{ $isMemberExpanded ? 'show' : null}}" id="memberWrapper" style="">
                    <ul class="nav ms-4">
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'members.index' || Route::currentRouteName() == 'members.create' || Route::currentRouteName() == 'members.edit' ? 'active' : '' }}"
                                href="{{ route('members.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-users text-primary text-sm opacity-10"></i>
                                </div>
                                <div class="d-flex justify-content-between" style="width:100%">
                                    <span class="nav-link-text ms-1">Member</span>
                                    @if($member_pending)
                                    <span class="count-notif">{{ $member_pending }}</span>
                                    @endif

                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            {{-- @php
            $isAffiliateExpanded = false;
            if (
            Route::currentRouteName() == 'affiliates.index' || Route::currentRouteName() == 'affiliates.create' ||
            Route::currentRouteName() == 'affiliates.edit'
            ) {
            $isAffiliateExpanded = true;
            }
            @endphp
            <li class="nav-item">
                <a data-bs-toggle="collapse" href="#affiliateWrapper"
                    class="nav-link {{ !$isAffiliateExpanded ? null : 'collapsed'}}" aria-controls="affiliateWrapper"
                    role="button" aria-expanded="false">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-link text-warning text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Affiliate</span>
                </a>
                <div class="collapse {{ $isAffiliateExpanded ? 'show' : null}}" id="affiliateWrapper" style="">
                    <ul class="nav ms-4">
                        <li class="nav-item">
                            <a class="nav-link {{ Route::currentRouteName() == 'affiliates.index' || Route::currentRouteName() == 'affiliates.create' || Route::currentRouteName() == 'affiliates.edit' ? 'active' : '' }}"
                                href="{{ route('affiliates.index') }}">
                                <div
                                    class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                    <i class="fas fa-user text-primary text-sm opacity-10"></i>
                                </div>
                                <span class="nav-link-text ms-1">Affiliator</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'orders.index' || Route::currentRouteName() == 'orders.create' || Route::currentRouteName() == 'orders.show' ? 'active' : '' }}"
                    href="{{ route('orders.index') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-shipping-fast text-primary text-sm opacity-10"></i>
                    </div>
                    <div class="d-flex justify-content-between" style="width:100%">
                        <span class="nav-link-text ms-1">Orders</span>
                        @if($order_count_pending)
                        <span class="count-notif">{{ $order_count_pending }}</span>
                        @endif
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'users.index' || Route::currentRouteName() == 'users.create' || Route::currentRouteName() == 'users.edit' ? 'active' : '' }}"
                    href="{{ route('users.index') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-users text-primary text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Users</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'message_rooms.index' ? 'active' : '' }}"
                    href="{{ route('message_rooms.index') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-comments text-primary text-sm opacity-10"></i>
                    </div>
                    <div class="d-flex justify-content-between" style="width:100%">
                        <span class="nav-link-text ms-1">Message</span>
                       <livewire:admin-message-count>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::currentRouteName() == 'admin.profile' ? 'active' : '' }}"
                    href="{{ route('admin.profile') }}">
                    <div
                        class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="fas fa-user text-primary text-sm opacity-10"></i>
                    </div>
                    <span class="nav-link-text ms-1">Profile</span>
                </a>
            </li>

        </ul>
    </div>

</aside>
