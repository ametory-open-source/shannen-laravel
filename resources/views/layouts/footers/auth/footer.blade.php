<footer class="footer pt-3  ">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                    © 2023 made with <i class="fa fa-heart"></i> by
                    <a href="https://biskod.com" class="font-weight-bold" target="_blank">BISKOD</a>
                    for a better web.
                </div>
            </div>

        </div>
    </div>
</footer>
