@if($web->is_online || auth()->user())
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{!! $web->name !!}</title>
    <meta name="description" content="{!! $web->description !!}">
    <meta name="title" content="{!! $web->name !!}">
    <meta name="keywords" content="{!! $web->keywords !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{!! $web->name !!}" />
    <meta property="og:description" content="{!! $web->description !!}" />
    <meta property="og:site_name" content="{!! $web->short_description !!}">
    <meta property="og:url" content="{{ url("/") }}">
    @if($web->image)
    <meta property="og:image" content="{!! $web->image !!}" />
    @endif
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- fontawesome css link -->
    <link rel="stylesheet" href="/web/assets/css/fontawesome-all.min.css">
    <!-- bootstrap css link -->
    <link rel="stylesheet" href="/web/assets/css/bootstrap.min.css">
    <!-- favicon -->
    <link rel="shortcut icon" href="/web/assets/images/fav.png" type="image/x-icon">
    <!-- swipper css link -->
    <link rel="stylesheet" href="/web/assets/css/swiper.min.css">
    <!-- lightcase css links -->
    <link rel="stylesheet" href="/web/assets/css/lightcase.css">
    <!-- odometer css link -->
    <link rel="stylesheet" href="/web/assets/css/odometer.css">
    <!-- line-awesome-icon css -->
    <link rel="stylesheet" href="/web/assets/css/icomoon.css">
    <!-- line-awesome-icon css -->
    <link rel="stylesheet" href="/web/assets/css/line-awesome.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="/web/assets/css/animate.css">
    <!-- aos.css -->
    <link rel="stylesheet" href="/web/assets/css/aos.css">
    <!-- nice select css -->
    <link rel="stylesheet" href="/web/assets/css/nice-select.css">
    <!-- main style css link -->
    <link rel="stylesheet" href="/web/assets/css/style.css">
    @stack("css")
</head>
<body>



<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Preloader
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<div class="preloader">
    <div class="drawing" id="loading">
        <div class="loading-dot"></div>
    </div>
</div>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Preloader
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


<div class="cursor"></div>
<div class="cursor-follower"></div>


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Header
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<header class="header-section three">
    <div class="header">
        <div class="header-bottom-area">
            <div class="container custom-container">
                <div class="header-menu-content">
                    <nav class="navbar navbar-expand-xl p-0">
                        <a class="site-logo site-title" href="/"><img src="{{ assetUrl($web->logo) }}" alt="site-logo" width="100"></a>
                        <button class="navbar-toggler d-block d-xl-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggle-bar"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                            <ul class="navbar-nav main-menu">
                                {!! $web->header_menu !!}
                            </ul>
                            <div class="header-right">
                                <div class="header-action-area">
                                    <div class="header-action">
                                        <a href="contact.html" class="btn--base">GET STARTED</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Header
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


@yield("content")


<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Start Footer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<footer class="footer-section pt-120">
    <div class="footer-element-one">
        <img src="/web/assets/images/element/element-48.png" alt="element">
    </div>
    <div class="footer-element-two">
        <img src="/web/assets/images/element/element-39.png" alt="element">
    </div>
    <div class="footer-element-three">
        <img src="/web/assets/images/element/element-40.png" alt="element">
    </div>
    <div class="footer-element-four">
        <img src="/web/assets/images/element/element-7.png" alt="element">
    </div>
    <div class="footer-element-five">
        <img src="/web/assets/images/element/element-41.png" alt="element">
    </div>
    <div class="footer-element-six">
        <img src="/web/assets/images/element/element-42.png" alt="element">
    </div>
    <div class="footer-element-seven">
        <img src="/web/assets/images/element/element-39.png" alt="element">
    </div>
    <div class="container">
        <div class="row mb-30-none">
            <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                <div class="footer-widget">
                    <div class="footer-logo">
                        <a class="site-logo site-title" href="/" ><img src="{{ assetUrl($web->logo) }}" alt="site-logo" width="100"></a>
                    </div>
                    <p>{{ $web->short_description }}</p>
                    <ul class="footer-social">
                        <li><a href="{{ $web->facebook_link }}"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{ $web->instagram_link }}"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 mb-30">
                <div class="footer-widget">
                    <h5 class="title">About us</h5>
                    <ul class="footer-list">
                        {!! strip_tags($web->footer_company_link, "<li><a>") !!}
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 mb-30">
                <div class="footer-widget">
                    <h4 class="title">Explore</h4>
                    <ul class="footer-list">
                        {!! strip_tags($web->footer_services_link, "<li><a>")  !!}
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 mb-30">
                <div class="footer-widget">

                </div>
            </div>
        </div>
    </div>
    <div class="copyright-wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-12 text-center">
                    <div class="copyright-area">
                        <p>Copyright © 2022 <a href="/">Softim</a>. All Rights Reserved. Designed by ThemeIM</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    End Footer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->




<!-- jquery -->
<script src="/web/assets/js/jquery-3.6.0.min.js"></script>
<!-- bootstrap js -->
<script src="/web/assets/js/bootstrap.min.js"></script>
<!-- swipper js -->
<script src="/web/assets/js/swiper.min.js"></script>
<!-- lightcase js-->
<script src="/web/assets/js/lightcase.js"></script>
<!-- odometer js -->
<script src="/web/assets/js/odometer.min.js"></script>
<!-- viewport js -->
<script src="/web/assets/js/viewport.jquery.js"></script>
<!-- aos js file -->
<script src="/web/assets/js/aos.js"></script>
<!-- nice select js -->
<script src="/web/assets/js/jquery.nice-select.js"></script>
<!-- isotope js -->
<script src="/web/assets/js/isotope.pkgd.min.js"></script>
<!-- tweenMax js -->
<script src="/web/assets/js/TweenMax.min.js"></script>
<!-- main -->
<script src="/web/assets/js/main.js"></script>
@stack("scripts")

</body>
</html>
@else
{!! $web->offline_template !!}
@endif
