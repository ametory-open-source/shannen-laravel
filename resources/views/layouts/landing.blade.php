@if($web->is_online || auth()->user())
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{!! $web->name !!}</title>
    <meta name="description" content="{!! $web->description !!}">
    <meta name="title" content="{!! $web->name !!}">
    <meta name="keywords" content="{!! $web->keywords !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{!! $web->name !!}" />
    <meta property="og:description" content="{!! $web->description !!}" />
    <meta property="og:site_name" content="{!! $web->short_description !!}">
    <meta property="og:url" content="{{ url("/") }}">
    @if($web->image)
    <meta property="og:image" content="{!! $web->image !!}" />
    @endif
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    <!-- Dependency Styles -->
    <link rel="stylesheet" href="/web/dependencies/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/fontawesome/css/all.min.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/swiper/css/swiper.min.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/wow/css/animate.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/simple-line-icons/css/simple-line-icons.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/themify-icons/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/components-elegant-icons/css/elegant-icons.min.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/magnific-popup/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="/web/dependencies/slick-carousel/css/slick.css" type="text/css">


    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="/web/assets/css/app.css" type="text/css">

    <!-- Google Web Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Barlow+Condensed:300,400,500,600,700,800%7CPoppins:300,400,500,600,700,800"
        rel="stylesheet">
    <style>
        .comment-avatar img {
            margin: 0;
        }

        .post-thumbnail .img-preview {
            -webkit-transition: all 0.4s cubic-bezier(0.72, 0.16, 0.345, 0.875) 0s;
            -o-transition: all 0.4s cubic-bezier(0.72, 0.16, 0.345, 0.875) 0s;
            transition: all 0.4s cubic-bezier(0.72, 0.16, 0.345, 0.875) 0s;
        }


    </style>
    @stack("css")
</head>

<body id="home-version-1" class="home-saas" data-style="default">

    <a href="#main_content" data-type="section-switch" class="return-to-top">
        <i class="fa fa-chevron-up"></i>
    </a>

    <div class="page-loader">
        <div class="page-loading-wrapper">
            <div class="loading loading07">
                @foreach (str_split($web->loading) as $item)
                <span data-text="{{$item}}">{{$item}}</span>
                @endforeach
            </div>
        </div>
    </div>


    <div id="main_content" class="main-content">
        @yield("content")
    </div>
    <!-- /#site -->

    <!-- Dependency Scripts -->
    <script src="/web/dependencies/jquery/jquery.min.js"></script>
    <script src="/web/dependencies/bootstrap/js/bootstrap.min.js"></script>
    <script src="/web/dependencies/swiper/js/swiper.min.js"></script>
    <script src="/web/dependencies/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="/web/dependencies/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="/web/dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <script src="/web/dependencies/jquery.appear/jquery.appear.js"></script>
    <script src="/web/dependencies/wow/js/wow.min.js"></script>
    <script src="/web/assets/js/TweenMax.min.js"></script>
    <script src="/web/dependencies/countUp.js/countUp.min.js"></script>
    <script src="/web/dependencies/bodymovin/lottie.min.js"></script>
    <script src="/web/dependencies/jquery.parallax-scroll/js/jquery.parallax-scroll.js"></script>
    <script src="/web/dependencies/wavify/wavify.js"></script>
    <script src="/web/dependencies/jquery.marquee/js/jquery.marquee.js"></script>
    <script src="/web/assets/js/jarallax.min.js"></script>
    <script src="/web/dependencies/gmap3/js/gmap3.min.js"></script>
    <script src="/web/dependencies/slick-carousel/js/slick.min.js"></script>
    <script src="/web/assets/js/jquery.parallax.min.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDk2HrmqE4sWSei0XdKGbOMOHN3Mm2Bf-M'></script>


    <!-- Site Scripts -->
    <script src="/web/assets/js/header.js"></script>
    <script src="/web/assets/js/app.js"></script>
    @stack("scripts")
</body>

</html>

@else
{!! $web->offline_template !!}
@endif
