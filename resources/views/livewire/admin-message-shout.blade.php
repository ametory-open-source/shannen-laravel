<div>
    <div class="d-flex">
        <div class="input-group">
            <input wire:keydown.enter="sendMessage('{{$room->id}}')" wire:model='message' type="text" class="form-control" placeholder="Type message here and press enter"
                aria-label="Message example input" onfocus="focused(this)"
                onfocusout="defocused(this)">
        </div>
        <button wire:click="sendMessage('{{$room->id}}')"  class="btn bg-gradient-primary mb-0 ms-2">
            <i class="ni ni-send"></i>
        </button>
    </div>
</div>
