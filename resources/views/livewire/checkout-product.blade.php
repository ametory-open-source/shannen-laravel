<div>
    <!--== Start Order Details Accordion ==-->
    <div class="checkout-order-details-wrap">
        @if($isLoading)
        <div class="loading">
            <h1>LOADING .... </h1>
        </div>
        @endif
        <div class="order-details-table-wrap table-responsive">

            <h2 class="title mb-25">{!! __('frontend.Your order') !!}</h2>

            <table class="table">
                <thead>
                    <tr>
                        <th class="product-name">{!! __('frontend.Product') !!}</th>
                        <th class="product-total">{!! __('frontend.Total') !!}</th>
                    </tr>
                </thead>
                <tbody class="table-body">
                    @foreach ($content as $id => $item)
                    <tr class="cart-item">
                        <td class="product-name">{{ $item->get('name') }} <p class="product-quantity"><i>{{
                                    money($item->get('price')) }} × {{number($item->get('quantity'))}}</i></p>
                        </td>
                        <td class="product-total">{{ money($item->get('price') * $item->get('quantity')) }}</td>
                    </tr>
                    @endforeach

                </tbody>
                <tfoot class="table-foot">
                    <tr class="cart-subtotal">
                        <th>{!! __('frontend.Subtotal') !!}</th>
                        <td>{{ money($total) }}</td>
                    </tr>
                    <tr class="shipping">
                        <th>{!! __('frontend.Shipping') !!}</th>
                        <td style="width: 300px">
                            <ul class="shipping-list" style="max-height: 300px;
                            overflow-y: auto;">
                            <li wire:key='SELF_PICKUP'
                            class="radio {{ $selectCost['id'] == 'SELF_PICKUP' ? 'active' : null}}">
                            <label for="radio1" wire:click="setShipping('SELF_PICKUP')">
                                <span style="font-weight: bold">
                                PICKUP SENDIRI:
                                </span>
                                <p style="margin-bottom:0"><span>{{ money(0) }}</span></p>
                                <p>Ambil Sendiri Pesanan anda</p>
                            </label>
                        </li>
                            @if($costs)

                            @if(count($costs))

                                @foreach ($costs as $cost)
                                <li wire:key='{{ $cost['id']}}'
                                    class="radio {{ $selectCost['id'] == $cost['id'] ? 'active' : null}}">
                                    <label for="radio1" wire:click="setShipping('{{ $cost['id']}}')">
                                        <span style="font-weight: bold">
                                        {{ $cost["name"]}}
                                        {{ $cost["service"]}} :
                                        </span>
                                        <p style="margin-bottom:0"><span>{{ money($cost["value"]) }}</span></p>
                                        <p> <small><i>etd: {{$cost["etd"]}}</i></small></p>

                                    </label>
                                </li>
                                @endforeach
                            @endif
                            @endif
                            </ul>
                            {{-- @if ($selectCost)
                            <div class="radio active">
                                {{ $selectCost['name'] }} {{ $selectCost['service'] }} : {{ money($selectCost['value'])
                                }}
                                <br> <small>etd: {{ $selectCost['etd'] }} day(s)</small>
                            </div>

                            @endif --}}

                        </td>


                    </tr>
                    @if($discount)
                    <tr class="order-total">
                        <th>Discount </th>
                        <td>{{ $discount }}%</td>
                    </tr>
                    @endif
                    <tr class="order-total">
                        <th>Total </th>
                        <td>{{ money($grandTotal) }}</td>
                    </tr>
                </tfoot>
            </table>
            <div class="col-md-12 billing-form-wrap">
                <form>

                    <div class="form-group mb-0">
                        <label for="order-notes">{!! __('frontend.Order notes (optional)') !!}</label>
                        <textarea wire:model='notes' id="order-notes" class="form-control"
                            placeholder="{{ __('frontend.notes_placeholder') }}"></textarea>
                    </div>
                </form>
            </div>
            <div class="shop-payment-method">
                <a wire:click='placeOrder' class="btn-place-order">{!! __('frontend.Place order') !!}</a>
            </div>
        </div>
    </div>
    <!--== End Order Details Accordion ==-->

</div>

@push('css')
<style>
    .radio {
        padding: 20px;
        border-radius: 10px;
        cursor: pointer;
    }

    .radio.active {
        background-color: rgb(215 250 215)
    }
</style>
@endpush
