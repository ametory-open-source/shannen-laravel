<div>
    <div class="card-header p-3">
        <h6>Friends</h6>
        <input type="email" wire:model='search' class="form-control" placeholder="Search Contact" aria-label="Email" onfocus="focused(this)"
            onfocusout="defocused(this)" >
    </div>
    <div class="card-body p-2">
        @foreach ($rooms as $room)
        @php
        $bg_color = "";
        $text_color = "text-muted";
        if ($room->admin_readed) {
        $bg_color = "border-radius-lg bg-gradient-primary";
        $text_color = "text-white";
        }
        @endphp
        <div wire:click='selectRoom("{{$room->id}}")' class="pointer d-block p-2 {{$bg_color}}">
            <div class="d-flex p-2">
                @if($room->user)
                @if ($room->user->avatar)
                <img alt="{{ $room->user->name }}" src="{{ assetUrl($room->user->avatar) }}" class="avatar shadow"
                    style="background: white">
                @else
                <img alt="{{ $room->user->name }}" src="/img/no-picture.jpeg" class="avatar shadow">
                @endif
                @endif
                @if($room->user)
                <div class="ms-3">
                    <div class="justify-content-between align-items-center">
                        <h6 class="{{ $text_color }} mb-0">{{ $room->user->name }}
                            @if($room->is_typing)
                            <span class="badge badge-success"></span>
                            @elseif($room->last_message_at)
                            <p class="{{ $text_color }} text-xs mb-2">{{ $room->last_message_at->diffForHumans() }}</p>
                            @else
                            @endif
                        </h6>

                        @if($room->is_typing)
                        <p class="{{ $text_color }} mb-0 text-sm">Typing...</p>
                        @else
                        <span class="{{ $text_color }} text-sm col-11 p-0 text-truncate d-block">{{ $room->last_message
                            }}</span>
                        @endif
                    </div>
                </div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
