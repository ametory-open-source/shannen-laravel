<div>
    <div class="myaccount-table table-responsive">
        <div class="chat-room">
            @if($room)
            @foreach ($room->messages as $chat)
            @if($chat->admin_id)
            <div class="row  justify-content-start mb-4">
                <div class="col-auto chat-bubble chat-admin">
                    {{ $chat->message }}
                    <div class="d-flex align-items-center text-sm opacity-6">
                        <i class="fas fa-check text-sm me-1 text-success"></i>
                        <small>{{ $chat->created_at->diffForHumans() }}</small>
                    </div>
                </div>
            </div>
            @endif
            @if($chat->user_id)
            <div class="row justify-content-end text-right mb-4">
                <div class="col-auto chat-bubble chat-user">
                    {{ $chat->message }}
                    <div class="d-flex align-items-center text-sm opacity-6">
                        <i class="fas fa-check text-sm me-1 text-success"></i>
                        <small>{{ $chat->created_at->diffForHumans() }}</small>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            @endif
        </div>
        @if($room)
        <livewire:user-message-shout key="{{ $room->id }}" :room='$room'>
        @endif
    </div>
</div>

@push('scripts')
    <script>
        let container = document.querySelector(".chat-room")
        $(function() {
            // container.scrollTop = container.scrollHeight
            container.scrollTo({ top: container.scrollHeight, behavior: "smooth" })

        })
        window.addEventListener('scrollDown', () => {
            setTimeout(() => {
                container.scrollTo({ top: container.scrollHeight, behavior: "smooth" })
            }, 500);

        })
    </script>
@endpush
