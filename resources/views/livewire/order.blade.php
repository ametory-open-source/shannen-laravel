<div>

    @if($orders->count() )
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>{!! __('frontend.Order') !!}</th>
                <th>{!! __('frontend.Date') !!}</th>
                <th>Kode</th>
                <th>Status</th>
                <th>Total</th>
                <th>{!! __('frontend.Action') !!}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $i => $item)
                <tr>
                    <td>{{  $orders->firstItem() + $i  }}</td>
                    <td>{{ $item->created_at->format("d M Y") }}</td>
                    <td>{{ $item->order_code }}</td>
                    <td class="text-left">
                        {{ __('frontend.'.$item->status) }}
                        @if($item->expired_date)
                        <p><small><i>Harap selesaikan pembayaran sebelum<br/>{{ $item->expired_date->format('d-M-Y H:m') }}</i></small></p>
                        @endif
                    </td>
                    <td>{{ money($item->grand_total) }}</td>
                    <td><a href="{{ route('affOrder.show', ['id' => $item->id])}}" class="check-btn sqr-btn ">{!! __('frontend.View') !!}</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $orders->links("vendor.livewire.bootstrap") }}
    @else
    <h6>No Orders Available</h6>
    @endif
</div>
