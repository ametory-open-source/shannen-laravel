<div>
    <x-modal>
        <x-slot name="title">
              Berhasil ditambahkan
        </x-slot>

        <x-slot name="content">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex  align-items-center">
                    <img src="{{ $product['feature_image'] }}" width="68" height="84" alt="{{ $product['name'] }}">
                    <h5 class="ms-2">{{ $product['name'] }}</h5>
                </div>
                <div>
                    <a href="/cart" class="red-button" style="text-align: center">Lihat Keranjang</a>
                </div>
            </div>
        </x-slot>

        <x-slot name="buttons">

        </x-slot>
    </x-modal>
</div>
