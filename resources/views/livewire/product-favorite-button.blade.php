
    <button wire:click="addToWishlist" type="button" class="product-action-btn action-btn-wishlist">
        @if ($isFavorited)
            <i class="fa-solid fa-heart" style="color: #ff6565"></i>
        @else
            <i class="fa-regular fa-heart"></i>
        @endif
    </button>

