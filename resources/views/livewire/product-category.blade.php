<div>
    <div class="product-widget">
        <h4 class="product-widget-title">{!! __('frontend.Categories') !!}</h4>
        <ul class="product-widget-category">
            @php
                $all = 0;
            @endphp
            @foreach ($categories as $item)
                @php
                    $all += $item->count;
                @endphp
                <li style="position: relative"><a wire:click="goto('{{$item->id}}')">{{$item->name}}
                    @if($item->count)
                    <span style="
                        position: absolute;
                        font-size: 7pt;
                        padding: 2px;
                        background-color: red;
                        border-radius: 50%;
                        color: white;
                        width: 20px;
                        height: 20px;
                        text-align: center;
                        right: -9px;
                    ">{{$item->count}}</span>
                    @endif

                </a></li>

            @endforeach
            <li style="position: relative"><a wire:click="allProduct">All Product<span style="
                position: absolute;
                font-size: 7pt;
                padding: 2px;
                background-color: red;
                border-radius: 50%;
                color: white;
                width: 20px;
                height: 20px;
                text-align: center;
                right: -9px;
            ">{{$all}}</span></a></li>
        </ul>
    </div>
</div>
