<div>
    @if ($content)
    <section class="section-space">
        <div class="container">
            <div class="shopping-cart-form table-responsive">
                <form action="#" method="post">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th class="product-remove">&nbsp;</th>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name">{{ __('frontend.Product') }}</th>
                                <th class="product-price">{{ __('frontend.Price') }}</th>
                                <th class="product-quantity">{{ __('frontend.Quantity') }}</th>
                                <th class="product-subtotal"{{ __('frontend.Total') }}></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($content) == 0)
                            <tr class="tbody-item">
                                <td class="product-remove" colspan="6">
                                    <small>Belum ada item pembelanjaan </small>
                                </td>
                            </tr>
                            @endif
                            @foreach ($content as $id => $item)
                            <tr class="tbody-item">
                                <td class="product-remove">
                                    <a class="remove" wire:click="removeFromCart('{{ $id }}')">×</a>
                                </td>
                                <td class="product-thumbnail">
                                    <div class="thumb">
                                        <a href="{{ $item->get('url') }}">
                                            <img src="{{ $item->get('feature_image') }}" width="68" height="84"
                                                alt="Image-HasTech">
                                        </a>
                                    </div>
                                </td>
                                <td class="product-name">
                                    <a class="title" href="">{{ $item->get('name') }}</a>
                                </td>
                                <td class="product-price">
                                    <span class="price">{{ money($item->get('price')) }}</span>
                                </td>
                                <td class="product-quantity">
                                    <div class="pro-qty">
                                        <input type="text" class="quantity" title="Quantity"
                                            wire:model="quantities.{{$id}}">
                                        <div class="dec qty-btn" wire:click="updateCartItem('{{ $id }}', 'minus')">-
                                        </div>
                                        <div class="inc qty-btn" wire:click="updateCartItem('{{ $id }}', 'plus')">+
                                        </div>
                                    </div>
                                </td>
                                <td class="product-subtotal">
                                    <span class="price">{{ money($item->get('quantity') * $item->get('price')) }}</span>
                                </td>
                            </tr>
                            @endforeach

                            {{-- <tr class="tbody-item-actions">
                                <td colspan="6">
                                    <button type="submit" class="btn-update-cart disabled" disabled>Update cart</button>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="row">
                <div class="col-12 col-lg-6">
                    {{-- <div class="coupon-wrap">
                        <h4 class="title">Coupon</h4>
                        <p class="desc">Enter your coupon code if you have one.</p>
                        <input type="text" class="form-control" placeholder="Coupon code">
                        <button type="button" class="btn-coupon">Apply coupon</button>
                    </div> --}}
                </div>
                <div class="col-12 col-lg-6">
                    <div class="cart-totals-wrap">
                        <h2 class="title">{{ __('frontend.Cart totals') }}</h2>
                        <table>
                            <tbody>
                                <tr class="cart-subtotal">
                                    <th>{{ __('frontend.Subtotal') }}</th>
                                    <td>
                                        <span class="amount">{{money( $total )}}</span>
                                    </td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>{{ __('frontend.Total Weight') }}</th>
                                    <td>
                                        <span class="amount">{{ countWeight($weight) }}</span>
                                    </td>
                                </tr>
                                {{-- <tr class="shipping-totals">
                                    <th>Shipping</th>
                                    <td>
                                        <livewire:shipping>
                                    </td>
                                </tr> --}}

                                <tr class="shipping-totals">
                                    <th>{{ __('frontend.Discount') }}</th>
                                    <td>
                                        {{ $discount }}%
                                    </td>
                                </tr>
                                @if($minimum_order - $total > 0)
                                <tr class="shipping-totals">
                                    <th></th>
                                    <td>
                                        <small><i>
                                            {{ __('frontend.order_remain', ["remain" => money( $minimum_order - $total ), "percent" => $active_discount])
                                            }}
                                        </i></small>
                                    </td>
                                </tr>
                                @endif
                                @if ($discount)
                                <tr class="shipping-totals">
                                    <th></th>
                                    <td>
                                        <small><i>
                                                {{ __('frontend.you saved')}} {{ money(($total * ($discount / 100)))
                                                }}
                                            </i></small>
                                    </td>
                                </tr>
                                @endif
                                <tr class="order-total">
                                    <th>{{ __('frontend.Total') }}</th>
                                    <td>
                                        <span class="amount">{{ money($grandTotal)
                                            }}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-end">
                            @if (auth(env('APP_AFFILIATE_GUARD'))->user())
                            @if($grandTotal == 0)
                            <p><small style="font-size:8pt">Silahkan tambahkan terlebih dahulu item pembelanjaan anda</small></p>
                            @elseif(auth(env('APP_AFFILIATE_GUARD'))->user()->addresses->count())
                            <a href="/checkout" class="checkout-button">{{ __('frontend.Proceed to checkout') }}</a>
                            @else
                            <a href="/profile?tab=address" class="checkout-button">{{ __('frontend.Proceed to checkout') }}</a>
                            <p><small style="font-size:8pt">Please ensure that at least one shipping address is saved in
                                    your account to proceed with your order</small></p>

                            @endif
                            @else
                            <a href="/login?redirect_to=/checkout" class="checkout-button">Login to checkout</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
</div>
