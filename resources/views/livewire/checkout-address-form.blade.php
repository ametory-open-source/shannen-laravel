<div class="row">

    <div class="col-md-12">
        <div class="form-group">
            <label for="province_name">{!! __('frontend.Province') !!}</label>
            <input id="province_name" type="text" class="form-control" value="{{ $province_name}}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="city_name">{!! __('frontend.City') !!}</label>
            <input id="city_name" type="text" class="form-control" value="{{ $city_name}}" readonly>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="subdistrict_name">{!! __('frontend.Subdistrict') !!}</label>
            <input id="subdistrict_name" type="text" class="form-control" value="{{ $subdistrict_name}}" readonly>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="village_name">{!! __('frontend.Village') !!}</label>
            <input id="village_name" type="text" class="form-control" value="{{ $village_name}}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="postal_code">{!! __('frontend.Postal Code') !!}</label>
            <input id="postal_code" type="text" class="form-control" value="{{ $postal_code}}" readonly>
        </div>
    </div>
</div>
