<div class="col-lg-6">
    <!--== Start Product Info Area ==-->
    <div class="product-details-content">
        <h5 class="product-details-collection">{{ $product->category->name}}</h5>
        <h3 class="product-details-title">{{ $product->name }}</h3>
        <div class="product-details-review mb-5">
            <div class="product-review-icon">
                {!! $product->rating_stars !!}
            </div>
            @if($product->review_count)
            <button type="button" class="product-review-show">{{ $product->review_count }} {!! __('frontend.reviews') !!}</button>
            @endif
        </div>
        <p class="mb-6">{!! $product->clean_description !!}</p>

        <div class="product-details-pro-qty">
            <div class="pro-qty">
                <input type="text" title="Quantity"  wire:model="quantity">
                <div wire:click="dec" class="dec qty-btn">-</div>
                <div wire:click="inc" class="inc qty-btn">+</div>
            </div>
        </div>
        <div class="product-details-action">
            <h4 class="price">{{ $product->price_format }}</h4>
            <div class="product-details-cart-wishlist">
                <button wire:click="modalAddToCart" type="button" class="btn" data-bs-toggle="modal" data-bs-target="#action-CartAddModal">{!! __('frontend.Add to cart') !!}</button>
            </div>
        </div>
    </div>
    <!--== End Product Info Area ==-->
</div>
