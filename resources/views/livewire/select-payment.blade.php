<div>
    <div class="shop-payment-method">
        <div id="PaymentMethodAccordion">
            <div class="card" wire:ignore>
                <div class="card-header" id="check_payments" >
                    <h5 class="title" data-bs-toggle="collapse" data-bs-target="#itemOne" wire:click="selectPayment('BANK_TRANSFER')"
                        aria-controls="itemOne" aria-expanded="true">{{ __('frontend.Direct bank transfer')}}</h5>
                </div>
                <div id="itemOne" class="collapse show" aria-labelledby="check_payments"
                    data-bs-parent="#PaymentMethodAccordion">
                    <div class="card-body bank_account_wrapper">
                        <p class="bank_account bank_account_header">{{ $web->company_bank }} {{ $web->company_bank_account_branch }}</p>
                        <p class="bank_account">{{ $web->company_bank_account_holder }}</p>
                        <p class="bank_account">{{ $web->company_bank_account_number }}</p>
                    </div>
                </div>
            </div>

            {{-- <div class="card" wire:ignore>
                <div class="card-header" id="check_payments4">
                    <h5 class="title" data-bs-toggle="collapse" data-bs-target="#itemFour" wire:click="selectPayment('PAYMENT_GATEWAY')"
                        aria-controls="itemTwo" aria-expanded="false">Payment Gateway</h5>
                </div>
                <div id="itemFour" class="collapse" aria-labelledby="check_payments4"
                    data-bs-parent="#PaymentMethodAccordion">
                    <div class="card-body">
                        <p class="p-text">Dengan opsi ini anda dapat membayar dengan berbagai pilihan pembayaran seperti wallet, credit card dan convenience store</p>
                    </div>
                </div>
            </div> --}}
        </div>

        @if($method == "PAYMENT_GATEWAY")

            <a target="_blank" href="/orders/{{$order->id}}/payment" class="btn-place-order">Bayar Sekarang</a>
        @else
            <a  href="javascript:void(0)" class="btn-place-order upload-payment-receipt">Upload Bukti Transfer</a>
            <form id="upload-receipt" action="" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" id="" style="visibility:hidden" accept="image/*">
            </form>
        @endif



    </div>
</div>

@push('scripts')
    <script>
        $('.upload-payment-receipt').on('click', function() {
            $("[name=file]").trigger('click');

        })
        $("[name=file]").change(function() {
            $("#upload-receipt").submit()
        });

    </script>
@endpush
