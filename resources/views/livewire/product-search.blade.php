<div>
    <input wire:model="search" type="search" placeholder="{!! __('frontend.Search Here') !!}">
    <button type="submit"><i class="fa fa-search"></i></button>
</div>
