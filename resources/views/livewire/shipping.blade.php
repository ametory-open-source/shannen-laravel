<div>

    @if(count($costs))
        <ul class="shipping-list">
            @foreach ($costs as $cost)
                <li wire:key='{{ $cost['id']}}' class="radio" >

                    <i class="fa fa-check"
                        @if($selectedCost["id"] == $cost["id"])
                            style="color: green"
                        @else
                            style="color: #ccc"
                        @endif
                    ></i>
                    <label for="radio1" wire:click="setShipping('{{ $cost['id']}}')" >{{ $cost["name"]}} {{ $cost["service"]}}: <span>{{ money($cost["value"]) }}</span>
                    <small><i>etd: {{$cost["etd"]}}</i></small>
                    </label>
                </li>
            @endforeach

        </ul>
    @endif
</div>
