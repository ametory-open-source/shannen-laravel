<div>
    <div class="loading" wire:loading >
        <h1>LOADING .... </h1>
     </div>
    <!--== Start Billing Accordion ==-->
    <div class="checkout-billing-details-wrap">
        <h2 class="title">{!! __('frontend.Billing details') !!}</h2>
        <div class="billing-form-wrap">

            <form action="#" method="post">

                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="form-group">
                            <label for="country">{!! __('frontend.Label') !!} <abbr class="required"
                                    title="required">*</abbr></label>
                            <select wire:change='addressChange' wire:model='address_id' id="country" class="form-control wide">
                                @foreach ($addresses as $item)
                                <option  value="{{ $item->id}}">{{ $item->label}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @if ($isLoading)
                    <h6>Loading ...</h6>
                @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="recipient_name">{!! __('frontend.Full Name') !!}</label>
                            <input id="recipient_name" type="text" class="form-control" value="{{ optional($address)->recipient_name}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">{!! __('frontend.Phone') !!}</label>
                            <input id="phone" type="text" class="form-control" value="{{ optional($address)->phone_number}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">{!! __('frontend.Email') !!}</label>
                            <input id="email" type="text" class="form-control" value="{{ optional($address)->email}}" readonly>
                        </div>
                    </div>


                </div>
                <livewire:checkout-address-form  :address='$address'>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address">{!! __('frontend.Address') !!}</label>
                            <textarea id="address" class="form-control"  readonly>{{ optional($address)->address}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">

                    {{-- <div id="CheckoutBillingAccordion2" class="col-md-12">
                        <div class="checkout-box" >
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input visually-hidden"
                                    id="ship-to-different-address" wire:model="shipping_is_different_address">
                                <label class="custom-control-label" for="ship-to-different-address">{!! __('frontend.Ship to a different address') !!}? {{ $shipping_is_different_address }}</label>
                            </div>
                        </div>
                        <div id="CheckoutTwo" class="collapse {{ $shipping_is_different_address ? 'show' : null }}" data-bs-parent="#CheckoutBillingAccordion2">
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    <div class="form-group">
                                        <label for="country">{!! __('frontend.Label') !!} <abbr class="required"
                                                title="required">*</abbr></label>
                                        <select wire:change='shippingAddressChange' wire:model='shipping_address_id' id="country" class="form-control wide">
                                            @foreach ($addresses as $item)
                                            <option  value="{{ $item->id}}">{{ $item->label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div> --}}

                    @if($shipping_address)
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="shipping_recipient_name">{!! __('frontend.Shipping Name') !!}</label>
                            <input id="shipping_recipient_name" type="text" class="form-control" value="{{ $shipping_address->recipient_name}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="shipping_phone">{!! __('frontend.Shipping Phone') !!}</label>
                            <input id="shipping_phone" type="text" class="form-control" value="{{ $shipping_address->phone_number}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="shipping_email">{!! __('frontend.Shipping Email') !!}</label>
                            <input id="shipping_email" type="text" class="form-control" value="{{ $shipping_address->email}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="shipping_address">{!! __('frontend.Shipping Address') !!}</label>
                            <textarea id="shipping_address" class="form-control"  readonly>{{ $shipping_address->address}}</textarea>
                        </div>
                    </div>
                    <livewire:checkout-address-form  :address='$shipping_address'>
                    @endif

                </div>
                @endif

            </form>
        </div>
    </div>
    <!--== End Billing Accordion ==-->
</div>

@push('css')
    <style>
        .form-control:disabled, .form-control[readonly] {
            background-color: #f9f9f9;
            opacity: 1;
        }
        .loading {
            position: fixed;
            width: 100%;
            height: 100%;
            display: block;
            left: 0;
            top: 0;
            background: rgba(255,255,255, 0.8);
            z-index: 9999999;
            text-align: center;
            padding: 50vh 20vw;
            color: #dedede;
        }
    </style>
@endpush
