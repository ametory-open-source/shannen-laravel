<div>
    <div wire:loading>
        <div style="min-height: 500px;width:100%" class="d-flex justify-content-center align-items-center">
            <h3 style="text-align: center">
                Loading Products...
            </h3>
        </div>
    </div>
    {{-- <button onclick="Livewire.emit('openModal', 'add-to-cart')">Open Modal</button> --}}
    <div class="row g-3 g-sm-6">
        @foreach ($products as $item)
        @php
        $item->setAffiliate($affiliate);
        @endphp
        <div class="col-6 col-lg-4 col-xl-4 mb-4 mb-sm-8" :wire:key="item-grid-{{$item->id}}">
            <!--== Start Product Item ==-->
            <div class="product-item product-st3-item">
                <div class="product-thumb">
                    <a class="d-block" href="{{ $item->url }}">
                        <img src="{{ $item->feature_image }}" width="370" height="450" alt="Image-HasTech">
                    </a>
                    @if ($item->promo_tag_label)
                    <span class="flag-new" style="background-color: {{$item->promo_tag_color}}">{{
                        $item->promo_tag_label }}</span>
                    @endif
                    <div class="product-action">
                        {{-- <button type="button" class="product-action-btn action-btn-quick-view" data-bs-toggle="modal"
                            >
                            <i class="fa fa-expand"></i>
                        </button> --}}
                        <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal"
                            wire:click="addToCart('{{ $item->id }}')"
                            >
                            <span>{!! __('frontend.Add to cart') !!}</span>
                        </button>
                        @if($frontendUser)
                        <button type="button" wire:click="addToWishlist('{{$item->id}}')"
                            class="product-action-btn action-btn-wishlist" data-bs-toggle="modal"
                            >
                            @if ($item->is_favorited)
                            <i class="fa-solid fa-heart" style="color: #ff6565"></i>
                            @else
                            <i class="fa-regular fa-heart"></i>
                            @endif
                        </button>
                        @endif
                    </div>
                </div>
                <div class="product-info">
                    <div class="product-rating">
                        <div class="rating">
                            {!! $item->rating_stars !!}
                        </div>
                        <div class="reviews">{{ $item->review_count }} {!! __('frontend.reviews') !!}</div>
                    </div>
                    <h4 class="title"><a href="{{ $item->url }}">{{ $item->name }}</a></h4>
                    <div class="prices">
                        <span class="price">{{ $item->price_format }}</span>
                        @if($item->strike_price_format)
                        <span class="price-old">{{ $item->strike_price_format }}</span>
                        @endif
                    </div>
                </div>
                <div class="product-action-bottom">
                    {{-- <button style="{{!$frontendUser ? 'width:100%' : null}}" type="button"
                        class="product-action-btn action-btn-quick-view" data-bs-toggle="modal"
                        >
                        <i class="fa fa-expand"></i>
                    </button> --}}
                    @if($frontendUser)
                    <button type="button" class="product-action-btn action-btn-wishlist" data-bs-toggle="modal"
                        >
                        @if ($item->is_favorited)
                        <i class="fa-solid fa-heart" style="color: #ff6565"></i>
                        @else
                        <i class="fa-regular fa-heart"></i>
                        @endif
                    </button>
                    @endif

                    <button type="button" class="product-action-btn action-btn-cart" data-bs-toggle="modal"
                        wire:click="addToCart('{{ $item->id }}')" >
                        <span>{!! __('frontend.Add to cart') !!}</span>
                    </button>
                </div>
            </div>
            <!--== End prPduct Item ==-->
        </div>

        <aside wire:ignore class="product-cart-view-modal modal fade" id="action-grid-{{ $item->id }}" tabindex="-1"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="product-quick-view-content">
                            <button type="button" class="btn-close" data-bs-dismiss="modal">
                                <span class="fa fa-close"></span>
                            </button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!--== Start Product Thumbnail Area ==-->
                                        <div class="product-single-thumb">
                                            <img src="{{ $item->feature_image }}" width="544" height="560"
                                                alt="Image-HasTech">
                                        </div>
                                        <!--== End Product Thumbnail Area ==-->
                                    </div>
                                    <livewire:product-modal-component :wire:key="$item->id" :product='$item' />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>

        <aside class="product-action-modal modal fade" id="action-grid-CartAdd-{{ $item->id }}" tabindex="-1"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="product-action-view-content">
                            <button type="button" class="btn-close" data-bs-dismiss="modal">
                                <i class="fa fa-times"></i>
                            </button>
                            <div class="modal-action-messages">
                                <i class="fa fa-check-square-o"></i> {{ __('frontend.Added to cart successfully!') }}
                            </div>
                            <div class="modal-action-product">
                                <div class="thumb">
                                    <img src="{{ $item->feature_image }}" alt="Organic Food Juice" width="466"
                                        height="320">
                                </div>
                                <h4 class="product-name"><a href="{{ $item->url }}">{{ $item->name }}</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        @endforeach

        {{ $products->links('frontend.components.modules.paginator') }}
    </div>


</div>
