<div>
    <div class="offcanvas-body">
        <ul class="aside-cart-product-list">
            @if ($content->count() > 0)
            @foreach ($content as $id => $item)
            <li class="aside-product-list-item">
                <a href="#/" class="remove" wire:click="removeFromCart('{{ $id }}')">×</a>
                <a href="{{ $item->get('url') }}">
                    <img src="{{ $item->get('feature_image') }}" width="68" height="84" alt="Image">
                    <span class="product-title">{{ $item->get('name') }}</span>
                </a>
                <div class="d-flex justify-content-between">
                    <span class="product-price">{{ $item->get('quantity') }} × {{ money($item->get('price')) }}</span>
                    <div>
                        <span wire:click="updateCartItem('{{ $id }}', 'plus')" style="cursor: pointer">+</span>
                        <span wire:click="updateCartItem('{{ $id }}', 'minus')" style="cursor: pointer">-</span>
                    </div>
                </div>
            </li>
            @endforeach
            @else
                <p class="text-3xl text-center mb-2">{{ __('frontend.cart is empty!' )}}</p>
            @endif
        </ul>
        <p class="cart-total"><span>Subtotal:</span><span class="amount">{{ money($total) }}</span></p>
        <a class="btn-total" href="/cart">{{ __('frontend.View cart' )}}</a>
        {{-- <a class="btn-total" href="product-checkout.html">Checkout</a> --}}
        <button style="width: 100%" class="btn-total btn-block"  wire:click="clearCart">{{ __('frontend.Clear Cart' )}}</button>
    </div>
<!--== End Aside Cart ==-->
</div>
