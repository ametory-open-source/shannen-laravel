<div>
    @if(!$room)
    <div class="card blur shadow-blur max-height-vh-70">
        <div class="card-body justify-content-center align-items-center d-flex" style="min-height: 300px">
            <div> No Message Selected</div>
        </div>
    </div>
    @else
    <div id="card-box" class="card blur shadow-blur  min-height-vh-60" >
        <div class="card-header shadow-lg">
            <div class="row">
                <div class="col-lg-10 col-8">
                    <div class="d-flex align-items-center">
                        @if ($room->user->avatar)
                            <img alt="{{ $room->user->name }}" src="{{ assetUrl($room->user->avatar) }}" class="avatar shadow" style="background: white">
                        @else
                            <img alt="{{ $room->user->name }}" src="/img/no-picture.jpeg" class="avatar shadow">
                        @endif
                        <div class="ms-3">
                            <h6 class="mb-0 d-block">{{ $room->user->name }}</h6>
                            @if($room->is_typing)
                                <span class="badge badge-success"></span>
                            @elseif($room->last_message_at)
                                <p class="text-xs mb-2">{{ $room->last_message_at->diffForHumans() }}</p>
                            @else
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-2 my-auto pe-0 d-flex justify-content-end">
                    <button class="btn btn-icon-only shadow-none text-dark mb-0 me-3 me-sm-0" type="button"
                        data-bs-toggle="tooltip" data-bs-placement="top" title=""
                        data-bs-original-title="Attachment">
                        <i class="fa fa-paperclip"></i>
                    </button>
                </div>
                {{-- <div class="col-lg-1 col-2 my-auto ps-0">
                    <div class="dropdown">
                        <button class="btn btn-icon-only shadow-none text-dark mb-0" type="button"
                            data-bs-toggle="dropdown">
                            <i class="ni ni-settings"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-end me-sm-n2 p-2" aria-labelledby="chatmsg">
                            <li>
                                <a class="dropdown-item border-radius-md" href="javascript:;">
                                    Profile
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item border-radius-md" href="javascript:;">
                                    Mute conversation
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item border-radius-md" href="javascript:;">
                                    Block
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item border-radius-md" href="javascript:;">
                                    Clear chat
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item border-radius-md text-danger" href="javascript:;">
                                    Delete chat
                                </a>
                            </li>
                        </ul>
                    </div>
                </div> --}}
            </div>
        </div>
        <div class="card-body overflow-auto overflow-x-hidden">
            @foreach ($room->messages as $chat)
                @if($chat->user_id)
                <div class="row justify-content-start mb-4">
                    <div class="col-auto">
                        <div class="card ">
                            <div class="card-body py-2 px-3">
                                <p class="mb-1">
                                    {{ $chat->message }}
                                </p>
                                <div class="d-flex align-items-center text-sm opacity-6">
                                    @if($chat->readed_by_admin)
                                    <i class="ni ni-check-bold text-sm me-1 text-success"></i>
                                    @endif
                                    <small>{{ $chat->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if($chat->admin_id)
                <div class="row justify-content-end text-right mb-4">
                    <div class="col-auto">
                        <div class="card bg-gray-200">
                            <div class="card-body py-2 px-3">
                                <p class="mb-1">
                                    {{ $chat->message }}
                                </p>
                                <div class="d-flex align-items-center justify-content-end text-sm opacity-6">
                                    <i class="ni ni-check-bold text-sm me-1"></i>
                                    <small>{{ $chat->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            @endforeach



            {{-- <div class="row justify-content-end text-right mb-4">
                <div class="col-auto">
                    <div class="card bg-gray-200">
                        <div class="card-body py-2 px-3">
                            <p class="mb-0">
                                At the end of the day … the native dev apps is where users are
                            </p>
                            <div class="d-flex align-items-center justify-content-end text-sm opacity-6">
                                <i class="ni ni-check-bold text-sm me-1"></i>
                                <small>4:42pm</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-start">
                <div class="col-auto">
                    <div class="card ">
                        <div class="card-body py-2 px-3">
                            <p class="mb-0">
                                Charlie is Typing...
                            </p>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
        <div class="card-footer d-block">
            <div class="align-items-center">
                <livewire:admin-message-shout :room='$room' key="{{ $room->id }}">
            </div>
        </div>
    </div>
    @endif
</div>
@push('css')
    <style>
        html {
            scroll-behavior: smooth;
        }
    </style>
@endpush
@push('scripts')
    <script>
        let container = document.querySelector("#card-box .card-body")
        $(function() {
            // container.scrollTop = container.scrollHeight
            container.scrollTo({ top: container.scrollHeight, behavior: "smooth" })

        })
        window.addEventListener('scrollDown', () => {
            setTimeout(() => {
                container.scrollTo({ top: container.scrollHeight, behavior: "smooth" })
            }, 500);

        })
    </script>
@endpush
