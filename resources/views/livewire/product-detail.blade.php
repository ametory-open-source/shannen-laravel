<div>
    <div class="row product-details">
        <div class="col-lg-6" wire:ignore>
            @if ($product->pictures->count() > 1)
            <div class="slideshow-container">
                @foreach ($product->pictures as $i => $picture)
                <div class="product-details-thumb mySlides newFade">
                    <div class="numbertext">{{$i + 1}} / {{$product->pictures->count()}}</div>
                    <img src="{{ $picture->picture_url }}" width="570" height="693" alt="{{ $picture->description }}">
                </div>
                @endforeach

                <a class="prev" onclick="plusSlides(-1)">❮</a>
                <a class="next" onclick="plusSlides(1)">❯</a>
                {{-- --}}
            </div>
            <div style="text-align:center">
                @foreach ($product->pictures as $i => $pic)
                <span class="dot" onclick="currentSlide({{ $i + 1}})"></span>
                @endforeach
            </div>
            @else
            <div class="product-details-thumb">
                <img src="{{ $product->feature_image }}" width="570" height="693"
                    alt="{{ $product->feature_image_caption }}">
                @if ($product->promo_tag_label)
                <span class="flag-new" style="background-color: {{$product->promo_tag_color}}">{{
                    $product->promo_tag_label }}</span>
                @endif
            </div>
            @endif
        </div>
        <div class="col-lg-6">
            <div class="product-details-content">
                <h5 class="product-details-collection"> <a href="{{ $product->category->url }}"> {{ $product->category->name }}</a></h5>
                <h3 class="product-details-title">{{ $product->name }}</h3>
                <div class="blog-detail-category">
                    @foreach (explode(",",$product->tags) as $tag)
                    <a class="category" data-bg-color="{{ randomPastelColor() }}" href="/products?tag={{$tag}}">{{ $tag }}</a>
                    @endforeach

                </div>
                <div class="product-details-review mb-7">
                    <div class="product-review-icon">
                        {!! $product->rating_stars !!}
                    </div>
                    <button type="button" class="product-review-show">{{ $product->review_count }} {!!
                        __('frontend.reviews') !!}</button>
                </div>
                {{-- <p class="mb-7">{!! $product->description !!}</p> --}}
                <div class="product-details-pro-qty">
                    <div class="pro-qty">
                        <input type="text" title="Quantity" wire:model="quantity">
                        <div wire:click="dec" class="dec qty-btn">-</div>
                        <div wire:click="inc" class="inc qty-btn">+</div>
                    </div>
                </div>
                <div class="product-details-action">
                    <h4 class="price">{{ $product->price_format }}</h4>
                    <div class="product-details-cart-wishlist">
                        @if($frontendUser)
                        <button type="button" wire:click="addToWishlist('{{$product->id}}')" class="btn-wishlist"

                            >
                            @if ($product->is_favorited)
                            <i class="fa-solid fa-heart" style="color: #ff6565"></i>
                            @else
                            <i class="fa-regular fa-heart"></i>
                            @endif
                        </button>
                        @endif
                    </div>
                </div>
                <div class="product-details-action">
                    <div class="">
                        <button wire:click="addToCart" type="button" class="btn">Add to cart</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@push('css')
<style>
    * {
        box-sizing: border-box
    }

    body {
        font-family: Verdana, sans-serif;
        margin: 0
    }

    .mySlides {
        display: none
    }

    img {
        vertical-align: middle;
    }

    /* Slideshow container */
    .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
    }

    /* Next & previous buttons */
    .slideshow-container .prev,
    .slideshow-container .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        padding: 16px;
        margin-top: -22px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
    }

    /* Position the "next button" to the right */
    .slideshow-container .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .slideshow-container .prev:hover,
    .slideshow-container .next:hover {
        background-color: rgba(0, 0, 0, 0.8);
    }

    /* Caption text */
    .slideshow-container .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
    }

    /* Number text (1/3 etc) */
    .slideshow-container .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* The dots/bullets/indicators */
    .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
    }

    .slideshow-container .active,
    .slideshow-container .dot:hover {
        background-color: #717171;
    }

    /* Fading animation */
    .slideshow-container .newFade {
        animation-name: fade;
        animation-duration: 1.5s;

    }

    @keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 300px) {

        .slideshow-container .prev,
        .slideshow-container .next,
        .slideshow-container .text {
            font-size: 11px
        }
    }
</style>
@endpush
@push('scripts')
<script>
    let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}
</script>
@endpush
