<div>
    <div class="myaccount-table table-responsive text-center">
        @if(!$products->count())
        <h6>No Products Available</h6>
        @else
        <table class="table table-bordered" style="">
            <thead class="thead-light">
                <tr>
                    <th >No</th>
                    <th>{{ __('frontend.Pic') }}</th>
                    <th>{{ __('frontend.Name') }}</th>
                    <th>{{ __('frontend.Price') }}</th>
                    <th>{{ __('frontend.Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $i => $item)
                <tr wire:key="item-{{ $item->id }}">
                    <td>{{  $products->firstItem() + $i  }}</td>
                    <td>
                        <div style="width: 100px" >
                        <img width="100" src="{{ $item->feature_image }}" alt="" srcset="" style="width: 100px;
                        height: 100px;
                        object-fit: cover;
                        padding: 3px;
                        color: white;
                        border: 1px solid #dedede;
                        border-radius: 5px;">
                        </div>
                    </td>
                    <td>
                        <div style="max-width: 300px;display:block; text-wrap:wrap; text-align:left" >
                            <p>{{ $item->name }}</p>
                        </div>
                    </td>
                    <td>{{ $item->price_format }}</td>
                    <td>
                        <a href="{{ $item->url}}" target="_blank">
                            <i class="fa-regular fa-eye"></i>
                        </a>
                        <a onclick="deleteWhislist('{{$item->id}}')" target="_blank">
                            <i class="fa-solid fa-trash"></i>
                        </a>

                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        {{ $products->links("vendor.livewire.bootstrap") }}
        @endif

    </div>
</div>
