<div>
    <div class="myaccount-table table-responsive ">
        @if(!$addresses->count())

        <p style="text-wrap:wrap">{!! __('frontend.address_attention') !!}</p>
        @else
        <table class="table table-bordered" style="">
            <thead class="thead-light">
                <tr>
                    <th>No</th>
                    <th>Label</th>
                    <th>{!! __('frontend.Recipient Name') !!}</th>
                    <th>{!! __('frontend.Phone') !!}</th>
                    <th>{!! __('frontend.Address') !!}</th>
                    <th>{!! __('frontend.Action') !!}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($addresses as $i => $item)
                <tr wire:key="item-{{ $item->id }}" class="{{ $user->address_id == $item->id ? 'default' : null}}">
                    <td>{{  $addresses->firstItem() + $i  }}</td>
                    <td>{{ $item->label }}</td>
                    <td>{{ $item->recipient_name }}</td>
                    <td>{{ $item->phone_number }}</td>
                    <td>
                        <div style="width: 200px; text-wrap:wrap; text-align:left">
                        {{ $item->address }}
                        </div>
                    </td>
                    <td>
                        <a href="/profile/address/{{$item->id}}"><i class="fa fa-pencil" style="color: grey"></i></a>
                        <a data-message="anda yakin akan menghapus alamat {{$item->label}}" data-href="/profile/address/{{$item->id}}/delete" onclick="btnDelete(this)"><i class="fa fa-trash" style="color: grey"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $addresses->links("vendor.livewire.bootstrap") }}
        @endif
    </div>
    <div class="account-details-form">
        <div class="single-input-item">
            <button wire:click='toForm' class="check-btn sqr-btn">{!! __('frontend.Add Address') !!}</button>
        </div>
    </div>

</div>
@push('css')
    <style>
        .default td{
            background-color: #e1fde9;
        }
    </style>
@endpush
