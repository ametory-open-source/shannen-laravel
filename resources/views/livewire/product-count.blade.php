<div>
    <h5 class="showing-pagination-results mt-5 mt-md-9 text-center text-md-end">{!! __('frontend.show_product_result', ["count" => $total]) !!}</h5>
</div>
