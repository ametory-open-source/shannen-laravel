@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Category'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h5>
                Create Category
            </h5>
        </div>
        <div class="card-body">
            <form action="/admin/blog_categories" method="post" class="form">
                @csrf
                <input type="hidden" name="picture" value="{{ old('picture') }}" class="picture">
                {{-- <div class="row mb-3">
                    <div id="dropzone" class="dropzone-wrapper dropzone col-md-8 offset-md-2">
                        <div class="dz-message needsclick">
                            <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                    class="path2"></span></i>
                            <!--begin::Info-->
                            <div class="ms-4">
                                <h3 class="fs-5 fw-bold text-gray-900 mb-1">Drop files here or click to upload.</h3>
                            </div>
                            <!--end::Info-->
                        </div>
                    </div>
                </div> --}}

                <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="title">Name</label></div>
                    <div class="col-md-6">
                        <input type="text" value="{{ old('name') }}" name="name" id="" class="form-control"
                            placeholder="Name">
                    </div>
                </div>
                {{-- <div class="form-group row">
                    <div class="col-md-2 offset-md-2"><label for="description">Description</label></div>
                    <div class="col-md-6">
                        <textarea rows="9" name="description" id="" class="form-control"
                            placeholder="Description">{{ old('description') }}</textarea>
                    </div>
                </div> --}}
                <div class="form-group row">
                    <div class="col-md-2 offset-md-4">
                        <button type="submit" class="btn bg-gradient-primary mt-5" style="min-width: 160px">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</main>
@push('scripts')
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
    // The dropzone method is added to jQuery elements and can
    // be invoked with an (optional) configuration object.
    $(".dropzone").dropzone({
        url: "/admin/file/upload" ,
        method: 'post',
	    headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        accept: function(file, done) {
            done()
        },
        success: function(file, response) {

            $(".picture").val(response.file)
        },

    });
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<style>


</style>
@endpush
@endsection
