@extends("layouts.landing")

@section('content')

    @includeWhen($web->show_header, "components.landing.header")
    @includeWhen($web->show_banner, "components.landing.banner")
    @includeWhen($web->show_performance, "components.landing.performance")
    @includeWhen($web->show_feature, "components.landing.feature")
    @includeWhen($web->show_client, "components.landing.client")
    @includeWhen($web->show_showcase, "components.landing.showcase")
    @includeWhen($web->show_platform, "components.landing.platform")
    @includeWhen($web->show_blog, "components.landing.blog")
    @includeWhen($web->show_call, "components.landing.call")
    @includeWhen($web->show_footer, "components.landing.footer")

@endsection