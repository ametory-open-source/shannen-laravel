

        <!--==========================-->
        <!--=         Banner         =-->
        <!--==========================-->
        <section class="banner banner-saas">
            <!-- <div class="banner-background">
                <img src="/web/media/banner/saas/down_bg.png" alt="banner-bg">
                <img src="/web/media/banner/saas/shape.png" class="transperent-shape" alt="shape">
            </div> -->

            <div class="overlay-shape" data-bg-image="/web/media/banner/saas/down_bg.png">
                <img src="/web/media/banner/saas/shape.png" class="transperent-shape" alt="shape">
            </div>

            <div class="banner-content-wrap">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="banner-content-inner">
                                <div class="banner-content">
                                    <h4 class="banner-sub-title wow gptFadeUp">{{ $web->banner_title }}</h4>
                                    <h1 class="banner-title wow gptFadeUp" data-wow-delay="0.3s">
                                        {!! $web->banner_subtitle !!}
                                    </h1>
                                    <a href="{{ $web->banner_link }}" class="gp-btn banner-btn color-seven wow gptFadeUp"
                                        data-wow-delay="0.4s">Request a Demo</a>
                                </div>
                                <!-- /.banner-content -->
                            </div>
                            <!-- /.banner-content-inner -->
                        </div>
                        <!-- /.col-lg-6 -->

                        <div class="col-lg-6">

                            <div class="animated-promo-mockup">
                                <img src="/web/media/banner/saas/man1.png" class="wow fadeInLeft man1" alt="mpckup">
                                
                                <img src="{{ $web->banner_picture != '' ? assetUrl($web->banner_picture) : '/web/media/banner/saas/mob.png'}}" class="wow fadeInLeft mob" alt="mpckup">
                                <img src="/web/media/banner/saas/man2.png" class="wow fadeInUp man2" alt="mpckup">
                                <img src="/web/media/banner/saas/tree.png" class="wow fadeInRight tree" alt="mpckup">
                                <img src="/web/media/banner/saas/cloud_01.png" class="cloud-one" alt="mpckup">
                                <img src="/web/media/banner/saas/cloud_02.png" class="cloud-three" alt="mpckup">
                                <img src="/web/media/banner/saas/cloud_03.png" class="cloud-two" alt="mpckup">
                                <img src="/web/media/banner/saas/line.png" class="line" alt="mpckup">
                            </div>
                            <!-- /.promo-mockup -->
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.banner-content-wrap -->

        </section>
        <!-- /.banner banner-two -->
