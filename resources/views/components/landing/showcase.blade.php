<!--============================-->
<!--=         Showcase         =-->
<!--============================-->
<section id="showcase">
    <div class="container">
        @isset($showcases[0])
        <div class="showcase-content">
            <div class="row">
                <div class="col-lg-6 gpt-order-2">
                    <div class="showcase-content style-one mt-5">
                        <div class="section-heading text-left">
                            <h2 class="section-title wow fadeInUp">
                                {!! $showcases[0]->title !!}
                            </h2>
                        </div>

                        {!! $showcases[0]->description !!}
                    </div>
                    <!-- /.showcase-content -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-6">
                    <div class="showcase-image-box style-one wow fadeInUp" data-wow-delay="0.3s">
                        <img src="{{ assetUrl($showcases[0]->picture) }}" alt="iphone">
                        <div class="animate-shape">
                            <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" x="0"
                                y="0" viewBox="0 0 350 570" width="350" height="570">
                                <path id="showcase-svg" fill-rule="evenodd" fill="rgb(252, 126, 87)"
                                    d="M175.000,-0.001 C271.650,-0.001 350.000,78.349 350.000,174.999 C350.000,271.649 345.650,569.999 249.000,569.999 C152.350,569.999 -0.000,271.649 -0.000,174.999 C-0.000,78.349 78.350,-0.001 175.000,-0.001 Z" />
                            </svg>
                        </div>
                    </div>
                    <!-- /.showcase-image-box -->
                </div>
                <!-- /.col-lg-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.showcase-content -->    
        @endisset
        
        @isset($showcases[1])
        <div class="showcase-content style-two">
            <div class="row">
                <div class="col-lg-5">
                    <div class="showcase-image-box wow fadeInUp">
                        <img src="{{ assetUrl($showcases[1]->picture) }}" alt="iphone">
                        <div class="animate-shape">

                            <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" x="0"
                                y="0" viewBox="0 0 401 514" width="401" height="514">
                                <path fill-rule="evenodd" fill="rgb(0, 192, 205)"
                                    d="M149.066,74.184 C195.488,181.016 365.373,85.326 395.934,197.727 C432.728,333.057 210.795,688.037 140.167,408.637 C81.834,177.879 38.111,309.301 4.655,197.727 C-21.312,111.127 66.106,-116.734 149.066,74.184 Z" />
                            </svg>
                        </div>
                    </div>
                    <!-- /.showcase-image-box -->
                </div>
                <!-- /.col-lg-5 -->

                <div class="col-lg-7">
                    <div class="showcase-content">
                        <div class="section-heading text-left">
                            <h2 class="section-title wow fadeInUp">
                                {!! $showcases[1]->title !!}
                            </h2>
                        </div>

                        {!! $showcases[1]->description !!}
                    </div>
                    <!-- /.showcase-content -->
                </div>
                <!-- /.col-lg-7 -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.showcase-content -->
        @endisset

        @isset($showcases[2])
        <div class="showcase-content style-three">
            <div class="row">
                <div class="col-lg-5 gpt-order-2">
                    <div class="showcase-content">
                        <div class="section-heading text-left">
                            <h2 class="section-title wow fadeInUp">
                                {!! $showcases[2]->title !!}
                            </h2>
                        </div>

                        {!! $showcases[2]->description !!}
                    </div>
                    <!-- /.showcase-content -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-7">
                    <div class="showcase-image-box style-three text-right wow fadeInUp">
                        <img src="{{ assetUrl($showcases[2]->picture) }}" alt="iphone">
                        <div class="animate-shape">
                            <svg preserveAspectRatio="xMidYMid meet" x="0" y="0" viewBox="0 0 405 525"
                                width="405" height="525">
                                <path fill-rule="evenodd" fill="rgb(110, 88, 252)"
                                    d="M212.987,0.999 C498.124,-14.567 356.102,179.116 304.863,300.906 C257.624,413.191 526.147,463.359 335.783,514.370 C92.922,579.448 3.310,321.876 0.083,197.703 C-3.082,75.954 105.163,6.886 212.987,0.999 Z" />
                            </svg>
                        </div>
                    </div>
                    <!-- /.showcase-image-box -->
                </div>
                <!-- /.col-lg-8 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.showcase-content -->
        @endisset
    </div>
    <!-- /.container -->
</section>
<!-- /#showcase -->