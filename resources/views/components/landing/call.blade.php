

        <!--===================================-->
        <!--=         Call To Action        =-->
        <!--===================================-->
        <section id="call-to-action-four">
            <div class="container">
                <div class="call-to-action-four text-center">
                    <div class="action-content wow gptFadeUp">
                        <h2 class="action-title">You will love our solutions</h2>

                        <p>Create your own e-commerce store with Ariya today.</p>

                    </div>
                    <!-- /.action-content -->

                    <div class="action-button wow gptFadeUp" data-wow-delay="0.5s">
                        <a href="login.html" class="gp-btn color-seven color-three">Get Started Free</a>
                    </div>
                    <!-- /.action-button -->
                </div>
                <!-- /.col-to-action -->
            </div>
            <!-- /.container -->

            <div class="left-shape">
                <img src="/web/media/call-to-action/left-shape.png" class="wow fadeInLeft" alt="shape left">
            </div>

            <div class="bottom-shape">
                <img src="/web/media/call-to-action/bottom-shape.png" class="wow fadeInRight" alt="shape bottom">
            </div>
        </section>
        <!-- /#call-to-action -->