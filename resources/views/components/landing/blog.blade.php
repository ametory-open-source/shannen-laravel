

        <!--=============================-->
        <!--=         Blog Post         =-->
        <!--=============================-->
        <section id="blog-grid-two">
            <div class="container">

                <div class="section-heading">
                    <h2 class="section-title">
                        {!! $web->blog_title !!}
                    </h2>
                </div>
                <!-- /.section-title -->

                <div class="row justify-content-center">
                    @foreach ($blogs as $item)
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <article class="blog-post-two">
                            <div class="feature-image">
                                <a href="/blog/{{ $item->slug }}">
                                    <img src="{{ assetUrl($item->feature_image) }}">
                                </a>
                            </div>
                            <!-- /.feature-image -->
                            <div class="blog-content">
                                <ul class="post-meta">
                                    <li><a href="/blog/{{ $item->slug }}">{{ $item->created_at->format("d M y")}}</a></li>
                                    <li>
                                        <a href="/blog?category={{ optional($item->category)->name }}">{{ optional($item->category)->name }}</a>
                                    </li>
                                </ul>

                                <h3 class="entry-title">
                                    <a href="/blog/{{ $item->slug }}">{{ $item->title }}</a>
                                </h3>

                                <ul class="post-meta">
                                    <li>BY <a href="/blog/{{ $item->slug }}">{{ $item->author }}</a></li>
                                </ul>
                            </div>
                            <!-- /.blog-content -->
                        </article>
                        <!-- /.blog-post-two -->
                    </div>
                    <!-- /.col-lg-4 col-md-6 col-sm-6 -->    
                    @endforeach
                    
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
        <!-- /#blog-grid -->