
        <!--===============================-->
        <!--=         Performance         =-->
        <!--===============================-->
        <section class="performance">
            <div class="container pr">
                <div class="gp-tabs">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="tab-content-inner">
                                <svg class="animate-shape-two" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="258px" height="205px">
                                    <path fill-rule="evenodd" opacity="0.102" fill="rgb(255, 152, 62)"
                                        d="M127.562,32.995 C198.653,32.995 258.000,-39.091 258.000,32.134 C258.000,103.360 200.369,204.088 129.278,204.088 C58.187,204.088 0.557,103.360 0.557,32.134 C0.557,-39.091 56.471,32.995 127.562,32.995 Z" />
                                </svg>

                                <svg xmlns="http://www.w3.org/2000/svg" class="animate-shape-one"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="430px" height="502px">
                                    <path fill-rule="evenodd" fill="rgb(255, 152, 62)"
                                        d="M209.387,181.142 C213.798,316.899 429.072,90.804 429.072,209.514 C429.072,328.223 317.513,645.971 214.536,424.455 C164.511,316.846 -0.000,328.223 -0.000,209.514 C-0.000,90.804 197.645,-180.248 209.387,181.142 Z" />
                                </svg>

                                <ul class="gp-tabs-content wow fadeIn">
                                    @foreach ($performances as $i => $item)
                                        @php
                                            
                                        @endphp
                                        <li data-content="{{ $item->subtitle }}" class="{{ !$i ? 'active-tab' : null }}">
                                            <div class="performance-tab-image">
                                                <img src="{{ assetUrl($item->picture) }}" alt="performance">
                                            </div>
                                        </li>    
                                    @endforeach
                                    

                                </ul> <!-- gp-tabs-content -->
                            </div><!-- /.tab-content-inner -->
                        </div><!-- /.col-lg-6 -->

                        <div class="col-lg-6">
                            <div class="gp-tab-contentens">
                                <div class="section-heading text-left">
                                    <h2 class="section-title wow fadeInUp">
                                        {{$web->performance_title}}
                                    </h2>

                                    <p class="wow fadeInUp" data-wow-delay="0.3s">
                                        {{$web->performance_subtitle}}
                                    </p>
                                </div>
                            </div>

                            <nav>
                                <ul class="gp-tabs-navigation wow fadeInUp" data-wow-delay="0.5s">
                                    @foreach ($performances as $i => $item)
                                    @php
                                        switch ($i%3) {
                                            case 1:
                                            $color = "red";
                                                break;
                                            case 2:
                                            $color = "purple";
                                                break;
                                            
                                            default:
                                            $color = "green";
                                                break;
                                        }
                                    @endphp
                                    <li data-content="{{ $item->subtitle }}" class="{{ !$i ? 'active-tab' : null }}">
                                        <div class="tab-icon">
                                            <i class="fa-2x  fad {{$item->icon}}" style="--fa-primary-color: {{$color}};"></i>
                                        </div>
                                        <a href="#0">{{$item->title}} </a>
                                    </li>
                                    @endforeach
                                    
                                </ul> <!-- gp-tabs-navigation -->
                            </nav>
                        </div><!-- /.col-lg-6 -->

                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="750px"
                            height="726px" class="tab-bg-shape">
                            <path fill-rule="evenodd" fill="rgb(250, 249, 253)"
                                d="M464.695,12.953 C614.502,45.457 750.000,129.472 750.000,378.000 C750.000,626.528 522.596,590.967 298.774,698.997 C51.835,818.183 -112.102,512.988 93.000,275.000 C255.297,86.680 214.419,-41.349 464.695,12.953 Z" />
                        </svg>
                    </div><!-- /.row -->
                </div> <!-- gp-tabs -->
            </div><!-- /.container -->
        </section><!-- /.performance -->

@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
@endpush