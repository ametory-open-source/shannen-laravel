

        <!--=========================-->
        <!--=        Footer         =-->
        <!--=========================-->
        <footer class="site-footer" id="footer-saas-two">
            <div class="container">
                <div class="footer-nner wow pixFadeUp">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="widget footer-widget widget-about">
                                <div class="footer-logo">
                                    <img src="{{ assetUrl($web->logo) }}" width="160">
                                </div>
                                <div class="copyright-text">
                                    <p class="copyright-text">{{ $web->copyright }}</p>
                                </div>

                                <ul class="footer-social-link">
                                    <li><a href="{{ $web->facebook_link }}"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="{{ $web->instagram }}"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="{{ $web->youtube_link }}"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="{{ $web->tiktok_link }}"><i class="fab fa-tiktok"></i></a></li>
                                </ul>
                            </div>
                            <!-- /.widget footer-widget -->
                        </div>
                        <!-- /.col-lg-3 col-md-6 -->

                        <div class="col-lg-3 col-md-6">
                            <div class="widget footer-widget">
                                <h3 class="widget-title">Download</h3>

                                <ul class="footer-menu">
                                    {!! $web->footer_left_menu !!}
                                </ul>
                            </div>
                            <!-- /.widget footer-widget -->
                        </div>
                        <!-- /.col-lg-3 col-md-6 -->

                        <div class="col-lg-3 col-md-6">
                            <div class="widget footer-widget">
                                <h3 class="widget-title">Quick Links</h3>

                                <ul class="footer-menu">
                                    {!! $web->footer_center_menu !!}
                                </ul>
                            </div>
                            <!-- /.widget footer-widget -->
                        </div>
                        <!-- /.col-lg-3 col-md-6 -->

                        <div class="col-lg-2 col-md-6">
                            <div class="widget footer-widget">
                                <h3 class="widget-title">Information</h3>

                                <ul class="footer-menu">
                                    {!! $web->footer_right_menu !!}
                                </ul>
                            </div>
                            <!-- /.widget footer-widget -->
                        </div>
                        <!-- /.col-lg-3 col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.footer-nner -->
            </div>
            <!-- /.container -->

            <div class="footer-bottom-shape">
                <div class="three-left">
                    <img src="/web/media/footer/tree_left.png" alt="tree">
                </div>

                <div class="three-right">
                    <img src="/web/media/footer/tree_right.png" alt="tree">
                </div>

                <div class="container">
                    <div class="footer-center-victor">
                        <div class="foo-shape-one">
                            <img src="/web/media/footer/man-female1.png" alt="man">
                        </div>

                        <div class="foo-shape-two">
                            <img src="/web/media/footer/man1.png" alt="man">
                        </div>

                        <div class="foo-shape-three">
                            <img src="/web/media/footer/man-female2.png" alt="man">
                        </div>

                        <div class="foo-shape-four">
                            <img src="/web/media/footer/man2.png" alt="man">
                        </div>

                        <div class="foo-shape-five">
                            <img src="/web/media/footer/vase.png" alt="man">
                        </div>
                    </div>
                    <!-- /.footer-center-victor -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.footer-bottom-shape -->
        </footer>
        <!-- /#footer -->