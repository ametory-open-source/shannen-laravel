<!--=========================-->
<!--=        Navbar         =-->
<!--=========================-->
<header class="site-header header-light-saas header-transparent header-fixed" data-header-fixed="true"
    data-mobile-menu-resolution="992">
    <div class="container">
        <div class="header-inner">

            <nav id="site-navigation" class="main-nav">

                <div class="site-logo">
                    <a href="/" class="logo">
                        <img src="{{ assetUrl($web->logo) }}" alt="site logo" class="main-logo" width="160">
                        <img src="{{ assetUrl($web->logo) }}" alt="site logo" class="logo-sticky" width="160">
                    </a>
                </div>
                <!-- /.site-logo -->

                <div class="menu-wrapper main-nav-container canvas-menu-wrapper" id="mega-menu-wrap">
                    <div class="canvas-header">
                        <div class="mobile-offcanvas-logo">
                            <a href="/">
                                <img src="{{ assetUrl($web->logo) }}" alt="site logo" class="logo-sticky" width="160">
                            </a>
                        </div>

                        <div class="close-menu" id="page-close-main-menu">
                            <i class="ti-close"></i>
                        </div>

                    </div>

                    {!! $web->header_menu !!}
                </div>
                <!-- /.menu-wrapper -->

                <div class="astriol-burger-menu" id="mobile-menu-open">
                    <span class="bar-one"></span>
                    <span class="bar-two"></span>
                    <span class="bar-three"></span>
                </div>
            </nav>
            <!-- /.site-nav -->
        </div>
        <!-- /.header-inner -->
    </div>
    <!-- /.container-full -->
</header>
<!-- /.site-header -->