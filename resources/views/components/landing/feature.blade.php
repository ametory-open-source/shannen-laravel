<!--===========================-->
<!--=         Feature         =-->
<!--===========================-->
<section id="feature">
    <div class="container">
        <div class="section-heading">
            <h2 class="section-title wow fadeInUp">
                {!! $web->feature_title !!}
            </h2>
        </div>
        <!-- /.section-heading -->

        <div class="row">
            @foreach ($features as $item)
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="icon-box style-four color-seven wow fadeInUp" data-wow-delay="0.3s">
                    <div class="icon-container">
                        <img src="{{ assetUrl($item->picture) }}" alt="service">
                    </div>

                    <div class="box-content">
                        <h3 class="box-title">
                            <a href="{{ $item->link }}">{{ $item->title }}</a>
                        </h3>

                        <p>
                            {{ $item->description }}
                        </p>
                    </div>
                </div>
                <!-- /.icon-box style-four color-seven -->
            </div>
            <!-- /.col-lg-4 col-md-6 col-sm-6 -->
            @endforeach
            

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /#feature -->