

        <!--===============================-->
        <!--=         Testimonial         =-->
        <!--===============================-->
        <section class="testimonials-apps">
            <div class="container">
                <div class="testimonial-top-wrapper">
                    <svg class="testimonial-bg-quote" preserveAspectRatio="xMidYMid meet" x="0" y="0"
                        viewBox="0 0 416 274" width="416" height="274">
                        <path fill-rule="evenodd" fill="rgb(50, 51, 117)"
                            d="M402.988,26.064 C340.422,26.064 286.385,63.267 261.550,116.762 C274.358,108.906 289.398,104.376 305.476,104.376 C352.073,104.376 390.037,142.415 390.037,189.184 C390.037,235.954 352.073,273.994 305.476,273.994 C258.879,273.994 220.978,235.954 220.978,189.184 C220.978,187.520 221.039,185.871 221.133,184.228 C221.073,183.715 220.978,183.212 220.978,182.685 C220.978,81.946 302.619,0.012 402.988,0.012 C410.224,0.012 416.002,5.803 416.002,12.995 C416.002,20.275 410.224,26.064 402.988,26.064 ZM181.999,26.064 C119.413,26.064 65.329,63.291 40.487,116.812 C53.311,108.927 68.377,104.376 84.486,104.376 C131.083,104.376 168.984,142.415 168.984,189.184 C168.984,235.954 131.083,273.994 84.486,273.994 C37.897,273.994 -0.002,235.954 -0.002,189.184 C-0.002,187.523 0.059,185.877 0.153,184.239 C0.091,183.724 -0.002,183.216 -0.002,182.685 C-0.002,81.946 81.631,0.012 181.999,0.012 C189.173,0.012 195.021,5.803 195.021,12.995 C195.021,20.275 189.173,26.064 181.999,26.064 Z" />
                    </svg>

                    <div class="testimonial-top">
                        @foreach ($clients as $item)
                        <div class="slide-item">
                            <div class="testimonial-app">
                                <div class="testi-content">
                                    <p>
                                        {{$item->description}}
                                    </p>
                                </div>
                            </div>
                            <!-- /.testimonial -->
                        </div>    
                        @endforeach
                    </div>
                </div>
                <!-- /.testimonial-top-wrapper -->

                <div class="testimonial-bottom">
                    @foreach ($clients as $item)
                    <div class="slide-item">
                        <div class="user-avatar">
                            <img src="{{ assetUrl($item->picture) }}" alt="astriol user-avatar">
                        </div>
                        <!-- /.user-avatar -->

                        <div class="info">
                            <h4 class="user-name">{{ $item->title }}</h4>
                            <span class="designation">{{ $item->subtitle }}</span>
                        </div>
                        <!-- /.testi-content -->
                    </div>
                    <!-- /.slide-item -->
                    @endforeach

                </div>
                <!-- /.testimonial-bottom -->
            </div>
            <!-- /.container -->
        </section>
        <!-- /.testimonials -->