

        <!--===========================-->
        <!--=         Platform        =-->
        <!--===========================-->
        <section id="platform">
            <div class="container">
                <div class="section-heading">
                    <h2 class="section-title wow fadeInUp">
                        {!! $web->platform_title !!}
                    </h2>
                </div>
                <!-- /.section-heading -->

                <div class="row">
                    @foreach ($platforms as $item)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="icon-list-box wow fadeInUp" data-wow-delay="0.3s">
                            <div class="list-icon">
                                <i class="ei ei-icon_check"></i>
                            </div>

                            <div class="content">
                                <p>
                                    {{ $item->title }}
                                </p>
                            </div>
                        </div>
                        <!-- /.icon-list-box -->
                    </div>
                    <!-- /.col-lg-4  col-md-4 -->    
                    @endforeach
                    

                    
                </div>
                <!-- /.row -->

                <div class="platform-mockup text-center">
                    <img src="{!! assetUrl($web->platform_picture) !!}" class="wow fadeInDown" data-wow-delay="0.7s" alt="mockup">
                </div>
                <!-- /.platform-mockup -->
            </div>
            <!-- /.container -->
        </section>
        <!-- /#platform -->