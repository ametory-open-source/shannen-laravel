@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])
@php
$user = auth()->user();

@endphp
@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Dashboard'])
<div class="container-fluid py-4">

    <div class="row mt-4">
        <div class="col-lg-12 mb-lg-0 mb-4">
            <div class="card z-index-2 h-100">
                <div class="card-header d-flex justify-content-between">
                    <h4 class="card-title">
                        Admin
                    </h4>

                </div>
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-md-6">
                            <form method="post">
                                @csrf
                                <h4>Profile</h4>
                                <div class="form-group">
                                    <label for="title">Name</label>
                                    <input type="text" class="form-control" id="name" aria-describedby="name"
                                        name="name" placeholder="Admin name ..."
                                        value="{{ old('name') ?? $user->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="title">Email</label>
                                    <input readonly type="text" class="form-control" id="email" aria-describedby="email"
                                        name="email" placeholder="Admin Email ..."
                                        value="{{ old('email') ?? $user->email }}">
                                </div>

                                <div class="form-group mt-5">
                                    <button type="submit" class="btn bg-gradient-primary"
                                        style="min-width: 160px">Save</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form action="/admin/change-password" method="post">
                                @csrf
                                <h4>Change Password</h4>
                                <div class="form-group">
                                    <label for="title">Current Password</label>
                                    <input type="password" class="form-control" id="old_password"
                                        aria-describedby="old_password" name="old_password"
                                        placeholder="Old Password ...">
                                </div>
                                <div class="form-group">
                                    <label for="title">New Password</label>
                                    <input type="password" class="form-control" id="password"
                                        aria-describedby="password" name="password" placeholder="New Password ...">
                                </div>
                                <div class="form-group">
                                    <label for="title">Repeat Password</label>
                                    <input type="password" class="form-control" id="repeat_password"
                                        aria-describedby="repeat_password" name="repeat_password"
                                        placeholder="Repeat Password ...">
                                </div>
                                <div class="form-group mt-5">
                                    <button type="submit" class="btn bg-gradient-primary"
                                        style="min-width: 160px">Save</button>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth.footer')
</div>
@endsection