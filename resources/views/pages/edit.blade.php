@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Pages'])
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">Edit Pages</div>
        <div class="card-body table-responsive">
            <form action="/admin/pages/{{ $page->id }}/update" method="post" class="form">
                @csrf
                @method("PUT")
                <input type="hidden" class="feature_image" name="feature_image"
                    value="{{ old('feature_image') ?? $page->feature_image }}">

                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" aria-describedby="title" name="title"
                                placeholder="Article title ..." value="{{ old('title') ?? $page->title }}">
                            <div class="input-group mb-3 group-slug">
                                <strong style="padding-top:6px">Slug</strong>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">{{ url("/p")}}/</span>
                                </div>
                                <input name="slug" type="text" class="form-control" id="basic-url"
                                    aria-describedby="basic-addon3" value="{{ old(" slug") ?? $page->title }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title">Content</label>
                            <textarea name="description" class="form-control summernote" id="description"
                                aria-describedby="description" placeholder="Article content ...">{{ old('description')
                                ?? $page->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="short_description">Short Description</label>
                            <textarea rows="5" class="form-control" id="short_description"
                                aria-describedby="short_description" name="short_description"
                                placeholder="Article Short Desc ...">{{ old('short_description') ??
                                $page->short_description }}</textarea>
                        </div>

                        <div class="form-group row mt-5">
                            <div class="col-md-12">
                                <button type="submit" class="btn bg-gradient-primary"
                                    style="min-width: 160px">Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">

                        <div class="form-group">

                            <label for="">Feature Image</label>
                            @if ($page->feature_image)
                            <img style="width: 100%" class="preview img-responsive mb-2"
                                src="{{ assetUrl($page->feature_image) }}" alt="" srcset="">
                            @endif
                            <div id="dropzone" class="dropzone-wrapper dropzone">
                                <div class="dz-message needsclick">
                                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                            class="path2"></span></i>
                                    <!--begin::Info-->
                                    <div class="ms-4">
                                        <h3 class="text-center fs-5 fw-bold text-gray-900 mb-1">Drop files here or click
                                            to upload.
                                        </h3>
                                    </div>
                                    <!--end::Info-->
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="feature_image_caption">Feature Image Caption</label>
                            <input type="text" class="form-control" id="feature_image_caption"
                                aria-describedby="feature_image_caption" name="feature_image_caption"
                                placeholder="caption ..."
                                value="{{ old('feature_image_caption') ?? $page->feature_image_caption }}" />

                        </div>
                        {{-- <div class="form-group">
                            <label for="keywords">Keywords</label>
                            <textarea rows="3" class="form-control" id="keywords" aria-describedby="keywords"
                                name="keywords" placeholder="Keywords ...">{{ old('keywords') ?? $page->keywords
                                }}</textarea>
                        </div> --}}

                        <div class="form-group">
                            <label for="status">Status</label>
                            <select type="text" class="form-control" id="status" aria-describedby="status"
                                name="status">
                                <option value="DRAFT" {{ $page->status == 'DRAFT' ? 'SELECTED' : null }}>Draft</option>
                                <option value="PUBLISHED" {{ $page->status == 'PUBLISHED' ? 'SELECTED' : null
                                    }}>Published</option>
                                <option value="UNPUBLISH" {{ $page->status == 'UNPUBLISH' ? 'SELECTED' : null
                                    }}>Unpublish</option>
                            </select>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
<script>
    var date = "";
    var baseURL ="{{ url('/') }}"

        $(function() {
            $("#title").keyup(function() {
                var title = $(this).val();
                var slug = title.replaceAll(" ", "-").toLowerCase();
                $("[name=slug]").val(slug)
                $("#slug").text(baseURL + "/p/" +slug)
            })
        })
</script>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    var baseAssetUrl = "{{ assetUrl("/") }}"
    Dropzone.autoDiscover = false;
    // The dropzone method is added to jQuery elements and can
    // be invoked with an (optional) configuration object.
    $(".dropzone").dropzone({
        url: "/admin/file/upload" ,
        method: 'post',
	    headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        accept: function(file, done) {
            done()
        },
        success: function(file, response) {
            $(".feature_image").val(response.file)
            $(".preview").attr("src", baseAssetUrl + response.file)
        },

    });

    // $('.icon-wrapper').click(function(el) {
    //     $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
    //     $(".icon").val($(this).data('icon'))
    // })
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endpush
@endsection
