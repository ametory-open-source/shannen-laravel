@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Orders'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h3>ORDER NUMBER : #{{ $order->order_code }}</h3>
                    <div>
                        <h4>{!! $order->status_badge !!}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Bill To</h5>
                            <p class="address_label address_name">
                                <strong>
                                    {{ $order->billing_address->recipient_name}}
                                </strong>
                            </p>
                            <p class="address_label">
                                <i class="fas fa-house"></i>
                                {{ $order->billing_address->address}}
                            </p>
                            <p class="address_label">
                                <i class="fas fa-phone"></i>
                                {{ $order->billing_address->phone_number}}
                            </p>
                            <p class="address_label">
                                <i class="fas fa-envelope"></i>
                                {{ $order->billing_address->email}}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h5>Ship To</h5>
                            <p class="address_label address_name">
                                <strong>
                                    {{ $order->shipping_address->recipient_name}}
                                </strong>
                            </p>
                            <p class="address_label">
                                <i class="fas fa-house"></i>
                                {{ $order->shipping_address->address}}
                            </p>
                            <p class="address_label">
                                <i class="fas fa-phone"></i>
                                {{ $order->shipping_address->phone_number}}
                            </p>
                            <p class="address_label">
                                <i class="fas fa-envelope"></i>
                                {{ $order->shipping_address->email}}
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Order Items</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>SKU</th>
                                <th>Produk</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Qty</th>
                                <th class="text-right">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $order->items as $index => $item)
                            <tr>
                                <td>
                                    {{$index + 1}}
                                </td>
                                <td>{{ $item->sku }}</td>
                                <td>{{ $item->name }}</td>
                                <td class="text-right">{{ money($item->price) }}</td>
                                <td class="text-right">{{ number($item->quantity) }}</td>
                                <td class="text-right">{{ money($item->price * $item->quantity) }}</td>
                            </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th colspan="4"><small> Sub Total </small></th>
                                <th class="text-right">{{money( $order->total) }}</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th colspan="4"><small> Discount </small></th>
                                <th class="text-right">{{number( $order->discount ?? 0) }} % </th>
                            </tr>
                            @if($order->discount)
                            <tr>
                                <th></th>
                                <th colspan="4"><small> Discount Amount </small></th>
                                <th class="text-right">{{money( $order->total * ($order->discount ?? 0)/ 100) }} </th>
                            </tr>
                            @endif
                            <tr>
                                <th></th>
                                <th colspan="4"><small> Shipping Cost </small></th>
                                <th class="text-right">{{money( $order->shipping_cost->value) }} </th>
                            </tr>
                            <tr>
                                <th></th>
                                <th colspan="4"><small> Grand Total </small></th>
                                <th class="text-right">{{money( $order->grand_total) }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Notes</h4>
                </div>
                <div class="card-body">
                    {!! $order->notes !!}
                </div>
            </div>
            @if($order->remarks)
            <div class="card mb-4">
                <div class="card-header">
                    <h4>Remarks</h4>
                </div>
                <div class="card-body">
                    {!! $order->remarks !!}
                </div>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>Payment Info</h4>
                </div>
                <div class="card-body">

                    @if($order->status == "UNPAID" && !$order->payment_receipt)
                        <small>Waiting for payment</small>
                    @endif

                    @if(!$order->payment_receipt)
                        <div class="form-group">
                            <div id="dropzone" class="dropzone-wrapper dropzone">
                                <div class="dz-message needsclick">
                                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                            class="path2"></span></i>
                                    <!--begin::Info-->
                                    <div class="ms-4">
                                        <h3 class="text-center fs-5 fw-bold text-gray-900 mb-0">Update Payment Receipt
                                        </h3>
                                    </div>
                                    <!--end::Info-->
                                </div>
                            </div>
                        </div>

                    @else
                        <a href="{{ assetUrl($order->payment_receipt) }}" target="_blank">
                            <img src="{{ assetUrl($order->payment_receipt) }}" alt="" srcset="" class="img-fluid">
                        </a>
                        @if($order->status == "PROCESSING" || $order->status == "REQUEST_PICKUP" || $order->status == "PICKING_UP" || $order->status ==
                        "DROPPING_OFF" || $order->status == "SHIPPING" || $order->status ==
                        "SHIPPED" || $order->status ==
                        "FINISHED")
                        @elseif ($order->status != "ORDER_ACCEPTED")
                            @form_open( ["url" =>route("orders.update", ["order" => $order]), "method" => "post", "id"
                            =>"update", "class" => "mt-5 form_update"])
                            @if($order->status != "PENDING" && $order->status != "REJECTED_BY_ADMIN")
                            <button type="submit" name="status" value="PENDING" id=""
                                class="btn bg-gradient-info btn-lg btn-block" style="width: 100%">Update To Pending Status</button>
                            @endif
                            @if($order->status != "PROCESSING" && $order->status != "REJECTED_BY_ADMIN")
                                <button type="submit" name="status" value="PROCESSING" id=""
                                    class="btn bg-gradient-primary btn-lg btn-block" style="width: 100%">Process Order</button>
                            @endif
                            @if($order->status != "REJECTED_BY_ADMIN")
                            <hr>
                            <h4>Tolak Pesanan</h4>
                                <div class="form-group">
                                    <label for="my-input">Remarks</label>
                                    <textarea id="my-input" class="form-control" type="text"  name="remarks" placeholder="rejection remarks"></textarea>
                                </div>
                                <button type="submit" name="status" value="REJECTED_BY_ADMIN" id="reject_by_admin"
                                    class="btn bg-gradient-danger btn-lg btn-block reject_by_admin" style="width: 100%">Reject Order</button>
                            @endif
                            @form_close()
                        @endif


                    @endif
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header">
                    <h4>Shipping</h4>
                </div>
                <div class="card-body">
                    <h3>{{ $order->shipping_cost->name }} - {{ $order->shipping_cost->service }}</h3>
                    @if($order->tracking_number)
                    <p style="margin-bottom: 0">Tracking Number</p>
                    <h3>{{$order->tracking_number}}</h3>
                    @endif
                    {{-- @if($order->status == "PENDING")
                    @form_open( ["url" =>route("orders.process", ["order" => $order]), "method" => "post", "id"
                    =>"process", "class" => "form_update"])
                    <button type="submit" name="" id="" class="btn bg-gradient-primary btn-lg btn-block"
                        style="width: 100%">Process Order</button>
                    @form_close()
                    @endif --}}
                    @if($order->status == "PROCESSING")
                    @form_open( ["url" =>route("orders.request_pickup", ["order" => $order]), "method" => "post", "id"
                    =>"request_pickup"])
                    <button type="submit" name="" id="" class="btn bg-gradient-info btn-lg btn-block"
                        style="width: 100%">Request Pickup</button>
                    @form_close()
                    @form_open( ["url" =>route("orders.update", ["order" => $order]), "method" => "post", "id"
                    =>"update", "class" => "mt-5"])
                    {{-- <div class="form-group">
                        <label for="my-input">Remarks</label>
                        <textarea id="my-input" class="form-control" type="text" name="remarks" placeholder="rejection remarks"></textarea>
                    </div>
                    <button type="submit" name="status" value="REJECTED_BY_ADMIN" id="reject_by_admin"
                    class="btn bg-gradient-danger btn-lg btn-block reject_by_admin" style="width: 100%">Reject Order</button> --}}
                    @form_close()
                    @endif
                    @if($order->status == "REQUEST_PICKUP" || $order->status == "PICKING_UP" || $order->status ==
                    "DROPPING_OFF" || $order->status == "SHIPPING" || $order->status ==
                    "SHIPPED" || $order->status ==
                    "FINISHED")



                    <h4 style="margin-bottom: 0">Order Tracking </h4>
                    <hr>
                    @if(isset($order->tracking_order["history"]))
                    @foreach ($order->tracking_order["history"] as $item)
                    <small>
                        {{ \Carbon\Carbon::parse( $item["updated_at"])->format("d M Y H:i:s") }}
                    </small>
                    <p> <i> {{$item["note"]}}</i></p>
                    @endforeach
                    @else
                    <p> <i> No data </i></p>
                    @endif
                    <hr>
                    @if($order->status == "REQUEST_PICKUP")
                    @form_open( ["url" =>route("orders.shipping", ["order" => $order]), "method" => "post", "id"
                    =>"shipping"])
                    <p>Order Picked Up</p>
                    <button type="submit" name="" id="" class="btn bg-gradient-warning btn-lg btn-block btn_shipping"
                        style="width: 100%">Change Status To Shipping</button>
                    @form_close()
                    @endif

                    @if($order->status != "SHIPPED" && $order->status != "FINISHED")
                    @form_open( ["url" =>route("orders.tracking", ["order" => $order]), "method" => "post", "id"
                    =>"tracking"])
                    <button type="submit" name="" id="" class="btn bg-gradient-info btn-lg btn-block"
                        style="width: 100%">Update Tracking Status</button>
                    @form_close()
                    @endif

                    @if($order->status == "SHIPPING")
                    @form_open( ["url" =>route("orders.shipped", ["order" => $order]), "method" => "post", "id"
                    =>"shipped"])
                    <button type="submit" name="" id="" class="btn bg-gradient-success btn-lg btn-block"
                        style="width: 100%">Change Status To Shipped</button>
                    @form_close()

                    @endif
                    @endif



                    @if($order->status == "SHIPPED")
                    <small>WAITING USER CONFIRMATION ....</small>

                    @endif
                    @if($order->status == "ORDER_ACCEPTED")
                    @form_open( ["url" =>route("orders.finished", ["order" => $order]), "method" => "post", "id"
                    =>"finished"])
                    <button type="submit" name="" id="" class="btn bg-gradient-success btn-lg btn-block"
                        style="width: 100%">Change Status To FINISHED</button>
                    @form_close()

                    @endif
                    @if ($message = Session::get('picked'))
                    <script>
                        var conf = confirm("Orderan telah diambil, ubah status")
                        if (conf) {
                            setTimeout(() => {
                                $("#shipping").submit()
                            }, 1000);
                        }
                    </script>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</main>

@endsection
@push('js')
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    var images = [];
    Dropzone.autoDiscover = false;


    // The dropzone method is added to jQuery elements and can
    // be invoked with an (optional) configuration object.
    $(".dropzone").dropzone({
        url: "/admin/orders/{{$order->id}}/update-receipt" ,
        method: 'post',
	    headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 1,
        maxFilesize: 10, // MB
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        accept: function(file, done) {
            done()
        },
        success: function(file, response) {
            location.reload()

        },

    });

    $(".btn-save").click(function() {
        $(".feature_image").val(JSON.stringify(images))
        $(".form").submit()
    })

    $(function() {

        if ($("[name=promo_tag]").val() == "custom") {
            $(".custom_promo_tag_wrapper").show();
        } else {
            $(".custom_promo_tag_wrapper").hide();
            $("[name=custom_promo_tag]").val("")
            $("[name=custom_promo_tag_color]").val("")
        }
    })

    $("[name=promo_tag]").change(function(d) {
        if (d.target.value == "custom") {
            $(".custom_promo_tag_wrapper").show();
        } else {
            $(".custom_promo_tag_wrapper").hide();
            $("[name=custom_promo_tag]").val("")
            $("[name=custom_promo_tag_color]").val("")
        }
    })

    // $('.icon-wrapper').click(function(el) {
    //     $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
    //     $(".icon").val($(this).data('icon'))
    // })
</script>
<script>
        $(".form_update").submit(function() {
            if(!confirm("anda yakin untuk Proses order ini?")){
                event.preventDefault();
            }

        })
        $("#request_pickup").submit(function() {
            if(!confirm("anda yakin untuk permintaan pengangkutan?")){
                event.preventDefault();
            }
        })
        $("#finished").submit(function() {
            if(!confirm("anda yakin untuk mengubah status menjadi FINISHED?")){
                event.preventDefault();
            }
        })
        $("#shipping").submit(function() {
            if(!confirm("anda yakin untuk  pesanan sudah diambil?")){
                event.preventDefault();
            }
        })
        $("#shipped").submit(function() {
            if(!confirm("Pastikan user menerima order tsb")){
                event.preventDefault();
            }
        })
        $(".reject_by_admin").click(function() {
            if(!confirm("anda yakin untuk akan menolak pesanan berikut?")){
                event.preventDefault();
            }
            if(!$("[name=remarks]").val()){
                alert("Masukan remarks / alasan penolakan pesanan terlebih dahulu")
                event.preventDefault();
            }

        })

        // $("#update").submit(function() {
        //     if($("[name=status]").val() == "REJECTED_BY_ADMIN"){
        //         event.preventDefault();
        //     }
        //     alert($("[name=status]").val())
        //     event.preventDefault();
        //     return
        // })
</script>
@endpush

@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />

@endpush
