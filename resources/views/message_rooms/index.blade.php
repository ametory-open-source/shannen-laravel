@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Messages'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-lg-4 col-md-5 col-12">
            <div class="card blur shadow-blur max-height-vh-70 overflow-auto overflow-x-hidden mb-5 mb-lg-0">

                    <livewire:admin-message-rooms >

            </div>
        </div>
        <div class="col-lg-8 col-md-7 col-12">
            <livewire:admin-message-box>
        </div>
    </div>
</div>
</main>
@push('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{{ $dataTable->scripts(attributes: ['type' => 'module']) }}
<script>

</script>
@endpush
@endsection
