@if ($message = Session::get('success'))
<div style="padding:60px" class="alert-wrapper">
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong style="color: white">{{ $message }}</strong>
    <button style="color: white" type="button" class="btn-close close-alert" data-bs-dismiss="alert" aria-label="Close">X</button>
  </div>
</div>
@endif


@if ($message = Session::get('error'))

<div style="padding:60px" class="alert-wrapper">
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong style="color: white">{{ $message }}</strong>
    <button style="color: white" type="button" class="btn-close close-alert" data-bs-dismiss="alert" aria-label="Close">X</button>
  </div>
</div>
@endif

@if ($message = Session::get('warning'))
<div style="padding:60px" class="alert-wrapper">
  <div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong style="color: white">{{ $message }}</strong>
    <button style="color: white" type="button" class="btn-close close-alert" data-bs-dismiss="alert" aria-label="Close">X</button>
  </div>
</div>
@endif

@if ($message = Session::get('info'))
<div style="padding:60px" class="alert-wrapper">
  <div class="alert alert-info alert-dismissible fade show" role="alert">
    <strong style="color: white">{{ $message }}</strong>
    <button style="color: white" type="button" class="btn-close close-alert" data-bs-dismiss="alert" aria-label="Close">X</button>
  </div>
</div>
@endif

@if ($errors->any())
<div style="padding:60px" class="alert-wrapper">

  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    @foreach ($errors->all() as $item)
    <strong style="color: white">{{$item}}</strong> <br>
    @endforeach

    <button style="color: white" type="button" class="btn-close close-alert" data-bs-dismiss="alert" aria-label="Close">X</button>
  </div>
</div>
@endif
