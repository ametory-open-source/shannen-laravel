@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
    @include('layouts.navbars.auth.topnav', ['title' => 'Products'])
        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h5>
                        Manage Product
                     </h5>
                     <a name="" id="" class="btn bg-gradient-primary btn-sm" href="/admin/products/create" role="button">+ Product</a>
                 </div>
                <div class="card-body table-responsive">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </main>
    @push('scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
    @endpush
    @push('css')
        <style>
            .img-product {
                width: 80px;
                height: 80px;
                object-fit: cover;
                border-radius: 10px;
                margin: 0 5px;
                cursor: pointer;
            }
        </style>
    @endpush
@endsection
