@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])
@php
    $form_title = "Add Product";
    if ($product->id) {
        $form_title = "Edit Product";
    }
@endphp
@section('content')
@include('layouts.navbars.auth.topnav', ['title' => $form_title])
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h4 class="card-title">
                {{ $form_title }}
            </h4>
            <button type="button" class="btn bg-gradient-primary btn-save" style="min-width: 160px">Save</button>
        </div>
        <div class="card-body table-responsive">
            <form action="/admin/products{{!$product->id ? null : '/'.$product->id.'/update'}}" method="post" class="form">
                @csrf
                @if($product->id)
                @method("PUT")
                @endif

                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="name" class="required">Name</label>
                            <input required type="text" class="form-control" id="name" aria-describedby="name" name="name"
                                placeholder="Article name ..." value="{{ old('name') ?? $product->name }}">
                            {{-- <small id="slug" class="form-text text-muted">
                                {{ url("product/".$product->slug) }}
                            </small> --}}
                            <div class="input-group mb-3 group-slug" >
                                <strong style="padding-top:6px">Slug</strong>
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon3">{{ url("/product")}}/</span>
                                </div>
                                <input name="slug" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" value="{{ old("slug") ?? $product->slug }}">
                              </div>
                        </div>

                        <div class="form-group">
                            <label for="title"  class="required">Content</label>
                            <textarea required name="description" class="form-control summernote" id="description"
                                aria-describedby="description"
                                placeholder="Article content ...">{{ old('description') ?? $product->description }}</textarea>
                        </div>




                        <div class="form-group">
                            <label for="materials"  class="required">SKU</label>
                            <input rows="5" required class="form-control" id="sku"
                                aria-describedby="sku" name="sku"
                                placeholder="Article SKU ..." value="{{ old('sku') ?? $product->sku }}" />
                        </div>
                        <div class="form-group">
                            <label for="materials" >Materials</label>
                            <textarea rows="5" class="form-control" id="materials"
                                aria-describedby="materials" name="materials"
                                placeholder="Article Materials ...">{{ old('materials') ?? $product->materials }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="other_info">Other Info</label>
                            <textarea rows="5" class="form-control" id="other_info"
                                aria-describedby="other_info" name="other_info"
                                placeholder="Article Other Info ...">{{ old('other_info') ?? $product->other_info }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="short_description">Short Description</label>
                            <textarea rows="5" class="form-control" id="short_description"
                                aria-describedby="short_description" name="short_description"
                                placeholder="Article Short Desc ...">{{ old('short_description') ?? $product->short_description }}</textarea>
                        </div>
                        {{-- <div class="form-group">
                            <label for="keywords">Keywords</label>
                            <textarea rows="3" class="form-control" id="keywords" aria-describedby="keywords"
                                name="keywords" placeholder="Keywords ...">{{ old('keywords') ?? $product->keywords }}</textarea>
                        </div> --}}
                        <div class="form-group row mt-5">

                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="">Feature Image</label>
                            <div id="dropzone" class="dropzone-wrapper dropzone">
                                <div class="dz-message needsclick">
                                    <i class="ki-duotone ki-file-up fs-3x text-primary"><span class="path1"></span><span
                                            class="path2"></span></i>
                                    <!--begin::Info-->
                                    <div class="ms-4">
                                        <h3 class="text-center fs-5 fw-bold text-gray-900 mb-1">Drop files here or click
                                            to upload.
                                        </h3>
                                    </div>
                                    <!--end::Info-->
                                </div>
                            </div>
                        </div>

                        <div id="uploaded">
                            @foreach ($product->pictures as $item)
                            <div class="form-group img-wrapper" data-index="">
                                <img src="{{ assetUrl($item->picture) }}" alt="" srcset="" class="img-responsive">
                                <input type="hidden" name="picture[]" value="{{ $item->picture }}" />
                                <input type="hidden" name="item_id[]" value="{{ $item->id }}" />
                                <label for="" class="mt-1">Caption</label>
                                <input class="form-control" name="caption[]" value="{{ $item->description }}" />
                                <div class="d-flex justify-content-between">
                                    @if(!$item->is_cover)
                                    <a href="/admin/products/{{$product->id}}/cover?item_id={{$item->id}}"
                                        class="btn bg-gradient-success btn-sm mt-2">Make Cover</a>
                                    @else
                                    <span style="height: 25px" class="badge bg-gradient-primary mt-2">Cover</span>
                                    @endif
                                    <a data-href="/admin/products/{{$product->id}}/delete-item?item_id={{$item->id}}"
                                        onclick="btnDelete(this)"
                                        class="btn btn-delete bg-gradient-danger btn-sm mt-2">Delete</a>
                                </div>

                            </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label for="price"  class="required">Price</label>
                            <input required  type="number" name="price" class="form-control"
                                 value="{{ old('price') ?? $product->price }}" />
                        </div>
                        <div class="form-group">
                            <label for="strike_price"><s>Strike Price</s></label>
                            <input type="number" name="strike_price" class="form-control"
                                 value="{{ old('strike_price') ?? $product->strike_price }}" />
                        </div>



                        <div class="form-group">
                            <label for="title"  class="required">Weight</label>
                            <div class="row">
                                <div class="col-9">
                                    <input required type="number" class="form-control" id="weight" aria-describedby="weight" name="weight" placeholder="weight ..." value="{{ old('weight') ?? $product->weight }}">
                                </div>
                                <div class="col-3">
                                    @form_select("weight_unit", ["g" => "Gram", "kg" => "Kg"], "g", ["class" => "form-control"])
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="dimension"  class="required">Dimensions</label>
                            <div class="row">
                                <div class="col-3">
                                    <input required type="number" class="form-control" id="dimension_length" aria-describedby="dimension_length" name="dimension_length" placeholder="L" value="{{ old('dimension_length') ?? $product->dimension_length }}">
                                </div>
                                <div class="col-3">
                                    <input required type="number" class="form-control" id="dimension_width" aria-describedby="dimension_width" name="dimension_width" placeholder="W" value="{{ old('dimension_width') ?? $product->dimension_width }}">
                                </div>
                                <div class="col-3">
                                    <input required type="number" class="form-control" id="dimension_height" aria-describedby="dimension_height" name="dimension_height" placeholder="H" value="{{ old('dimension_height') ?? $product->dimension_height }}">
                                </div>
                                <div class="col-3">
                                    @form_select("dimension_unit", ["cm" => "cm", "m" => "m"], "cm", ["class" => "form-control"])
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="product_category_id"  class="required">Category</label>
                            <select required type="text" class="form-control" id="product_category_id"
                                aria-describedby="product_category_id" name="product_category_id">
                                @foreach ($categories as $item)
                                <option value="{{ $item->id }}" {{ (old('product_category_id') ?? $product->product_category_id) == $item->id ? 'SELECTED' : null }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="promo_tag">Promo Tag</label>

                                @form_select("promo_tag", ["custom" => "Custom", "new" => "New", "sale" => "Sale", "discount" => "Discount", ], old("promo_tag") ??  $product->promo_tag, ["class" => "form-control"])

                        </div>

                        <div class="custom_promo_tag_wrapper">
                            <div class="form-group ">
                                <label for="custom_promo_tag">Custom Promo Tag</label>
                                    <input type="text" name="custom_promo_tag" id="" class="form-control" placeholder="Custom Promo Tag"
                                        value="{{ old("custom_promo_tag") ??  $product->custom_promo_tag }}">

                            </div>
                            <div class="form-group">
                                <label for="custom_promo_tag_color">Custom Promo Tag Color</label>
                                    <input type="color" name="custom_promo_tag_color" id="" class="form-control" placeholder="Color"
                                        value="{{  old("custom_promo_tag_color") ??  $product->custom_promo_tag_color }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <input data-role="tagsinput" name="tags"
                                 value="{{ old('tags') ?? $product->tags }}" />
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select type="text" class="form-control" id="status" aria-describedby="status"
                                name="status">
                                <option value="DRAFT" {{ $product->status == 'DRAFT' ? 'SELECTED' : null }}>Draft</option>
                                <option value="PUBLISHED" {{ $product->status == 'PUBLISHED' ? 'SELECTED' : null }}>Published</option>
                                <option value="UNPUBLISH" {{ $product->status == 'UNPUBLISH' ? 'SELECTED' : null }}>Unpublish</option>
                            </select>

                        </div>
                        <div class="form-group mt-5">
                        </div>
                        <div class="form-check form-switch mt-3">
                            <input class="form-check-input" name="review_enabled" type="checkbox" role="switch"
                                id="review_enabled" {{ $product->review_enabled ? 'CHECKED' : null}}>
                            <label class="form-check-label" for="review_enabled">Show Review</label>
                        </div>
                        <div class="form-check form-switch mt-3">
                            <input class="form-check-input" name="review_moderation" type="checkbox" role="switch"
                                id="review_moderation" {{ $product->review_moderation ? 'CHECKED' : null}}>
                            <label class="form-check-label" for="review_moderation">Moderation Review</label>
                        </div>
                        <div class="form-check form-switch mt-3">
                            <input class="form-check-input" name="is_top_sale" type="checkbox" role="switch"
                                id="is_top_sale" {{ $product->is_top_sale ? 'CHECKED' : null}}>
                            <label class="form-check-label" for="is_top_sale">Top Sale</label>
                        </div>
                        <div class="form-check form-switch mt-3">
                            <input class="form-check-input" name="is_feature_product" type="checkbox" role="switch"
                                id="is_feature_product" {{ $product->is_feature_product ? 'CHECKED' : null}}>
                            <label class="form-check-label" for="is_feature_product">Feature Product</label>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<template id="template">
    <div class="form-group img-wrapper" data-index="">
        <img src="" alt="" srcset="" class="img-responsive">
        <input type="hidden" name="picture[]" value="" />
        <label for="" class="mt-1">Caption</label>
        <input class="form-control" name="caption[]" value="" />
    </div>
</template>
@push('scripts')
<script>
    var date = new Date() / 1;
    var baseURL ="{{ url('/') }}"

        $(function() {
            $("#name").keyup(function() {
                var name = $(this).val();
                var slug = name.replaceAll(" ", "-").toLowerCase();
                if (name.length == 0) slug = ""
                $("[name=slug]").val(slug)
                $("#slug").text(baseURL + "/product/" +slug)
            })
        })
</script>
<script src="/assets/js/bootstrap-tagsinput.min.js"></script>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<script>
    var images = [];
    Dropzone.autoDiscover = false;
    // The dropzone method is added to jQuery elements and can
    // be invoked with an (optional) configuration object.
    $(".dropzone").dropzone({
        url: "/admin/file/upload" ,
        method: 'post',
	    headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        maxFiles: 6,
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        accept: function(file, done) {
            done()
        },
        success: function(file, response) {
            var $template = $($("#template").html())
            $template.find("[name='picture[]']").val(response.file)
            $template.find("img").attr("src", response.url)

            $("#uploaded").prepend($template)
        },

    });

    $(".btn-save").click(function() {
        $(".feature_image").val(JSON.stringify(images))
        $(".form").submit()
    })

    $(function() {

        if ($("[name=promo_tag]").val() == "custom") {
            $(".custom_promo_tag_wrapper").show();
        } else {
            $(".custom_promo_tag_wrapper").hide();
            $("[name=custom_promo_tag]").val("")
            $("[name=custom_promo_tag_color]").val("")
        }
    })

    $("[name=promo_tag]").change(function(d) {
        if (d.target.value == "custom") {
            $(".custom_promo_tag_wrapper").show();
        } else {
            $(".custom_promo_tag_wrapper").hide();
            $("[name=custom_promo_tag]").val("")
            $("[name=custom_promo_tag_color]").val("")
        }
    })

    // $('.icon-wrapper').click(function(el) {
    //     $('.icon-selected').attr("class", "fad fa-3x icon-selected "+$(this).data('icon'))
    //     $(".icon").val($(this).data('icon'))
    // })
</script>
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<style>
    .img-responsive {
        width: 100%;
        border-radius: 10px;
        margin-bottom: 10px;
    }

    .img-wrapper {
        padding: 10px;
        background-color: #fefefe;
        border: 1px solid #eee;
    }
</style>
@endpush
@endsection
