@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Users'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <form enctype="multipart/form-data" action="/admin/users/{{$user->id}}/update-avatar" method="post"
        id="update-avatar">
        @csrf
        <input type="file" accept="image/*" name="file" id="" style="visibility: hidden">
    </form>

    <div class="card">
        <div class="card-header">Edit User</div>
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <form action="/admin/users/{{$user->id}}/update" method="post">
                        @method("PUT")
                        @csrf
                        <div class="form-group">
                            @if($user->avatar)
                            <img src="{{ assetUrl($user->avatar) }}"
                                class="rounded-circle img-fluid border border-2 border-white pointer">
                            @form_hidden("avatar", $user->avatar)
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input readonly type="text" class="form-control" id="name" aria-describedby="name" name="name"
                                placeholder="name ..." value="{{ old('name') ?? $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input readonly type="email" readonly class="form-control" id="email" aria-describedby="email"
                                name="email" placeholder="email ..." value="{{ old('email') ?? $user->email }}">
                        </div>
                        <div  class="form-group">
                            <label for="phone_number">Telp</label>
                            <input readonly type="text" class="form-control" id="phone_number" aria-describedby="phone_number"
                                name="phone_number" placeholder="phone_number ..."
                                value="{{ old('phone_number') ?? $user->phone_number }}">
                        </div>
                        {{-- <button type="submit" class="btn bg-gradient-success btn-save"
                            style="min-width: 160px">Save</button> --}}
                        @if($user->suspended_at)
                        <div class="alert alert-primary text-white d-flex justify-content-between" role="alert">
                            User telah di suspend
                            <a href="/admin/users/{{$user->id}}/unsuspend" class="text-right" style="font-weight: bold; font-size:9pt;color:white;padding-top:2px">Aktifkan kembali</a>
                        </div>
                        @else
                        <a href="/admin/users/{{$user->id}}/suspend" class="btn bg-gradient-danger btn-save me-3"
                            style="min-width: 160px">Suspend User</a>
                        @endif

                    </form>
                </div>
                <div class="col-4 d-flex justify-content-center">

                </div>
            </div>
        </div>
    </div>

</div>
</main>
@push('scripts')
<script>
    $('.rounded-circle').click(function(){
            $('[name=file]').trigger("click")
        })
    $('[name=file]').change(function(){
        $("#update-avatar").submit()
    })
</script>
@endpush
@endsection
