@extends("layouts.landing")

@section('content')
@includeWhen($web->show_header, "components.landing.header")
<section id="content">
    <div class="container">
        {!! $static->content !!}
    </div>
</section>

@includeWhen($web->show_footer, "components.landing.footer")
@endsection
@push("css")
<style>
    {!! $static->styles !!}
</style>
@endpush
@push("scripts")
<script>
    {!! $static->scripts !!}
</script>
@endpush