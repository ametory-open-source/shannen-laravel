@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])


@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Statics'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h5>
                Edit Static
            </h5>
            <button type="button" class="btn bg-gradient-primary save" style="min-width: 160px">Save</button>
        </div>
        <div class="card-body">
            <form action="/admin/statics/{{$static->id}}/update" method="post" class="form">
                @csrf
                @method("PUT")
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input type="text" class="form-control" id="name" aria-describedby="name" name="name"
                                placeholder="Static name ..." value="{{  $static->name }}">
                            <small id="slug" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <label for="keywords">Description</label>
                            <textarea rows="3" class="form-control" id="description" aria-describedby="description"
                                name="description" placeholder="Description ...">{{   $static->description  }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea rows="9" name="content" class="form-control" id="content"
                                aria-describedby="content"
                                placeholder="Article content ...">{{ $static->content }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="styles">Styles</label>
                            <textarea rows="9" name="styles" class="form-control" id="styles"
                                aria-describedby="styles"
                                placeholder="Article styles ...">{{  $static->styles }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="scripts">Scripts</label>
                            <textarea rows="9" name="scripts" class="form-control" id="scripts"
                                aria-describedby="scripts"
                                placeholder="Article scripts ...">{{  $static->scripts }}</textarea>
                        </div>


                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>
</main>
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.js" integrity="sha512-sSWQXoxIkE0G4/xqLngx5C53oOZCgFRxWE79CvMX2X0IKx14W3j9Dpz/2MpRh58xb2W/h+Y4WAHJQA0qMMuxJg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/keymap/sublime.min.js" integrity="sha512-SV3qeFFtzcmGtUQPLM7HLy/7GKJ/x3c2PdiF5GZQnbHzIlI2q7r77y0IgLLbBDeHiNfCSBYDQt898Xp0tcZOeA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/htmlmixed/htmlmixed.min.js" integrity="sha512-HN6cn6mIWeFJFwRN9yetDAMSh+AK9myHF1X9GlSlKmThaat65342Yw8wL7ITuaJnPioG0SYG09gy0qd5+s777w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/xml/xml.min.js" integrity="sha512-LarNmzVokUmcA7aUDtqZ6oTS+YXmUKzpGdm8DxC46A6AHu+PQiYCUlwEGWidjVYMo/QXZMFMIadZtrkfApYp/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/css/css.min.js" integrity="sha512-rQImvJlBa8MV1Tl1SXR5zD2bWfmgCEIzTieFegGg89AAt7j/NBEe50M5CqYQJnRwtkjKMmuYgHBqtD1Ubbk5ww==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/mode/javascript/javascript.min.js" integrity="sha512-Cbz+kvn+l5pi5HfXsEB/FYgZVKjGIhOgYNBwj4W2IHP2y8r3AdyDCQRnEUqIQ+6aJjygKPTyaNT2eIihaykJlw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js" integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/css-hint.min.js" integrity="sha512-1BD3lo262s+DtDFoeD8+ssEL1zVJU8SHWWMtUyxfLqMV/jQDEnyQlS/CL3mD1kkLSv2hhYq1sdcA/zAPDz4JVA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/addon/hint/javascript-hint.min.js" integrity="sha512-omIxBxPdObb7b3giwJtPBiB86Mey/ds7qyKFcRiaLQgDxoSR+UgCYEFO7jRZzPOCZAICabGCraEhOSa71U1zFA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>



<script>
    window.onload = function() {

        CodeMirror.fromTextArea(document.getElementById(`content`),{
            theme:  "monokai",
            mode:  "htmlmixed",
            htmlMode: true,
            keyMap: "sublime",
            indentUnit: 4,
            tabSize: 4,
            indentWithTabs: false,
            lineNumbers: true,
            autoCloseTags: true,
            lineWrapping: true,
            autofocus: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
        });
        CodeMirror.fromTextArea(document.getElementById(`styles`),{
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            mode: "css",
            keyMap: "sublime",
            theme: 'monokai',
            indentUnit: 4,
            tabSize: 4,
            autoCloseTags: true,
            lineWrapping: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
        });
        CodeMirror.fromTextArea(document.getElementById(`scripts`),{
            theme:  "monokai",
            mode:  "javascript",
            keyMap: "sublime",
            htmlMode: true,
            indentUnit: 4,
            tabSize: 4,
            indentWithTabs: false,
            lineNumbers: true,
            autoCloseTags: true,
            lineWrapping: true,
            autofocus: true,
        });
    }

    $("button.save").click(function() {
        $(".form").submit()
    })
  
</script>
    
@endpush
@push("css")
<link rel="stylesheet" href="/assets/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/codemirror.min.css" integrity="sha512-uf06llspW44/LZpHzHT6qBOIVODjWtv4MxCricRxkzvopAlSWnTf6hpZTFxuuZcuNE9CBQhqE0Seu1CoRk84nQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.65.13/theme/monokai.min.css" integrity="sha512-R6PH4vSzF2Yxjdvb2p2FA06yWul+U0PDDav4b/od/oXf9Iw37zl10plvwOXelrjV2Ai7Eo3vyHeyFUjhXdBCVQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush
@endsection