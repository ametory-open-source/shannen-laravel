@extends("frontend.layout")

@section('content')
    <div class="container d-flex mb-5">
        <img src="{{ assetUrl($web->not_found) }}" alt="" srcset="" class="img-responsive img">
    </div>
    <div class="" style="margin-top: 100px">
    </div>

@endsection

@push("css")
    <style>
        .img {
            width: 60%;
            margin: auto;
            margin-top: 200px;
        }
    </style>
@endpush
