@extends('layouts.app', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav', ['title' => 'Comments'])
<!-- End Navbar -->
<div class="container-fluid py-4">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Comment Moderation</h4>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="">{{ $comment->name }} <span class="text-primary">{{ $comment->created_at->format("d-m-Y
                        H:i") }}</span></label>
                <textarea readonly class="form-control">{{ $comment->comment }}</textarea>
            </div>
            <div class="form-group">
                <label for="">Article</label>
                <h5>{{ $comment->blog->title }}</h5>
            </div>

            <form action="/admin/comments/{{ $comment->id }}/approval" method="post">
                @csrf
                <button value="PUBLISHED" name="status" type="submit" class="btn bg-gradient-primary" style="min-width: 160px">Approve</button>
                <button value="UNPUBLISH" name="status" type="submit" class="btn bg-gradient-danger" style="min-width: 160px">Reject</button>
            </form>


        </div>
    </div>
</div>
</main>
@push('scripts')

@endpush
@endsection